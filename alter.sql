alter table cafe_schet add num int default 0;

-- Function: "trRashod"()

-- DROP FUNCTION "trRashod"();

CREATE OR REPLACE FUNCTION "trRashod"()
  RETURNS trigger AS
$BODY$declare 
uid integer;
user_id integer;
tid integer;
volume integer;
cnt integer;
uname varchar(255);
xcnt integer;
begin

uid = NEW.usluga_id;
user_id = NEW.user_id;
cnt =NEW.cnt;
select name into uname from cafe_usluga where id=NEW.usluga_id;
for tid,volume in execute 'select tovar_id,volume from cafe_sostav where usluga_id='||NEW.usluga_id
loop
	insert into cafe_rashod (usluga_id,user_id,tovar_id,volume,prim,dt) values (uid,user_id,tid,volume*cnt,uname||' - '||cnt,current_timestamp);

	select count(*) into xcnt from cafe_sklad where tovar_id=tid;
	if xcnt=0 then 
		insert into cafe_sklad (tovar_id,volume,dt) values (tid,cnt,current_timestamp);
	else
		update cafe_sklad set volume=cafe_sklad.volume-cnt,dt=current_timestamp where tovar_id=tid;	
	end if;
	
end loop;
return NEW;
end;$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION "trRashod"()
  OWNER TO postgres;
COMMENT ON FUNCTION "trRashod"() IS 'Запись продуктов в расход';



CREATE TRIGGER "triggerRashod"
  AFTER INSERT
  ON cafe_zakazhistory
  FOR EACH ROW
  EXECUTE PROCEDURE "trRashod"();
  
alter table cafe_tovar add is_pf boolean default false;
