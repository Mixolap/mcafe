rm -rf /opt/distrib
mkdir /opt/distrib
cp -rf /opt/mcafe /opt/distrib
cd /opt/distrib/mcafe
rm -rf .svn
rm -rf install_linux.doc
rm -rf mcafe1.db
rm -rf TODO.txt
rm -rf passes.txt
rm -rf mcafe.json
rm -rf caffein.nsi
#rm -rf .htpasswd
rm -rf printed/*
rm -rf print/*
rm -rf build_distrib.sh
rm -rf mprinter 
find /opt/distrib -name "*.pyc" -delete
find /opt/distrib -name "*.pyo" -delete
find /opt/distrib -name "*.o" -delete
find /opt/distrib -name Makefile -delete
find /opt/distrib -name "moc_*" -delete
find /opt/distrib -name "ui_*" -delete
find /opt/distrib -name "*.user" -delete
cd /opt/distrib
tar -czf /opt/caffein-1.3.tar.gz mcafe
