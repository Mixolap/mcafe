from django.contrib import admin

from .models import *

@admin.register(Usluga)
class UslugaAdmin(admin.ModelAdmin):
    search_fields = ['name']
    list_display = ['name', 'check_name', 'menu', 'price', 'active']

@admin.register(Option)
class OptionAdmin(admin.ModelAdmin):
    search_fields = ['name', 'code', 'value']
    list_display = ['name', 'code', 'value']

@admin.register(Stol)
class StolAdmin(admin.ModelAdmin):
    search_fields = ['name',]
    list_display = ['name', 'visible', 'dt', 'status']


@admin.register(Tovar)
class TovarAdmin(admin.ModelAdmin):
    search_fields = ['name',]
    list_display = ['name', 'menu', 'volume', 'mvolume', 'portion', 'is_pf']
    list_filter = ['menu', ]


# @admin.register(Prihod)
# class PrihodAdmin(admin.ModelAdmin):
#     search_fields = ['tovar',]
#     list_display = ['tovar', 'volume', 'cnt1', 'volume1', 'price', 'prim', 'dt']

@admin.register(Menu)
class MenuAdmin(admin.ModelAdmin):
    search_fields = ['name',]
    list_display = ['name', 'pid', 'np', 'active']
    list_filter = ['np', ]

@admin.register(Discount)
class DiscountAdmin(admin.ModelAdmin):
    search_fields = ['card_number', 'fio', 'phone', 'prim']
    list_display_links = ['card_number', 'fio', 'phone']
    list_display = ['card_number', 'fio', 'phone', 'discount_value', 'balance', 'is_active']
    list_filter = ['dt_create', ] #'discount']
    readonly_fields = ['balance', 'discount', 'dt_last']
