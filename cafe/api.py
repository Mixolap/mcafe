#-*- encoding: utf-8 -*-
import json
from django.http import HttpResponse
from .models import *

def schetZakaz(s):
    data = []
    for d in Zakaz.objects.filter(schet=s):
        data.append({
            "id":d.usluga.id,
            "name":d.usluga.name,
            "count":d.cnt,        
        })
    return data
    

def schets(request,stol_id):
    return HttpResponse(json.dumps([{
            'id':d.id,
            'sname':d.num,
            'zakaz':schetZakaz(d),
            'prim':d.prim,
            'deposit':d.deposit(),
            'deposit_ost':d.depositOst(),
            'may_delete': Zakaz.objects.filter(schet=d).count()==0,
            'new_deposit_card_value':'0',
            'calc_price':d.calcPrice(),
            'calc_price_skidka':d.calcPriceSkidka(),
        } for d in Schet.objects.filter(stol_id=stol_id).order_by('id')]))
    
def schetDelete(request,sid):
    schet = Schet.objects.get(pk=sid)
    if Zakaz.objects.filter(schet=schet).count()>0:
        return HttpResponse('no')
    schet.delete()
    return HttpResponse('ok')
    
    
    
    
    
    