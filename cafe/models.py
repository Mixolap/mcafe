#-*- encoding: utf-8 -*-
from django.db import models
from django.contrib import admin
from django.contrib.auth.models import User
from django.contrib.auth.models import Group

from django.db.models import Sum
from django.db import connection
import datetime,os
VOLUMES = ((0, u"г"), (1, u"кг"), (2, u"шт"), (3, u"л"))
"""
Опции программы:
DEMO 1 - демонстрационная версия
RAZDACHA 1  - раздача
NEW_ZAKAZ 0 1 - новая форма заказа (для тача)
CONFIRM_ZAKAZ 0 1 подтверждение заказа 
HAVE_PRINTER 1 0 наличие принтера (если 0 то выдает чеки на страницу)
HAVE_DEPOSIT 0 1 наличие депозита
HAVE_PRECHEK 0 1 наличие пречека
HAVE_BRON 0 1 наличие бронирования
HAVE_CARD 0 1 возможность оплаты картой
OFFICIANT_NAME value наименование обслуживающего персонала (Официант, Кассир)
PRINTER value модель принтера (название используется в имени шаблона для принтера)
INN value ИНН
OGRN value ОГРН
PHONE value Телефон
addr value Адрес
name value Наименование заведения
sstart int_value Начало рабочей смены (с какого часа начинается рабочая смена)
admin_password Пароль администратора для клиентского приложения
AUTO_ADD_CONTENT 1 - если стоит 1, то при добавление услуги она же добавляется в состав (контент)
ZAL_TEMPLATE zal.html - шаблон зала  
"""

def isBuh(request):
    if request.user.is_anonymous: return False
    return Group.objects.filter(pk=1,user=request.user).count()>0
    
def isAdmin(request):
    if request.user.is_anonymous: return False
    return Group.objects.filter(pk=2,user=request.user).count()>0
    
def isOfficiant(request):
    if request.user.is_anonymous: return True
    return Group.objects.filter(pk=3,user=request.user).count()>0    


def get_option(name,default=None):
    op = Option.objects.filter(code__iexact=name)
    if op.count()==0: return default    
    return op[0].value
    
def set_option(name,value):
    op = Option.objects.filter(code__iexact=name)
    if op.count()==0:
        op = Option(code=name)
    else:
        op = op[0]    
    op.value = value
    op.save()
    
    
# дата начала смены
def dtStart(dt=None):
    s_hour = int(Option.objects.get(pk=1).value)
    #print "s_hour=",s_hour 
    if dt==None:  
        dt = datetime.datetime.now()
        dt = datetime.datetime(dt.year,dt.month,dt.day,s_hour)
    #print dt.hour,s_hour
    if datetime.datetime.now().hour<s_hour: 
        dt -= datetime.timedelta(days=1)
        dt = datetime.datetime(dt.year,dt.month,dt.day,s_hour)
    #print "dt=",dt
    return dt

# дата окончания смены
def dtEnd(dt):
    s_hour = int(Option.objects.get(pk=1).value)    
    
    if dt.hour>=s_hour: 
        dt += datetime.timedelta(days=1)
        dt = datetime.datetime(dt.year,dt.month,dt.day,s_hour)
    return dt
# подсчёт суммы денег за период картой или наличными

def cash_summ(dt1, dt2, type):
    # print('type=', type, 'dt1=', dt1, 'dt2=', dt2)
    cur = connection.cursor()
    sql = "select sum(summ) from cafe_cash where card=%s and dt>='%s' and dt<='%s'"%(type, dt1, dt2)
    print('cash_summ=', sql)
    cur.execute(sql)
    summ = 0
    for c in cur:
        if c[0]==None: continue
        summ = c[0]
    return summ
# Стол
class Stol(models.Model):
    name = models.CharField(max_length=255,verbose_name=u"Наименование")
    status = models.IntegerField(default=0,choices=((0,u"Свободен"),(1,u"Есть заказ")),verbose_name=u"Статус")
    dt     = models.DateTimeField(auto_now_add=True)
    visible = models.IntegerField(default=1,choices=((0,u"Невидим"),(1,u"Видим")))

    class Meta:
        verbose_name_plural=u"Столы"
        ordering = ("id",)

    def __unicode__(self):
        return self.name

    def hasZakaz(self):
        return Schet.objects.filter(stol=self,status=0).count()>0

    def hasBron(self):
        return self.bron().count()>0

    def bron(self):
        return Bron.objects.filter(stol=self,dt__gte=datetime.datetime.now()-datetime.timedelta(hours=1),dt__lte=datetime.datetime.now()+datetime.timedelta(hours=20)).order_by("dt")
        
    def recalc(self):
        if self.hasZakaz():
            self.status = 1
        else:
            self.status = 0
            
        self.save()

    def checks(self):
        return Schet.objects.filter(stol=self,status=1,dt_close__gte=dtStart())

    def schets(self):
        return Schet.objects.filter(stol=self,status=0,precheck=0,dt_close__gte=dtStart())

    def hasPrecheck(self):
        return Schet.objects.filter(stol=self,precheck=1,status=0).count()
        
# Бронь
class Bron(models.Model):
    stol   = models.ForeignKey(Stol, on_delete=models.CASCADE, verbose_name=u"Стол")
    prim   = models.CharField(max_length=255,null=True,blank=True)
    dt     = models.DateTimeField(verbose_name=u"Дата")
    class Meta:
        verbose_name_plural=u"Бронь"
        ordering=("-dt",)
    def __unicode__(self):
        return self.stol.name+" - "+str(self.dt)
    def today(self):
        cd = datetime.datetime.now()
        dt = self.dt
        return dt.day==cd.day and dt.month==cd.month and dt.year==cd.year
    def is_now(self):
        return self.dt<datetime.datetime.now()
# Разделы меню
class Menu(models.Model):
    name = models.CharField(max_length=255,verbose_name=u"Наименование раздела")
    pid  = models.IntegerField(default=0,verbose_name=u"Родительский раздел")
    np   = models.IntegerField(default=100,verbose_name=u"Номер по списку")
    dt   = models.DateTimeField(auto_now_add=True)
    active = models.IntegerField(default=1)
    class Meta:
        db_table = "cafe_menu"
        verbose_name_plural=u"Меню"
        ordering = ("np","id")
    
    def __unicode__(self):
        return str(self.id)+". "+self.name+" - "+str(self.pid)
    
    def __str__(self):
        return str(self.id)+". "+self.name+" - "+str(self.pid)

    def children(self):
        return Menu.objects.filter(pid=self.id,active=1)
    def usluga(self):
        return Usluga.objects.filter(menu=self,active=1)
    def parent(self):
        if self.pid==0: return None
        return Menu.objects.get(pk=self.pid)
    def submenu_count(self):
        cnt1 = self.children().count()*3
        for d in self.children():
            cnt1 += d.usluga().count()
        print("cnt1=", cnt1)
        return cnt1
# Услуга
class Usluga(models.Model):    
    name   = models.CharField(max_length=255,verbose_name=u"Наименование услуги")
    check_name = models.CharField(max_length=255,null=True,blank=True,verbose_name=u"Наименование услуги в чеке")
    description = models.TextField(null=True,blank=True,verbose_name=u"Описание")
    menu   = models.ForeignKey(Menu, verbose_name=u"Пункт меню", on_delete=models.CASCADE)    
    dt     = models.DateTimeField(auto_now_add=True)
    price  = models.FloatField(verbose_name=u"Цена")
    np     = models.IntegerField(default=100)
    active = models.IntegerField(default=1)
    class Meta:
        db_table = "cafe_usluga"
        verbose_name_plural=u"Услуга"
        ordering=("np","id")
    
    def __unicode__(self):
        return self.name
    
    def menuTop(self):
        return Menu.objects.get(pk=self.menu.pid)
    
    def sostav(self):
        return Sostav.objects.filter(usluga=self)
    
    def hasSostav(self):
        return self.sostav().count()
    
    def get_check_name(self):
        if self.check_name==None or self.check_name=="": return self.name[:24]
        return self.check_name

class Discount(models.Model):
    card_number = models.CharField(max_length=255, unique=True, verbose_name='Номер карты')
    fio = models.CharField(max_length=1024, null=True, blank=True, verbose_name='ФИО')
    phone = models.CharField(max_length=1024, null=True, blank=True, verbose_name='Телефон')
    discount = models.IntegerField(default=0, choices=((0, 'Кэшбек'), (1, 'Скидка')), verbose_name='Тип карты')
    discount_value = models.IntegerField(default=10, verbose_name='Размер скидки, %')
    balance = models.FloatField(default=0, verbose_name='Баланс')
    prim = models.TextField(null=True, blank=True, verbose_name='Примечание')
    is_active = models.BooleanField(default=True, verbose_name='Принимать')
    dt_create = models.DateTimeField(auto_now=True, verbose_name=u"Дата заведения карты")
    dt_last = models.DateTimeField(null=True, blank=True, verbose_name=u"Последнее использование")
    class Meta:
        verbose_name_plural = "Скидочные (накопительные) карты"
        verbose_name = "скидочную (накопительную) карту"

    def schet_count(self):
        return DiscountHistory.objects.filter(discount=self).count()


# Счет
class Schet(models.Model):
    stol         = models.ForeignKey(Stol,verbose_name=u"Стол", on_delete=models.CASCADE)
    status       = models.IntegerField(default=0,choices=((0,u"Открыт"),(1,u"Закрыт")),verbose_name=u"Статус")
    price        = models.FloatField(default=0,verbose_name=u"Цена")
    name         = models.CharField(max_length=255)
    dt_open      = models.DateTimeField(auto_now_add=True,verbose_name=u"Дата открытия")
    dt_close     = models.DateTimeField(null=True,blank=True,verbose_name=u"Дата закрытия")
    user_open    = models.ForeignKey(User,related_name="user_open", on_delete=models.CASCADE,verbose_name=u"Пользователь открывший")
    user_close   = models.ForeignKey(User,related_name="user_close",null=True,blank=True, on_delete=models.CASCADE,verbose_name=u"Пользователь закрывший")
    skidka       = models.IntegerField(default=0,verbose_name=u"Скидка в процентах")
    price_skidka = models.FloatField(default=0,verbose_name=u"Цена со скидкой")
    card         = models.IntegerField(default=0,choices=((0,u"наличные"),(1,u"карта"),(2,u"примечание")), verbose_name=u"Закрыто картой")
    precheck     = models.IntegerField(default=0,verbose_name=u"Пробит пречек")
    prim         = models.CharField(max_length=255,null=True,blank=True,verbose_name=u"Примечание")
    skidka_fix   = models.FloatField(default=0,verbose_name=u"Скидка с фиксированной суммой")
    num          = models.IntegerField(default=0)
    discount_card = models.ForeignKey(Discount, null=True, blank=True, on_delete=models.SET_NULL, verbose_name=u"Скидочная карта")

    class Meta:
        ordering = ("-dt_close",)
        
    def save(self, *args, **kwargs):
        if int(self.status)==0:
            self.stol.status=1
            self.stol.save()
        self.price = self.calcPrice()        
        self.price_skidka = self.calcPriceSkidka()
        for d in Zakaz.objects.filter(schet=self):
            d.price_skidka=d.price-d.price*(float(self.skidka)/100.) - self.skidka_fix
            d.save()
        # если осталась сумма от депозита, то в окончательную цену записывать всю сумму депозита
        if self.status==1 and self.depositOst()>0:
            self.price_skidka = self.deposit()
            
        super(Schet, self).save(*args, **kwargs)

    def zakazes(self):
        zakazes = []
        for menu in Menu.objects.filter(pid=0, active=1).order_by('np'):
            zakazes.append(Zakaz.objects.filter(schet=self, menu_id=menu.pk, cnt__gt=0))
        return zakazes

    # кухня
    def zakaz1(self):
        return Zakaz.objects.filter(schet=self, menu_id=1, cnt__gt=0)
    # бар
    def zakaz2(self):
        return Zakaz.objects.filter(schet=self, menu_id=2, cnt__gt=0)
    # кальян
    def zakaz3(self):
        return Zakaz.objects.filter(schet=self, menu_id=3, cnt__gt=0)        
    # бойпосуды
    def zakaz4(self):
        return Zakaz.objects.filter(schet=self, menu_id=36, cnt__gt=0)        
        
    def hasZakaz(self):
        return Zakaz.objects.filter(schet=self).count()>0
    def calcPrice(self):
        x = Zakaz.objects.filter(schet=self).aggregate(Sum("price"))["price__sum"]
        if x==None: x=0
        return x
    
    def calcPriceWithSkidka(self):
        pr = self.calcPrice()
        summ = 0
        if self.skidka==0: summ = pr - self.skidka_fix
        else:
            summ = pr - pr*(float(self.skidka)/100.) - self.skidka_fix
        return summ
    # к оплате
    def calcPriceSkidka(self):
        summ = self.calcPriceWithSkidka() - self.deposit()
        if summ<0: summ = 0
        return summ
        
    def deposit(self):
        if Deposit.objects.filter(schet=self).count()==0: return 0
        return Deposit.objects.filter(schet=self).aggregate(Sum("summ"))["summ__sum"]
        
    def depositOst(self):
        summ = self.deposit() - self.calcPriceWithSkidka()
        if summ<0: summ = 0
        return summ
        
    def history(self):
        return SchetHistory.objects.filter(schet=self)
    
    def mayClose(self):
        return self.summOstatok() <= 0
    
    def summOstatok(self):
        if Cash.objects.filter(schet=self).count()==0: return self.calcPriceSkidka()
        return self.calcPriceSkidka() - Cash.objects.filter(schet=self).aggregate(Sum("summ"))["summ__sum"]
        
        
## История счета.
# 1 - открыт
# 2 - добавлено блюдо
# 3 - скидка
# 4 - напечатан пречек
# 5 - перенесен на другой стол
# 6 - закрыт
# 7 - отменен пречек
# 8 - отменено блюда (отмена услуги)
# 9 - добавлено блюдо (перенос)
# 10 - отменено блюдо (перенос)
# 11 - добавлен депозит
# 12 - внесены наличные деньги
# 13 - внесены деньги по карте

class SchetHistory(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    schet  = models.ForeignKey(Schet, on_delete=models.CASCADE)
    dt = models.DateTimeField(auto_now_add=True)
    operation = models.IntegerField()
    text = models.CharField(max_length=255)
    class Meta:
        verbose_name_plural=u"аудит счета"
        ordering = ("id",)
    def __unicode__(self):
        return self.text     

# Заказ
class Zakaz(models.Model):
    schet  = models.ForeignKey(Schet, on_delete=models.CASCADE)
    status = models.IntegerField(default=0,choices=((0,u"Открыт"),(1,u"Отправлен")),verbose_name=u"Статус")
    menu   = models.ForeignKey(Menu, on_delete=models.CASCADE, verbose_name=u"Раздел меню")
    usluga = models.ForeignKey(Usluga, on_delete=models.CASCADE, verbose_name=u"Услуга")
    cnt    = models.IntegerField(default=0,verbose_name=u"Количество")
    price  = models.FloatField(default=0)
    price_skidka = models.FloatField(default=0)
    prim   = models.TextField(null=True,blank=True,verbose_name=u"Примечание")
    dt     = models.DateTimeField(auto_now_add=True)
    class Meta:
        verbose_name_plural=u"Заказ"
        ordering = ("id",)
    def __unicode__(self):
        return self.schet.stol.name   

# История заказов
class ZakazHistory(models.Model):
    schet  = models.ForeignKey(Schet, on_delete=models.CASCADE)
    zakaz  = models.ForeignKey(Zakaz, on_delete=models.CASCADE)
    status = models.IntegerField(default=0,choices=((0,u"Открыт"),(1,u"Отправлен")),verbose_name=u"Статус")
    menu   = models.ForeignKey(Menu, on_delete=models.CASCADE, verbose_name=u"Раздел меню")
    usluga = models.ForeignKey(Usluga, on_delete=models.CASCADE, verbose_name=u"Услуга")
    cnt    = models.IntegerField(default=1,verbose_name=u"Количество")
    price  = models.FloatField(default=0)
    prim   = models.TextField(null=True,blank=True,verbose_name=u"Примечание")
    dt     = models.DateTimeField(auto_now_add=True)
    user   = models.ForeignKey(User, on_delete=models.CASCADE)    
    class Meta:
        verbose_name_plural=u"История заказов"
        ordering = ("id",)
    def __unicode__(self):
        return self.schet.stol.name
    def save(self, *args, **kwargs):
        #self.schet.price += self.cnt*self.price
        #self.schet.save()
        self.menu=self.usluga.menuTop()
        # создание заказа
        zs = Zakaz.objects.filter(schet=self.schet,usluga=self.usluga,menu=self.menu)
        if zs.count()>=1:
            xzs = Zakaz.objects.filter(schet=self.schet,usluga=self.usluga,menu=self.menu,cnt__gt=0)
            if xzs.count()>0: z=xzs[0]
            else:
                z = zs[0]
        else:
            z,cr = Zakaz.objects.get_or_create(schet=self.schet,usluga=self.usluga,menu=self.menu)
        #print z.id,cr,self.usluga.id
        self.zakaz = z
        z.status = self.status
        z.menu   = self.menu
        z.cnt    = ZakazHistory.objects.filter(zakaz=self.zakaz,usluga=self.usluga).aggregate(Sum('cnt'))['cnt__sum']
        if z.cnt==None: z.cnt=0
        z.cnt   += self.cnt
        z.price  = z.cnt*self.usluga.price
        z.save()
        # запись в расход товара выполняется триггером!!!!!
        #for d in self.usluga.sostav():
        #    Rashod(usluga=self.usluga,user=self.user,tovar=d.tovar,volume=d.volume*self.cnt,prim="%s - %d"%(self.usluga.name,self.cnt)).save()
        
        super(ZakazHistory, self).save(*args, **kwargs)

# Товар
class Tovar(models.Model):
    name    = models.CharField(max_length=255,unique=True,verbose_name=u"Наименование")
    volume  = models.IntegerField(default=1,verbose_name=u"Стандартный объем (г,мл,шт)")
    mvolume = models.IntegerField(default=0,verbose_name=u"Минимальный объем на складе")
    menu    = models.ForeignKey(Menu, on_delete=models.CASCADE, default=1, null=True, blank=True, verbose_name=u"Пункт меню")
    portion = models.IntegerField(default=1,verbose_name=u"Размер порции")
    is_pf   = models.BooleanField(default=False,verbose_name=u"Полуфабрикат")
    class Meta:
        verbose_name_plural = u"Ингридиенты"
        ordering=("name",)
    def __unicode__(self):
        if self.menu: return self.menu.name+" - "+self.name
        return self.name
        
    def sostav(self):
        return PfSostav.objects.filter(pf=self)
        
# Приход    
class Prihod(models.Model):
    tovar  = models.ForeignKey(Tovar, on_delete=models.CASCADE, verbose_name=u"Товар")
    volume = models.FloatField(verbose_name=u"Объем всего")
    cnt1   = models.FloatField(default=1,verbose_name=u"Сколько единиц")
    #cnt2   = models.IntegerField(verbose_name=u"По сколько")
    volume1= models.IntegerField(verbose_name=u"Объем единицы")
    dt     = models.DateTimeField(verbose_name=u"Дата прихода")
    #box    = models.ForeignKey(Box,null=True,blank=True,verbose_name=u"Тара")
    #volume_name = models.IntegerField(default=0,choices=VOLUMES,verbose_name=u"Размерность")
    price  = models.FloatField(default=0)
    prim   = models.TextField(null=True, blank=True)
    user   = models.ForeignKey(User, on_delete=models.CASCADE)
    class Meta:
        ordering = ("id",)
    def save(self, *args, **kwargs):
        sk,cr = Sklad.objects.get_or_create(tovar=self.tovar)
        sk.volume += float(self.volume)
        sk.save()        
        super(Prihod, self).save(*args, **kwargs)        
    def price1(self):
        if self.cnt1==0: return 0
        return self.price/self.cnt1
    
# Расход
class Rashod(models.Model):
    tovar  = models.ForeignKey(Tovar, on_delete=models.CASCADE,verbose_name=u"Товар")
    volume = models.FloatField(default=0,verbose_name=u"Объем всего")    
    dt     = models.DateTimeField(auto_now=True,verbose_name=u"Дата изменения")
    usluga = models.ForeignKey(Usluga, on_delete=models.CASCADE,null=True,blank=True,verbose_name=u"Услуга")
    prim   = models.TextField(null=True,blank=True,verbose_name=u"Примечание")
    user   = models.ForeignKey(User, on_delete=models.CASCADE,verbose_name=u"Пользователь")
    def save(self, *args, **kwargs):
        sk,cr = Sklad.objects.get_or_create(tovar=self.tovar)
        sk.volume -= float(self.volume)
        sk.save()        
        super(Rashod, self).save(*args, **kwargs)                
    class Meta:
        ordering=("-id",)

# Наличие на складе
class Sklad(models.Model):
    tovar  = models.ForeignKey(Tovar, on_delete=models.CASCADE,verbose_name=u"Товар")
    volume = models.FloatField(default=0,verbose_name=u"Объем всего")    
    dt     = models.DateTimeField(auto_now=True,verbose_name=u"Дата изменения")
    class Meta:
        ordering=("tovar__name",)

# Состав услуги    
class Sostav(models.Model):
    usluga = models.ForeignKey(Usluga, on_delete=models.CASCADE,verbose_name=u"Услуга")
    tovar  = models.ForeignKey(Tovar, on_delete=models.CASCADE,verbose_name=u"Товар")
    volume = models.FloatField(verbose_name=u"Объем (гр,мл,шт)")
    class Meta:
        verbose_name_plural = u"Состав"
    def __unicode__(self):
        return "%s - %s - %f"%(self.usluga.name,self.tovar.name,self.volume)

# Скидки
class Skidka(models.Model):
    value = models.IntegerField(verbose_name=u"Размер скидки")
    
    def __str__(self):
        return str(self.value)
        
    class Meta:
        verbose_name_plural = u"Скидки"
        ordering=("value",)

# Плейлисты
class Playlist(models.Model):
    name = models.CharField(max_length=255)
    path = models.CharField(max_length=255)
    def __unicode__(self):
        return self.name + " - " + self.path
    class Meta:
        verbose_name_plural = u"Плейлисты"
        ordering=(("id"),)
    def music(self):
        return [x for x in Music.objects.filter(playlist=self) if os.path.exists(x.fpath)]

# Файлы в плейлистах
class Music(models.Model):
    title    = models.CharField(max_length=255)
    playlist = models.ForeignKey(Playlist, on_delete=models.CASCADE)
    num      = models.IntegerField(default=0)
    fpath    = models.CharField(max_length=255)
    def __unicode__(self):
        return self.title
    class Meta:
        ordering=(("num"),)
    def fname(self):
        return self.fpath.split("/")[-1]

# Опции 
class Option(models.Model):
    name = models.CharField(max_length=255,verbose_name=u"Наименование параметра")
    code = models.CharField(max_length=255,verbose_name=u"Код параметра")
    value = models.TextField(verbose_name=u"Значение параметра")
    class Meta:
        verbose_name_plural = u"Параметры"
    def __unicode__(self):
        return self.name+" - "+self.value

## Принтеры.
class Printer(models.Model):
    name = models.CharField(max_length=255,verbose_name=u"Наименование")
    pid = models.IntegerField(verbose_name=u"Код принтера в программе печати")
    target = models.IntegerField(default=0,choices=((0,"не используется"),(1,"чеки"),(2,"кухня"),(10,"кухня/чеки")),verbose_name=u"Назначение")
    class Meta:
        verbose_name_plural = u"Принтеры"
        ordering=(("pk"),)
    def __unicode__(self):
        return self.name

## Категории расходов.
class RashodCat(models.Model):
    name = models.CharField(max_length=255,verbose_name=u"Наименование")
    
    class Meta:
        verbose_name_plural = u"Категории расходов"
        ordering=(("pk"),)

    def __unicode__(self):
        return self.name    

    def __str__(self):
        return self.name    

## Дополнительные расходы.    
class DopRashod(models.Model):
    dt   = models.DateField(verbose_name=u"Дата")    
    user = models.ForeignKey(User, null=True, blank=True, on_delete=models.SET_NULL, verbose_name=u"Сотрудник")
    cat  = models.ForeignKey(RashodCat, on_delete=models.CASCADE,verbose_name=u"Категория")
    summ = models.FloatField(verbose_name=u"Сумма")
    prim = models.TextField(verbose_name=u"Примечание")
    class Meta:
        verbose_name_plural = u"Дополнительные расходы"
        ordering=(("-dt"),)
    def __unicode__(self):
        return "%s - %s: %s"%(str(self.dt),self.cat.name,str(self.summ))

## Смена
class Smena(models.Model):
    dt_open    = models.DateTimeField(null=True,blank=True,verbose_name=u"Дата открытия")    
    dt_close   = models.DateTimeField(null=True,blank=True,verbose_name=u"Дата закрытия")    
    who        = models.TextField(null=True,blank=True,verbose_name=u"Кто в смене")
    prim       = models.TextField(null=True,blank=True,verbose_name=u"Примечание к смене")
    user_close = models.ForeignKey(User, null=True, blank=True, on_delete=models.CASCADE, verbose_name=u"Сотрудник, закрывший смену")
    sum        = models.FloatField(default=0,null=True,blank=True,verbose_name=u"Выручка за смену")

## Возврат услуги после пробития чека (борщ вернули, потому что недосоленый)
class Vozvrat(models.Model):
    user   = models.ForeignKey(User, on_delete=models.CASCADE, verbose_name=u"Сотрудник")
    usluga = models.ForeignKey(Usluga, on_delete=models.CASCADE, verbose_name=u"Услуга")    
    cnt    = models.IntegerField(default=1,verbose_name=u"Количество")
    summ   = models.FloatField(default=0,verbose_name=u"Сумма")
    dt     = models.DateTimeField(auto_now_add=True,verbose_name=u"Дата возврата")
    

class Deposit(models.Model):
    schet = models.ForeignKey(Schet, on_delete=models.CASCADE, verbose_name=u"Счёт")
    summ  = models.FloatField(default=0,verbose_name=u"Сумма депозита")
    card  = models.IntegerField(default=0,choices=((0,u"наличные"),(1,u"карта"),(2,u"примечание")),verbose_name=u"Закрыто картой")
    dt    = models.DateTimeField(auto_now=True,verbose_name=u"Дата ввода")
    user  = models.ForeignKey(User, on_delete=models.CASCADE, verbose_name=u"Пользователь")
    
## Поступление денег по счетам.

class Cash(models.Model):
    schet = models.ForeignKey(Schet, on_delete=models.CASCADE, verbose_name=u"Счёт")
    summ  = models.FloatField(default=0,verbose_name=u"Сумма")
    card  = models.IntegerField(default=0,choices=((0,u"наличные"),(1,u"карта"),(2,u"примечание")),verbose_name=u"Закрыто картой")
    dt    = models.DateTimeField(auto_now=True,verbose_name=u"Дата ввода")
    user  = models.ForeignKey(User, on_delete=models.CASCADE, verbose_name=u"Пользователь")
    deposit = models.ForeignKey(Deposit, null=True, blank=True, on_delete=models.CASCADE, verbose_name=u"Депозит")
 
    
class Guest(models.Model):
    card = models.CharField(max_length=255,null=True,blank=True)
    fio  = models.CharField(max_length=255,null=True,blank=True)
    prim = models.TextField(null=True,blank=True)


class PfSostav(models.Model):
    pf     = models.ForeignKey(Tovar, on_delete=models.CASCADE, related_name="pf_tovar")
    tovar  = models.ForeignKey(Tovar, on_delete=models.CASCADE, related_name="tovar")
    volume = models.FloatField(verbose_name=u"Объем (гр,мл,шт)")
  
class DiscountHistory(models.Model):
    discount = models.ForeignKey(Discount, on_delete=models.CASCADE)
    schet = models.ForeignKey(Schet, null=True, blank=True, on_delete=models.SET_NULL)
    volume = models.FloatField()
    dt_create = models.DateTimeField(auto_now=True)

# admin.site.register(Option)    
# admin.site.register(Stol)
# admin.site.register(Menu)
# admin.site.register(Usluga)
#admin.site.register(Tovar)
admin.site.register(Printer)
admin.site.register(Skidka)
#admin.site.register(Playlist)
admin.site.register(RashodCat)
admin.site.register(DopRashod)
