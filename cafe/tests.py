#-*- encoding: utf-8 -*-

import datetime
import os
import subprocess
import json

from django.test import TestCase,Client
from django.contrib.auth.models import User
from .models import *

class CafeTest(TestCase):
    def setUp(self):
        #subprocess.Popen('psql -U postgres test_caffein < /opt/mcafe/alter.sql')
        #os.system('psql -U postgres test_caffein < /opt/mcafe/cafe/sql/Sostav.postgresql_psycopg2.sql 1>/dev/null 2>/dev/null')
        
        self.u1 = User.objects.create_user(username="user1",password="1")
        self.u0 = User.objects.create_user(username="root",password="1")
        self.u0.is_superuser = True
        self.u0.is_staff = True
        self.u0.save()
        
        #print "ucount=",User.objects.all()[0].pk,User.objects.all()[1].pk
        self.client = Client()
        for i in range(0, 40):
            self.stol = Stol.objects.create(name='stol_test%d'%i)
            
        self.menu = Menu.objects.create(name='menu_test')
        self.submenu = Menu.objects.create(name='submenu_test',pid=self.menu.id)
        self.usluga = Usluga.objects.create(menu=self.submenu,price=100,name='usluga_test')
        self.tovar = Tovar.objects.create(name='tovar_test',menu=self.menu)
        
    def test_smoke(self):
        self.assertEqual(self.client.get('/zal/').status_code,200)
        self.assertEqual(self.client.get('/accounts/login/').status_code,200)
        self.client.login(username="user1",password="1")
        self.assertEqual(self.client.get('/menu/').status_code,200)
        
    def test_login(self):
        uid = str(User.objects.all()[0].pk)
        r = self.client.post('/accounts/login/',{"userid":uid,"password":"2"})
        self.assertEqual(r.status_code,200) # неверная аутентификация
        r = self.client.post('/accounts/login/',{"userid":uid,"password":"1"})
        self.assertEqual(r.status_code,302) # верная аутентификация
        r = self.client.get('/accounts/logout/')
        self.assertEqual(r.status_code,302) # выход
        
    def test_rashod(self):
        schet = Schet.objects.create(stol=self.stol, name='1', user_open=self.u1)
        cnt = 2
        tovar = self.tovar
        usluga = self.usluga
        # приход товара
        Prihod.objects.create(tovar=tovar, volume=100, volume1=1, user=self.u1, dt=datetime.datetime.now())
        self.assertEqual(Sklad.objects.get(tovar=tovar).volume, 100)
        Prihod.objects.create(tovar=tovar, volume=200, volume1=1, user=self.u1, dt=datetime.datetime.now())
        self.assertEqual(Sklad.objects.get(tovar=tovar).volume, 300)
        # заполнение состава блюда
        Sostav.objects.create(tovar=tovar, volume=25, usluga=usluga)
        # произошёл заказ
        ZakazHistory(schet=schet, status=1, usluga=usluga, cnt=cnt, price=usluga.price*cnt, user=self.u1).save()
        # self.assertEqual(Sklad.objects.get(tovar=tovar).volume,250)
        self.assertEqual(Sklad.objects.get(tovar=tovar).volume, 300)
    
    def test_admin(self):
        self.client.login(username="user1",password="1")
        r = self.client.get("/admin/")
        self.assertEqual(r.status_code,302)
        self.client.logout()
        self.client.login(username="root",password="1")
        r = self.client.get("/admin/")
        self.assertEqual(r.status_code,200)
        
    def test_closeschet(self):
        schet = Schet.objects.create(stol=self.stol,name='1',user_open=self.u1)
        sid = schet.id
        usluga = self.usluga
        
        self.client.login(username="user1",password="1")
        # депозит
        params = {
            "deposit_schet_id":sid,
            "deposit":100,
            "card":0, # наличными
        }
        r = self.client.post('/check/deposit/',params)
        self.assertEqual(r.status_code,302)
        self.assertEqual(Deposit.objects.count(),1)
        self.assertEqual(Cash.objects.filter(schet=schet,deposit=Deposit.objects.all()[0]).count(),1)
        # заказ услуги 
        params = {
            "schet":json.dumps([
                {
                    "sid":sid,
                    "usluga":[
                        {"cnt":6,"id":usluga.id},
                    ]
                }
            ])
        }
        r = self.client.post('/zakaz/%d/'%(self.stol.id),params)        
        self.assertEqual(r.status_code, 200)
        self.assertEqual(ZakazHistory.objects.count(), 1)
        self.assertEqual(Schet.objects.get(pk=sid).status, 0)
        self.assertEqual(Schet.objects.get(pk=sid).price, 600)
        # закрытие счёта наличными
        params={
            "summ":200,
        }
        r = self.client.post('/check/close/%d/'%(sid),params)
        self.assertEqual(Cash.objects.filter(schet=schet).count(), 2)
        self.assertEqual(Schet.objects.get(pk=sid).status, 0)
        # закрытие счёта картой
        params={
            "summ":400,
        }
        r = self.client.post('/check/close/%d/1/'%(sid),params)
        self.assertEqual(Cash.objects.filter(schet=schet).count(), 3)        
        self.assertEqual(Schet.objects.get(pk=sid).status, 1)
        
        dt1 = datetime.datetime.now()-datetime.timedelta(days=1)
        dt2 = datetime.datetime.now()
        self.assertEqual(cash_summ(dt1, dt2, 0), 300)
        self.assertEqual(cash_summ(dt1, dt2, 1), 400)
        
    def test_prim(self):
        schet = Schet.objects.create(stol=self.stol,name='1',user_open=self.u1)
        params = {
            "prim_schet_id":schet.id,
            "prim":"test prim",
        }
        r = self.client.post('/check/prim/',params)
        self.assertEqual(r.status_code,302) # перенаправление

        
        
        
