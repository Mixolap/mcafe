#-*- encoding: utf-8 -*-
from django.shortcuts import render
from django.urls import reverse
from django.contrib.auth.models import Group,User
from django.http import HttpResponse,HttpResponseRedirect,Http404
from django.http import JsonResponse
from django.template.loader import get_template
from django.template import Context
from django.utils import timezone
from django.db import connection
from django.db.models import Max,Avg,Sum
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.decorators import login_required
from django.conf import settings
from .models import *

import json
import os
import shutil
import time
import random

BUH_ID      = 1
ADMIN_ID    = 2
OFFISANT_ID = 3

def render_to_html(template_name, ctx={}):
    template = get_template(template_name)
    return template.render(ctx) + "" ## хак чтобы SafeText превратить в str


def requestParam(request,name,data=None,value=None):
    if request.POST.get(name)!=None:
        return request.POST.get(name)
    data = json.loads(data)
    if name in data.keys(): return data[name]        
    return value


def home(request,template_name="home.html"):
    # os.system("sh /opt/mcafe/alter.sh")
    os.system("sh /opt/caffein/mcafe/alter.sh")
    
    OFFICIANT_NAME = get_option("OFFICIANT_NAME",u"Официант")
    CAFE_NAME = get_option("name",u"")
    

    return render(request, template_name, locals())

def login_user(request,template_name="login.html"):
    suser = request.GET.get("admin","")=="1"
    if get_option("DEMO")=="1":
        if suser:          
            if request.user.is_authenticated():
                logout(request)
            user = authenticate(username="Administrator", password="1")
            if user==None: 
                user = User.objects.create_user(username="Administrator", password="1",first_name=u"Администратор",is_staff=True,is_superuser=True)
                for group in Group.objects.all():
                    user.groups.add(group)
                user = authenticate(username="Administrator", password="1")
            login(request,user)
            return HttpResponseRedirect("/administrator/")
        else:
            user = authenticate(username="officiant1", password="1")
            login(request,user)
            return HttpResponseRedirect(request.GET.get("next","/zal/"))
    
    users = User.objects.filter(is_active=True,is_superuser=suser).exclude(pk=1).exclude(first_name="").order_by("username")
    if request.method=="POST":
        #print request.POST.get("username")
        username = User.objects.get(pk=request.POST.get("userid")).username
        password = request.POST.get("password")
        ref      = request.POST.get("ref","/administrator/")
        #print "ref=",ref
        if ref=="" or "/login/" in ref: ref="/administrator/"
        user = authenticate(username=username, password=password)
        
        if user==None: 
            msg=u"Неверное имя пользователя или пароль"
            return render(request,template_name,locals())
        login(request,user)
        
        return HttpResponseRedirect(ref)
    else:
        if request.user.is_anonymous: 
            ref = request.GET.get("next")
            if ref==None:
                ref = request.META.get("HTTP_REFERER")
            return render(request,template_name,locals())
        logout(request)
        return HttpResponseRedirect(request.META.get("HTTP_REFERER")) 

def logout_user(request):
    logout(request)
    next = request.GET.get("next","/")
    if get_option("DEMO")=="1": next = "/"
    return HttpResponseRedirect(next)

def makeDemoZakaz():
    #print "makeDemoZakaz"
    uslugs = list(Usluga.objects.filter(active=True).exclude(menu__pid=36))
    stols  = list(Stol.objects.filter(visible=True).exclude(id=12))
    for schet in Schet.objects.filter(status=0,dt_open__gte=datetime.datetime.now()-datetime.timedelta(hours=10)):
        schet.status = 1
        schet.user_close = schet.user_open
        schet.dt_close = schet.dt_open+datetime.timedelta(minutes=random.randint(5,50))
        schet.save()
        schet.stol.recalc()
    dt = datetime.datetime.now()
    mins = 0
    for i in range(0, 3):
        user  = User.objects.all()[0]
        stol  = random.choice(stols)
        schet = Schet(stol=stol,price=0,status=0,name="test",user_open=user)            
        schet.save()
        schet.dt_open = dt + datetime.timedelta(minutes=mins)
        schet.num = str(schet.id)
        schet.save()
        logSchet(user,schet,1,u"счет #"+str(schet.num)+u" открыт")
        bcount = random.randint(1,7)
        for b in range(0, bcount):
            usluga = random.choice(uslugs)
            cnt = random.randint(1,3)
            ZakazHistory(schet=schet,status=1,usluga=usluga,cnt=cnt,price=usluga.price*cnt,user=user).save()
            logSchet(user,schet,2,u"добавлен заказ %s в количестве %d"%(usluga.name,cnt))
    stol  = random.choice(stols)
    Bron(stol=stol,prim="89050000000 Николай",dt=datetime.datetime(dt.year,dt.month,dt.day,dt.hour)+datetime.timedelta(hours=2)).save()

def zal(request,template_name="zal_common.html"):
    if os.path.exists(os.path.join(settings.PROJECT_ROOT,"templates/zal.html")): template_name="zal.html"
    stol = Stol.objects.filter(visible=1)
    if get_option('DEMO')=="1" and get_option('RAZDACHA')!="1" and Schet.objects.filter(status=0, dt_open__gte=datetime.datetime.now()-datetime.timedelta(hours=2)).count()==0:
        makeDemoZakaz()
    no_payed = False
    # no_payed = Schet.objects.all().count()>1500
    if no_payed:
        # if os.path.exists(os.path.join(settings.BASE_DIR,"templates/zal.html")):
            # os.remove(os.path.join(settings.BASE_DIR,"templates/zal.html"))
        # if os.path.exists(os.path.join(settings.BASE_DIR,"templates/zal_common.html")):
            # os.remove(os.path.join(settings.BASE_DIR,"templates/zal_common.html"))
        return HttpResponse(u"Программа не оплачена, обратитесь к разработчику http://caffein-system.ru")
    
    stol = Stol.objects.all().order_by("id")
    zal_template = get_option('ZAL_TEMPLATE', 'zal.html')
    print(os.path.join(settings.PROJECT_ROOT, "templates/" + zal_template))
    if not os.path.exists(os.path.join(settings.PROJECT_ROOT, "templates/" + zal_template)): 
        zal_template="zal_common.html"

    return render(request, zal_template, locals())
    
def bron(request,id,template_name="bron.html"):
    stol = Stol.objects.get(pk=id)
    
    if request.method=="POST":
        date = request.POST.get("date").split(".")
        time = request.POST.get("time").split(":")
        
        dt = datetime.datetime(int(date[2]),int(date[1]),int(date[0]),int(time[0]),int(time[1]))
        Bron(stol=stol,prim=request.POST.get("prim",""),dt=dt).save()
    elif request.GET.get("rm")!=None:
        Bron.objects.get(pk=request.GET.get("rm")).delete()
    bron = Bron.objects.filter(stol=stol)
    
    times = []
    for x in range(0,24):        
        times.append("%d:00"%(x))
        times.append("%d:30"%(x))
        
    stols = Stol.objects.all()
    return render(request,template_name,locals())

def bronAll(request,template_name="bron_all.html"):
    stols = Stol.objects.filter(visible=True)
    return render(request,template_name,locals())

# аудит счета.    
def logSchet(user,schet,op,text):
    SchetHistory(user=user,schet=schet,operation=op,text=text).save()    

def printerTemplate(tname):
    printer = get_option("PRINTER","")
    if printer=="": return tname 
    return tname+"."+printer

def printKuhnia(s, uss, template_name="print_kuhnia.html"):
    if len(uss)==0: return
    data = render_to_html(printerTemplate(template_name), locals())
    for p in Printer.objects.filter(target__in=[2,10]):
        fname = os.path.join(settings.PRINT_DIR,"%d_%d_kitchen.htm"%(p.pid,s.id))
        f = open(fname, "w")
        f.write("type3\n"+data)
        f.close()

def printBar(s, uss, template_name="print_bar.html"):
    if len(uss)==0: return 
    data = render_to_html(printerTemplate(template_name),locals())
    for p in Printer.objects.filter(target__in=[2,10]):
        fname = os.path.join(settings.PRINT_DIR,"%d_%d_bar.htm"%(p.pid,s.id))
        f = open(fname, "w")
        f.write("type4\n"+data)
        f.close()
        
def printPrecheck(s,is_check=0,template_name="precheck.html"):
    is_check = int(is_check)
    opt_name  = get_option("name")
    opt_addr  = get_option("addr")
    opt_phone = get_option("phone")
    opt_inn   = get_option("inn")
    opt_ogrn  = get_option("ogrn")
    data = render_to_html(printerTemplate(template_name),locals())
    fname = ""
    for p in Printer.objects.filter(target__in=[2,10]):
        pref = ""
        if is_check==0:
            fname = os.path.join(settings.PRINT_DIR,"%d_%d_precheck.htm"%(p.pid,s.id))
            pref = "type1\n"
        else:
            fname = os.path.join(settings.PRINT_DIR,"%d_%d_check.htm"%(p.pid,s.id))
            pref = "type2\n"
        f = open(fname, "w")
        f.write(pref+data)
        f.close()
    return fname
    
@login_required
def zakaz(request,id,user_id=0,template_name="zakaz.html"):
    user_id = int(user_id)
    id      = int(id)
    stols   = Stol.objects.all()
    stol    = Stol.objects.get(pk=id)

    if not os.path.exists(settings.PRINT_DIR):
        os.mkdir(settings.PRINT_DIR)

    if stol.status==1:
        sch = Schet.objects.filter(stol=stol,status=0)
        if sch.count()>0:
            user_id = sch[0].user_open.id
    if user_id==0 and request.user.is_anonymous:
        if get_option("HAVE_OFFICIANTS")=="1":
            template_name = "select_user.html"
            users = Group.objects.get(pk=OFFISANT_ID).user_set.all().order_by("-id")
            return render(request,template_name,locals())    
        else:
            user_id = User.objects.filter(is_staff=False).order_by("id")[0].id
    
    if request.user.is_anonymous:
        user = User.objects.get(pk=user_id)
    else:
        user = request.user
#    print "ZAKAZ",request.is_ajax()
    if request.is_ajax() or request.method=="POST":
        d = request.POST.get("schet")
        #print "ZAKAZ",d
        #print "ZAKAZ",json.loads(d)
        try:
            data = json.loads(request.POST.get("schet"))
        except:
            data = json.loads(request.read())["schet"]
        #print "data=",data
        for d in data:
            sid = d["sid"]
            
            try:
                schet = Schet.objects.filter(pk=int(sid))
            except:
                schet = Schet.objects.filter(stol=stol,status=0,name=d["name"])
                
            if schet.count()==0:
                schet = Schet(stol=stol,price=0,status=0,name=d["name"],user_open=user)
                
                sn = get_option("schet_num")
                if sn==None: sn=1
                else: sn = int(sn)+1
                schet.num = sn
                schet.save()
                set_option("schet_num",sn)
                logSchet(user,schet,1,u"счет #"+str(schet.num)+u" открыт")
            else:
                schet = schet[0]
                
            uss = []
            bar_zakaz = []
            #t1 = time.time()
            for u in d["usluga"]:                
                cnt = float(u["cnt"])
                usluga = Usluga.objects.get(pk=u["id"])
                pid = usluga.menu.pid
                if pid==settings.KITCHEN_ID or  pid==settings.KALIAN_ID or pid==settings.BOY_ID:
                    uss.append([usluga,cnt])
                if pid==settings.BAR_ID:
                    bar_zakaz.append([usluga,cnt])
                if cnt>0:
                    ZakazHistory(schet=schet,status=1,usluga=usluga,cnt=cnt,price=usluga.price*cnt,user=user).save()
                    logSchet(user,schet,2,u"добавлен заказ %s в количестве %d"%(usluga.name,cnt))
                    
            if len(d["usluga"])>0:
                printKuhnia(schet, uss) 
                printBar(schet, bar_zakaz) 
            
            schet.save()
                
                
        return HttpResponse('ok')
    
    
    menu   = Menu.objects.filter(pid=0,active=1)
    schet  = Schet.objects.filter(stol=stol,status=0).order_by("id")
    if request.GET.get("skidka")=="1":
        for s in schet:
            s.skidka = 100
            s.save()
    schet_open  = Schet.objects.filter(stol=stol,status=0,precheck=0)
    no_prechecks = schet_open.count()>0
    skidka = Skidka.objects.all()
    
    OFFICIANT_NAME  = get_option("OFFICIANT_NAME",u"Официант")
    HAVE_CARD       = get_option("HAVE_CARD",1) # наличие карты - по умолчанию принимают карты
    HAVE_PRECHEK    = get_option("HAVE_PRECHEK",1) # печатаем ли пречеки
    HAVE_DEPOSIT    = get_option("HAVE_DEPOSIT",0) # подтверждаем ли заказ отправляя его на обработку
    CONFIRM_ZAKAZ   = get_option("CONFIRM_ZAKAZ",1) # подтверждаем ли заказ отправляя его на обработку
    
    edit = 0
    
    #print get_option("NEW_ZAKAZ",0)
    
    if request.GET.get("old","")=="1":
        return render(request,template_name,locals()) 
    
    if get_option("NEW_ZAKAZ",0)=="1":
        template_name = "zakaz_new.html"
        return render(request, template_name, locals()) 
    elif get_option("NEW_ZAKAZ",0)=="2":
        template_name = "zakaz_2.html"
        return render(request, template_name, locals())         
    
    return render(request,template_name,locals()) 
    
@login_required
def editZakaz(request, id, template_name="zakaz.html"):
    id = int(id)
    schet = Schet.objects.get(pk=id)
    stols = Stol.objects.all()
    # stol = schet.stol
    stol = schet.stol
    schet  = Schet.objects.filter(pk=id)
    schet_open  = Schet.objects.filter(stol=stol,status=0,precheck=0)
    edit = 1
    return render(request,template_name,locals()) 

def precheck(request,id,template_name="precheck.html"):    
    s = Schet.objects.get(pk=id)    
    if s.status==1 or (s.precheck==0 and get_option("HAVE_PRECHEK","1")=="1"):
        s.precheck=1
        s.save()    
        logSchet(s.user_open,s,4,u"Печать пречека")
        fname = printPrecheck(s)
        
        if get_option("HAVE_PRINTER",1)=="1":
            return HttpResponseRedirect(reverse('zal'))#render(request,template_name,locals())   
        else:
            f = open(fname,"r")            
            return HttpResponse(f.read())
    else: # отмена пречека
        s.precheck=0
        s.save()    
        logSchet(s.user_open,s,7,u"Отмена пречека")        
        return HttpResponseRedirect(reverse('zakaz',args=[s.stol.id]))#render(request,template_name,locals())   
    
def getCardName(card):
    if card==0: return u"наличные"
    elif card==1: return u"картой"
    else: return u"примечанием"
#@login_required

    

def checkClose(request, id, card=0, template_name="check.html"):
    user = None
    print_kuhnya = False # печатать кухню и бар при закрытии чека
    if request.user.is_anonymous:
        uname = request.GET.get("uname")
        passwd = request.GET.get("passwd")
        user = authenticate(username=uname, password=passwd)
        if user==None: return HttpResponse("error user or password")
        print_kuhnya = True
    else:
        user = request.user
    
    card = int(card)
    id = int(id)
    
    s = Schet.objects.get(pk=id)    
    
    summ = int(request.POST.get("summ",0))
    if summ==0:
        summ = s.calcPriceSkidka()
        Cash(schet=s, summ=summ, card=card, user=user).save()
    else:
        Cash(schet=s, summ=summ, card=card, user=user).save()
        
    logSchet(user,s,12,u"внесены деньги %d р. %s"%(summ, getCardName(card)))
        
    if not s.mayClose():
        return HttpResponseRedirect(reverse('zakaz',args=[s.stol.id]))
    
    s.status = 1
    s.user_close = user
    s.dt_close = timezone.now()
    s.card = card
    if card!=0:
        s.prim = request.POST.get("prim")
    s.save()
    # перерасчёт на скидочную карту, если таковая имеется
    if s.discount_card:
        dcard = s.discount_card
        volume = s.price_skidka * (dcard.discount_value / 100.) 
        dcard.dt_last = timezone.now()
        dcard.balance += volume - s.skidka_fix
        dcard.save()
        DiscountHistory(schet=s, discount=dcard, volume=volume).save()

    s.stol.recalc()
    #logSchet(s.user_open,s,6,u"счет #%d закрыт и напечатан чек"%(s.id))
    logSchet(user,s,6,u"счет #%d закрыт и напечатан чек"%(s.id))
    if print_kuhnya:
        # печатать кухню
        uss = []
        for z in Zakaz.objects.filter(schet=s):                
            if z.usluga.menuTop().id != settings.KITCHEN_ID: continue
            if z.cnt<=0: continue
            cnt = z.cnt
            usluga = z.usluga
            uss.append([usluga, cnt])
        printKuhnia(s, uss)

        # печать бара
        uss = []
        for z in Zakaz.objects.filter(schet=s):                
            if z.usluga.menuTop().id != settings.BAR_ID: continue
            if z.cnt<=0: continue
            cnt = z.cnt
            usluga = z.usluga
            uss.append([usluga, cnt])
        printBar(s, uss)
        
    printPrecheck(s,1)
    #return HttpResponseRedirect(reverse('schet',args=[1]))
    if get_option('RAZDACHA')=="1":
        return HttpResponseRedirect('/zakaz/1/')
    return HttpResponseRedirect(reverse('zal'))
    
def schetAddCash(request):
    id   = int(request.POST.get("close_schet_id"))
    card = int(request.POST.get("close_card"))
    return checkClose(request,id,card)
    
@login_required
def skidka(request,id,sid):
    s = Schet.objects.get(pk=id)    
    s.skidka = Skidka.objects.get(pk=sid).value
    s.save()

    logSchet(request.user,s,3,u"указана скидка %s%%"%(str(s.skidka)))

    return HttpResponseRedirect(reverse('zakaz',args=[s.stol.id]))
    
@login_required    
def skidkaFix(request):
    s = Schet.objects.get(pk=request.POST.get("schet_id"))
    dc_id = request.POST.get("discount_card_id")
    skidka_fix = 0
    try:
        skidka_fix = int(request.POST.get("summ", 0))
    except:
        pass

    if dc_id!=None and dc_id!="" and dc_id!='0':
        dc = Discount.objects.get(pk=dc_id)
        s.discount_card = dc
        s.save()
        audit_message = u"указана карта %s"%(dc.card_number, )
        if skidka_fix>0:
            audit_message += u" со списанием %s"%(str(skidka_fix), )
        logSchet(request.user, s, 3, audit_message)
    elif s.discount_card:
        audit_message = u"убрана карта %s"%(s.discount_card.card_number, )
        s.discount_card = None
        s.save()
        logSchet(request.user, s, 3, audit_message)

    s.skidka_fix = skidka_fix
    s.prim = request.POST.get("prim")
    s.save()
    logSchet(request.user, s, 3, u"указана скидка %s р."%(str(s.skidka_fix)))

    return HttpResponseRedirect(reverse('zakaz',args=[s.stol.id]))

## перенос счета на другой стол стола.
@login_required
def checkMove(request,stol_id,schet_id):
    s = Schet.objects.get(pk=schet_id)
    stol = Stol.objects.get(pk=stol_id)
    xstol = s.stol
    if stol.id==xstol.id: return HttpResponseRedirect(reverse("zakaz",args=[stol_id]))
    s.stol = stol
    s.save()
    xstol.recalc()
    stol.recalc()
    logSchet(request.user,s,5,u"перенесен со стола %s на стол %s"%(xstol.name,stol.name))
    return HttpResponseRedirect(reverse("zakaz",args=[stol_id]))
    
## перенос услуги в другой счет
@login_required    
def uslugaMove(request,id,id_to):
    id = int(id) # с какого счета снять
    id_to = int(id_to) # на какой счет занести, если =0 то создать новый счет
    from_schet = Schet.objects.get(pk=id)
    stol = from_schet.stol
    user = from_schet.user_open
    if id_to==0:
        # создать новый счет
        to_schet = Schet(stol=stol,price=0,status=0,name=str(Schet.objects.filter(stol=stol).count()+1),user_open=user)
        to_schet.save()
        logSchet(user,to_schet,1,u"счет #"+str(to_schet.id)+u" открыт")        
    else:
        to_schet = Schet.objects.get(pk=id_to)
    uslugs = request.POST.getlist("usluga[]")
    #print uslugs
    for d in uslugs:
        uid = d.split(";")[0]
        cnt = int(d.split(";")[1])
        if cnt==0: continue
        zakaz = Zakaz.objects.filter(pk=uid,schet=from_schet)
        if zakaz.count()==0: continue
        zakaz = zakaz[0]
        if cnt>zakaz.cnt: cnt=zakaz.cnt
        ZakazHistory(schet=from_schet,status=1,usluga=zakaz.usluga,cnt=0-cnt,price=zakaz.usluga.price*cnt,user=user).save()        
        logSchet(request.user,from_schet,9,u"перенос заказа %s в количестве %d (убран в счет #%d)"%(zakaz.usluga.name,cnt,to_schet.id))
        ZakazHistory(schet=to_schet,status=1,usluga=zakaz.usluga,cnt=cnt,price=zakaz.usluga.price*cnt,user=user).save()        
        logSchet(request.user,to_schet,10,u"перенос заказа %s в количестве %d (добавлен из счета #%d)"%(zakaz.usluga.name,cnt,from_schet.id))
        
    return HttpResponse("ok")
## отмена услуги
@login_required    
def uslugaCancel(request,id):
    id = int(id)
    #s = Schet.objects.get(pk=id)
    uslugs = request.POST.getlist("usluga[]")
    #print uslugs
    for d in uslugs:
        uid = d.split(";")[0]
        cnt = int(d.split(";")[1])
        if cnt==0: continue
        zakaz = Zakaz.objects.get(pk=uid)
        user = zakaz.schet.user_open
        if cnt>zakaz.cnt: cnt=zakaz.cnt
        ZakazHistory(schet=zakaz.schet,status=1,usluga=zakaz.usluga,cnt=0-cnt,price=zakaz.usluga.price*cnt,user=user).save()        
        logSchet(request.user,zakaz.schet,8,u"отмена услуги %s в количестве %d"%(zakaz.usluga.name,cnt))
        
    return HttpResponse("ok")
    
#@login_required    
def administrator(request,template_name="administrator.html"):
    if not request.user.is_superuser: 
        logout(request)
        return HttpResponseRedirect(reverse("login")+"?admin=1&next=/administrator/")
    
    stols = Stol.objects.all()
    for s in stols: s.recalc()
    cur  = connection.cursor()
    cur.execute("select s.id from cafe_sklad s,cafe_tovar t where s.tovar_id=t.id and s.volume<t.mvolume")
    sk = Sklad.objects.filter(pk__in=[c[0] for c in cur])    
    dstart = dtStart()
    sch_open   = Schet.objects.filter(status=0).count()
    sch_closed = Schet.objects.filter(status=1,dt_close__gte=dstart).count()
    stol_zakaz = Stol.objects.filter(status=1)
    stol_bron  = Bron.objects.filter(dt__gte=datetime.datetime.now()-datetime.timedelta(hours=1))
    
    return render(request,template_name,locals())   
    
@login_required    
def buhgalter(request,template_name="buhgalter.html"):
    return render(request,template_name,locals())       

def checkUslugsContent():
    if get_option("AUTO_ADD_CONTENT")!="1": return
    for d in Usluga.objects.all():
        if d.hasSostav()==0:
            t = Tovar.objects.filter(name__iexact=d.name,menu=d.menuTop())
            if t.count()==0:
                t = Tovar(name=d.name,menu=d.menuTop())
                t.save()
            else:
                t = t[0]
            #t,cr = Tovar.objects.get_or_create(name=d.name,menu=d.menuTop())
            Sostav(usluga=d,tovar=t,volume=1).save()
            #t.menu = d.menuTop()
            #t.save()

@login_required
def menu(request,template_name="menu.html"):
    stols = Stol.objects.all()
    if request.method=="POST":
        menu  = Menu.objects.get(pk=request.POST.get("menu_id"))
        name  = request.POST.get("usluga_name")
        check_name  = request.POST.get("usluga_check_name")
        price = float(request.POST.get("usluga_price"))
        desc  = request.POST.get("usluga_desc")
        Usluga(menu=menu, name=name, check_name=check_name, price=price, description=desc).save()
        checkUslugsContent()
        if request.POST.get("gotoref")=="1": return HttpResponseRedirect(request.META.get("HTTP_REFERER"))
    
    menu = Menu.objects.filter(pid=0)
    pfs = Tovar.objects.filter(is_pf=True)
    return render(request,template_name,locals())       
    
@login_required
def usluga(request,uid,template_name="usluga.html"):
    if not isBuh(request): raise Http404()
    stols = Stol.objects.all()
    us = Usluga.objects.get(pk=uid)
    if request.method=="POST":
        name  = request.POST.get("usluga_name")
        if name!=None:
            us.name  = request.POST.get("usluga_name")
            us.check_name  = request.POST.get("usluga_check_name")
            us.price = float(request.POST.get("usluga_price").replace(",", "."))
            us.description  = request.POST.get("usluga_desc")
            us.np    = request.POST.get("usluga_np")
            us.save()
        else:
            tovars  = request.POST.getlist("tovar[]")
            volumes = request.POST.getlist("volume[]")
            if len(tovars)!=len(volumes): raise Http404()
            us.sostav().delete()
            for i in range(0, len(tovars)):
                tovar  = Tovar.objects.get(pk=tovars[i])
                if volumes[i]=='': volumes[i]=0
                volume = float(volumes[i].replace(",","."))
                Sostav(usluga=us,tovar=tovar,volume=volume).save()
            
    tovar = Tovar.objects.all()
    return render(request,template_name,locals())       
    
@login_required
def removeUsluga(request,uid):
    if not isBuh(request): raise Http404()
    stols = Stol.objects.all()
    us = Usluga.objects.get(pk=uid)
    us.delete()
    return HttpResponseRedirect(reverse('menu'))
    
@login_required
def removeMenu(request,id):
    if not isBuh(request): raise Http404()
    m = Menu.objects.get(pk=id)
    # if Menu.objects.filter(pid=m.id).count()>0 or Usluga.objects.filter(menu=m).count()>0: raise Http404('menu not empty')
    m.delete()
    return HttpResponseRedirect(reverse('menu'))    
    
@login_required
def editMenu(request,id,template_name=u"edit_menu.html"):
    if not isBuh(request): raise Http404()
    stols = Stol.objects.all()
    if request.method=="POST":
        name = request.POST.get("menu_name")
        if name!=None:
            Menu(name=name,pid=id).save()
    menu = Menu.objects.get(pk=id)    
    return render(request,template_name,locals())       

def toDate(text):
    if text==None: return datetime.datetime.now()
    dt1 = text.split(".")
    if len(dt1[2])==2:
        dt1 = datetime.datetime(int("20"+dt1[2]),int(dt1[1]),int(dt1[0]))    
    else:
        dt1 = datetime.datetime(int(dt1[2]),int(dt1[1]),int(dt1[0]))    
    return dt1
    
@login_required
def prihod(request,id=0,template_name="prihod.html"):
    id = int(id)
    if request.method=="POST":
        dt = toDate(request.GET.get("dt1"))
        tovar   = request.POST.getlist("tovar[]")
        cnt1    = request.POST.getlist("cnt1[]")
        price   = request.POST.getlist("price[]")
        volume1 = request.POST.getlist("volume1[]")
        volume  = request.POST.getlist("volume[]")
        prim    = request.POST.getlist("prim[]")
        for i in range(0, len(tovar)):
            p = Prihod()
            p.dt      = dt
            p.tovar   = Tovar.objects.get(pk=tovar[i])
            p.cnt1    = cnt1[i].replace(",",".")
            p.price   = price[i]
            p.volume1 = volume1[i]
            p.volume  = volume[i]
            p.prim    = prim[i]
            p.user    = request.user
            #p.dt      = datetime.datetime.now()
            
            p.save()
        #return HttpResponseRedirect(reverse("prihod",args=(int(id),)))
    dt = datetime.datetime.now()
    dt1 = request.GET.get("dt1")
    
    if dt1==None:         
        dt1=dt
        
    else: 
        dt1 = dt1.split(".")
        dt1 = datetime.datetime(int(dt1[2]),int(dt1[1]),int(dt1[0]))
    dt2 = dt1+datetime.timedelta(days=1)
    may_add = 1#(dt1.year==dt.year and dt1.month==dt.month and dt1.day==dt.day)
    prihod = Prihod.objects.filter(dt__gte=datetime.datetime(dt1.year,dt1.month,dt1.day),dt__lt=dt2)
    tovar = Tovar.objects.all()
    stols = Stol.objects.all()
    
    if id != 0: 
        menu = Menu.objects.get(pk=id)
        tovar = tovar.filter(menu_id=id)
        prihod = prihod.filter(tovar__menu_id=id)
    return render(request,template_name,locals())       

@login_required
def sklad(request,template_name="sklad.html"):
    
    if request.method=="POST":
        sklad_id = request.POST.get('sklad_id')
        prim = request.POST.get('prim')
        new_cnt = int(request.POST.get('ostatok'))
        sk = Sklad.objects.get(pk=sklad_id)
        rashod_volume = sk.volume - new_cnt

        if rashod_volume > 0:
            Rashod.objects.create(tovar=sk.tovar, volume=rashod_volume, prim=prim, user=request.user)
        else:
            Prihod.objects.create(tovar=sk.tovar, cnt1=new_cnt - sk.volume, volume1=1, volume=new_cnt-sk.volume, dt=timezone.now(), prim=prim, user=request.user)

        return HttpResponseRedirect("/sklad/")

    sk = Sklad.objects.all()
    stols = Stol.objects.all()
    
    data= {}
    dt = datetime.datetime.now()
    pr = Prihod.objects.filter(dt__gte=datetime.datetime(dt.year,dt.month,dt.day))
    for d in pr:
        #print d
        if d.tovar.id in data.keys():
            data[d.tovar.id]["prihod"] += d.volume
        else:
            data[d.tovar.id]={"id":d.tovar.id, "name":d.tovar.name, "prihod":d.volume, "rashod":0,"sklad":Sklad.objects.get(tovar=d.tovar).volume}
        
    rs = Rashod.objects.filter(dt__gte=datetime.datetime(dt.year,dt.month,dt.day))
    for d in rs:
        if d.tovar.id in data.keys():
            data[d.tovar.id]["rashod"] += d.volume
        else:
            if Sklad.objects.filter(tovar=d.tovar).count()>0:
                data[d.tovar.id]={"id":d.tovar.id,"name":d.tovar.name,"rashod":d.volume,"prihod":0,"sklad":Sklad.objects.get(tovar=d.tovar).volume}
    
    balance = []
    for k,v in data.items():
        balance.append(v)
    #balance=list(balance)
    #print balance
    
    return render(request, template_name, locals())       

@login_required
def rashod(request,id=0,template_name="rashod.html"):
    if id!=0:
        tovar = Tovar.objects.get(pk=id)
        
        if request.method=="POST":
            if not isBuh(request): raise Http404()
            tovar   = request.POST.getlist("tovar[]")
            volume  = request.POST.getlist("volume[]")
            prim    = request.POST.getlist("prim[]")
            for i in range(0, len(tovar)):
                r = Rashod()
                r.tovar   = Tovar.objects.get(pk=tovar[i])
                r.volume  = volume[i]
                r.prim    = prim[i]
                r.user    = request.user
                r.dt      = timezone.now()
                r.save()
            
        rs = Rashod.objects.filter(tovar_id=id)
        pr = Prihod.objects.filter(tovar_id=id)        
        
    stols = Stol.objects.all()
    return render(request,template_name,locals())       
    
@login_required
def schet(request,status,template_name="schet.html"):
    #print "dtstart=",dtStart()
    dt = dtStart()
    dt1 = request.GET.get("dt1")
    
    if dt1==None:         
        dt1 = dtStart()
    else: 
        dt1 = dt1.split(".")
        dt1 = dtStart(datetime.datetime(int(dt1[2]),int(dt1[1]),int(dt1[0]),int(get_option("sstart")),0,0))
    dt2 = dtEnd(dt1)
    ss = Schet.objects.filter(dt_open__gte=dt1,dt_open__lte=dt2)
    #print dt1,dt2
    stols = Stol.objects.all()
    return render(request,template_name,locals())       

def mp3tag(fpath,name):
    from mutagen.easyid3 import EasyID3
    try:
        id3info = EasyID3(fpath)
        #print id3info.keys()
    except: return ""
    if name not in id3info.keys(): return ""
    try:
        return  unicode(id3info[name][0].encode('ISO-8859-1'),'cp1251')
    except:
        return id3info[name][0]   

@login_required
def music(request,template_name="music.html"):    
    for m in Music.objects.all():
        if not os.path.exists(m.fpath): m.delete()
        
    for p in Playlist.objects.all():
        num = 0
        for xm in Music.objects.filter(playlist=p).order_by("num"):
            num += 1
            xm.num = num
            xm.save()

    plist = Playlist.objects.all()
    for p in plist:
        if os.path.exists(p.path):            
            for d in os.listdir(p.path):
                if d[-4:].lower()!=".mp3": continue
                fpath = os.path.join(p.path,d)
                if Music.objects.filter(fpath=fpath).count()>0: continue
                
                title = mp3tag(fpath,'artist') +" - "+ mp3tag(fpath,'title')
                
                title = title.strip()
                if len(title)<4: title = d
                
                num = Music.objects.filter(playlist=p).aggregate(Max('num'))['num__max']
                if num==None: num=0
                
                m = Music()
                m.playlist = p
                m.title = title
                m.fpath = fpath
                m.num = num+1
                m.save()
    if request.GET.get("s")=="1": return render(request,"music_list.htm",locals())       
    return render(request,template_name,locals())       

@login_required
def musicUp(request,id):
    m = Music.objects.get(pk=id)

    mm = list(Music.objects.filter(playlist=m.playlist).order_by("num"))
    for i,xm in enumerate(mm):
        if xm.id==m.id:
            if i==0: break
            n = mm[i-1].num
            mm[i-1].num = m.num
            m.num = n
            
            mm[i-1].save()
            m.save()
            break

    return HttpResponse('ok')
    
@login_required
def musicDown(request,id):
    m = Music.objects.get(pk=id)
    
    mm = list(Music.objects.filter(playlist=m.playlist).order_by("num"))
    for i,xm in enumerate(mm):
        if xm.id==m.id:
            if i+1==len(mm): break
            n = mm[i+1].num
            mm[i+1].num = m.num
            m.num = n
            
            mm[i+1].save()
            m.save()
            break
    
    return HttpResponse('ok')  
    
def reports(request,template_name="reports.html"):
    return render(request,template_name,locals())       

def mToStr(m):
    m = int(m)
    d = [u"Нулябрь",u"Январь",u"Февраль",u"Март",u"Апрель",u"Май",u"Июнь",u"Июль",u"Август",u"Сентябрь",u"Октябрь",u"Ноябрь",u"Декабрь"]
    return d[m]

def monthStart(m,y):
    m = int(m)
    y = int(y)
    return datetime.datetime(y,m,1,0,0,0)
    
def monthEnd(m,y):
    m = int(m)
    y = int(y)    
    dt = datetime.datetime(y,m,1,0,0,0)
    dt += datetime.timedelta(days=31)
    dt = datetime.datetime(dt.year,dt.month,1)-datetime.timedelta(days=1)
    return dt 

def report_prihod_data(dt1,dt2,type=0):
    xtovar = {}
    for t in Tovar.objects.all():
        xtovar[t.id]=t.name
        
    cur = connection.cursor()
    cur.execute("select tovar_id,sum(cp.volume),sum(cp.price) from cafe_prihod cp, cafe_tovar ct where cp.tovar_id=ct.id and ct.menu_id=%s and dt>=%s and dt<=%s group by tovar_id",(type,dt1,dt2))
    pr = [{"name":xtovar[c[0]],"volume":c[1],"price":c[2]} for c in cur]
    pr.sort(key = lambda x: x["price"], reverse=True)
    cur.execute("select sum(price) from cafe_prihod cp, cafe_tovar ct where cp.tovar_id=ct.id and ct.menu_id=%s and dt>=%s and dt<=%s",(type,dt1,dt2))
    total = cur.fetchone()[0] or 0
    
    return pr,total

@login_required
def report_dates(request):
    dt1 = request.GET.get("dt1")
    dt2 = request.GET.get("dt2")
    if dt1=="": dt1=dtStart().strftime("%d.%m.%Y")
    if dt1==None:         
        dt1 = dtStart()
    else: 
        dt1 = dt1.split(".")
        dt1 = datetime.datetime(int(dt1[2]),int(dt1[1]),int(dt1[0]),int(get_option("sstart")),0,0)
        
    #dt1 = dtStart()
    if dt2==None or dt2=="" or dt2=="undefined":
        dt2 = dtEnd(dt1)
    else:
        dt2 = dt2.split(".")
        #dt2 = dtEnd(datetime.datetime(int(dt2[2]),int(dt2[1]),int(dt2[0]),int(get_option("sstart")),0,0))    
        dt2 = datetime.datetime(int(dt2[2]),int(dt2[1]),int(dt2[0]),6,0,0)
        # dt2 += datetime.timedelta(days=1)
    
    
    dnow = datetime.datetime.now()
    wdt1_2 = dnow
    wdt1_1 = dnow - datetime.timedelta(days=dnow.weekday())
    wdt2_2 = wdt1_1-datetime.timedelta(days=1)
    wdt2_1 = wdt1_1-datetime.timedelta(days=7)
    
    cur = connection.cursor()
    if settings.DATABASE=="sqlite3":
        q = "select distinct strftime('%%m',dt),strftime('%%Y',dt) from cafe_zakaz"
        cur.execute(q)
        monthes = [{"name":mToStr(c[0])+" "+str(int(c[1])),"dt1":monthStart(c[0],c[1]),"dt2":monthEnd(c[0],c[1])} for c in cur]
        
        q = "select distinct strftime('%%Y',dt) as x from cafe_zakaz order by x desc"
        cur.execute(q)
        years = [{"name":str(int(c[0])),"dt1":datetime.datetime(int(c[0]),1,1),"dt2":datetime.datetime(int(c[0]),12,31)} for c in cur]

    else:
        q = "select distinct date_part('month',dt),date_part('year',dt) from cafe_zakaz"
        cur.execute(q)
        monthes = [{"name":mToStr(c[0])+" "+str(int(c[1])),"dt1":monthStart(c[0],c[1]),"dt2":monthEnd(c[0],c[1])} for c in cur]
        
        q = "select distinct date_part('year',dt) as x from cafe_zakaz order by x desc"
        cur.execute(q)
        years = [{"name":str(int(c[0])),"dt1":datetime.datetime(int(c[0]),1,1),"dt2":datetime.datetime(int(c[0]),12,31)} for c in cur]
    
    return dt1,dt2,wdt1_2,wdt1_1,wdt2_2,wdt2_1,monthes,years

@login_required
def report_prihod(request,template_name="report_prihod.html"):
    
    dt1,dt2,wdt1_2,wdt1_1,wdt2_2,wdt2_1,monthes,years = report_dates(request)
    
    total = 0
    topmenus = Menu.objects.filter(pid=0, active=1).order_by('np')
    pr = []
    for menu in topmenus:
        pr, xtotal = report_prihod_data(dt1, dt2, menu.id)
        print(menu.name)
        pr.append({
            "name": menu.name,
            "pr": pr,
            "total": xtotal,
            })
        total += xtotal
    # pr1,total1 = report_prihod_data(dt1,dt2,1)
    # pr2,total2 = report_prihod_data(dt1,dt2,2)
    # pr3,total3 = report_prihod_data(dt1,dt2,3)
    # pr4,total4 = report_prihod_data(dt1,dt2,4)
    
    # pr=[
    #     {"name":u"Кухня","pr":pr1,"total":total1},
    #     {"name":u"Бар","pr":pr2,"total":total2},
    #     {"name":u"Кальянная","pr":pr3,"total":total3},
    #     {"name":u"Посуда и оборудование","pr":pr4,"total":total4},
    # ]
        
    # total = total1+total2+total3+total4
    
    return render(request,template_name,locals())       

def dohod(dt1,dt2):
    cur = connection.cursor()
    cur.execute("select sum(price_skidka) from cafe_schet where status=1 and dt_close>=%s and dt_close<=%s",(dt1,dt2))
    total = cur.fetchone()[0] or 0
    return total


def report_dohod_data(dt1, dt2, mtype=0):
    # print('report_dohod_data', mtype, dt1, dt2)
    cur = connection.cursor()
    xusluga = {}
    for t in Usluga.objects.all():
        xusluga[t.id]=t.name
        
    cur = connection.cursor()
    sql = "select usluga_id,sum(cp.cnt),sum(cp.price_skidka) from cafe_zakaz cp, cafe_usluga ct, cafe_schet cs where cp.usluga_id=ct.id and cp.menu_id=%s and cp.schet_id=cs.id and cs.status=1 and cp.dt>='%s' and cp.dt<='%s' group by usluga_id"%(mtype, dt1, dt2)
    cur.execute(sql)
    #print "select usluga_id,sum(cp.cnt),sum(cp.price_skidka) from cafe_zakaz cp, cafe_usluga ct where cp.usluga_id=ct.id and cp.menu_id=%s and cp.dt>=%s and cp.dt<=%s group by usluga_id"%(mtype,dt1,dt2)
    pr = [{"name": xusluga[c[0]], "volume": c[1], "price": c[2]} for c in cur]
    pr.sort(key=lambda x: x["volume"])
    # print(sql)
    # print('mtype=', mtype, 'pr=', pr, 'dt1=', dt1, 'dt2=', dt2)
    
    #return pr,total    
    cur.execute("select sum(cz.price_skidka) from cafe_zakaz cz, cafe_schet cs where cz.schet_id=cs.id and cs.status=1 and menu_id=%s and dt>=%s and dt<=%s",(mtype,dt1,dt2))
    total = cur.fetchone()[0] or 0
    
    vz_summ = Vozvrat.objects.filter(dt__gte=dt1, dt__lte=dt2, usluga__menu_id=mtype).aggregate(Sum("summ"))["summ__sum"] or 0 
    total -= vz_summ
    

    for d in Menu.objects.filter(pid=mtype):
        xpr, xtotal = report_dohod_data(dt1, dt2, d.id)
        pr += xpr
        total += xtotal
    return pr, total
    
@login_required
def report_dohod(request, template_name="report_dohod.html"):
    
    for d in Schet.objects.filter(status=0):
        d.save()
    
    dt1, dt2, wdt1_2, wdt1_1, wdt2_2, wdt2_1, monthes, years = report_dates(request)    
    print('report_dohod', dt1, dt2)


    total = 0
    topmenus = Menu.objects.filter(pid=0, active=1).order_by('np')
    permenu = []
    print('topmenus=', topmenus.count())
    for menu in topmenus:
        pr, xtotal = report_dohod_data(dt1, dt2, menu.id)
        print(menu.name, xtotal)
        permenu.append({
            "name": menu.name,
            "pr": pr,
            "total": xtotal,
            })
        total += xtotal
    # по официантам
    cur = connection.cursor()
    cur.execute("select user_open_id, sum(price_skidka) from cafe_schet where status=1 and dt_open>=%s and dt_open<=%s group by user_open_id",(dt1,dt2))
    ofs = [[c[0],c[1]] for c in cur]
    
    oficiants = []
    total_of = 0
    for d in ofs:
        oficiants.append({
            "user": User.objects.get(pk=d[0]),
            "total": d[1],
        })
        total_of += d[1]
    oficiants.sort(key=lambda x: x["total"], reverse=True)
    
    # доход - расход
    
    cur.execute("select sum(price) from cafe_prihod where dt>=%s and dt<=%s",(dt1, dt2))
    total_rashod = cur.fetchone()[0] or 0
    cur.execute("select sum(summ) from cafe_doprashod where dt>=%s and dt<%s",(dt1.date(), dt2.date()))
    dop_rashod = cur.fetchone()[0] or 0
    total_clear = total - total_rashod - dop_rashod
    
    nal_summ     = cash_summ(dt1, dt2, 0)
    card_summ    = cash_summ(dt1, dt2, 1)
    total_summ   = nal_summ + card_summ
    deposit_summ = total_summ - total_clear

    cdt = datetime.datetime.now()
    prev_dt = cdt - datetime.timedelta(days=1)

    return render(request, template_name, locals())        


def report_skidka_data(dt1,dt2):
    cur = connection.cursor()
    cur.execute("select skidka,count(*),sum(price)-sum(price_skidka) as x from cafe_schet where skidka!=0 and dt_open>=%s and dt_open<=%s group by skidka order by x desc",(dt1,dt2))
    skidka = [[c[0],c[1],c[2]] for c in cur]
    total = 0
    for d in skidka:
        total += d[2]
    return skidka,total
    
def report_skidka_data_full(dt1,dt2):
    data  = [s for s in Zakaz.objects.filter(dt__gte=dt1, dt__lte=dt2, schet__status=1).exclude(schet__skidka=0)]
    data += [s for s in Zakaz.objects.filter(dt__gte=dt1, dt__lte=dt2, schet__status=1).exclude(schet__skidka_fix=0)]
    total = 0
    for d in data:
        total += d.price
    return data, total  
    
## отчет по скидкам.
@login_required
def report_skidka(request,template_name="report_skidka.html"):        
    dt1,dt2,wdt1_2,wdt1_1,wdt2_2,wdt2_1,monthes,years = report_dates(request)    
    skidka,total = report_skidka_data(dt1,dt2)
    cdt = datetime.datetime.now()
    prev_dt = cdt - datetime.timedelta(days=1)

    return render(request,template_name,locals())        
    
    
    
@login_required
def printers(request,id=0,template_name="printers.html"):
    
    if id!=0:
        p = Printer.objects.get(pk=id)
        p.target = request.GET.get("t",0)
        p.save()
        return HttpResponseRedirect(reverse("printers"))
    f = open(os.path.join(settings.PRINT_DIR,"printers.txt"),"r")
    data = f.read()
    f.close()
    #data = subprocess.Popen([pr,"-l"], stdout = subprocess.PIPE,stderr = subprocess.STDOUT).stdout.read() 
    for d in data.split("\n"):
        if d=="": continue
        d = d.split(".")
        if len(d)<2: continue
        pid = d[0]
        del d[0]
        pname = ".".join(d)
        
        Printer.objects.get_or_create(name=pname,pid=pid)
    printers = Printer.objects.all()
    return render(request,template_name,locals())        
    
@login_required    
def report_doprashod(request,template_name="report_doprashod.html"):    
    dt1,dt2,wdt1_2,wdt1_1,wdt2_2,wdt2_1,monthes,years = report_dates(request) 
    
    if request.method=="POST":
        dt = request.POST.get("dt")
        dt = dt.split(".")
        dt = datetime.datetime(int(dt[2]),int(dt[1]),int(dt[0]))#,int(get_option("sstart")),0,0)
        
        user = User.objects.get(pk=request.POST.get("user"))
        cat = RashodCat.objects.get(pk=request.POST.get("cat"))
        summ = request.POST.get("summ")
        prim = request.POST.get("prim")
        DopRashod(dt=dt,user=user,cat=cat,summ=summ,prim=prim).save()
    
    dr = DopRashod.objects.filter(dt__gte=dt1.date(),dt__lt=dt2.date())
    total = 0
    for d in dr:
        total += d.summ
        
    users = User.objects.filter(is_active=True)
    cats = RashodCat.objects.all()
    return render(request,template_name,locals())

def lastMonthDay(year,month):
    xdt = datetime.datetime(year,month,1)+datetime.timedelta(days=31)
    return (datetime.datetime(xdt.year,xdt.month,1)-datetime.timedelta(days=1)).day
    

def maxDohodStol(dt1,dt2):
    cur = connection.cursor()
    cur.execute("select stol_id,sum(price_skidka) as x from cafe_schet where dt_open>=%s and dt_open<=%s group by stol_id order by x desc",(dt1,dt2))
    xdata = []
    
    for c in cur:
        xdata.append(c[0])
        if len(xdata)==3: break
    data = []
    for d in xdata:
        data.append(Stol.objects.get(pk=d))
        
    return data

def bestOfficiant(dt1,dt2):
    cur = connection.cursor()
    cur.execute("select user_open_id,sum(price_skidka) as x from cafe_schet where dt_open>=%s and dt_open<=%s group by user_open_id order by x desc",(dt1,dt2))
    xdata = []
    
    for c in cur:
        xdata.append(c[0])
        break
    data = []
    for d in xdata:
        data.append(User.objects.get(pk=d))
    if len(data)>0: return data[0].get_full_name()
    return ""


def maxUsluga(dt1,dt2):
    cur = connection.cursor()
    cur.execute("select usluga_id,count(*) as x from cafe_zakaz where dt>=%s and dt<=%s group by usluga_id order by x desc",(dt1,dt2))
    xdata = []
    
    for c in cur:
        xdata.append(c[0])
        if len(xdata)==5: break
        
    data = []
    for d in xdata:
        data.append(Usluga.objects.get(pk=d))
        
    return data

@login_required
def report_statistics(request,template_name="report_statistics.html"):
    if not isBuh(request) and not isAdmin(request): return HttpResponseRedirect("/")
    curdt = datetime.datetime.now()
    data = []
    for i in range(0, 6):                
        dt1 = datetime.datetime(curdt.year,curdt.month,1)
        dt2 = datetime.datetime(curdt.year,curdt.month,lastMonthDay(curdt.year,curdt.month))
        data.append({        
            "name":mToStr(curdt.month)+" "+str(curdt.year),
            "dohod":dohod(dt1,dt2),
            "avg_schet":Schet.objects.filter(dt_open__gte=dt1,dt_open__lte=dt2).exclude(price_skidka=0).aggregate(Avg('price_skidka'))['price_skidka__avg'],
            "max_stol":maxDohodStol(dt1,dt2),
            "max_usluga":maxUsluga(dt1,dt2),
            "best_officiant":bestOfficiant(dt1,dt2),
        })
        curdt -= datetime.timedelta(days=31)
    data.reverse()
    
    return render(request,template_name,locals())
    

def sendPrint(request):
    fname = ""
    for d in os.listdir(settings.PRINT_DIR):
        if d[-4:]==".htm":
            fname = d
            break
    response = HttpResponse(content_type='application/binary');
    if fname=="": 
        response.write(fname)
        return response
        
    fullname = os.path.join(settings.PRINT_DIR,fname)    
    f = open(fullname,"rb")
    response.write(f.read())
    f.close()
    response['Content-Disposition'] = 'attachment; filename='+fname
    
    shutil.move(fullname,os.path.join(settings.PRINT_DIR+"ed",fname))        
    return response

@login_required
def clearbase(request):
    if not request.user.is_superuser: return HttpResponse("Недостаточно прав")
    Schet.objects.all().delete()
    Prihod.objects.all().delete()
    Rashod.objects.all().delete()
    DopRashod.objects.all().delete()
    SchetHistory.objects.all().delete()
    Zakaz.objects.all().delete()
    ZakazHistory.objects.all().delete()
    Sklad.objects.all().delete()
    Smena.objects.all().delete()
    Smena(dt_open=datetime.datetime.now()).save()
    Bron.objects.all().delete()
    Vozvrat.objects.all().delete()
    Cash.objects.all().delete()
    Deposit.objects.all().delete()
    return HttpResponse("База очищена")

def recalcSmenaSum(dt1,dt2):
    vz_summ = Vozvrat.objects.filter(dt__gte=dt1,dt__lte=dt2).aggregate(Sum("summ"))["summ__sum"]
    if vz_summ==None: vz_summ=0
    psm = Schet.objects.filter(dt_open__gte=dt1,dt_open__lte=dt2).aggregate(Sum("price_skidka"))["price_skidka__sum"] 
    if psm==None: psm = 0
    return psm - vz_summ

def calcTovarPrihod(tovar,dt1,dt2):
    p = Prihod.objects.filter(tovar=tovar,dt__gte=dt1,dt__lte=dt2).aggregate(Sum("volume"))["volume__sum"]
    if p==None: return 0
    return float(p)/float(tovar.portion)
    
def calcTovarRashod(tovar,dt1,dt2):
    p = Rashod.objects.filter(tovar=tovar,dt__gte=dt1,dt__lte=dt2).aggregate(Sum("volume"))["volume__sum"]
    if p==None: return 0
    return float(p)/float(tovar.portion)

    
def calcCurrentVolume(tovar):
    sk = Sklad.objects.filter(tovar=tovar)
    if sk.count()==0: return 0
    p = sk[0].volume
    if p==None: return 0
    return float(p)/float(tovar.portion)
    
def getTovarSklad(dt_start,dt_end,type):
    data = []
    for t in Tovar.objects.filter(menu_id=type):
        p = calcTovarPrihod(t,dt_start,dt_end)
        r = calcTovarRashod(t,dt_start,dt_end)
        o = calcCurrentVolume(t)
        if p==0 and r==0 and o==0: continue
        data.append({
            "tovar":t,
            "prihod":p,
            "rashod":r,
            "ostatok":o,
        })
    return data
    
    
@login_required
def closeSmena(request,id=0,template_name="smena_close.html"):
    id = int(id)
    if Smena.objects.all().count()==0:
        last_sm = Smena()        
        last_sm.dt_open  = datetime.datetime.now()
        last_sm.dt_close = datetime.datetime.now()
        last_sm.save()
    else:
        if id!=0: 
            last_sm = Smena.objects.get(pk=id)
            current_sm = Smena.objects.get(pk=id)
        else:
            last_sm = Smena.objects.all().order_by("-id")[0]
    
    dt_end = datetime.datetime.now()    
    dt_start = last_sm.dt_open
    
    if request.method=="POST":
        sm = Smena()
        sm.dt_open    = last_sm.dt_close
        sm.dt_close   = datetime.datetime.now()
        sm.user_close = request.user
        sm.who        = request.POST.get("who")
        sm.prim       = request.POST.get("prim")
        sm.sum        = recalcSmenaSum(sm.dt_close,dt_end)
        sm.save()
        logout(request)
        return HttpResponseRedirect("/")    
        
    prev_sm = None # предыдущая смена
    for d in Smena.objects.all().order_by("id"):
        if d.id==last_sm.id: break
        prev_sm = d
    
    next_sm = None
    for d in Smena.objects.all().order_by("-id"):
        if d.id==last_sm.id: break
        next_sm = d
        
    if next_sm!=None: 
        dt_start = next_sm.dt_open
        dt_end = next_sm.dt_close    
    if dt_end==None:
        dt_end = datetime.datetime.now()
    
    dt_close = last_sm.dt_close
    if dt_close==None:
        dt_close = datetime.datetime.now() - datetime.timedelta(days=365)
    if dt_start == None:
        dt_start = datetime.datetime.now() - datetime.timedelta(days=365)
    
    sum = recalcSmenaSum(dt_start,dt_end)
    
    data1 = getTovarSklad(dt_start,dt_end,1)
    data2 = getTovarSklad(dt_start,dt_end,2)

        
    vz = Vozvrat.objects.filter(dt__gte=dt_start,dt__lte=dt_end)
    vz_summ = Vozvrat.objects.filter(dt__gte=dt_start,dt__lte=dt_end).aggregate(Sum("summ"))["summ__sum"]
    if request.GET.get("print")=="1":
        template_name="smena_print.html"
        
    pr1,total1 = report_dohod_data(dt_start,dt_end,1)
    pr2,total2 = report_dohod_data(dt_start,dt_end,2)
    
    skidka,total_skidka = report_skidka_data_full(dt_start,dt_end)
    pr1,total1 = report_dohod_data(dt_start,dt_end,1)
    pr2,total2 = report_dohod_data(dt_start,dt_end,2)
    return render(request,template_name,locals())
@login_required
def vozvrat(request,template_name="vozvrat.html"):
    if request.is_ajax():
        data = json.loads(request.POST.get("vozvrat"))
        for d in data:
            u   = Usluga.objects.get(pk=d["id"])
            cnt = d["cnt"]
            Vozvrat(user=request.user,usluga=u,cnt=cnt,summ=u.price*cnt).save()
        return HttpResponse("ok")
    menu   = Menu.objects.filter(pid=0)        
    stol = {"name":u"Возврат"}
    OFFICIANT_NAME  = get_option("OFFICIANT_NAME",u"Официант")    
    return render(request,template_name,locals())
    
@login_required
def checkPrim(request):
    sid = int(request.POST.get("prim_schet_id",0))
    if sid==0:
        data = request.read()
        sid  = requestParam(request,"prim_schet_id",data)
        prim = requestParam(request,"prim",data)
    else:        
        prim = request.POST.get("prim","")
    s = Schet.objects.get(pk=sid)
    s.prim = prim
    s.save()
    if request.is_ajax(): return HttpResponse('ok')
    return HttpResponseRedirect(reverse('zakaz',args=[s.stol.id]))

@login_required
def checkDeposit(request):
    sid  = int(request.POST.get("deposit_schet_id",0))
    summ = float(request.POST.get("deposit",0))
    card = int(request.POST.get("card",0))
    
    if sid==None:
        data = request.read()
        sid  = int(requestParam(request,"deposit_schet_id",data))
        summ = float(requestParam(request,"deposit",data,0))
        card = float(requestParam(request,"card",data,0))
    
    if sid!=0:
        s = Schet.objects.get(pk=sid)
    else:
        stol = Stol.objects.get(pk=request.POST.get("deposit_stol_id"))
        s = Schet(stol=stol,price=0,status=0,name="Счет 1",user_open=request.user)
        s.save()
    
    if summ>0:
        dp = Deposit()
        dp.schet = s
        dp.summ  = summ
        dp.user  = request.user
        dp.card  = card
        dp.save()
        
        Cash(schet=s,summ=summ,user=request.user,card=card,deposit=dp).save()
        
        is_card = u"картой"
        if dp.card==0:
            is_card = u"наличными"
        logSchet(request.user,s,11,u"добавлен депозит %d р. %s"%(summ,is_card))
        
    if request.is_ajax(): return HttpResponse('ok')
    return HttpResponseRedirect(reverse('zakaz',args=[s.stol.id]))

def apiAuth(request):
    uname = request.GET.get("uname")
    passwd = request.GET.get("passwd")
    user = authenticate(username=uname, password=passwd)
    if user==None: return HttpResponse("Неверное имя пользователя или пароль")
    return HttpResponse("")

def apiCheckUslugsContent(request):
    checkUslugsContent()
    return HttpResponse('ok')

def apiCheckSklad(request):
    ids = [d.id for d in Tovar.objects.all()]
    cur = connection.cursor()
    for d in ids:
        cur.execute("select sum(volume) from cafe_rashod where tovar_id=%d"%d)
        cnt1 = 0
        for c in cur:
            cnt1 = c[0]
        if cnt1==None: cnt1=0
        cur.execute("select sum(volume) from cafe_prihod where tovar_id=%d"%d)
        cnt2 = 0
        for c in cur:
            cnt2 = c[0]
        if cnt2==None: cnt2=0
        cur.execute("update cafe_sklad set volume=%d where tovar_id=%d"%(cnt2-cnt1,d))
    
    # проверка таблицы Cash
    # по счетам
    for s in Schet.objects.filter(status=1,card__in=[0,1]):
        if Cash.objects.filter(schet=s).count()==0:
            c = Cash(schet=s,summ=s.price_skidka,user=s.user_close)
            c.card = s.card
            c.save()
    # по депозитам
    for d in Deposit.objects.all():
        if Cash.objects.filter(deposit=d).count()>0: continue
        Cash(schet=d.schet,summ=d.summ,card=d.card,user=d.user,deposit=d).save()
    
    return HttpResponse('ok')
    
@login_required
def apiDemo(request):
    if get_option("DEMO")!="1": return HttpResponse("not DEMO")
    clearbase(request)
    # заполнение демо-данными
    dt_start = datetime.datetime(2015,3,1)
    dt = dt_start
    
    uslugs = list(Usluga.objects.filter(active=True).exclude(menu__pid=36))
    stols  = list(Stol.objects.filter(visible=True))
    
    while dt<=datetime.datetime.now():
        #print dt
        mins = 0
        check_count = random.randint(5,15)
        for i in range(0, check_count):
            user  = User.objects.get(pk=random.choice([19,13]))
            stol  = random.choice(stols)
            schet = Schet(stol=stol,price=0,status=0,name="test",user_open=user)            
            schet.save()
            schet.dt_open = dt + datetime.timedelta(minutes=mins)
            schet.num = str(schet.id)
            schet.save()
            logSchet(user,schet,1,u"счет #"+str(schet.num)+u" открыт")
            bcount = random.randint(1,7)
            for b in range(0, bcount):
                usluga = random.choice(uslugs)
                cnt = random.randint(1,3)
                ZakazHistory(schet=schet,status=1,usluga=usluga,cnt=cnt,price=usluga.price*cnt,user=user).save()
                logSchet(user,schet,2,u"добавлен заказ %s в количестве %d"%(usluga.name,cnt))
            
            
            schet.status = 1
            schet.user_close = user
            schet.dt_close = schet.dt_open+datetime.timedelta(minutes=random.randint(5,50))
            schet.save()
            schet.stol.recalc()
            
            for z in Zakaz.objects.filter(schet=schet):
                z.dt = schet.dt_open
                z.save()
            #logSchet(s.user_open,s,6,u"счет #%d закрыт и напечатан чек"%(s.id))
            logSchet(user,schet,6,u"счет #%s закрыт и напечатан чек"%(schet.num))
            mins += random.randint(10,30)
    
        dt += datetime.timedelta(days=1)
    
    for t in Tovar.objects.all():
        sum = Rashod.objects.filter(tovar=t).aggregate(Sum("volume"))["volume__sum"]
        if sum==None: continue
        sum += 100
        Prihod(tovar=t,volume=sum,cnt1=sum,volume1=1,price=3*sum,dt=datetime.datetime.now(),user_id=2).save()
    
    return HttpResponse('ok')
    
@login_required
def guests(request,template_name="guests.html"):
    
    if request.method=="POST":
        card = request.POST.get("card","")
        fio  = request.POST.get("fio","")
        prim = request.POST.get("prim","")
        
        if card!="" or fio!="" or prim!="":
            gs = Guest.objects.filter(card=card)
            if gs.count()==0:
                Guest(card=card,fio=fio,prim=prim).save()
                msg = u"Гость %s добавлен"%(fio)
                good = True
            else:
                msg = u"Введённый номер карты уже зарегистрирован на %s"%(gs[0].fio)
                bad = True
        
    data = Guest.objects.all()
    return render(request,template_name,locals())
    
@login_required
def guestEdit(request,id,template_name="guest_edit.html"):
    guest = Guest.objects.get(pk=id)
    
    if request.GET.get("rm")=="1":
        guest.delete()
        return HttpResponseRedirect(reverse("guests"))
    
    if request.method=="POST":
        guest.card = request.POST.get("card","")
        guest.fio  = request.POST.get("fio","")
        guest.prim = request.POST.get("prim","")
        guest.save()
        return HttpResponseRedirect(reverse("guests"))
    
    return render(request,template_name,locals())
    
@login_required
def editPf(request, id=0, template_name="pf_edit.html"):
    if not isBuh(request): raise Http404()
    stols = Stol.objects.all()
    if id!=0: 
        pf = Tovar.objects.get(pk=id)
        if request.GET.get("rm")=="1":
            pf.delete()
            return HttpResponseRedirect(reverse('menu'))
            
    if request.method=="POST":
        name  = request.POST.get("name")
        if id==0:
            pf = Tovar()
            pf.name   = name
            pf.volume = 0
            pf.is_pf  = True
            pf.save()
        
        tovars  = request.POST.getlist("tovar[]")
        volumes = request.POST.getlist("volume[]")
        if len(tovars)!=len(volumes): raise Http404()
        
        PfSostav.objects.filter(pf=pf).delete()
        
        for i in range(0, len(tovars)):
            tovar  = Tovar.objects.get(pk=tovars[i])
            volume = float(volumes[i].replace(",","."))
            PfSostav(pf=pf,tovar=tovar,volume=volume).save()
        return HttpResponseRedirect(reverse('menu'))
    tovar = Tovar.objects.all()
    
    return render(request,template_name,locals())
    
@login_required
def discounts(request, template_name="discounts.html"):
    
    if request.GET.get("card", "")!="":
        cards = [ {
                "id": d.id,
                "card_number": d.card_number,
                "fio": d.fio,
                "phone": d.phone,
                "balance": d.balance,
                "discount_value": d.discount_value,
                "prim": d.prim,
                "is_active": d.is_active,
            } for d in Discount.objects.filter(card_number__iexact=request.GET.get("card"))
        ]
        return JsonResponse({"status": "success", "cards": cards})

    objects = Discount.objects.all().order_by('-id')
    return render(request, template_name, locals())
