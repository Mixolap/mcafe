--
-- PostgreSQL database dump
--

SET statement_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;

--
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET search_path = public, pg_catalog;

--
-- Name: trRashod(); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION "trRashod"() RETURNS trigger
    LANGUAGE plpgsql
    AS $$declare 
uid integer;
user_id integer;
tid integer;
volume integer;
cnt integer;
uname varchar(255);
xcnt integer;
begin

uid = NEW.usluga_id;
user_id = NEW.user_id;
cnt =NEW.cnt;
select name into uname from cafe_usluga where id=NEW.usluga_id;
for tid,volume in execute 'select tovar_id,volume from cafe_sostav where usluga_id='||NEW.usluga_id
loop
	insert into cafe_rashod (usluga_id,user_id,tovar_id,volume,prim,dt) values (uid,user_id,tid,volume*cnt,uname||' - '||cnt,current_timestamp);

	select count(*) into xcnt from cafe_sklad where tovar_id=tid;
	if xcnt=0 then 
		insert into cafe_sklad (tovar_id,volume,dt) values (tid,cnt,current_timestamp);
	else
		update cafe_sklad set volume=cafe_sklad.volume-cnt,dt=current_timestamp where tovar_id=tid;	
	end if;
	
end loop;
return NEW;
end;$$;


ALTER FUNCTION public."trRashod"() OWNER TO postgres;

--
-- Name: FUNCTION "trRashod"(); Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON FUNCTION "trRashod"() IS 'Запись продуктов в расход';


SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: auth_group; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE auth_group (
    id integer NOT NULL,
    name character varying(80) NOT NULL
);


ALTER TABLE public.auth_group OWNER TO postgres;

--
-- Name: auth_group_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE auth_group_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.auth_group_id_seq OWNER TO postgres;

--
-- Name: auth_group_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE auth_group_id_seq OWNED BY auth_group.id;


--
-- Name: auth_group_permissions; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE auth_group_permissions (
    id integer NOT NULL,
    group_id integer NOT NULL,
    permission_id integer NOT NULL
);


ALTER TABLE public.auth_group_permissions OWNER TO postgres;

--
-- Name: auth_group_permissions_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE auth_group_permissions_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.auth_group_permissions_id_seq OWNER TO postgres;

--
-- Name: auth_group_permissions_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE auth_group_permissions_id_seq OWNED BY auth_group_permissions.id;


--
-- Name: auth_permission; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE auth_permission (
    id integer NOT NULL,
    name character varying(50) NOT NULL,
    content_type_id integer NOT NULL,
    codename character varying(100) NOT NULL
);


ALTER TABLE public.auth_permission OWNER TO postgres;

--
-- Name: auth_permission_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE auth_permission_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.auth_permission_id_seq OWNER TO postgres;

--
-- Name: auth_permission_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE auth_permission_id_seq OWNED BY auth_permission.id;


--
-- Name: auth_user; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE auth_user (
    id integer NOT NULL,
    password character varying(128) NOT NULL,
    last_login timestamp with time zone NOT NULL,
    is_superuser boolean NOT NULL,
    username character varying(30) NOT NULL,
    first_name character varying(30) NOT NULL,
    last_name character varying(30) NOT NULL,
    email character varying(75) NOT NULL,
    is_staff boolean NOT NULL,
    is_active boolean NOT NULL,
    date_joined timestamp with time zone NOT NULL
);


ALTER TABLE public.auth_user OWNER TO postgres;

--
-- Name: auth_user_groups; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE auth_user_groups (
    id integer NOT NULL,
    user_id integer NOT NULL,
    group_id integer NOT NULL
);


ALTER TABLE public.auth_user_groups OWNER TO postgres;

--
-- Name: auth_user_groups_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE auth_user_groups_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.auth_user_groups_id_seq OWNER TO postgres;

--
-- Name: auth_user_groups_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE auth_user_groups_id_seq OWNED BY auth_user_groups.id;


--
-- Name: auth_user_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE auth_user_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.auth_user_id_seq OWNER TO postgres;

--
-- Name: auth_user_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE auth_user_id_seq OWNED BY auth_user.id;


--
-- Name: auth_user_user_permissions; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE auth_user_user_permissions (
    id integer NOT NULL,
    user_id integer NOT NULL,
    permission_id integer NOT NULL
);


ALTER TABLE public.auth_user_user_permissions OWNER TO postgres;

--
-- Name: auth_user_user_permissions_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE auth_user_user_permissions_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.auth_user_user_permissions_id_seq OWNER TO postgres;

--
-- Name: auth_user_user_permissions_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE auth_user_user_permissions_id_seq OWNED BY auth_user_user_permissions.id;


--
-- Name: cafe_bron; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE cafe_bron (
    id integer NOT NULL,
    stol_id integer NOT NULL,
    prim character varying(255),
    dt timestamp with time zone NOT NULL
);


ALTER TABLE public.cafe_bron OWNER TO postgres;

--
-- Name: cafe_bron_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE cafe_bron_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.cafe_bron_id_seq OWNER TO postgres;

--
-- Name: cafe_bron_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE cafe_bron_id_seq OWNED BY cafe_bron.id;


--
-- Name: cafe_deposit; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE cafe_deposit (
    id integer NOT NULL,
    schet_id integer NOT NULL,
    summ double precision NOT NULL,
    card integer NOT NULL,
    dt timestamp with time zone NOT NULL,
    user_id integer NOT NULL
);


ALTER TABLE public.cafe_deposit OWNER TO postgres;

--
-- Name: cafe_deposit_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE cafe_deposit_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.cafe_deposit_id_seq OWNER TO postgres;

--
-- Name: cafe_deposit_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE cafe_deposit_id_seq OWNED BY cafe_deposit.id;


--
-- Name: cafe_doprashod; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE cafe_doprashod (
    id integer NOT NULL,
    dt date NOT NULL,
    cat_id integer NOT NULL,
    summ double precision NOT NULL,
    user_id integer,
    prim text
);


ALTER TABLE public.cafe_doprashod OWNER TO postgres;

--
-- Name: cafe_doprashod_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE cafe_doprashod_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.cafe_doprashod_id_seq OWNER TO postgres;

--
-- Name: cafe_doprashod_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE cafe_doprashod_id_seq OWNED BY cafe_doprashod.id;


--
-- Name: cafe_menu; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE cafe_menu (
    id integer NOT NULL,
    name character varying(255) NOT NULL,
    pid integer NOT NULL,
    np integer NOT NULL,
    dt timestamp with time zone NOT NULL,
    active integer NOT NULL
);


ALTER TABLE public.cafe_menu OWNER TO postgres;

--
-- Name: cafe_menu_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE cafe_menu_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.cafe_menu_id_seq OWNER TO postgres;

--
-- Name: cafe_menu_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE cafe_menu_id_seq OWNED BY cafe_menu.id;


--
-- Name: cafe_music; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE cafe_music (
    id integer NOT NULL,
    title character varying(255) NOT NULL,
    playlist_id integer NOT NULL,
    num integer NOT NULL,
    fpath character varying(255) NOT NULL
);


ALTER TABLE public.cafe_music OWNER TO postgres;

--
-- Name: cafe_music_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE cafe_music_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.cafe_music_id_seq OWNER TO postgres;

--
-- Name: cafe_music_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE cafe_music_id_seq OWNED BY cafe_music.id;


--
-- Name: cafe_option; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE cafe_option (
    id integer NOT NULL,
    name character varying(255) NOT NULL,
    code character varying(255) NOT NULL,
    value text NOT NULL
);


ALTER TABLE public.cafe_option OWNER TO postgres;

--
-- Name: cafe_option_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE cafe_option_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.cafe_option_id_seq OWNER TO postgres;

--
-- Name: cafe_option_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE cafe_option_id_seq OWNED BY cafe_option.id;


--
-- Name: cafe_playlist; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE cafe_playlist (
    id integer NOT NULL,
    name character varying(255) NOT NULL,
    path character varying(255) NOT NULL
);


ALTER TABLE public.cafe_playlist OWNER TO postgres;

--
-- Name: cafe_playlist_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE cafe_playlist_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.cafe_playlist_id_seq OWNER TO postgres;

--
-- Name: cafe_playlist_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE cafe_playlist_id_seq OWNED BY cafe_playlist.id;


--
-- Name: cafe_prihod; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE cafe_prihod (
    id integer NOT NULL,
    tovar_id integer NOT NULL,
    volume double precision NOT NULL,
    cnt1 double precision NOT NULL,
    volume1 integer NOT NULL,
    dt timestamp with time zone NOT NULL,
    price double precision NOT NULL,
    prim text,
    user_id integer NOT NULL
);


ALTER TABLE public.cafe_prihod OWNER TO postgres;

--
-- Name: cafe_prihod_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE cafe_prihod_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.cafe_prihod_id_seq OWNER TO postgres;

--
-- Name: cafe_prihod_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE cafe_prihod_id_seq OWNED BY cafe_prihod.id;


--
-- Name: cafe_printer; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE cafe_printer (
    id integer NOT NULL,
    name character varying(255) NOT NULL,
    pid integer NOT NULL,
    target integer NOT NULL
);


ALTER TABLE public.cafe_printer OWNER TO postgres;

--
-- Name: cafe_printer_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE cafe_printer_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.cafe_printer_id_seq OWNER TO postgres;

--
-- Name: cafe_printer_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE cafe_printer_id_seq OWNED BY cafe_printer.id;


--
-- Name: cafe_rashod; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE cafe_rashod (
    id integer NOT NULL,
    tovar_id integer NOT NULL,
    volume double precision NOT NULL,
    dt timestamp with time zone NOT NULL,
    usluga_id integer,
    prim text,
    user_id integer NOT NULL
);


ALTER TABLE public.cafe_rashod OWNER TO postgres;

--
-- Name: cafe_rashod_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE cafe_rashod_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.cafe_rashod_id_seq OWNER TO postgres;

--
-- Name: cafe_rashod_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE cafe_rashod_id_seq OWNED BY cafe_rashod.id;


--
-- Name: cafe_rashodcat; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE cafe_rashodcat (
    id integer NOT NULL,
    name character varying(255) NOT NULL
);


ALTER TABLE public.cafe_rashodcat OWNER TO postgres;

--
-- Name: cafe_rashodcat_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE cafe_rashodcat_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.cafe_rashodcat_id_seq OWNER TO postgres;

--
-- Name: cafe_rashodcat_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE cafe_rashodcat_id_seq OWNED BY cafe_rashodcat.id;


--
-- Name: cafe_schet; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE cafe_schet (
    id integer NOT NULL,
    stol_id integer NOT NULL,
    status integer NOT NULL,
    price double precision NOT NULL,
    name character varying(255) NOT NULL,
    dt_open timestamp with time zone NOT NULL,
    dt_close timestamp with time zone,
    user_open_id integer NOT NULL,
    user_close_id integer,
    skidka integer NOT NULL,
    price_skidka double precision NOT NULL,
    card integer NOT NULL,
    precheck integer DEFAULT 0,
    prim character varying(255),
    skidka_fix double precision DEFAULT 0,
    num integer DEFAULT 0
);


ALTER TABLE public.cafe_schet OWNER TO postgres;

--
-- Name: cafe_schet_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE cafe_schet_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.cafe_schet_id_seq OWNER TO postgres;

--
-- Name: cafe_schet_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE cafe_schet_id_seq OWNED BY cafe_schet.id;


--
-- Name: cafe_schethistory; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE cafe_schethistory (
    id integer NOT NULL,
    user_id integer NOT NULL,
    schet_id integer NOT NULL,
    dt timestamp with time zone NOT NULL,
    operation integer NOT NULL,
    text character varying(255) NOT NULL
);


ALTER TABLE public.cafe_schethistory OWNER TO postgres;

--
-- Name: cafe_schethistory_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE cafe_schethistory_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.cafe_schethistory_id_seq OWNER TO postgres;

--
-- Name: cafe_schethistory_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE cafe_schethistory_id_seq OWNED BY cafe_schethistory.id;


--
-- Name: cafe_skidka; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE cafe_skidka (
    id integer NOT NULL,
    value integer NOT NULL
);


ALTER TABLE public.cafe_skidka OWNER TO postgres;

--
-- Name: cafe_skidka_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE cafe_skidka_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.cafe_skidka_id_seq OWNER TO postgres;

--
-- Name: cafe_skidka_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE cafe_skidka_id_seq OWNED BY cafe_skidka.id;


--
-- Name: cafe_sklad; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE cafe_sklad (
    id integer NOT NULL,
    tovar_id integer NOT NULL,
    volume double precision NOT NULL,
    dt timestamp with time zone NOT NULL
);


ALTER TABLE public.cafe_sklad OWNER TO postgres;

--
-- Name: cafe_sklad_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE cafe_sklad_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.cafe_sklad_id_seq OWNER TO postgres;

--
-- Name: cafe_sklad_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE cafe_sklad_id_seq OWNED BY cafe_sklad.id;


--
-- Name: cafe_smena; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE cafe_smena (
    id integer NOT NULL,
    dt_open timestamp with time zone,
    dt_close timestamp with time zone,
    who text,
    prim text,
    user_close_id integer,
    sum double precision
);


ALTER TABLE public.cafe_smena OWNER TO postgres;

--
-- Name: cafe_smena_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE cafe_smena_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.cafe_smena_id_seq OWNER TO postgres;

--
-- Name: cafe_smena_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE cafe_smena_id_seq OWNED BY cafe_smena.id;


--
-- Name: cafe_sostav; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE cafe_sostav (
    id integer NOT NULL,
    usluga_id integer NOT NULL,
    tovar_id integer NOT NULL,
    volume double precision NOT NULL
);


ALTER TABLE public.cafe_sostav OWNER TO postgres;

--
-- Name: cafe_sostav_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE cafe_sostav_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.cafe_sostav_id_seq OWNER TO postgres;

--
-- Name: cafe_sostav_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE cafe_sostav_id_seq OWNED BY cafe_sostav.id;


--
-- Name: cafe_stol; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE cafe_stol (
    id integer NOT NULL,
    name character varying(255) NOT NULL,
    status integer NOT NULL,
    dt timestamp with time zone NOT NULL,
    visible integer NOT NULL
);


ALTER TABLE public.cafe_stol OWNER TO postgres;

--
-- Name: cafe_stol_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE cafe_stol_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.cafe_stol_id_seq OWNER TO postgres;

--
-- Name: cafe_stol_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE cafe_stol_id_seq OWNED BY cafe_stol.id;


--
-- Name: cafe_tovar; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE cafe_tovar (
    id integer NOT NULL,
    name character varying(255) NOT NULL,
    volume integer NOT NULL,
    mvolume integer NOT NULL,
    menu_id integer,
    portion integer DEFAULT 1
);


ALTER TABLE public.cafe_tovar OWNER TO postgres;

--
-- Name: cafe_tovar_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE cafe_tovar_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.cafe_tovar_id_seq OWNER TO postgres;

--
-- Name: cafe_tovar_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE cafe_tovar_id_seq OWNED BY cafe_tovar.id;


--
-- Name: cafe_usluga; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE cafe_usluga (
    id integer NOT NULL,
    name character varying(255) NOT NULL,
    description text,
    menu_id integer NOT NULL,
    dt timestamp with time zone NOT NULL,
    price double precision NOT NULL,
    np integer NOT NULL,
    active integer NOT NULL,
    check_name character varying(255)
);


ALTER TABLE public.cafe_usluga OWNER TO postgres;

--
-- Name: cafe_usluga_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE cafe_usluga_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.cafe_usluga_id_seq OWNER TO postgres;

--
-- Name: cafe_usluga_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE cafe_usluga_id_seq OWNED BY cafe_usluga.id;


--
-- Name: cafe_vozvrat; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE cafe_vozvrat (
    id integer NOT NULL,
    user_id integer NOT NULL,
    usluga_id integer NOT NULL,
    cnt integer NOT NULL,
    dt timestamp with time zone NOT NULL,
    summ double precision DEFAULT 0
);


ALTER TABLE public.cafe_vozvrat OWNER TO postgres;

--
-- Name: cafe_vozvrat_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE cafe_vozvrat_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.cafe_vozvrat_id_seq OWNER TO postgres;

--
-- Name: cafe_vozvrat_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE cafe_vozvrat_id_seq OWNED BY cafe_vozvrat.id;


--
-- Name: cafe_zakaz; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE cafe_zakaz (
    id integer NOT NULL,
    schet_id integer NOT NULL,
    status integer NOT NULL,
    menu_id integer NOT NULL,
    usluga_id integer NOT NULL,
    cnt integer NOT NULL,
    price double precision NOT NULL,
    price_skidka double precision NOT NULL,
    prim text,
    dt timestamp with time zone NOT NULL
);


ALTER TABLE public.cafe_zakaz OWNER TO postgres;

--
-- Name: cafe_zakaz_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE cafe_zakaz_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.cafe_zakaz_id_seq OWNER TO postgres;

--
-- Name: cafe_zakaz_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE cafe_zakaz_id_seq OWNED BY cafe_zakaz.id;


--
-- Name: cafe_zakazhistory; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE cafe_zakazhistory (
    id integer NOT NULL,
    schet_id integer NOT NULL,
    zakaz_id integer NOT NULL,
    status integer NOT NULL,
    menu_id integer NOT NULL,
    usluga_id integer NOT NULL,
    cnt integer NOT NULL,
    price double precision NOT NULL,
    prim text,
    dt timestamp with time zone NOT NULL,
    user_id integer NOT NULL
);


ALTER TABLE public.cafe_zakazhistory OWNER TO postgres;

--
-- Name: cafe_zakazhistory_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE cafe_zakazhistory_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.cafe_zakazhistory_id_seq OWNER TO postgres;

--
-- Name: cafe_zakazhistory_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE cafe_zakazhistory_id_seq OWNED BY cafe_zakazhistory.id;


--
-- Name: django_admin_log; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE django_admin_log (
    id integer NOT NULL,
    action_time timestamp with time zone NOT NULL,
    user_id integer NOT NULL,
    content_type_id integer,
    object_id text,
    object_repr character varying(200) NOT NULL,
    action_flag smallint NOT NULL,
    change_message text NOT NULL,
    CONSTRAINT django_admin_log_action_flag_check CHECK ((action_flag >= 0))
);


ALTER TABLE public.django_admin_log OWNER TO postgres;

--
-- Name: django_admin_log_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE django_admin_log_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.django_admin_log_id_seq OWNER TO postgres;

--
-- Name: django_admin_log_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE django_admin_log_id_seq OWNED BY django_admin_log.id;


--
-- Name: django_content_type; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE django_content_type (
    id integer NOT NULL,
    name character varying(100) NOT NULL,
    app_label character varying(100) NOT NULL,
    model character varying(100) NOT NULL
);


ALTER TABLE public.django_content_type OWNER TO postgres;

--
-- Name: django_content_type_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE django_content_type_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.django_content_type_id_seq OWNER TO postgres;

--
-- Name: django_content_type_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE django_content_type_id_seq OWNED BY django_content_type.id;


--
-- Name: django_migrations; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE django_migrations (
    id integer NOT NULL,
    app character varying(255) NOT NULL,
    name character varying(255) NOT NULL,
    applied timestamp with time zone NOT NULL
);


ALTER TABLE public.django_migrations OWNER TO postgres;

--
-- Name: django_migrations_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE django_migrations_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.django_migrations_id_seq OWNER TO postgres;

--
-- Name: django_migrations_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE django_migrations_id_seq OWNED BY django_migrations.id;


--
-- Name: django_session; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE django_session (
    session_key character varying(40) NOT NULL,
    session_data text NOT NULL,
    expire_date timestamp with time zone NOT NULL
);


ALTER TABLE public.django_session OWNER TO postgres;

--
-- Name: django_site; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE django_site (
    id integer NOT NULL,
    domain character varying(100) NOT NULL,
    name character varying(50) NOT NULL
);


ALTER TABLE public.django_site OWNER TO postgres;

--
-- Name: django_site_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE django_site_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.django_site_id_seq OWNER TO postgres;

--
-- Name: django_site_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE django_site_id_seq OWNED BY django_site.id;


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY auth_group ALTER COLUMN id SET DEFAULT nextval('auth_group_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY auth_group_permissions ALTER COLUMN id SET DEFAULT nextval('auth_group_permissions_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY auth_permission ALTER COLUMN id SET DEFAULT nextval('auth_permission_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY auth_user ALTER COLUMN id SET DEFAULT nextval('auth_user_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY auth_user_groups ALTER COLUMN id SET DEFAULT nextval('auth_user_groups_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY auth_user_user_permissions ALTER COLUMN id SET DEFAULT nextval('auth_user_user_permissions_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY cafe_bron ALTER COLUMN id SET DEFAULT nextval('cafe_bron_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY cafe_deposit ALTER COLUMN id SET DEFAULT nextval('cafe_deposit_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY cafe_doprashod ALTER COLUMN id SET DEFAULT nextval('cafe_doprashod_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY cafe_menu ALTER COLUMN id SET DEFAULT nextval('cafe_menu_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY cafe_music ALTER COLUMN id SET DEFAULT nextval('cafe_music_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY cafe_option ALTER COLUMN id SET DEFAULT nextval('cafe_option_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY cafe_playlist ALTER COLUMN id SET DEFAULT nextval('cafe_playlist_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY cafe_prihod ALTER COLUMN id SET DEFAULT nextval('cafe_prihod_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY cafe_printer ALTER COLUMN id SET DEFAULT nextval('cafe_printer_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY cafe_rashod ALTER COLUMN id SET DEFAULT nextval('cafe_rashod_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY cafe_rashodcat ALTER COLUMN id SET DEFAULT nextval('cafe_rashodcat_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY cafe_schet ALTER COLUMN id SET DEFAULT nextval('cafe_schet_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY cafe_schethistory ALTER COLUMN id SET DEFAULT nextval('cafe_schethistory_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY cafe_skidka ALTER COLUMN id SET DEFAULT nextval('cafe_skidka_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY cafe_sklad ALTER COLUMN id SET DEFAULT nextval('cafe_sklad_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY cafe_smena ALTER COLUMN id SET DEFAULT nextval('cafe_smena_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY cafe_sostav ALTER COLUMN id SET DEFAULT nextval('cafe_sostav_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY cafe_stol ALTER COLUMN id SET DEFAULT nextval('cafe_stol_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY cafe_tovar ALTER COLUMN id SET DEFAULT nextval('cafe_tovar_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY cafe_usluga ALTER COLUMN id SET DEFAULT nextval('cafe_usluga_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY cafe_vozvrat ALTER COLUMN id SET DEFAULT nextval('cafe_vozvrat_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY cafe_zakaz ALTER COLUMN id SET DEFAULT nextval('cafe_zakaz_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY cafe_zakazhistory ALTER COLUMN id SET DEFAULT nextval('cafe_zakazhistory_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY django_admin_log ALTER COLUMN id SET DEFAULT nextval('django_admin_log_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY django_content_type ALTER COLUMN id SET DEFAULT nextval('django_content_type_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY django_migrations ALTER COLUMN id SET DEFAULT nextval('django_migrations_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY django_site ALTER COLUMN id SET DEFAULT nextval('django_site_id_seq'::regclass);


--
-- Data for Name: auth_group; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY auth_group (id, name) FROM stdin;
1	Бухгалтер
2	Администратор
3	Официант
\.


--
-- Name: auth_group_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('auth_group_id_seq', 3, true);


--
-- Data for Name: auth_group_permissions; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY auth_group_permissions (id, group_id, permission_id) FROM stdin;
\.


--
-- Name: auth_group_permissions_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('auth_group_permissions_id_seq', 1, false);


--
-- Data for Name: auth_permission; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY auth_permission (id, name, content_type_id, codename) FROM stdin;
1	Can add permission	1	add_permission
2	Can change permission	1	change_permission
3	Can delete permission	1	delete_permission
4	Can add group	2	add_group
5	Can change group	2	change_group
6	Can delete group	2	delete_group
7	Can add user	3	add_user
8	Can change user	3	change_user
9	Can delete user	3	delete_user
10	Can add content type	4	add_contenttype
11	Can change content type	4	change_contenttype
12	Can delete content type	4	delete_contenttype
13	Can add session	5	add_session
14	Can change session	5	change_session
15	Can delete session	5	delete_session
16	Can add site	6	add_site
17	Can change site	6	change_site
18	Can delete site	6	delete_site
19	Can add log entry	7	add_logentry
20	Can change log entry	7	change_logentry
21	Can delete log entry	7	delete_logentry
22	Can add stol	8	add_stol
23	Can change stol	8	change_stol
24	Can delete stol	8	delete_stol
25	Can add bron	9	add_bron
26	Can change bron	9	change_bron
27	Can delete bron	9	delete_bron
28	Can add menu	10	add_menu
29	Can change menu	10	change_menu
30	Can delete menu	10	delete_menu
31	Can add usluga	11	add_usluga
32	Can change usluga	11	change_usluga
33	Can delete usluga	11	delete_usluga
34	Can add schet	12	add_schet
35	Can change schet	12	change_schet
36	Can delete schet	12	delete_schet
37	Can add schet history	13	add_schethistory
38	Can change schet history	13	change_schethistory
39	Can delete schet history	13	delete_schethistory
40	Can add zakaz	14	add_zakaz
41	Can change zakaz	14	change_zakaz
42	Can delete zakaz	14	delete_zakaz
43	Can add zakaz history	15	add_zakazhistory
44	Can change zakaz history	15	change_zakazhistory
45	Can delete zakaz history	15	delete_zakazhistory
46	Can add tovar	16	add_tovar
47	Can change tovar	16	change_tovar
48	Can delete tovar	16	delete_tovar
49	Can add prihod	17	add_prihod
50	Can change prihod	17	change_prihod
51	Can delete prihod	17	delete_prihod
52	Can add rashod	18	add_rashod
53	Can change rashod	18	change_rashod
54	Can delete rashod	18	delete_rashod
55	Can add sklad	19	add_sklad
56	Can change sklad	19	change_sklad
57	Can delete sklad	19	delete_sklad
58	Can add sostav	20	add_sostav
59	Can change sostav	20	change_sostav
60	Can delete sostav	20	delete_sostav
61	Can add skidka	21	add_skidka
62	Can change skidka	21	change_skidka
63	Can delete skidka	21	delete_skidka
64	Can add playlist	22	add_playlist
65	Can change playlist	22	change_playlist
66	Can delete playlist	22	delete_playlist
67	Can add music	23	add_music
68	Can change music	23	change_music
69	Can delete music	23	delete_music
70	Can add option	24	add_option
71	Can change option	24	change_option
72	Can delete option	24	delete_option
73	Can add printer	25	add_printer
74	Can change printer	25	change_printer
75	Can delete printer	25	delete_printer
76	Can add rashod cat	26	add_rashodcat
77	Can change rashod cat	26	change_rashodcat
78	Can delete rashod cat	26	delete_rashodcat
79	Can add dop rashod	27	add_doprashod
80	Can change dop rashod	27	change_doprashod
81	Can delete dop rashod	27	delete_doprashod
82	Can add smena	28	add_smena
83	Can change smena	28	change_smena
84	Can delete smena	28	delete_smena
85	Can add vozvrat	29	add_vozvrat
86	Can change vozvrat	29	change_vozvrat
87	Can delete vozvrat	29	delete_vozvrat
\.


--
-- Name: auth_permission_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('auth_permission_id_seq', 87, true);


--
-- Data for Name: auth_user; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY auth_user (id, password, last_login, is_superuser, username, first_name, last_name, email, is_staff, is_active, date_joined) FROM stdin;
5	pbkdf2_sha256$20000$Zx5Uf95UGKtj$ieqmUC6Mn9+pVgqqdETutjeLtYqUUoPQpkczgmoZLns=	2015-06-27 12:42:45.423245+03	f	kassir1	Кассир 1			f	t	2014-07-19 23:27:25+03
3	pbkdf2_sha256$20000$V0toXgMH6rTl$Y92nqVvK8QbYz5BCvti91yWndogeibz8k2n+c9cMQjo=	2014-09-16 19:16:26+03	f	kassir2	Кассир 2			f	t	2014-07-19 23:27:08+03
4	pbkdf2_sha256$20000$fiHam32Ulw9R$6hltslPm8sjOBWNL5qz/+j3IgI89kpBmrFr2GcMOtVo=	2014-12-15 22:56:04+03	f	kassir3	Кассир 3			f	t	2014-07-19 23:27:18+03
7	pbkdf2_sha256$12000$rTX9zvHpT18b$TU/MRrRuQNUBq+MISMecW2FruezPX8ZKIaLNI6HmPJg=	2015-03-02 21:20:37+03	t	admin	Администратор			t	f	2015-03-01 20:54:44+03
1	pbkdf2_sha256$20000$J6BiLIAPToJx$tAGabSc+UoUpdT6HOHG9m9BS+6ee+L2olbVbOc4JRa4=	2015-06-27 13:08:48+03	t	caffein				t	t	2014-07-19 23:25:46+03
\.


--
-- Data for Name: auth_user_groups; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY auth_user_groups (id, user_id, group_id) FROM stdin;
19	5	3
20	3	3
21	4	3
32	7	1
33	7	2
34	7	3
35	1	1
36	1	2
37	1	3
\.


--
-- Name: auth_user_groups_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('auth_user_groups_id_seq', 37, true);


--
-- Name: auth_user_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('auth_user_id_seq', 7, true);


--
-- Data for Name: auth_user_user_permissions; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY auth_user_user_permissions (id, user_id, permission_id) FROM stdin;
82	7	1
83	7	2
84	7	3
85	7	4
86	7	5
87	7	6
88	7	7
89	7	8
90	7	9
91	7	10
92	7	11
93	7	12
94	7	13
95	7	14
96	7	15
97	7	16
98	7	17
99	7	18
100	7	19
101	7	20
102	7	21
103	7	22
104	7	23
105	7	24
106	7	25
107	7	26
108	7	27
109	7	28
110	7	29
111	7	30
112	7	31
113	7	32
114	7	33
115	7	34
116	7	35
117	7	36
118	7	37
119	7	38
120	7	39
121	7	40
122	7	41
123	7	42
124	7	43
125	7	44
126	7	45
127	7	46
128	7	47
129	7	48
130	7	49
131	7	50
132	7	51
133	7	52
134	7	53
135	7	54
136	7	55
137	7	56
138	7	57
139	7	58
140	7	59
141	7	60
142	7	61
143	7	62
144	7	63
145	7	64
146	7	65
147	7	66
148	7	67
149	7	68
150	7	69
151	7	70
152	7	71
153	7	72
154	7	73
155	7	74
156	7	75
157	7	76
158	7	77
159	7	78
160	7	79
161	7	80
162	7	81
\.


--
-- Name: auth_user_user_permissions_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('auth_user_user_permissions_id_seq', 162, true);


--
-- Data for Name: cafe_bron; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY cafe_bron (id, stol_id, prim, dt) FROM stdin;
406	1		2014-12-13 16:00:00+03
407	1		2014-12-13 09:30:00+03
408	1		2014-12-13 19:00:00+03
409	1		2014-12-13 21:00:00+03
\.


--
-- Name: cafe_bron_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('cafe_bron_id_seq', 425, true);


--
-- Data for Name: cafe_deposit; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY cafe_deposit (id, schet_id, summ, card, dt, user_id) FROM stdin;
\.


--
-- Name: cafe_deposit_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('cafe_deposit_id_seq', 1, false);


--
-- Data for Name: cafe_doprashod; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY cafe_doprashod (id, dt, cat_id, summ, user_id, prim) FROM stdin;
4	2014-05-07	1	80000	\N	\N
\.


--
-- Name: cafe_doprashod_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('cafe_doprashod_id_seq', 4, true);


--
-- Data for Name: cafe_menu; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY cafe_menu (id, name, pid, np, dt, active) FROM stdin;
1	Кухня	0	1	2014-03-17 11:51:13.547+03	1
2	Бар	0	2	2014-03-17 11:51:29.596+03	1
49	первые блюда	1	100	2015-03-01 22:04:18.871353+03	1
51	вторые блюда	1	100	2015-03-01 22:13:30.792663+03	1
53	гарниры	1	100	2015-06-29 16:21:56.301221+03	1
54	салаты	1	100	2015-06-29 16:23:33.202601+03	1
55	пиво	2	100	2015-06-29 16:33:18.292652+03	1
56	кола	2	100	2015-06-29 16:42:13.58683+03	1
58	вода	2	100	2015-06-29 16:48:05.344129+03	1
59	шоколад	2	100	2015-06-29 16:56:12.182279+03	1
60	выпечка	2	100	2015-06-29 16:59:35.352281+03	1
61	прочее	2	100	2015-06-29 17:08:52.068857+03	1
\.


--
-- Name: cafe_menu_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('cafe_menu_id_seq', 61, true);


--
-- Data for Name: cafe_music; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY cafe_music (id, title, playlist_id, num, fpath) FROM stdin;
\.


--
-- Name: cafe_music_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('cafe_music_id_seq', 1, false);


--
-- Data for Name: cafe_option; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY cafe_option (id, name, code, value) FROM stdin;
4	Адрес заведения	addr	г. Воронеж, ул. Хользунова, 13
5	Телефон	PHONE	8 (919) 185-45-55
6	ОГРН	OGRN	313366833900197
7	ИНН	INN	362503050907
8	Наименование обслуживающего персонала	OFFICIANT_NAME	Кассир
1	Начало рабочей смены	sstart	9
9	Оплата картой	HAVE_CARD	0
3	Наименование заведения	name	Кафе 05
10	Бронирование	HAVE_BRON	0
11	Наличие пречека	HAVE_PRECHEK	0
12	подтверждение заказа 	CONFIRM_ZAKAZ	0
13	Принтер	PRINTER	sewoo_lk_tl202
14			1
15	schet_num	schet_num	15
\.


--
-- Name: cafe_option_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('cafe_option_id_seq', 15, true);


--
-- Data for Name: cafe_playlist; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY cafe_playlist (id, name, path) FROM stdin;
1	Плейлист 1	/opt/mcafe/static/mp3/1
2	Плейлист 2	/opt/mcafe/static/mp3/2
3	Плейлист 3	/opt/mcafe/static/mp3/3
\.


--
-- Name: cafe_playlist_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('cafe_playlist_id_seq', 3, true);


--
-- Data for Name: cafe_prihod; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY cafe_prihod (id, tovar_id, volume, cnt1, volume1, dt, price, prim, user_id) FROM stdin;
11	164	40000	2	20000	2015-03-05 20:20:38.756099+03	0		7
\.


--
-- Name: cafe_prihod_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('cafe_prihod_id_seq', 11, true);


--
-- Data for Name: cafe_printer; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY cafe_printer (id, name, pid, target) FROM stdin;
1	Star TSP100 Cutter (TSP143)	1	10
2	Microsoft XPS Document Writer	2	0
3	Microsoft Office Document Image Writer	3	0
4	Fax	4	0
5	TSP143-(STR_T-001)	1	0
\.


--
-- Name: cafe_printer_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('cafe_printer_id_seq', 5, true);


--
-- Data for Name: cafe_rashod; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY cafe_rashod (id, tovar_id, volume, dt, usluga_id, prim, user_id) FROM stdin;
11178	164	1000	2015-03-02 21:25:09.787807+03	238	БОРЩ(ГОВЯДИНА) ПОРЦИЯ - 2	5
11179	164	250	2015-03-02 21:25:09.854713+03	239	БОРЩ(ГОВЯДИНА) 0.5 - 1	5
11180	164	500	2015-03-04 22:25:20.869313+03	238	БОРЩ(ГОВЯДИНА) ПОРЦИЯ - 1	1
11181	164	500	2015-03-04 22:25:25.084619+03	238	БОРЩ(ГОВЯДИНА) ПОРЦИЯ - 1	1
11182	164	500	2015-03-04 22:25:51.454726+03	238	БОРЩ(ГОВЯДИНА) ПОРЦИЯ - 1	1
11183	164	500	2015-03-05 20:15:56.335998+03	238	БОРЩ(ГОВЯДИНА) ПОРЦИЯ - 1	5
11184	164	250	2015-06-27 13:22:33.529879+03	239	БОРЩ(ГОВЯДИНА) 0.5 - 1	5
11185	164	500	2015-06-27 13:22:36.686941+03	238	БОРЩ(ГОВЯДИНА) ПОРЦИЯ - 1	5
11186	164	-500	2015-06-27 13:22:44.678436+03	238	БОРЩ(ГОВЯДИНА) ПОРЦИЯ - -1	5
11187	164	250	2015-06-27 13:23:42.868693+03	239	БОРЩ(ГОВЯДИНА) 0.5 - 1	5
11188	164	250	2015-06-27 13:23:43.322562+03	239	БОРЩ(ГОВЯДИНА) 0.5 - 1	5
11189	164	250	2015-06-27 13:23:43.822752+03	239	БОРЩ(ГОВЯДИНА) 0.5 - 1	5
11190	164	250	2015-06-27 13:23:44.037164+03	239	БОРЩ(ГОВЯДИНА) 0.5 - 1	5
11191	164	250	2015-06-27 13:23:44.380464+03	239	БОРЩ(ГОВЯДИНА) 0.5 - 1	5
11192	164	250	2015-06-27 13:23:44.623351+03	239	БОРЩ(ГОВЯДИНА) 0.5 - 1	5
11193	164	250	2015-06-27 13:23:44.880735+03	239	БОРЩ(ГОВЯДИНА) 0.5 - 1	5
11194	164	500	2015-06-27 13:23:51.530919+03	238	БОРЩ(ГОВЯДИНА) ПОРЦИЯ - 1	5
11195	164	250	2015-06-27 13:23:52.242733+03	239	БОРЩ(ГОВЯДИНА) 0.5 - 1	5
11196	164	250	2015-06-27 13:23:52.771055+03	239	БОРЩ(ГОВЯДИНА) 0.5 - 1	5
11197	164	250	2015-06-27 13:23:53.272597+03	239	БОРЩ(ГОВЯДИНА) 0.5 - 1	5
11198	164	500	2015-06-27 13:23:53.815387+03	238	БОРЩ(ГОВЯДИНА) ПОРЦИЯ - 1	5
11199	164	250	2015-06-27 13:24:09.580819+03	239	БОРЩ(ГОВЯДИНА) 0.5 - 1	5
11200	164	250	2015-06-27 13:24:09.881838+03	239	БОРЩ(ГОВЯДИНА) 0.5 - 1	5
11201	164	250	2015-06-27 13:24:10.353005+03	239	БОРЩ(ГОВЯДИНА) 0.5 - 1	5
11202	164	250	2015-06-27 13:24:10.596093+03	239	БОРЩ(ГОВЯДИНА) 0.5 - 1	5
11203	164	250	2015-06-27 13:26:01.045576+03	239	БОРЩ(ГОВЯДИНА) 0.5 - 1	5
11204	164	250	2015-06-27 13:28:46.249145+03	239	БОРЩ(ГОВЯДИНА) 0.5 - 1	5
11205	164	250	2015-06-27 13:28:59.395468+03	239	БОРЩ(ГОВЯДИНА) 0.5 - 1	5
11206	164	250	2015-06-27 13:29:01.010938+03	239	БОРЩ(ГОВЯДИНА) 0.5 - 1	5
11207	164	500	2015-06-27 13:43:17.482705+03	238	БОРЩ(ГОВЯДИНА) ПОРЦИЯ - 1	5
11208	164	500	2015-06-27 13:43:19.265959+03	238	БОРЩ(ГОВЯДИНА) ПОРЦИЯ - 1	5
11209	164	500	2015-06-27 13:43:19.480265+03	238	БОРЩ(ГОВЯДИНА) ПОРЦИЯ - 1	5
11210	164	500	2015-06-27 13:43:19.694789+03	238	БОРЩ(ГОВЯДИНА) ПОРЦИЯ - 1	5
11211	164	500	2015-06-27 13:43:19.966327+03	238	БОРЩ(ГОВЯДИНА) ПОРЦИЯ - 1	5
11212	164	500	2015-06-27 13:43:20.166501+03	238	БОРЩ(ГОВЯДИНА) ПОРЦИЯ - 1	5
11213	164	500	2015-06-27 13:43:20.453674+03	238	БОРЩ(ГОВЯДИНА) ПОРЦИЯ - 1	5
11214	164	500	2015-06-27 13:43:20.62395+03	238	БОРЩ(ГОВЯДИНА) ПОРЦИЯ - 1	5
11215	164	500	2015-06-27 13:43:20.824165+03	238	БОРЩ(ГОВЯДИНА) ПОРЦИЯ - 1	5
11216	164	500	2015-06-27 13:43:27.399105+03	238	БОРЩ(ГОВЯДИНА) ПОРЦИЯ - 1	5
11217	164	500	2015-06-27 13:43:27.627891+03	238	БОРЩ(ГОВЯДИНА) ПОРЦИЯ - 1	5
11218	164	500	2015-06-27 13:43:28.214108+03	238	БОРЩ(ГОВЯДИНА) ПОРЦИЯ - 1	5
11219	164	500	2015-06-27 13:43:28.457026+03	238	БОРЩ(ГОВЯДИНА) ПОРЦИЯ - 1	5
11220	164	500	2015-06-27 13:43:28.686233+03	238	БОРЩ(ГОВЯДИНА) ПОРЦИЯ - 1	5
11221	164	500	2015-06-27 13:43:28.871494+03	238	БОРЩ(ГОВЯДИНА) ПОРЦИЯ - 1	5
11222	164	500	2015-06-29 14:55:30.309203+03	238	БОРЩ(ГОВЯДИНА) ПОРЦИЯ - 1	5
11223	164	500	2015-06-29 15:51:47.896444+03	238	БОРЩ(ГОВЯДИНА) ПОРЦИЯ - 1	5
11224	164	500	2015-06-29 15:52:18.854586+03	238	БОРЩ(ГОВЯДИНА) ПОРЦИЯ - 1	5
11225	164	500	2015-06-29 15:54:04.473537+03	238	БОРЩ(ГОВЯДИНА) ПОРЦИЯ - 1	5
11226	164	250	2015-06-29 15:54:05.159443+03	239	БОРЩ(ГОВЯДИНА) 0.5 - 1	5
\.


--
-- Name: cafe_rashod_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('cafe_rashod_id_seq', 11226, true);


--
-- Data for Name: cafe_rashodcat; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY cafe_rashodcat (id, name) FROM stdin;
1	Аренда
2	Интернет
3	Зарплата Лене
4	Зарплата ирине
\.


--
-- Name: cafe_rashodcat_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('cafe_rashodcat_id_seq', 4, true);


--
-- Data for Name: cafe_schet; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY cafe_schet (id, stol_id, status, price, name, dt_open, dt_close, user_open_id, user_close_id, skidka, price_skidka, card, precheck, prim, skidka_fix, num) FROM stdin;
1251	1	1	750	Счет 1	2015-03-01 21:16:24.383197+03	2015-03-01 21:16:32.026925+03	7	7	0	750	0	0	\N	0	0
1252	13	1	1500	Счет 1	2015-03-01 21:50:12.033005+03	2015-03-01 21:51:06.471062+03	7	7	100	0	0	0	\N	0	0
1253	14	1	270	Счет 1	2015-03-01 22:03:28.399926+03	2015-03-01 22:10:55.325354+03	7	7	100	0	0	1	\N	0	0
1254	14	1	550	Счет 1	2015-03-01 22:11:09.929566+03	2015-03-01 22:38:10.392247+03	7	7	0	550	0	0	\N	0	0
1255	14	1	1070	Счет 1	2015-03-01 22:47:54.958831+03	2015-03-01 23:11:12.79751+03	7	7	100	0	0	0	\N	0	0
1256	1	1	470	Счет 1	2015-03-02 21:25:09.651557+03	2015-03-02 21:25:13.325817+03	5	5	0	470	0	0	\N	0	0
1257	1	1	270	Счет 1	2015-03-03 21:49:25.670368+03	2015-03-03 21:49:44.333744+03	5	5	0	170	0	0	qwerty	100	0
1258	1	1	270	Счет 1	2015-03-04 21:22:31.81383+03	2015-03-04 21:22:42.297879+03	1	1	0	270	0	0	\N	0	0
1259	1	1	1440	Счет 1	2015-03-04 22:25:20.68906+03	2015-03-04 22:25:53.009054+03	1	1	0	1440	0	0	\N	0	0
1260	1	1	410	Счет 1	2015-03-05 20:10:19.742102+03	2015-03-05 20:10:41.217318+03	5	5	0	410	0	0	\N	0	0
1261	1	1	280	Счет 1	2015-03-05 20:15:56.232225+03	2015-03-05 20:15:57.214132+03	5	5	0	280	0	0	\N	0	0
1262	1	1	950	Счет 1	2015-06-27 12:48:01.790228+03	2015-06-27 12:48:05.40624+03	5	5	0	950	0	0	\N	0	1
1263	1	1	470	1	2015-06-27 13:15:15.168+03	2015-06-27 13:15:20.002042+03	4	4	0	470	0	0	\N	0	1
1264	1	1	380	2	2015-06-27 13:15:53.033+03	2015-06-27 13:15:56.45597+03	4	4	0	380	0	0	\N	0	2
1265	1	1	530	3	2015-06-27 13:20:04.198+03	2015-06-27 13:20:16.842017+03	4	4	0	530	0	0	\N	0	3
1266	1	1	770	4	2015-06-27 13:20:27.482+03	2015-06-27 13:22:58.365976+03	4	5	0	770	0	0	\N	0	4
1267	1	1	6620	5	2015-06-27 13:23:20.764+03	2015-06-27 13:25:49.592861+03	5	5	0	6620	0	0	\N	0	5
1268	1	1	330	6	2015-06-27 13:25:50.849+03	2015-06-27 13:26:05.163397+03	5	5	0	330	0	0	\N	0	6
1269	1	1	4740	7	2015-06-27 13:27:47.341+03	2015-06-27 13:39:06.954257+03	5	5	0	4740	0	0	\N	0	7
1270	1	1	6010	8	2015-06-27 13:43:07.148+03	2015-06-29 14:55:05.708582+03	5	5	0	6010	0	0	\N	0	8
1271	1	1	270	9	2015-06-29 14:55:28.876+03	2015-06-29 14:55:34.15358+03	5	5	0	270	0	0	\N	0	9
1272	1	1	270	10	2015-06-29 15:51:43.869+03	2015-06-29 15:51:54.581316+03	5	5	0	270	0	0	\N	0	10
1273	1	1	400	11	2015-06-29 15:52:18.107+03	2015-06-29 15:52:26.509639+03	5	5	0	400	0	0	\N	0	11
1274	1	1	330	12	2015-06-29 15:54:03.468+03	2015-06-29 15:54:10.327762+03	5	5	0	330	0	0	\N	0	12
1275	1	1	210	13	2015-06-29 15:54:52.358+03	2015-06-29 15:55:03.865637+03	5	5	0	210	0	0	\N	0	13
1276	1	1	380	14	2015-06-29 17:09:08.54+03	2015-06-29 17:09:17.277314+03	5	5	0	380	0	0	\N	0	14
1277	1	1	470	15	2015-06-29 17:20:00.722+03	2015-06-29 17:20:17.009916+03	5	5	0	470	0	0	\N	0	15
\.


--
-- Name: cafe_schet_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('cafe_schet_id_seq', 1277, true);


--
-- Data for Name: cafe_schethistory; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY cafe_schethistory (id, user_id, schet_id, dt, operation, text) FROM stdin;
9184	7	1251	2015-03-01 21:16:24.395112+03	1	счет #1251 открыт
9185	7	1251	2015-03-01 21:16:24.479167+03	2	добавлен заказ мясо по французски в количестве 5
9186	7	1251	2015-03-01 21:16:32.058825+03	6	счет #1251 закрыт и напечатан чек
9187	7	1252	2015-03-01 21:50:12.045148+03	1	счет #1252 открыт
9188	7	1252	2015-03-01 21:50:12.112517+03	2	добавлен заказ мясо по французски в количестве 10
9189	7	1252	2015-03-01 21:50:19.850698+03	3	указана скидка 100%
9190	7	1252	2015-03-01 21:51:06.502674+03	6	счет #1252 закрыт и напечатан чек
9191	7	1253	2015-03-01 22:03:28.411981+03	1	счет #1253 открыт
9192	7	1253	2015-03-01 22:03:28.479222+03	2	добавлен заказ мясо по французски в количестве 1
9193	7	1253	2015-03-01 22:09:22.576366+03	2	добавлен заказ XИНКАЛ ПО-ДАГЕСТАНСКИ(ГОВЯДИНА) в количестве 1
9194	7	1253	2015-03-01 22:09:28.182092+03	3	указана скидка 100%
9195	7	1253	2015-03-01 22:09:42.085847+03	4	Печать пречека
9196	7	1253	2015-03-01 22:10:55.357012+03	6	счет #1253 закрыт и напечатан чек
9197	7	1254	2015-03-01 22:11:09.941554+03	1	счет #1254 открыт
9198	7	1254	2015-03-01 22:11:09.983307+03	2	добавлен заказ ШУРПА в количестве 1
9199	7	1254	2015-03-01 22:11:10.024914+03	2	добавлен заказ ПИТИ в количестве 1
9200	5	1254	2015-03-01 22:21:59.745072+03	3	указана скидка 5%
9201	5	1254	2015-03-01 22:22:01.452772+03	3	указана скидка 100%
9202	5	1254	2015-03-01 22:22:02.877078+03	3	указана скидка 0%
9203	5	1254	2015-03-01 22:22:52.22073+03	2	добавлен заказ XИНКАЛ ПО-ДАГЕСТАНСКИ(ГОВЯДИНА) в количестве 1
9204	7	1254	2015-03-01 22:38:10.445251+03	6	счет #1254 закрыт и напечатан чек
9205	7	1255	2015-03-01 22:47:54.971527+03	1	счет #1255 открыт
9206	7	1255	2015-03-01 22:47:55.030047+03	2	добавлен заказ ХИНКАЛ ПО-ДАГЕСТАНСКИ(БАРАНИНА) в количестве 1
9207	7	1255	2015-03-01 22:58:14.113669+03	2	добавлен заказ ПЛОВ в количестве 1
9208	7	1255	2015-03-01 22:59:41.571476+03	2	добавлен заказ ХИНКАЛ ПО-ДАГЕСТАНСКИ(БАРАНИНА) в количестве 1
9209	7	1255	2015-03-01 22:59:41.614284+03	2	добавлен заказ ШУРПА в количестве 1
9210	7	1255	2015-03-01 22:59:41.655924+03	2	добавлен заказ ПИТИ в количестве 1
9211	7	1255	2015-03-01 22:59:41.69761+03	2	добавлен заказ МЯСО ПО-ФРАНЦУЗКИ в количестве 1
9212	7	1255	2015-03-01 23:11:12.863563+03	6	счет #1255 закрыт и напечатан чек
9213	5	1256	2015-03-02 21:25:09.662608+03	1	счет #1256 открыт
9214	5	1256	2015-03-02 21:25:09.721354+03	2	добавлен заказ XИНКАЛ ПО-ДАГЕСТАНСКИ(ГОВЯДИНА) в количестве 1
9215	5	1256	2015-03-02 21:25:09.804938+03	2	добавлен заказ БОРЩ(ГОВЯДИНА) ПОРЦИЯ в количестве 2
9216	5	1256	2015-03-02 21:25:09.871536+03	2	добавлен заказ БОРЩ(ГОВЯДИНА) 0.5 в количестве 1
9217	5	1256	2015-03-02 21:25:13.378097+03	6	счет #1256 закрыт и напечатан чек
9218	5	1257	2015-03-03 21:49:25.68255+03	1	счет #1257 открыт
9219	5	1257	2015-03-03 21:49:25.724563+03	2	добавлен заказ ХИНКАЛ ПО-ДАГЕСТАНСКИ(БАРАНИНА) в количестве 1
9220	5	1257	2015-03-03 21:49:44.399085+03	6	счет #1257 закрыт и напечатан чек
9221	1	1258	2015-03-04 21:22:31.843127+03	1	счет #1258 открыт
9222	1	1258	2015-03-04 21:22:31.985179+03	2	добавлен заказ XИНКАЛ ПО-ДАГЕСТАНСКИ(ГОВЯДИНА) в количестве 1
9223	1	1258	2015-03-04 21:22:42.338575+03	6	счет #1258 закрыт и напечатан чек
9224	1	1259	2015-03-04 22:25:20.701158+03	1	счет #1259 открыт
9225	1	1259	2015-03-04 22:25:20.744538+03	2	добавлен заказ XИНКАЛ ПО-ДАГЕСТАНСКИ(ГОВЯДИНА) в количестве 1
9226	1	1259	2015-03-04 22:25:20.786124+03	2	добавлен заказ ШУРПА в количестве 1
9227	1	1259	2015-03-04 22:25:20.969716+03	2	добавлен заказ БОРЩ(ГОВЯДИНА) ПОРЦИЯ в количестве 1
9228	1	1259	2015-03-04 22:25:25.017696+03	2	добавлен заказ XИНКАЛ ПО-ДАГЕСТАНСКИ(ГОВЯДИНА) в количестве 1
9229	1	1259	2015-03-04 22:25:25.051015+03	2	добавлен заказ ШУРПА в количестве 1
9230	1	1259	2015-03-04 22:25:25.100934+03	2	добавлен заказ БОРЩ(ГОВЯДИНА) ПОРЦИЯ в количестве 1
9231	1	1259	2015-03-04 22:25:51.379945+03	2	добавлен заказ XИНКАЛ ПО-ДАГЕСТАНСКИ(ГОВЯДИНА) в количестве 1
9232	1	1259	2015-03-04 22:25:51.413224+03	2	добавлен заказ ШУРПА в количестве 1
9233	1	1259	2015-03-04 22:25:51.471608+03	2	добавлен заказ БОРЩ(ГОВЯДИНА) ПОРЦИЯ в количестве 1
9234	1	1259	2015-03-04 22:25:53.062522+03	6	счет #1259 закрыт и напечатан чек
9235	5	1260	2015-03-05 20:10:19.762637+03	1	счет #1260 открыт
9236	5	1260	2015-03-05 20:10:19.838151+03	2	добавлен заказ ХИНКАЛ ПО-ДАГЕСТАНСКИ(БАРАНИНА) в количестве 1
9237	5	1260	2015-03-05 20:10:19.879952+03	2	добавлен заказ ПИТИ в количестве 1
9238	5	1260	2015-03-05 20:10:41.252866+03	6	счет #1260 закрыт и напечатан чек
9239	5	1261	2015-03-05 20:15:56.24403+03	1	счет #1261 открыт
9240	5	1261	2015-03-05 20:15:56.286157+03	2	добавлен заказ ШУРПА в количестве 1
9241	5	1261	2015-03-05 20:15:56.378017+03	2	добавлен заказ БОРЩ(ГОВЯДИНА) ПОРЦИЯ в количестве 1
9242	5	1261	2015-03-05 20:15:56.419657+03	2	добавлен заказ ХАРЧО(ГОВЯДИНА) ПОРЦИЯ в количестве 1
9243	5	1261	2015-03-05 20:15:57.260972+03	6	счет #1261 закрыт и напечатан чек
9244	5	1262	2015-06-27 12:48:01.895416+03	1	счет #1 открыт
9245	5	1262	2015-06-27 12:48:02.124189+03	2	добавлен заказ XИНКАЛ ПО-ДАГЕСТАНСКИ(ГОВЯДИНА) в количестве 2
9246	5	1262	2015-06-27 12:48:02.238559+03	2	добавлен заказ ХИНКАЛ ПО-ДАГЕСТАНСКИ(БАРАНИНА) в количестве 1
9247	5	1262	2015-06-27 12:48:02.424238+03	2	добавлен заказ ШУРПА в количестве 1
9248	5	1262	2015-06-27 12:48:05.583557+03	6	счет #1262 закрыт и напечатан чек
9249	1	1263	2015-06-27 13:15:15.235+03	2	счет №1 открыт
9250	4	1263	2015-06-27 13:15:20.171129+03	6	счет #1263 закрыт и напечатан чек
9251	1	1264	2015-06-27 13:15:53.049+03	2	счет №1 открыт
9252	4	1264	2015-06-27 13:15:56.763397+03	6	счет #1264 закрыт и напечатан чек
9253	1	1265	2015-06-27 13:20:04.214+03	2	счет №1 открыт
9254	4	1265	2015-06-27 13:20:17.032876+03	6	счет #1265 закрыт и напечатан чек
9255	1	1266	2015-06-27 13:20:27.497+03	2	счет №3 открыт
9256	5	1266	2015-06-27 13:22:44.73+03	6	отмена услуги БОРЩ(ГОВЯДИНА) ПОРЦИЯ в количестве 1
9257	5	1266	2015-06-27 13:22:47.191+03	6	отмена услуги ПИТИ в количестве 1
9258	5	1266	2015-06-27 13:22:58.623361+03	6	счет #1266 закрыт и напечатан чек
9259	1	1267	2015-06-27 13:23:20.783+03	2	счет №4 открыт
9260	5	1267	2015-06-27 13:25:49.862986+03	6	счет #1267 закрыт и напечатан чек
9261	1	1268	2015-06-27 13:25:50.91+03	2	счет №5 открыт
9262	5	1268	2015-06-27 13:26:05.386474+03	6	счет #1268 закрыт и напечатан чек
9263	1	1269	2015-06-27 13:27:47.358+03	2	счет №6 открыт
9264	5	1269	2015-06-27 13:39:07.234449+03	6	счет #1269 закрыт и напечатан чек
9297	1	1270	2015-06-27 13:43:07.419+03	2	счет №1 открыт
9298	5	1270	2015-06-29 14:55:06.574403+03	6	счет #1270 закрыт и напечатан чек
9299	1	1271	2015-06-29 14:55:29.003+03	2	счет №8 открыт
9300	5	1271	2015-06-29 14:55:34.5119+03	6	счет #1271 закрыт и напечатан чек
9301	1	1272	2015-06-29 15:51:44.37+03	2	счет №1 открыт
9302	5	1272	2015-06-29 15:51:55.239219+03	6	счет #1272 закрыт и напечатан чек
9303	1	1273	2015-06-29 15:52:18.171+03	2	счет №10 открыт
9304	5	1273	2015-06-29 15:52:27.246209+03	6	счет #1273 закрыт и напечатан чек
9305	1	1274	2015-06-29 15:54:03.485+03	2	счет №11 открыт
9306	5	1274	2015-06-29 15:54:10.530412+03	6	счет #1274 закрыт и напечатан чек
9307	1	1275	2015-06-29 15:54:52.375+03	2	счет №12 открыт
9308	5	1275	2015-06-29 15:55:04.051453+03	6	счет #1275 закрыт и напечатан чек
9309	1	1276	2015-06-29 17:09:08.558+03	2	счет №1 открыт
9310	5	1276	2015-06-29 17:09:17.475319+03	6	счет #1276 закрыт и напечатан чек
9311	1	1277	2015-06-29 17:20:00.739+03	2	счет №1 открыт
9312	5	1277	2015-06-29 17:20:07.795188+03	7	Отмена пречека
9313	5	1277	2015-06-29 17:20:17.191331+03	6	счет #1277 закрыт и напечатан чек
\.


--
-- Name: cafe_schethistory_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('cafe_schethistory_id_seq', 9313, true);


--
-- Data for Name: cafe_skidka; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY cafe_skidka (id, value) FROM stdin;
1	5
2	10
3	100
4	0
5	20
\.


--
-- Name: cafe_skidka_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('cafe_skidka_id_seq', 5, true);


--
-- Data for Name: cafe_sklad; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY cafe_sklad (id, tovar_id, volume, dt) FROM stdin;
449	164	36709	2015-06-29 15:54:05.159443+03
\.


--
-- Name: cafe_sklad_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('cafe_sklad_id_seq', 449, true);


--
-- Data for Name: cafe_smena; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY cafe_smena (id, dt_open, dt_close, who, prim, user_close_id, sum) FROM stdin;
1	2015-03-02 20:53:45.210328+03	2015-03-02 20:53:45.210339+03	\N	\N	\N	0
\.


--
-- Name: cafe_smena_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('cafe_smena_id_seq', 1, true);


--
-- Data for Name: cafe_sostav; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY cafe_sostav (id, usluga_id, tovar_id, volume) FROM stdin;
418	243	164	500
419	244	164	250
420	245	165	500
422	265	165	250
\.


--
-- Name: cafe_sostav_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('cafe_sostav_id_seq', 422, true);


--
-- Data for Name: cafe_stol; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY cafe_stol (id, name, status, dt, visible) FROM stdin;
13	Возврат	0	2015-03-01 20:36:00.727682+03	1
14	Персонал	0	2015-03-01 20:36:04.808068+03	1
1	Раздача	0	2014-03-17 11:52:13.732+03	1
\.


--
-- Name: cafe_stol_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('cafe_stol_id_seq', 14, true);


--
-- Data for Name: cafe_tovar; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY cafe_tovar (id, name, volume, mvolume, menu_id, portion) FROM stdin;
164	БОРЩ(ГОВЯДИНА) 	20000	0	1	500
165	ХАРЧО ГОВЯДИНА	1	0	1	500
\.


--
-- Name: cafe_tovar_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('cafe_tovar_id_seq', 165, true);


--
-- Data for Name: cafe_usluga; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY cafe_usluga (id, name, description, menu_id, dt, price, np, active, check_name) FROM stdin;
249	ЖАРКОЕ ПО-ДОМАШНЕМУ		51	2015-03-01 22:15:06.973028+03	130	100	1	\N
250	ПЕЛЬМЕНИ		51	2015-03-01 22:15:20.59962+03	150	100	1	\N
251	ГУЛЯШ		51	2015-03-01 22:15:34.204769+03	100	100	1	\N
252	КОТЛЕТЫ		51	2015-03-01 22:15:47.192116+03	120	100	1	\N
254	КУРИЦА ЖАРЕНАЯ		51	2015-03-01 22:16:59.344756+03	110	100	1	\N
253	РЫБА ЖАРЕНАЯ		51	2015-03-01 22:16:33.192069+03	90	100	1	\N
256	ШАШЛЫК ИЗ ГОВЯДИНЫ		51	2015-03-01 22:21:18.18197+03	130	100	1	\N
257	ШАШЛЫК ИЗ БАРАНИНЫ		51	2015-03-01 22:21:38.589055+03	150	100	1	\N
258	ЛЮЛЯ-КЕБАБ		51	2015-03-01 22:22:25.065283+03	130	100	1	\N
260	ЯИЧНИЦА		51	2015-03-01 22:24:53.756002+03	70	100	1	\N
261	ЯИЧНИЦА С СОСИСКАМИ		51	2015-03-01 22:25:21.719567+03	120	100	1	\N
262	ОМЛЕТ		51	2015-03-01 22:26:38.586074+03	100	100	1	\N
263	СОСИСКИ		51	2015-03-01 22:26:51.340918+03	50	100	1	\N
264	ТУШЕНАЯ БАРАНИНА С КАРТОШКОЙ		51	2015-03-01 22:29:31.919087+03	150	100	1	\N
269	СУП С ФРИКАДЕЛЬКАМИ		49	2015-06-29 16:13:14.042303+03	120	70	1	\N
270	СУП ГРИБНОЙ		49	2015-06-29 16:13:52.444016+03	60	75	1	\N
292	КУЛЕР 0.5		55	2015-06-29 16:34:30.626691+03	75	100	1	\N
271	МАНТЫ		49	2015-06-29 16:14:15.184156+03	150	80	1	\N
293	ГУСЬ		55	2015-06-29 16:35:26.371909+03	90	100	1	\N
272	ОКРОШКА		49	2015-06-29 16:14:39.469653+03	100	85	1	\N
294	ТУБОРГ		55	2015-06-29 16:35:37.958146+03	95	100	1	\N
246	ПЛОВ		51	2015-03-01 22:13:56.328763+03	100	5	1	\N
295	ЖИГУЛЁВСКОЕ 0.5		55	2015-06-29 16:35:57.334276+03	65	100	1	\N
247	МЯСО ПО-ФРАНЦ ГОВЯДИНА		51	2015-03-01 22:14:24.666737+03	120	10	1	\N
273	МЯСО ПО-ФРАНЦ КУРИЦА		51	2015-06-29 16:17:00.924563+03	170	6	1	\N
248	ОТБИВНАЯ ГОВЯДИНА		51	2015-03-01 22:14:41.909553+03	130	15	1	\N
274	ОТБИВНАЯ КУРИЦА		51	2015-06-29 16:17:52.254368+03	130	11	1	\N
255	ШАШЛЫК ИЗ КУРИЦЫ		51	2015-03-01 22:20:49.56826+03	130	100	1	\N
234	ХИНКАЛ ПО ДАГЕСТ-КИ ГОВЯДИНА		49	2015-03-01 22:05:10.915302+03	270	5	1	XИНКАЛ ПО-ДАГЕСТАНСКИ(ГОВЯДИНА)
235	ХИНКАЛ ПО ДАГЕСТ-КИ БАРАНИНА		49	2015-03-01 22:08:29.250487+03	270	10	1	\N
236	АВАРСКИЙ ХИНКАЛ ГОВЯДИНА		49	2015-03-01 22:08:44.592786+03	300	15	1	\N
237	АВАРСКИЙ ХИНКАЛ БАРАНИНА		49	2015-03-01 22:08:57.25854+03	300	20	1	\N
238	ХИНКАЛ ПО ЧЕЧЕНСКИ ГОВЯДИНА		49	2015-03-01 22:09:39.258143+03	270	25	1	\N
239	ХИНКАЛ ПО ЧЕЧЕНСКИ БАРАНИНА		49	2015-03-01 22:10:05.795311+03	270	30	1	\N
241	ШУРПА БАРАНИНА		49	2015-03-01 22:11:05.112607+03	140	35	1	\N
242	ПИТИ БАРАНИНА		49	2015-03-01 22:11:38.586073+03	140	40	1	\N
243	БОРЩ ГОВЯДИНА		49	2015-03-01 22:11:58.258844+03	100	45	1	\N
244	БОРЩ ГОВЯДИНА 0.5		49	2015-03-01 22:12:11.62045+03	60	50	1	\N
245	ХАРЧО ГОВЯДИНА		49	2015-03-01 22:12:29.473829+03	100	55	1	\N
265	ХАРЧО ГОВЯДИНА 0.5		49	2015-06-29 16:08:04.763994+03	60	55	1	\N
266	ЛАГМАН ГОВЯДИНА		49	2015-06-29 16:11:28.335672+03	120	60	1	\N
267	СУП С ТЕФТЕЛЯМИ (ГОВЯДИНА)		49	2015-06-29 16:12:12.076885+03	150	65	1	\N
268	ЛАПША ДОМАШ (КУРИЦА)		49	2015-06-29 16:12:40.552033+03	100	65	1	\N
259	ВАРЕНИКИ С КАРТОШКОЙ		51	2015-03-01 22:23:07.057304+03	100	100	1	\N
275	ВАРЕНИКИ С ТВОР		51	2015-06-29 16:20:25.837222+03	100	100	1	\N
276	СОУС ПО ДАГЕСТ-КИ		51	2015-06-29 16:20:46.59227+03	150	100	1	\N
277	КАРТОФ ПЮРЕ		53	2015-06-29 16:22:17.531636+03	60	5	1	\N
296	ЖИГУЛЁВСКОЕ 1.25		55	2015-06-29 16:36:29.911529+03	160	100	1	\N
278	КАРТОФ ФРИ		53	2015-06-29 16:22:40.604866+03	60	10	1	\N
279	МАКАРОНЫ		53	2015-06-29 16:23:00.783851+03	60	100	1	\N
280	ГРЕЧКА		53	2015-06-29 16:23:09.556645+03	60	100	1	\N
281	РИС		53	2015-06-29 16:23:17.189469+03	60	100	1	\N
282	ОВОЩНОЙ		54	2015-06-29 16:23:52.576852+03	70	100	1	\N
283	ИЗ СВЕЖЕЙ КАПУСТЫ		54	2015-06-29 16:24:07.35568+03	50	100	1	\N
284	МОРКОВЬ ПО КОР		54	2015-06-29 16:24:21.99868+03	50	100	1	\N
285	КРАБОВЫЙ САЛАТ		54	2015-06-29 16:24:33.499457+03	60	100	1	\N
286	ОЛИВЬЕ		54	2015-06-29 16:24:42.397535+03	50	100	1	\N
287	СВЕКЛА С ЧЕСНОКОМ		54	2015-06-29 16:24:57.289558+03	50	100	1	\N
288	СЕЛЁДКА ПОД ШУБОЙ		54	2015-06-29 16:25:28.984354+03	60	100	1	\N
289	СЫР (БРЫНЗА)		54	2015-06-29 16:25:40.550882+03	70	100	1	\N
290	АРСЕНАЛЬНОЕ 0.5		55	2015-06-29 16:33:46.407175+03	75	100	1	\N
291	АРСЕНАЛЬНОЕ 2.5		55	2015-06-29 16:34:07.163415+03	230	100	1	\N
297	БАРХАТНОЕ 0.5		55	2015-06-29 16:36:43.107717+03	80	100	1	\N
298	КУЛЛЕР 1Л		55	2015-06-29 16:37:54.840518+03	170	100	1	\N
299	БАЛТИКА №9 1Л		55	2015-06-29 16:38:10.430143+03	170	100	1	\N
300	БАЛТИКА №7		55	2015-06-29 16:38:29.589322+03	80	100	1	\N
301	КАРЛСБЕРГ		55	2015-06-29 16:38:45.966301+03	110	100	1	\N
302	ХОЛЬСТЕН		55	2015-06-29 16:38:55.812297+03	90	100	1	\N
303	БОЛЬШАЯ КРУЖКА		55	2015-06-29 16:39:29.993291+03	170	100	1	\N
304	ЯРПИВО		55	2015-06-29 16:39:39.343867+03	170	100	1	\N
305	ЯРПИВО 1Л		55	2015-06-29 16:39:53.409439+03	130	100	1	\N
306	NO Ж/Б		55	2015-06-29 16:40:12.057101+03	75	100	1	\N
307	NO		55	2015-06-29 16:40:22.135467+03	90	100	1	\N
308	РАЗЛИВНОЕ		55	2015-06-29 16:40:38.041477+03	90	100	1	\N
309	БАЛТИКА №9 Ж/Б		55	2015-06-29 16:40:55.791286+03	75	100	1	\N
310	БАЛТИКА №3		55	2015-06-29 16:41:14.630638+03	70	100	1	\N
311	БАЛТИКА №9		55	2015-06-29 16:41:24.666459+03	70	100	1	\N
312	КОКА-КОЛА 1Л		56	2015-06-29 16:42:36.545126+03	90	100	1	\N
314	КОКА-КОЛА Ж/Б		56	2015-06-29 16:43:22.379906+03	50	100	1	\N
315	ДОБРЫЙ		56	2015-06-29 16:43:41.367476+03	100	100	1	\N
316	БЁРН 0.5		56	2015-06-29 16:43:59.559927+03	140	100	1	\N
317	БЁРН 0.2		56	2015-06-29 16:44:12.62313+03	100	100	1	\N
318	ПЕПСИ 1Л		56	2015-06-29 16:45:28.829155+03	90	100	1	\N
313	КОКА-КОЛА 0.5		56	2015-06-29 16:43:06.894567+03	70	100	1	\N
319	ПЕПСИ 0.5		56	2015-06-29 16:45:50.144009+03	70	100	1	\N
320	ЛИПТОН 1.25		56	2015-06-29 16:46:00.798093+03	90	100	1	\N
321	ЛИПТОН 0.5		56	2015-06-29 16:46:17.366028+03	75	100	1	\N
322	ФРУКТ САД 1Л		56	2015-06-29 16:46:36.1547+03	90	100	1	\N
323	ФРУКТ САД 0.5		56	2015-06-29 16:46:53.691052+03	60	100	1	\N
324	ФРУКТ САД 0.2		56	2015-06-29 16:47:06.171032+03	30	100	1	\N
325	ФРУКТ САД 2Л		56	2015-06-29 16:47:17.848985+03	170	100	1	\N
326	ПЕПСИ ЖБ		56	2015-06-29 16:47:32.137095+03	35	100	1	\N
327	ИСТОК 1.25		58	2015-06-29 16:50:00.365297+03	50	100	1	\N
328	ИСТОК 0.5		58	2015-06-29 16:50:09.940646+03	25	100	1	\N
329	ИСТОК 0.2		58	2015-06-29 16:50:24.207307+03	20	100	1	\N
330	ВОДА МАЛИНА		58	2015-06-29 16:50:39.021353+03	45	100	1	\N
331	ЛИМОНАД 1.25		58	2015-06-29 16:50:51.375371+03	60	100	1	\N
332	ЛИМОНАД 0.6		58	2015-06-29 16:51:11.209001+03	45	100	1	\N
333	ПАЛПИ 0.5		58	2015-06-29 16:51:22.246265+03	70	100	1	\N
334	ПАЛПИ МАНДЬЮ		58	2015-06-29 16:52:14.781211+03	70	100	1	\N
335	ПАЛПИ СТЕКЛО		58	2015-06-29 16:52:23.956755+03	50	100	1	\N
336	ПАЛПИ ТЕМПО		58	2015-06-29 16:52:34.741986+03	70	100	1	\N
337	СОК ТОМАТНЫЙ		58	2015-06-29 16:52:46.469904+03	30	100	1	\N
338	КЕФИР		58	2015-06-29 16:52:54.678754+03	30	100	1	\N
339	КОМПОТ		58	2015-06-29 16:53:04.170921+03	20	100	1	\N
340	СЛИВКИ		58	2015-06-29 16:53:16.202428+03	10	100	1	\N
341	МАЛ КОКТЕЙЛЬ		58	2015-06-29 16:53:24.788495+03	30	100	1	\N
342	АДРЕНАЛИН 0.5		58	2015-06-29 16:53:41.195608+03	140	100	1	\N
343	АДРЕНАЛИН 0.2		58	2015-06-29 16:53:59.099522+03	100	100	1	\N
344	АЙРАН		58	2015-06-29 16:54:14.050218+03	40	100	1	\N
345	ПАЛПИ 1Л		58	2015-06-29 16:55:02.001942+03	100	100	1	\N
346	КИТ КАТ М		59	2015-06-29 16:56:35.358921+03	50	100	1	\N
347	КИТ КАТ Б		59	2015-06-29 16:56:44.699107+03	80	100	1	\N
348	ПИКНИК		59	2015-06-29 16:56:53.184407+03	80	100	1	\N
349	СНИКЕРС Б		59	2015-06-29 16:57:07.335811+03	60	100	1	\N
350	СНИКЕРС М		59	2015-06-29 16:57:16.490859+03	40	100	1	\N
351	АЛЬПЕНГОЛЬД		59	2015-06-29 16:57:26.993524+03	75	100	1	\N
352	МИЛКИВЕЙ Б		59	2015-06-29 16:57:37.822644+03	45	100	1	\N
353	МИЛКИВЕЙ М		59	2015-06-29 16:57:48.957376+03	25	100	1	\N
354	ДАВ 		59	2015-06-29 16:58:00.163762+03	115	100	1	\N
355	ВОЗДУШНЫЙ		59	2015-06-29 16:58:09.859091+03	75	100	1	\N
356	НАТС Б		59	2015-06-29 16:58:18.117638+03	70	100	1	\N
357	НАТС М		59	2015-06-29 16:58:26.315614+03	50	100	1	\N
358	КИТ КАТ СРЕД		59	2015-06-29 16:58:39.721849+03	70	100	1	\N
359	НЕСКВИК Б		59	2015-06-29 16:58:52.749415+03	45	100	1	\N
360	НЕСКВИК М		59	2015-06-29 16:59:04.104687+03	25	100	1	\N
361	АЛЁНКА		59	2015-06-29 16:59:11.59174+03	80	100	1	\N
362	ЛАВАШ		60	2015-06-29 16:59:57.910732+03	20	100	1	\N
363	ПИРОЖКИ		60	2015-06-29 17:00:07.006976+03	25	100	1	\N
364	САМСА		60	2015-06-29 17:00:15.222394+03	60	100	1	\N
365	ЧЕБУРЕКИ		60	2015-06-29 17:00:25.737825+03	60	100	1	\N
366	МЯСО ЧУДУ		60	2015-06-29 17:00:46.798542+03	50	100	1	\N
367	ТВОРОГ		60	2015-06-29 17:00:59.049052+03	60	100	1	\N
368	ЛЮЛЯ ГОВЯД		60	2015-06-29 17:01:11.529477+03	130	100	1	\N
369	ЛЮЛЯ БАР		60	2015-06-29 17:01:26.484682+03	150	100	1	\N
370	ЛЮЛЯ КУРИЦА		60	2015-06-29 17:01:38.076101+03	120	100	1	\N
371	ХАЧАПУРИ		60	2015-06-29 17:01:50.072679+03	70	100	1	\N
372	ПРЯНИКИ		60	2015-06-29 17:02:07.613859+03	50	100	1	\N
373	КУКУРУЗ ЛЕПЕШКИ		60	2015-06-29 17:02:25.662921+03	25	100	1	\N
374	БОЛЬШАЯ КРУЖКА 0.5		55	2015-06-29 17:06:31.690975+03	60	100	1	\N
375	ЗАЖИГАЛКА		61	2015-06-29 17:13:04.706555+03	35	100	1	\N
376	КОФЕ		61	2015-06-29 17:13:11.534454+03	60	100	1	\N
377	СЕМЕЧКИ БОЛ		61	2015-06-29 17:13:23.034378+03	25	100	1	\N
378	СЕМЕЧКИ МАЛ		61	2015-06-29 17:13:34.573729+03	15	100	1	\N
379	ЛЕЙС БОЛ		61	2015-06-29 17:14:07.448041+03	60	100	1	\N
380	ЛЕЙС МАЛ		61	2015-06-29 17:14:16.559066+03	35	100	1	\N
381	МАМБА		61	2015-06-29 17:14:31.042785+03	25	100	1	\N
382	КАРТЫ		61	2015-06-29 17:14:40.629665+03	25	100	1	\N
383	АНТИПОЛИЦ		61	2015-06-29 17:14:51.967492+03	50	100	1	\N
384	РЫБА		61	2015-06-29 17:15:05.550523+03	70	100	1	\N
385	ПАКЕТ		61	2015-06-29 17:15:14.86136+03	10	100	1	\N
386	СИМКИ		61	2015-06-29 17:15:25.685105+03	100	100	1	\N
387	АРАХИС ЖИКО		61	2015-06-29 17:15:39.676274+03	30	100	1	\N
388	АРАХИС ЧАКО		61	2015-06-29 17:15:48.655043+03	35	100	1	\N
389	ЧОКО-ПАЙ		61	2015-06-29 17:16:00.980047+03	20	100	1	\N
390	ПЮРЕ ДОШИРАК		61	2015-06-29 17:16:52.946543+03	40	100	1	\N
391	РУЛЕТ		61	2015-06-29 17:17:05.639361+03	60	100	1	\N
392	ММС-МАЛ		61	2015-06-29 17:17:18.559636+03	40	100	1	\N
393	БОН-ПАРИ		61	2015-06-29 17:17:28.054239+03	60	100	1	\N
\.


--
-- Name: cafe_usluga_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('cafe_usluga_id_seq', 393, true);


--
-- Data for Name: cafe_vozvrat; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY cafe_vozvrat (id, user_id, usluga_id, cnt, dt, summ) FROM stdin;
1	5	234	1	2015-03-05 20:10:47.865569+03	0
2	5	234	1	2015-03-05 20:16:01.881003+03	270
\.


--
-- Name: cafe_vozvrat_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('cafe_vozvrat_id_seq', 2, true);


--
-- Data for Name: cafe_zakaz; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY cafe_zakaz (id, schet_id, status, menu_id, usluga_id, cnt, price, price_skidka, prim, dt) FROM stdin;
4546	1256	1	1	234	1	270	270	\N	2015-03-02 21:25:09.679177+03
4547	1256	1	1	238	2	140	140	\N	2015-03-02 21:25:09.745152+03
4548	1256	1	1	239	1	60	60	\N	2015-03-02 21:25:09.820656+03
4549	1257	1	1	235	1	270	170	\N	2015-03-03 21:49:25.69778+03
4550	1258	1	1	234	1	270	270	\N	2015-03-04 21:22:31.894768+03
4537	1253	1	1	234	1	270	0	\N	2015-03-01 22:09:22.546331+03
4551	1259	1	1	234	3	810	810	\N	2015-03-04 22:25:20.71671+03
4552	1259	1	1	236	3	420	420	\N	2015-03-04 22:25:20.759087+03
4538	1254	1	1	236	1	140	140	\N	2015-03-01 22:11:09.956845+03
4539	1254	1	1	237	1	140	140	\N	2015-03-01 22:11:09.998305+03
4540	1254	1	1	234	1	270	270	\N	2015-03-01 22:22:52.189026+03
4553	1259	1	1	238	3	210	210	\N	2015-03-04 22:25:20.800668+03
4554	1260	1	1	235	1	270	270	\N	2015-03-05 20:10:19.794625+03
4555	1260	1	1	237	1	140	140	\N	2015-03-05 20:10:19.852541+03
4556	1261	1	1	236	1	140	140	\N	2015-03-05 20:15:56.259262+03
4557	1261	1	1	238	1	70	70	\N	2015-03-05 20:15:56.300516+03
4541	1255	1	1	235	2	540	0	\N	2015-03-01 22:47:55.00334+03
4542	1255	1	1	246	1	100	0	\N	2015-03-01 22:58:14.066642+03
4543	1255	1	1	236	1	140	0	\N	2015-03-01 22:59:41.585844+03
4544	1255	1	1	237	1	140	0	\N	2015-03-01 22:59:41.62862+03
4545	1255	1	1	247	1	150	0	\N	2015-03-01 22:59:41.670185+03
4588	1267	0	1	234	1	270	270	\N	2015-06-27 13:23:52.498+03
4559	1262	1	1	234	2	540	540	\N	2015-06-27 12:48:01.963673+03
4560	1262	1	1	235	1	270	270	\N	2015-06-27 12:48:02.182994+03
4561	1262	1	1	236	1	140	140	\N	2015-06-27 12:48:02.304504+03
4562	1263	0	1	244	1	60	60	\N	2015-06-27 13:15:15.272+03
4563	1263	0	1	235	1	270	270	\N	2015-06-27 13:15:15.387+03
4564	1263	0	1	237	1	140	140	\N	2015-06-27 13:15:15.417+03
4565	1264	0	1	247	1	150	150	\N	2015-06-27 13:15:53.079+03
4566	1264	0	1	249	1	130	130	\N	2015-06-27 13:15:53.109+03
4567	1264	0	1	251	1	100	100	\N	2015-06-27 13:15:53.138+03
4568	1265	0	1	244	2	120	120	\N	2015-06-27 13:20:04.244+03
4569	1265	0	1	235	1	270	270	\N	2015-06-27 13:20:06.589+03
4570	1265	0	1	237	1	140	140	\N	2015-06-27 13:20:09.439+03
4571	1266	0	1	244	1	60	60	\N	2015-06-27 13:20:27.528+03
4572	1266	0	1	235	1	270	270	\N	2015-06-27 13:20:29.193+03
4573	1266	0	1	237	0	0	0	\N	2015-06-27 13:20:30.881+03
4574	1266	0	1	239	1	60	60	\N	2015-06-27 13:22:33.364+03
4575	1266	0	1	241	2	120	120	\N	2015-06-27 13:22:34.468+03
4576	1266	0	1	245	1	70	70	\N	2015-06-27 13:22:35.095+03
4577	1266	0	1	242	1	120	120	\N	2015-06-27 13:22:35.9+03
4579	1266	0	1	238	0	0	0	\N	2015-06-27 13:22:36.667+03
4590	1268	0	1	239	1	60	60	\N	2015-06-27 13:26:01.005+03
4591	1268	0	1	241	1	60	60	\N	2015-06-27 13:26:01.236+03
4592	1268	0	1	237	1	140	140	\N	2015-06-27 13:26:01.552+03
4604	1271	0	1	244	1	60	60	\N	2015-06-29 14:55:29.052+03
4605	1271	0	1	236	1	140	140	\N	2015-06-29 14:55:29.699+03
4606	1271	0	1	238	1	70	70	\N	2015-06-29 14:55:30.268+03
4593	1269	0	1	244	13	780	780	\N	2015-06-27 13:27:47.388+03
4594	1269	0	1	235	11	2970	2970	\N	2015-06-27 13:27:47.835+03
4595	1269	0	1	237	3	420	420	\N	2015-06-27 13:28:45.82+03
4596	1269	0	1	239	3	180	180	\N	2015-06-27 13:28:46.229+03
4597	1269	0	1	241	3	180	180	\N	2015-06-27 13:28:47.377+03
4598	1269	0	1	245	3	210	210	\N	2015-06-27 13:28:47.828+03
4607	1272	0	1	244	1	60	60	\N	2015-06-29 15:51:44.734+03
4608	1272	0	1	236	1	140	140	\N	2015-06-29 15:51:47.398+03
4609	1272	0	1	238	1	70	70	\N	2015-06-29 15:51:47.838+03
4580	1267	0	1	244	4	240	240	\N	2015-06-27 13:23:20.828+03
4581	1267	0	1	235	8	2160	2160	\N	2015-06-27 13:23:21.147+03
4582	1267	0	1	237	7	980	980	\N	2015-06-27 13:23:21.975+03
4583	1267	0	1	245	5	350	350	\N	2015-06-27 13:23:23.06+03
4584	1267	0	1	241	4	240	240	\N	2015-06-27 13:23:25.103+03
4585	1267	0	1	239	14	840	840	\N	2015-06-27 13:23:42.844+03
4586	1267	0	1	238	2	140	140	\N	2015-06-27 13:23:51.502+03
4587	1267	0	1	236	10	1400	1400	\N	2015-06-27 13:23:51.734+03
4610	1273	0	1	236	1	140	140	\N	2015-06-29 15:52:18.355+03
4611	1273	0	1	238	1	70	70	\N	2015-06-29 15:52:18.813+03
4613	1273	0	1	242	1	120	120	\N	2015-06-29 15:52:20.112+03
4599	1270	0	1	235	1	270	270	\N	2015-06-27 13:43:07.548+03
4601	1270	0	1	238	15	1050	1050	\N	2015-06-27 13:43:17.461+03
4602	1270	0	1	236	27	3780	3780	\N	2015-06-27 13:43:23.12+03
4603	1270	0	1	244	7	420	420	\N	2015-06-27 13:43:55.907+03
4614	1274	0	1	244	1	60	60	\N	2015-06-29 15:54:03.516+03
4615	1274	0	1	236	1	140	140	\N	2015-06-29 15:54:03.918+03
4616	1274	0	1	238	1	70	70	\N	2015-06-29 15:54:04.444+03
4617	1274	0	1	239	1	60	60	\N	2015-06-29 15:54:05.136+03
4618	1275	0	1	253	1	90	90	\N	2015-06-29 15:54:52.405+03
4619	1275	0	1	252	1	120	120	\N	2015-06-29 15:54:52.967+03
4620	1276	0	1	242	1	140	140	\N	2015-06-29 17:09:08.587+03
4621	1276	0	1	244	1	60	60	\N	2015-06-29 17:09:09.014+03
4622	1276	0	1	265	1	60	60	\N	2015-06-29 17:09:09.682+03
4623	1276	0	1	287	1	50	50	\N	2015-06-29 17:09:11.682+03
4624	1276	0	1	289	1	70	70	\N	2015-06-29 17:09:12.074+03
4625	1277	0	1	239	1	270	270	\N	2015-06-29 17:20:00.784+03
4626	1277	0	1	242	1	140	140	\N	2015-06-29 17:20:00.978+03
4627	1277	0	1	244	1	60	60	\N	2015-06-29 17:20:01.278+03
\.


--
-- Name: cafe_zakaz_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('cafe_zakaz_id_seq', 4627, true);


--
-- Data for Name: cafe_zakazhistory; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY cafe_zakazhistory (id, schet_id, zakaz_id, status, menu_id, usluga_id, cnt, price, prim, dt, user_id) FROM stdin;
5438	1253	4537	1	1	234	1	270	\N	2015-03-01 22:09:22.569215+03	7
5439	1254	4538	1	1	236	1	140	\N	2015-03-01 22:11:09.976376+03	7
5440	1254	4539	1	1	237	1	140	\N	2015-03-01 22:11:10.017774+03	7
5441	1254	4540	1	1	234	1	270	\N	2015-03-01 22:22:52.21375+03	5
5442	1255	4541	1	1	235	1	270	\N	2015-03-01 22:47:55.023262+03	7
5443	1255	4542	1	1	246	1	100	\N	2015-03-01 22:58:14.106438+03	7
5444	1255	4541	1	1	235	1	270	\N	2015-03-01 22:59:41.56444+03	7
5445	1255	4543	1	1	236	1	140	\N	2015-03-01 22:59:41.60694+03	7
5446	1255	4544	1	1	237	1	140	\N	2015-03-01 22:59:41.648596+03	7
5447	1255	4545	1	1	247	1	150	\N	2015-03-01 22:59:41.69028+03	7
5448	1256	4546	1	1	234	1	270	\N	2015-03-02 21:25:09.706437+03	5
5449	1256	4547	1	1	238	2	140	\N	2015-03-02 21:25:09.796468+03	5
5450	1256	4548	1	1	239	1	60	\N	2015-03-02 21:25:09.863098+03	5
5451	1257	4549	1	1	235	1	270	\N	2015-03-03 21:49:25.717774+03	5
5452	1258	4550	1	1	234	1	270	\N	2015-03-04 21:22:31.964585+03	1
5453	1259	4551	1	1	234	1	270	\N	2015-03-04 22:25:20.736436+03	1
5454	1259	4552	1	1	236	1	140	\N	2015-03-04 22:25:20.778785+03	1
5455	1259	4553	1	1	238	1	70	\N	2015-03-04 22:25:20.927899+03	1
5456	1259	4551	1	1	234	1	270	\N	2015-03-04 22:25:25.010746+03	1
5457	1259	4552	1	1	236	1	140	\N	2015-03-04 22:25:25.043663+03	1
5458	1259	4553	1	1	238	1	70	\N	2015-03-04 22:25:25.092535+03	1
5459	1259	4551	1	1	234	1	270	\N	2015-03-04 22:25:51.372913+03	1
5460	1259	4552	1	1	236	1	140	\N	2015-03-04 22:25:51.40584+03	1
5461	1259	4553	1	1	238	1	70	\N	2015-03-04 22:25:51.463191+03	1
5462	1260	4554	1	1	235	1	270	\N	2015-03-05 20:10:19.814707+03	5
5463	1260	4555	1	1	237	1	140	\N	2015-03-05 20:10:19.872492+03	5
5464	1261	4556	1	1	236	1	140	\N	2015-03-05 20:15:56.279308+03	5
5465	1261	4557	1	1	238	1	70	\N	2015-03-05 20:15:56.369646+03	5
5467	1262	4559	1	1	234	2	540	\N	2015-06-27 12:48:02.052361+03	5
5468	1262	4560	1	1	235	1	270	\N	2015-06-27 12:48:02.223894+03	5
5469	1262	4561	1	1	236	1	140	\N	2015-06-27 12:48:02.409718+03	5
5470	1263	4562	1	1	244	1	60	\N	2015-06-27 13:15:15.316+03	4
5471	1263	4563	1	1	235	1	270	\N	2015-06-27 13:15:15.402+03	4
5472	1263	4564	1	1	237	1	140	\N	2015-06-27 13:15:15.43+03	4
5473	1264	4565	1	1	247	1	150	\N	2015-06-27 13:15:53.094+03	4
5474	1264	4566	1	1	249	1	130	\N	2015-06-27 13:15:53.123+03	4
5475	1264	4567	1	1	251	1	100	\N	2015-06-27 13:15:53.151+03	4
5476	1265	4568	1	1	244	1	60	\N	2015-06-27 13:20:04.272+03	4
5477	1265	4568	1	1	244	1	60	\N	2015-06-27 13:20:05.412+03	4
5478	1265	4569	1	1	235	1	270	\N	2015-06-27 13:20:06.618+03	4
5479	1265	4570	1	1	237	1	140	\N	2015-06-27 13:20:09.46+03	4
5480	1266	4571	1	1	244	1	60	\N	2015-06-27 13:20:27.541+03	4
5481	1266	4572	1	1	235	1	270	\N	2015-06-27 13:20:29.213+03	4
5482	1266	4573	1	1	237	1	140	\N	2015-06-27 13:20:30.9+03	4
5483	1266	4574	1	1	239	1	60	\N	2015-06-27 13:22:33.529+03	5
5484	1266	4575	1	1	241	1	60	\N	2015-06-27 13:22:34.485+03	5
5485	1266	4576	1	1	245	1	70	\N	2015-06-27 13:22:35.114+03	5
5486	1266	4577	1	1	242	1	120	\N	2015-06-27 13:22:35.943+03	5
5488	1266	4579	1	1	238	1	70	\N	2015-06-27 13:22:36.686+03	5
5489	1266	4579	1	1	238	-1	-70	\N	2015-06-27 13:22:44.677+03	5
5490	1266	4573	1	1	237	-1	-140	\N	2015-06-27 13:22:47.141+03	5
5491	1266	4575	1	1	241	1	60	\N	2015-06-27 13:22:53.92+03	5
5492	1267	4580	1	1	244	1	60	\N	2015-06-27 13:23:20.856+03	5
5493	1267	4581	1	1	235	1	270	\N	2015-06-27 13:23:21.17+03	5
5494	1267	4582	1	1	237	1	140	\N	2015-06-27 13:23:21.999+03	5
5495	1267	4583	1	1	245	1	70	\N	2015-06-27 13:23:23.085+03	5
5496	1267	4583	1	1	245	1	70	\N	2015-06-27 13:23:24.883+03	5
5497	1267	4584	1	1	241	1	60	\N	2015-06-27 13:23:25.129+03	5
5498	1267	4583	1	1	245	1	70	\N	2015-06-27 13:23:25.397+03	5
5499	1267	4585	1	1	239	1	60	\N	2015-06-27 13:23:42.868+03	5
5500	1267	4584	1	1	241	1	60	\N	2015-06-27 13:23:43.122+03	5
5501	1267	4585	1	1	239	1	60	\N	2015-06-27 13:23:43.321+03	5
5502	1267	4585	1	1	239	1	60	\N	2015-06-27 13:23:43.822+03	5
5503	1267	4585	1	1	239	1	60	\N	2015-06-27 13:23:44.036+03	5
5504	1267	4585	1	1	239	1	60	\N	2015-06-27 13:23:44.379+03	5
5505	1267	4585	1	1	239	1	60	\N	2015-06-27 13:23:44.622+03	5
5506	1267	4585	1	1	239	1	60	\N	2015-06-27 13:23:44.88+03	5
5507	1267	4581	1	1	235	1	270	\N	2015-06-27 13:23:48.026+03	5
5508	1267	4580	1	1	244	1	60	\N	2015-06-27 13:23:48.553+03	5
5509	1267	4581	1	1	235	1	270	\N	2015-06-27 13:23:49.01+03	5
5510	1267	4582	1	1	237	1	140	\N	2015-06-27 13:23:49.497+03	5
5511	1267	4582	1	1	237	1	140	\N	2015-06-27 13:23:49.882+03	5
5512	1267	4584	1	1	241	1	60	\N	2015-06-27 13:23:50.154+03	5
5513	1267	4583	1	1	245	1	70	\N	2015-06-27 13:23:50.411+03	5
5514	1267	4583	1	1	245	1	70	\N	2015-06-27 13:23:50.726+03	5
5515	1267	4584	1	1	241	1	60	\N	2015-06-27 13:23:51.269+03	5
5516	1267	4586	1	1	238	1	70	\N	2015-06-27 13:23:51.53+03	5
5517	1267	4587	1	1	236	1	140	\N	2015-06-27 13:23:51.759+03	5
5518	1267	4587	1	1	236	1	140	\N	2015-06-27 13:23:51.998+03	5
5519	1267	4585	1	1	239	1	60	\N	2015-06-27 13:23:52.242+03	5
5520	1267	4588	1	1	234	1	270	\N	2015-06-27 13:23:52.516+03	5
5521	1267	4585	1	1	239	1	60	\N	2015-06-27 13:23:52.77+03	5
5522	1267	4585	1	1	239	1	60	\N	2015-06-27 13:23:53.271+03	5
5523	1267	4586	1	1	238	1	70	\N	2015-06-27 13:23:53.814+03	5
5524	1267	4587	1	1	236	1	140	\N	2015-06-27 13:23:55.815+03	5
5525	1267	4587	1	1	236	1	140	\N	2015-06-27 13:23:56.073+03	5
5526	1267	4587	1	1	236	1	140	\N	2015-06-27 13:23:56.373+03	5
5527	1267	4587	1	1	236	1	140	\N	2015-06-27 13:23:56.644+03	5
5528	1267	4587	1	1	236	1	140	\N	2015-06-27 13:23:56.93+03	5
5529	1267	4587	1	1	236	1	140	\N	2015-06-27 13:23:57.188+03	5
5530	1267	4587	1	1	236	1	140	\N	2015-06-27 13:23:57.459+03	5
5531	1267	4587	1	1	236	1	140	\N	2015-06-27 13:23:57.702+03	5
5532	1267	4581	1	1	235	1	270	\N	2015-06-27 13:24:08.58+03	5
5533	1267	4580	1	1	244	1	60	\N	2015-06-27 13:24:08.794+03	5
5534	1267	4580	1	1	244	1	60	\N	2015-06-27 13:24:09.051+03	5
5535	1267	4585	1	1	239	1	60	\N	2015-06-27 13:24:09.58+03	5
5536	1267	4585	1	1	239	1	60	\N	2015-06-27 13:24:09.88+03	5
5537	1267	4582	1	1	237	1	140	\N	2015-06-27 13:24:10.095+03	5
5538	1267	4585	1	1	239	1	60	\N	2015-06-27 13:24:10.352+03	5
5539	1267	4585	1	1	239	1	60	\N	2015-06-27 13:24:10.595+03	5
5540	1267	4582	1	1	237	1	140	\N	2015-06-27 13:24:10.839+03	5
5541	1267	4582	1	1	237	1	140	\N	2015-06-27 13:24:11.038+03	5
5542	1267	4582	1	1	237	1	140	\N	2015-06-27 13:24:11.312+03	5
5543	1267	4581	1	1	235	1	270	\N	2015-06-27 13:24:11.51+03	5
5544	1267	4581	1	1	235	1	270	\N	2015-06-27 13:24:11.739+03	5
5545	1267	4581	1	1	235	1	270	\N	2015-06-27 13:24:12.139+03	5
5546	1267	4581	1	1	235	1	270	\N	2015-06-27 13:24:12.339+03	5
5548	1268	4590	1	1	239	1	60	\N	2015-06-27 13:26:01.044+03	5
5549	1268	4591	1	1	241	1	60	\N	2015-06-27 13:26:01.259+03	5
5550	1268	4592	1	1	237	1	140	\N	2015-06-27 13:26:01.574+03	5
5551	1269	4593	1	1	244	1	60	\N	2015-06-27 13:27:47.402+03	5
5552	1269	4593	1	1	244	1	60	\N	2015-06-27 13:27:47.584+03	5
5553	1269	4594	1	1	235	1	270	\N	2015-06-27 13:27:47.859+03	5
5554	1269	4594	1	1	235	1	270	\N	2015-06-27 13:27:48.127+03	5
5555	1269	4594	1	1	235	1	270	\N	2015-06-27 13:27:48.356+03	5
5556	1269	4594	1	1	235	1	270	\N	2015-06-27 13:27:48.613+03	5
5557	1269	4594	1	1	235	1	270	\N	2015-06-27 13:27:48.885+03	5
5558	1269	4593	1	1	244	1	60	\N	2015-06-27 13:28:40.913+03	5
5559	1269	4593	1	1	244	1	60	\N	2015-06-27 13:28:41.013+03	5
5560	1269	4593	1	1	244	1	60	\N	2015-06-27 13:28:44.086+03	5
5561	1269	4593	1	1	244	1	60	\N	2015-06-27 13:28:44.558+03	5
5562	1269	4593	1	1	244	1	60	\N	2015-06-27 13:28:44.786+03	5
5563	1269	4593	1	1	244	1	60	\N	2015-06-27 13:28:45.029+03	5
5564	1269	4594	1	1	235	1	270	\N	2015-06-27 13:28:45.244+03	5
5565	1269	4594	1	1	235	1	270	\N	2015-06-27 13:28:45.559+03	5
5566	1269	4595	1	1	237	1	140	\N	2015-06-27 13:28:45.848+03	5
5567	1269	4596	1	1	239	1	60	\N	2015-06-27 13:28:46.248+03	5
5568	1269	4597	1	1	241	1	60	\N	2015-06-27 13:28:47.406+03	5
5569	1269	4598	1	1	245	1	70	\N	2015-06-27 13:28:47.849+03	5
5570	1269	4593	1	1	244	1	60	\N	2015-06-27 13:28:54.349+03	5
5571	1269	4594	1	1	235	1	270	\N	2015-06-27 13:28:54.563+03	5
5572	1269	4594	1	1	235	1	270	\N	2015-06-27 13:28:54.849+03	5
5573	1269	4593	1	1	244	1	60	\N	2015-06-27 13:28:57.808+03	5
5574	1269	4593	1	1	244	1	60	\N	2015-06-27 13:28:58.036+03	5
5575	1269	4593	1	1	244	1	60	\N	2015-06-27 13:28:58.285+03	5
5576	1269	4594	1	1	235	1	270	\N	2015-06-27 13:28:58.908+03	5
5577	1269	4595	1	1	237	1	140	\N	2015-06-27 13:28:59.151+03	5
5578	1269	4596	1	1	239	1	60	\N	2015-06-27 13:28:59.394+03	5
5579	1269	4597	1	1	241	1	60	\N	2015-06-27 13:28:59.638+03	5
5580	1269	4598	1	1	245	1	70	\N	2015-06-27 13:28:59.938+03	5
5581	1269	4598	1	1	245	1	70	\N	2015-06-27 13:29:00.238+03	5
5582	1269	4597	1	1	241	1	60	\N	2015-06-27 13:29:00.51+03	5
5583	1269	4596	1	1	239	1	60	\N	2015-06-27 13:29:01.01+03	5
5584	1269	4595	1	1	237	1	140	\N	2015-06-27 13:29:01.285+03	5
5585	1269	4593	1	1	244	1	60	\N	2015-06-27 13:29:01.739+03	5
5586	1269	4594	1	1	235	1	270	\N	2015-06-27 13:29:02.454+03	5
5587	1270	4599	1	1	235	1	270	\N	2015-06-27 13:43:07.648+03	5
5589	1270	4601	1	1	238	1	70	\N	2015-06-27 13:43:17.482+03	5
5590	1270	4601	1	1	238	1	70	\N	2015-06-27 13:43:19.265+03	5
5591	1270	4601	1	1	238	1	70	\N	2015-06-27 13:43:19.479+03	5
5592	1270	4601	1	1	238	1	70	\N	2015-06-27 13:43:19.694+03	5
5593	1270	4601	1	1	238	1	70	\N	2015-06-27 13:43:19.965+03	5
5594	1270	4601	1	1	238	1	70	\N	2015-06-27 13:43:20.165+03	5
5595	1270	4601	1	1	238	1	70	\N	2015-06-27 13:43:20.452+03	5
5596	1270	4601	1	1	238	1	70	\N	2015-06-27 13:43:20.623+03	5
5597	1270	4601	1	1	238	1	70	\N	2015-06-27 13:43:20.823+03	5
5598	1270	4602	1	1	236	1	140	\N	2015-06-27 13:43:23.157+03	5
5599	1270	4602	1	1	236	1	140	\N	2015-06-27 13:43:24.897+03	5
5600	1270	4602	1	1	236	1	140	\N	2015-06-27 13:43:25.125+03	5
5601	1270	4602	1	1	236	1	140	\N	2015-06-27 13:43:25.469+03	5
5602	1270	4602	1	1	236	1	140	\N	2015-06-27 13:43:25.626+03	5
5603	1270	4602	1	1	236	1	140	\N	2015-06-27 13:43:25.84+03	5
5604	1270	4602	1	1	236	1	140	\N	2015-06-27 13:43:26.083+03	5
5605	1270	4602	1	1	236	1	140	\N	2015-06-27 13:43:26.783+03	5
5606	1270	4602	1	1	236	1	140	\N	2015-06-27 13:43:27.112+03	5
5607	1270	4601	1	1	238	1	70	\N	2015-06-27 13:43:27.398+03	5
5608	1270	4601	1	1	238	1	70	\N	2015-06-27 13:43:27.627+03	5
5609	1270	4601	1	1	238	1	70	\N	2015-06-27 13:43:28.213+03	5
5610	1270	4601	1	1	238	1	70	\N	2015-06-27 13:43:28.456+03	5
5611	1270	4601	1	1	238	1	70	\N	2015-06-27 13:43:28.685+03	5
5612	1270	4601	1	1	238	1	70	\N	2015-06-27 13:43:28.87+03	5
5619	1270	4602	1	1	236	1	140	\N	2015-06-27 13:43:34.716+03	5
5620	1270	4602	1	1	236	1	140	\N	2015-06-27 13:43:34.917+03	5
5621	1270	4602	1	1	236	1	140	\N	2015-06-27 13:43:35.16+03	5
5622	1270	4602	1	1	236	1	140	\N	2015-06-27 13:43:35.509+03	5
5623	1270	4602	1	1	236	1	140	\N	2015-06-27 13:43:35.69+03	5
5624	1270	4602	1	1	236	1	140	\N	2015-06-27 13:43:35.875+03	5
5625	1270	4602	1	1	236	1	140	\N	2015-06-27 13:43:36.089+03	5
5626	1270	4602	1	1	236	1	140	\N	2015-06-27 13:43:36.303+03	5
5627	1270	4602	1	1	236	1	140	\N	2015-06-27 13:43:36.532+03	5
5628	1270	4602	1	1	236	1	140	\N	2015-06-27 13:43:36.793+03	5
5629	1270	4602	1	1	236	1	140	\N	2015-06-27 13:43:37.004+03	5
5630	1270	4602	1	1	236	1	140	\N	2015-06-27 13:43:37.204+03	5
5631	1270	4602	1	1	236	1	140	\N	2015-06-27 13:43:50.139+03	5
5632	1270	4602	1	1	236	1	140	\N	2015-06-27 13:43:50.511+03	5
5633	1270	4603	1	1	244	1	60	\N	2015-06-27 13:43:55.989+03	5
5634	1270	4603	1	1	244	1	60	\N	2015-06-27 13:44:09.55+03	5
5635	1270	4602	1	1	236	1	140	\N	2015-06-27 14:34:01.154+03	5
5636	1270	4603	1	1	244	1	60	\N	2015-06-27 14:34:01.399+03	5
5637	1270	4602	1	1	236	1	140	\N	2015-06-27 14:34:39.82+03	5
5638	1270	4602	1	1	236	1	140	\N	2015-06-27 14:34:40.233+03	5
5639	1270	4603	1	1	244	1	60	\N	2015-06-27 14:34:40.705+03	5
5640	1270	4602	1	1	236	1	140	\N	2015-06-27 14:34:40.905+03	5
5641	1270	4603	1	1	244	1	60	\N	2015-06-27 14:34:41.249+03	5
5642	1270	4603	1	1	244	1	60	\N	2015-06-27 14:34:42.121+03	5
5643	1270	4603	1	1	244	1	60	\N	2015-06-27 14:34:42.449+03	5
5644	1271	4604	1	1	244	1	60	\N	2015-06-29 14:55:29.167+03	5
5645	1271	4605	1	1	236	1	140	\N	2015-06-29 14:55:29.724+03	5
5646	1271	4606	1	1	238	1	70	\N	2015-06-29 14:55:30.308+03	5
5647	1272	4607	1	1	244	1	60	\N	2015-06-29 15:51:45.093+03	5
5648	1272	4608	1	1	236	1	140	\N	2015-06-29 15:51:47.44+03	5
5649	1272	4609	1	1	238	1	70	\N	2015-06-29 15:51:47.895+03	5
5650	1273	4610	1	1	236	1	140	\N	2015-06-29 15:52:18.558+03	5
5651	1273	4611	1	1	238	1	70	\N	2015-06-29 15:52:18.853+03	5
5653	1273	4613	1	1	242	1	120	\N	2015-06-29 15:52:20.148+03	5
5654	1274	4614	1	1	244	1	60	\N	2015-06-29 15:54:03.529+03	5
5655	1274	4615	1	1	236	1	140	\N	2015-06-29 15:54:03.944+03	5
5656	1274	4616	1	1	238	1	70	\N	2015-06-29 15:54:04.472+03	5
5657	1274	4617	1	1	239	1	60	\N	2015-06-29 15:54:05.158+03	5
5658	1275	4618	1	1	253	1	90	\N	2015-06-29 15:54:52.419+03	5
5659	1275	4619	1	1	252	1	120	\N	2015-06-29 15:54:52.99+03	5
5660	1276	4620	1	1	242	1	140	\N	2015-06-29 17:09:08.616+03	5
5661	1276	4621	1	1	244	1	60	\N	2015-06-29 17:09:09.03+03	5
5662	1276	4622	1	1	265	1	60	\N	2015-06-29 17:09:09.702+03	5
5663	1276	4623	1	1	287	1	50	\N	2015-06-29 17:09:11.704+03	5
5664	1276	4624	1	1	289	1	70	\N	2015-06-29 17:09:12.104+03	5
5665	1277	4625	1	1	239	1	270	\N	2015-06-29 17:20:00.814+03	5
5666	1277	4626	1	1	242	1	140	\N	2015-06-29 17:20:00.998+03	5
5667	1277	4627	1	1	244	1	60	\N	2015-06-29 17:20:01.298+03	5
\.


--
-- Name: cafe_zakazhistory_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('cafe_zakazhistory_id_seq', 5667, true);


--
-- Data for Name: django_admin_log; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY django_admin_log (id, action_time, user_id, content_type_id, object_id, object_repr, action_flag, change_message) FROM stdin;
1	2014-07-19 23:26:59.369552+03	1	3	2	Pasha	1	
2	2014-07-19 23:27:08.851896+03	1	3	3	Lena	1	
3	2014-07-19 23:27:18.989103+03	1	3	4	Nastia	1	
4	2014-07-19 23:27:25.239793+03	1	3	5	Jena	1	
5	2014-07-19 23:28:13.567023+03	1	2	1	Бухгалтер	1	
6	2014-07-19 23:28:23.610613+03	1	2	2	Администратор	1	
7	2014-07-19 23:28:30.131571+03	1	2	3	Официант	1	
8	2014-07-19 23:28:51.614701+03	1	3	5	Jena	2	Изменен password,first_name и groups.
9	2014-07-19 23:28:56.387602+03	1	3	3	Lena	2	Изменен password и groups.
10	2014-07-19 23:29:06.679491+03	1	3	4	Nastia	2	Изменен password,first_name и groups.
11	2014-07-19 23:29:13.175275+03	1	3	3	Lena	2	Изменен password и first_name.
12	2014-07-19 23:29:21.973051+03	1	3	2	Pasha	2	Изменен password,first_name и groups.
13	2014-07-19 23:29:42.495901+03	1	3	2	Pasha	2	Изменен password,is_staff и is_superuser.
43	2015-03-01 19:53:50.459563+03	1	24	8	Наименование обслуживающего персонала - Кассир	1	
44	2015-03-01 19:54:26.206303+03	1	3	5	kassir1	2	Изменен username и first_name.
45	2015-03-01 19:54:39.183483+03	1	3	3	kassir2	2	Изменен username и first_name.
46	2015-03-01 19:54:49.528043+03	1	3	4	kassir3	2	Изменен username и first_name.
47	2015-03-01 19:59:56.739401+03	1	24	8	Наименование обслуживающего персонала - Кассир	2	Изменен code.
48	2015-03-01 20:03:42.48742+03	1	8	2	2	3	
49	2015-03-01 20:03:42.500317+03	1	8	3	3	3	
50	2015-03-01 20:03:42.508708+03	1	8	4	4	3	
51	2015-03-01 20:03:42.516939+03	1	8	5	5	3	
52	2015-03-01 20:03:42.525242+03	1	8	6	6	3	
53	2015-03-01 20:03:42.533608+03	1	8	7	7	3	
54	2015-03-01 20:03:42.541948+03	1	8	8	8	3	
55	2015-03-01 20:03:42.550255+03	1	8	9	9	3	
56	2015-03-01 20:03:42.558572+03	1	8	10	10	3	
57	2015-03-01 20:03:42.566888+03	1	8	11	0	3	
58	2015-03-01 20:03:42.575219+03	1	8	12	Бар	3	
59	2015-03-01 20:03:51.096916+03	1	8	1	Раздача	2	Изменен name.
60	2015-03-01 20:14:24.924672+03	1	24	1	Начало рабочей смены - 9	2	Изменен value.
61	2015-03-01 20:19:13.911726+03	1	10	3	3. Кальянная - 0	3	
62	2015-03-01 20:19:13.926018+03	1	10	36	36. Порча имущества - 0	3	
63	2015-03-01 20:19:13.934287+03	1	10	38	38. кальян на чаше - 3	3	
64	2015-03-01 20:19:13.942631+03	1	10	39	39. колба - 3	3	
65	2015-03-01 20:19:13.950895+03	1	10	40	40. замена чаши - 3	3	
66	2015-03-01 20:19:31.695909+03	1	10	32	32. Фирменные предложения от кальянщика - 3	3	
67	2015-03-01 20:19:31.719048+03	1	10	34	34. Кальяны на свежих фруктах - 3	3	
68	2015-03-01 20:19:31.727148+03	1	10	37	37. Прейскурант - 36	3	
69	2015-03-01 20:24:26.688817+03	1	24	9	Оплата картой - 0	1	
70	2015-03-01 20:36:00.740913+03	1	8	13	Возврат	1	
71	2015-03-01 20:36:04.809325+03	1	8	14	Персонал	1	
72	2015-03-01 20:40:53.444854+03	1	3	2	Pasha	3	
73	2015-03-01 20:40:53.46081+03	1	3	6	sasha	3	
74	2015-03-01 20:41:36.338455+03	1	24	3	Наименование заведения - Кафе 05	2	Изменен value.
75	2015-03-01 20:53:24.043031+03	1	10	9	9. Холодные закуски - 1	3	
76	2015-03-01 20:53:24.053446+03	1	10	4	4. Салаты - 1	3	
77	2015-03-01 20:53:24.061669+03	1	10	10	10. Горячие закуски - 1	3	
78	2015-03-01 20:53:24.070199+03	1	10	11	11. Восточные сладости - 1	3	
79	2015-03-01 20:53:24.078336+03	1	10	12	12. Десерты - 1	3	
80	2015-03-01 20:53:24.086698+03	1	10	14	14. Чай - 2	3	
81	2015-03-01 20:53:24.095001+03	1	10	15	15. Безалкогольные напитки 0,5 - 2	3	
82	2015-03-01 20:53:24.103329+03	1	10	16	16. Алкогольные коктейли - 2	3	
83	2015-03-01 20:53:24.111672+03	1	10	17	17. Безалкогольные коктейли - 2	3	
84	2015-03-01 20:53:24.119981+03	1	10	18	18. Молочные коктейли - 2	3	
85	2015-03-01 20:53:24.128315+03	1	10	19	19. Водка - 2	3	
86	2015-03-01 20:53:24.136651+03	1	10	20	20. Коньяк - 2	3	
87	2015-03-01 20:53:24.144986+03	1	10	21	21. Джин - 2	3	
88	2015-03-01 20:53:24.153307+03	1	10	22	22. Виски - 2	3	
89	2015-03-01 20:53:24.161634+03	1	10	23	23. Ром - 2	3	
90	2015-03-01 20:53:24.169952+03	1	10	24	24. Текила - 2	3	
91	2015-03-01 20:53:24.178284+03	1	10	25	25. Ликеры - 2	3	
92	2015-03-01 20:53:24.186636+03	1	10	26	26. Настойки - 2	3	
93	2015-03-01 20:53:24.194951+03	1	10	27	27. Вермуты - 2	3	
94	2015-03-01 20:53:24.20328+03	1	10	28	28. Игристые вина - 2	3	
95	2015-03-01 20:53:24.211613+03	1	10	29	29. Красные вина - 2	3	
96	2015-03-01 20:53:24.219997+03	1	10	30	30. Белое вино - 2	3	
97	2015-03-01 20:53:24.228321+03	1	10	31	31. Пиво - 2	3	
98	2015-03-01 20:53:24.236651+03	1	10	41	41. сок в ассортименте - 2	3	
99	2015-03-01 20:53:24.244987+03	1	10	42	42. мороженое - 2	3	
100	2015-03-01 20:53:24.253308+03	1	10	43	43. добавки - 1	3	
101	2015-03-01 20:54:44.823762+03	1	3	7	admin	1	
102	2015-03-01 20:54:58.437339+03	1	3	7	admin	2	Изменен is_staff,is_superuser и groups.
103	2015-03-01 20:56:49.342372+03	1	3	7	admin	2	Изменен first_name.
104	2015-03-01 21:02:28.360955+03	1	3	7	admin	2	Изменен groups.
105	2015-03-01 21:03:06.834817+03	1	3	7	admin	2	Изменен password.
106	2015-03-01 21:10:57.852194+03	7	3	7	admin	2	Изменен user_permissions.
107	2015-03-01 21:12:32.168397+03	1	16	14	Кухня - апельсины	3	
108	2015-03-01 21:12:32.178908+03	1	16	74	Кухня - бананы 	3	
109	2015-03-01 21:12:32.187116+03	1	16	13	Бар - берн	3	
110	2015-03-01 21:12:32.195359+03	1	16	84	Кухня - блинчики п/ф	3	
111	2015-03-01 21:12:32.20372+03	1	16	8	Бар - бонаква 0.5	3	
112	2015-03-01 21:12:32.212032+03	1	16	139	Кухня - бурма пахлава	3	
113	2015-03-01 21:12:32.22037+03	1	16	105	Кухня - ветчина	3	
114	2015-03-01 21:12:32.228705+03	1	16	52	Бар - вино бардолино	3	
115	2015-03-01 21:12:32.237037+03	1	16	98	Кухня - виноград	3	
116	2015-03-01 21:12:32.245355+03	1	16	57	Бар - вино карменер	3	
117	2015-03-01 21:12:32.253688+03	1	16	56	Бар - вино кастелано	3	
118	2015-03-01 21:12:32.262016+03	1	16	60	Бар - вино кастелано белое	3	
119	2015-03-01 21:12:32.27034+03	1	16	53	Бар - вино кьянти	3	
120	2015-03-01 21:12:32.27868+03	1	16	61	Бар - вино маркиз де альтило	3	
121	2015-03-01 21:12:32.287008+03	1	16	55	Бар - вино медок	3	
122	2015-03-01 21:12:32.295326+03	1	16	150	Бар - вино на розлив 150гр.	3	
123	2015-03-01 21:12:32.303733+03	1	16	58	Бар - вино пино гриджио	3	
124	2015-03-01 21:12:32.31205+03	1	16	63	Бар - вино савиньон боско	3	
125	2015-03-01 21:12:32.320375+03	1	16	54	Бар - вино санджовезе	3	
126	2015-03-01 21:12:32.328713+03	1	16	62	Бар - вино шабли	3	
127	2015-03-01 21:12:32.337037+03	1	16	59	Бар - вино шардоне	3	
128	2015-03-01 21:12:32.345363+03	1	16	81	Бар - виски балантинес	3	
129	2015-03-01 21:12:32.3537+03	1	16	29	Бар - виски блэк лейбл	3	
130	2015-03-01 21:12:32.362018+03	1	16	30	Бар - виски глен шир	3	
131	2015-03-01 21:12:32.37035+03	1	16	25	Бар - виски джек дениелс	3	
132	2015-03-01 21:12:32.378689+03	1	16	26	Бар - виски джемисом	3	
133	2015-03-01 21:12:32.387008+03	1	16	28	Бар - виски ред лейбл	3	
134	2015-03-01 21:12:32.395365+03	1	16	27	Бар - виски чивас	3	
135	2015-03-01 21:12:32.403698+03	1	16	10	Бар - витель 0.25	3	
136	2015-03-01 21:12:32.411998+03	1	16	20	Бар - водка белуга	3	
137	2015-03-01 21:12:32.420337+03	1	16	19	Бар - водка журавли	3	
138	2015-03-01 21:12:32.42867+03	1	16	3	Бар - водка мягков клюква	3	
139	2015-03-01 21:12:32.437064+03	1	16	17	Бар - водка мягков серебро	3	
140	2015-03-01 21:12:32.44551+03	1	16	18	Бар - водка пять озер	3	
141	2015-03-01 21:12:32.453827+03	1	16	15	Кухня - грейпфруты	3	
142	2015-03-01 21:12:32.462234+03	1	16	106	Кухня - грудинка копченая	3	
143	2015-03-01 21:12:32.470385+03	1	16	130	Кухня - груша свежая	3	
144	2015-03-01 21:12:32.478716+03	1	16	23	Бар - джин бифитер	3	
145	2015-03-01 21:12:32.48704+03	1	16	24	Бар - джин гордонс	3	
146	2015-03-01 21:12:32.49536+03	1	16	157	Бар - диван залит	3	
147	2015-03-01 21:12:32.503702+03	1	16	154	Бар - зарплата кальян	3	
148	2015-03-01 21:12:32.512017+03	1	16	153	Бар - зарплата оф	3	
149	2015-03-01 21:12:32.520342+03	1	16	135	Кухня - изюм	3	
150	2015-03-01 21:12:32.52868+03	1	16	122	Кухня - капуста пекинская	3	
151	2015-03-01 21:12:32.537009+03	1	16	103	Кухня - карбонат	3	
152	2015-03-01 21:12:32.545336+03	1	16	118	Кухня - карнишоны	3	
153	2015-03-01 21:12:32.553673+03	1	16	136	Кухня - кешью	3	
154	2015-03-01 21:12:32.562056+03	1	16	131	Кухня - киви	3	
155	2015-03-01 21:12:32.570409+03	1	16	5	Бар - кока кола 0.25	3	
156	2015-03-01 21:12:32.578725+03	1	16	4	Бар - кока кола 0.5	3	
157	2015-03-01 21:12:32.587045+03	1	16	77	Бар - кока кола розлив	3	
158	2015-03-01 21:12:32.595372+03	1	16	104	Кухня - колбаса с.к	3	
159	2015-03-01 21:12:32.603723+03	1	16	21	Бар - коньяк арарат	3	
160	2015-03-01 21:12:32.612024+03	1	16	22	Бар - коньяк мартель	3	
161	2015-03-01 21:12:32.620368+03	1	16	151	Бар - косячки	3	
162	2015-03-01 21:12:32.628695+03	1	16	123	Кухня - крабовые палочки	3	
163	2015-03-01 21:12:32.637018+03	1	16	85	Кухня - креветки к пиву маленькие	3	
164	2015-03-01 21:12:32.645343+03	1	16	127	Кухня - креветки тигровые	3	
165	2015-03-01 21:12:32.653703+03	1	16	124	Кухня - кукуруза консервированая	3	
166	2015-03-01 21:12:32.662004+03	1	16	133	Кухня - курага	3	
167	2015-03-01 21:12:32.67034+03	1	16	125	Кухня - куриное филе	3	
168	2015-03-01 21:12:32.67867+03	1	16	80	Бар - лайм свежий	3	
169	2015-03-01 21:12:32.687+03	1	16	51	Бар - ламбруско белое	3	
170	2015-03-01 21:12:32.695384+03	1	16	50	Бар - ламбруско красное	3	
171	2015-03-01 21:12:32.703721+03	1	16	42	Бар - ликер бейлис	3	
172	2015-03-01 21:12:32.712047+03	1	16	38	Бар - ликер калуа	3	
173	2015-03-01 21:12:32.720371+03	1	16	41	Бар - ликер куантро	3	
174	2015-03-01 21:12:32.728709+03	1	16	39	Бар - ликер малибу	3	
175	2015-03-01 21:12:32.737028+03	1	16	40	Бар - ликер самбука	3	
176	2015-03-01 21:12:32.74539+03	1	16	16	Кухня - лимоны	3	
177	2015-03-01 21:12:32.753698+03	1	16	107	Кухня - лист салата	3	
178	2015-03-01 21:12:32.762032+03	1	16	114	Кухня - лук зеленый	3	
179	2015-03-01 21:12:32.770349+03	1	16	116	Кухня - лук репчатый	3	
180	2015-03-01 21:12:32.778688+03	1	16	121	Кухня - майонез	3	
181	2015-03-01 21:12:32.787006+03	1	16	48	Бар - мартини асти	3	
182	2015-03-01 21:12:32.795337+03	1	16	45	Бар - мартини бьянко	3	
183	2015-03-01 21:12:32.803674+03	1	16	46	Бар - мартини россо	3	
184	2015-03-01 21:12:32.811994+03	1	16	47	Бар - мартини экстра драй	3	
185	2015-03-01 21:12:32.820396+03	1	16	108	Кухня - маслины	3	
186	2015-03-01 21:12:32.828727+03	1	16	73	Бар - молоко бар	3	
187	2015-03-01 21:12:32.837047+03	1	16	72	Бар - мороженое	3	
188	2015-03-01 21:12:32.845381+03	1	16	79	Бар - мята свежая	3	
189	2015-03-01 21:12:32.853715+03	1	16	44	Бар - настойка абсент	3	
190	2015-03-01 21:12:32.862032+03	1	16	43	Бар - настойка егермастер	3	
191	2015-03-01 21:12:32.870366+03	1	16	110	Кухня - огурец свежий	3	
192	2015-03-01 21:12:32.878697+03	1	16	117	Кухня - окорок куриный копченый	3	
193	2015-03-01 21:12:32.887027+03	1	16	99	Кухня - орех грецкий	3	
194	2015-03-01 21:12:32.89535+03	1	16	138	Кухня - пахлава	3	
195	2015-03-01 21:12:32.903699+03	1	16	112	Кухня - перец болгарский	3	
196	2015-03-01 21:12:32.912007+03	1	16	9	Бар - перье 0.33	3	
197	2015-03-01 21:12:32.920335+03	1	16	100	Кухня - петрушка свежая	3	
198	2015-03-01 21:12:32.928672+03	1	16	70	Бар - пиво бавария	3	
199	2015-03-01 21:12:32.937004+03	1	16	68	Бар - пиво гролш	3	
200	2015-03-01 21:12:32.945329+03	1	16	66	Бар - пиво козел светлый	3	
201	2015-03-01 21:12:32.953726+03	1	16	67	Бар - пиво козел темный	3	
202	2015-03-01 21:12:32.962041+03	1	16	65	Бар - пиво миллер	3	
203	2015-03-01 21:12:32.970451+03	1	16	64	Бар - пиво редс	3	
204	2015-03-01 21:12:32.97872+03	1	16	71	Бар - пиво розливное	3	
205	2015-03-01 21:12:32.987043+03	1	16	69	Бар - пиво эфес	3	
206	2015-03-01 21:12:32.995376+03	1	16	111	Кухня - помидор свежий	3	
207	2015-03-01 21:12:57.208783+03	1	16	109	Кухня - помидор черри	3	
208	2015-03-01 21:12:57.225974+03	1	16	155	Бар - расходники	3	
209	2015-03-01 21:12:57.234285+03	1	16	152	Кухня - РАХАТ ЛУКУМ В АССОРТИМЕНТЕ	3	
210	2015-03-01 21:12:57.242605+03	1	16	113	Кухня - редис свежий	3	
211	2015-03-01 21:12:57.250922+03	1	16	31	Бар - ром бакарди блэк	3	
212	2015-03-01 21:12:57.259261+03	1	16	33	Бар - ром бакарди голд	3	
213	2015-03-01 21:12:57.267629+03	1	16	32	Бар - ром бакарди супериор	3	
214	2015-03-01 21:12:57.275935+03	1	16	76	Бар - сироп барный	3	
215	2015-03-01 21:12:57.284274+03	1	16	128	Кухня - сметана	3	
216	2015-03-01 21:12:57.292606+03	1	16	78	Бар - сок в ассортименте	3	
217	2015-03-01 21:12:57.30093+03	1	16	7	Бар - спрайт 0.25	3	
218	2015-03-01 21:12:57.309252+03	1	16	6	Бар - спрайт 0.5	3	
219	2015-03-01 21:12:57.317586+03	1	16	49	Бар - спуманте просеко	3	
220	2015-03-01 21:12:57.325905+03	1	16	126	Кухня - сухари	3	
221	2015-03-01 21:12:57.33424+03	1	16	97	Кухня - сыр голандский	3	
222	2015-03-01 21:12:57.342578+03	1	16	102	Кухня - сыр Джугас	3	
223	2015-03-01 21:12:57.350902+03	1	16	95	Кухня - сыр мааздам	3	
224	2015-03-01 21:12:57.359229+03	1	16	96	Кухня - сыр охотничий	3	
225	2015-03-01 21:12:57.367568+03	1	16	115	Кухня - сыр фета	3	
226	2015-03-01 21:12:57.375886+03	1	16	35	Бар - текила ольмека голд	3	
227	2015-03-01 21:12:57.384216+03	1	16	34	Бар - текила ольмека серебро	3	
228	2015-03-01 21:12:57.392551+03	1	16	37	Бар - текила сауза голд	3	
229	2015-03-01 21:12:57.40095+03	1	16	36	Бар - текила сауза сильвер	3	
230	2015-03-01 21:12:57.40927+03	1	16	101	Кухня - укроп свежий	3	
231	2015-03-01 21:12:57.41761+03	1	16	12	Бар - фанта 0.25	3	
232	2015-03-01 21:12:57.425922+03	1	16	11	Бар - фанта 0.5	3	
233	2015-03-01 21:12:57.43425+03	1	16	132	Кухня - финики	3	
234	2015-03-01 21:12:57.442608+03	1	16	137	Кухня - фундук	3	
235	2015-03-01 21:12:57.45093+03	1	16	1	Бар - чай по 200 руб.	3	
236	2015-03-01 21:12:57.459247+03	1	16	2	Бар - чай по 250 руб.	3	
237	2015-03-01 21:12:57.467584+03	1	16	134	Кухня - чернослив	3	
238	2015-03-01 21:12:57.475897+03	1	16	149	Бар - швепс 0.5	3	
239	2015-03-01 21:12:57.484229+03	1	16	156	Бар - шоколад в ассортименте	3	
240	2015-03-01 21:12:57.492574+03	1	16	129	Кухня - яблоки свежие	3	
241	2015-03-01 21:12:57.50089+03	1	16	75	Бар - ягоды барные замороженые	3	
242	2015-03-01 21:12:57.509407+03	1	16	119	Кухня - яйцо куриное	3	
243	2015-03-01 21:12:57.517611+03	1	16	120	Кухня - яйцо перепилиное	3	
244	2015-03-01 21:14:22.84248+03	7	16	163	Кухня - мясо по франц	1	
245	2015-03-01 21:17:22.031503+03	1	24	10	Бронирование - 0	1	
246	2015-03-01 22:05:18.541044+03	1	24	11	Наличие пречека - 0	1	
247	2015-03-02 21:23:18.097133+03	7	16	164	Кухня - БОРЩ(ГОВЯДИНА) 	1	
248	2015-03-02 21:26:03.964173+03	7	24	12	подтверждение заказа  - 0	1	
249	2015-03-04 21:18:48.579824+03	1	24	13	Принтер - sewoo_lk_tl202	1	
250	2015-06-27 13:22:11.805466+03	1	3	7	admin	2	Changed is_active.
251	2015-06-27 13:27:48.880033+03	1	11	234	XИНКАЛ ПО-ДАГЕСТКИ (ГОВЯДИНА)	2	Changed name and check_name.
252	2015-06-27 13:29:11.714986+03	1	11	234	XИНКАЛ (ГОВЯДИНА)	2	Changed name.
253	2015-06-29 15:54:00.013221+03	1	3	1	caffein	2	Changed groups.
254	2015-06-29 17:21:36.632835+03	1	16	163	Кухня - мясо по франц	3	
255	2015-06-29 17:22:33.716361+03	1	16	165	Кухня - ХАРЧО ГОВЯДИНА	1	
256	2015-06-29 17:22:41.563612+03	1	16	164	Кухня - БОРЩ(ГОВЯДИНА) 	2	No fields changed.
257	2015-06-29 17:22:52.994703+03	1	16	165	Кухня - ХАРЧО ГОВЯДИНА	2	Changed portion.
\.


--
-- Name: django_admin_log_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('django_admin_log_id_seq', 257, true);


--
-- Data for Name: django_content_type; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY django_content_type (id, name, app_label, model) FROM stdin;
1	permission	auth	permission
2	group	auth	group
3	user	auth	user
4	content type	contenttypes	contenttype
5	session	sessions	session
6	site	sites	site
7	log entry	admin	logentry
8	stol	cafe	stol
9	bron	cafe	bron
10	menu	cafe	menu
11	usluga	cafe	usluga
12	schet	cafe	schet
13	schet history	cafe	schethistory
14	zakaz	cafe	zakaz
15	zakaz history	cafe	zakazhistory
16	tovar	cafe	tovar
17	prihod	cafe	prihod
18	rashod	cafe	rashod
19	sklad	cafe	sklad
20	sostav	cafe	sostav
21	skidka	cafe	skidka
22	playlist	cafe	playlist
23	music	cafe	music
24	option	cafe	option
25	printer	cafe	printer
26	rashod cat	cafe	rashodcat
27	dop rashod	cafe	doprashod
28	smena	cafe	smena
29	vozvrat	cafe	vozvrat
\.


--
-- Name: django_content_type_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('django_content_type_id_seq', 31, true);


--
-- Data for Name: django_migrations; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY django_migrations (id, app, name, applied) FROM stdin;
1	contenttypes	0001_initial	2015-06-27 12:46:31.202693+03
2	auth	0001_initial	2015-06-27 12:46:31.238004+03
3	admin	0001_initial	2015-06-27 12:46:31.267281+03
4	contenttypes	0002_remove_content_type_name	2015-06-27 12:46:31.295208+03
5	auth	0002_alter_permission_name_max_length	2015-06-27 12:46:31.325442+03
6	auth	0003_alter_user_email_max_length	2015-06-27 12:46:31.353385+03
7	auth	0004_alter_user_username_opts	2015-06-27 12:46:31.382091+03
8	auth	0005_alter_user_last_login_null	2015-06-27 12:46:31.409997+03
9	auth	0006_require_contenttypes_0002	2015-06-27 12:46:31.438707+03
10	sessions	0001_initial	2015-06-27 12:46:31.467581+03
11	sites	0001_initial	2015-06-27 12:46:31.495934+03
\.


--
-- Name: django_migrations_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('django_migrations_id_seq', 11, true);


--
-- Data for Name: django_session; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY django_session (session_key, session_data, expire_date) FROM stdin;
6jnqi3t2tt478kmucft4dfsyqz93p101	ZGVhMWZkYWU0ZTM1ZDdjNzNmMjViMDRlODk5MTY0NGEwMmI2YjFjMjp7Il9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX2F1dGhfdXNlcl9pZCI6NH0=	2014-10-01 13:11:46.997499+03
fwi8z0eko6u3g1a7jee5v3v2z8iso02a	OWVlYWE5MmY0ODY0NjJkYmZlNTRkZjE2MjA3OWIxZGRiYjJjYjc4YTp7fQ==	2014-09-19 01:16:10.389665+03
ndy2qbo4epbuz2qh5vxmr95jo5yrrhwd	NGE4OTQ5YjlkMjFiZjdiNGUxZGM2MmI4YmJjZjhiNzc5ODM3NzJmOTp7InRlc3Rjb29raWUiOiJ3b3JrZWQiLCJfYXV0aF91c2VyX2lkIjo2LCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCJ9	2014-08-03 00:18:58.636928+03
5yxd794znhkq1juh305m1b51w4z32zpz	OWVlYWE5MmY0ODY0NjJkYmZlNTRkZjE2MjA3OWIxZGRiYjJjYjc4YTp7fQ==	2014-08-10 03:22:13.327435+03
hafs4o7c5bql4gz4q7ldi8qmu5f2k1em	MGYwMWNjMWY0MWE1MTBjZTEzMDRlNzExYzYxOThlYWEyODQ5Y2JlZTp7Il9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX2F1dGhfdXNlcl9pZCI6M30=	2014-09-19 13:28:13.690983+03
kf3eprp0m7l6kjxgyqybt3u0awemlb2n	OWVlYWE5MmY0ODY0NjJkYmZlNTRkZjE2MjA3OWIxZGRiYjJjYjc4YTp7fQ==	2014-08-23 03:07:19.426013+03
b8guix193ijxbtpqxtivi0cr5dve8e6y	OWVlYWE5MmY0ODY0NjJkYmZlNTRkZjE2MjA3OWIxZGRiYjJjYjc4YTp7fQ==	2014-08-29 01:12:11.113134+03
j290eblk2sk2pbuwok7ps9eqk2ghyoq0	ZmQxZjk0N2ViYmNlMWNmYmRiODg1M2Q2YzM3NTkxNDVhY2JiNjY4ODp7Il9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX2F1dGhfdXNlcl9pZCI6Nn0=	2014-09-21 02:55:22.458676+03
b0jwp8rizekr0aex7u6esyercexvf0bs	OWVlYWE5MmY0ODY0NjJkYmZlNTRkZjE2MjA3OWIxZGRiYjJjYjc4YTp7fQ==	2014-09-28 03:43:54.923211+03
hedjzohe8lmrjkjk6fhgqecd5ud0uksc	OWVlYWE5MmY0ODY0NjJkYmZlNTRkZjE2MjA3OWIxZGRiYjJjYjc4YTp7fQ==	2014-09-11 01:26:59.158983+03
ctq49eqmfn95ae11p2wqiuugy8n8vh2u	YjY4MmVhYTI0MDI1YWJiNjQ3ZTFkMDc0NGVkNWNkMWE5N2RiZTQ4Yjp7Il9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX2F1dGhfdXNlcl9pZCI6Mn0=	2014-12-23 20:09:13.805367+03
vazvf0rdhk8g4p0us6iovrut6z0pxtrz	YjY4MmVhYTI0MDI1YWJiNjQ3ZTFkMDc0NGVkNWNkMWE5N2RiZTQ4Yjp7Il9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX2F1dGhfdXNlcl9pZCI6Mn0=	2014-12-27 16:37:49.474653+03
8on2yfmxk1kv0q8blqpmx7bir6l20e5q	OWVlYWE5MmY0ODY0NjJkYmZlNTRkZjE2MjA3OWIxZGRiYjJjYjc4YTp7fQ==	2014-12-27 18:13:53.837916+03
7xq7ihstmeqe2mznlyky71n2gvat19dm	YjY4MmVhYTI0MDI1YWJiNjQ3ZTFkMDc0NGVkNWNkMWE5N2RiZTQ4Yjp7Il9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX2F1dGhfdXNlcl9pZCI6Mn0=	2014-12-29 20:10:16.290854+03
0saq5mub3y920s72qomt30gzalpu1790	YjY4MmVhYTI0MDI1YWJiNjQ3ZTFkMDc0NGVkNWNkMWE5N2RiZTQ4Yjp7Il9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX2F1dGhfdXNlcl9pZCI6Mn0=	2014-12-29 22:56:50.339838+03
g1m1l08qqy6ym51j43czq7pl5e0cpgiv	YjY4MmVhYTI0MDI1YWJiNjQ3ZTFkMDc0NGVkNWNkMWE5N2RiZTQ4Yjp7Il9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX2F1dGhfdXNlcl9pZCI6Mn0=	2015-02-18 20:09:32.630393+03
mbr0xq9lpsasbx9e4iwhyku5rxs64gl7	ZWMxNjk3NjYzODkxNDA1Y2Q5ZTJmOWMzMDk3OTk3NTIwZTkwYmIzNTp7Il9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX2F1dGhfdXNlcl9pZCI6MSwiX21lc3NhZ2VzIjoiW1tcIl9fanNvbl9tZXNzYWdlXCIsMCwyNSxcIm9wdGlvbiBcXFwiXFx1MDQxZFxcdTA0MzBcXHUwNDM4XFx1MDQzY1xcdTA0MzVcXHUwNDNkXFx1MDQzZVxcdTA0MzJcXHUwNDMwXFx1MDQzZFxcdTA0MzhcXHUwNDM1IFxcdTA0MzdcXHUwNDMwXFx1MDQzMlxcdTA0MzVcXHUwNDM0XFx1MDQzNVxcdTA0M2RcXHUwNDM4XFx1MDQ0ZiAtIFxcdTA0MWFcXHUwNDMwXFx1MDQ0NFxcdTA0MzUgMDVcXFwiIFxcdTA0MzFcXHUwNDRiXFx1MDQzYiBcXHUwNDQzXFx1MDQ0MVxcdTA0M2ZcXHUwNDM1XFx1MDQ0OFxcdTA0M2RcXHUwNDNlIFxcdTA0MzhcXHUwNDM3XFx1MDQzY1xcdTA0MzVcXHUwNDNkXFx1MDQzNVxcdTA0M2QuXCJdLFtcIl9fanNvbl9tZXNzYWdlXCIsMCwyNSxcIlxcdTA0MjNcXHUwNDQxXFx1MDQzZlxcdTA0MzVcXHUwNDQ4XFx1MDQzZFxcdTA0M2UgXFx1MDQ0M1xcdTA0MzRcXHUwNDMwXFx1MDQzYlxcdTA0MzVcXHUwNDNkXFx1MDQ0YiAyNiBcXHUwNDFjXFx1MDQzNVxcdTA0M2RcXHUwNDRlLlwiXSxbXCJfX2pzb25fbWVzc2FnZVwiLDAsMjUsXCJcXHUwNDNmXFx1MDQzZVxcdTA0M2JcXHUwNDRjXFx1MDQzN1xcdTA0M2VcXHUwNDMyXFx1MDQzMFxcdTA0NDJcXHUwNDM1XFx1MDQzYlxcdTA0NGMgXFxcImFkbWluXFxcIiBcXHUwNDMxXFx1MDQ0YlxcdTA0M2IgXFx1MDQ0M1xcdTA0NDFcXHUwNDNmXFx1MDQzNVxcdTA0NDhcXHUwNDNkXFx1MDQzZSBcXHUwNDM0XFx1MDQzZVxcdTA0MzFcXHUwNDMwXFx1MDQzMlxcdTA0M2JcXHUwNDM1XFx1MDQzZC4gXFx1MDQxZFxcdTA0MzhcXHUwNDM2XFx1MDQzNSBcXHUwNDMyXFx1MDQ0YiBcXHUwNDNjXFx1MDQzZVxcdTA0MzZcXHUwNDM1XFx1MDQ0MlxcdTA0MzUgXFx1MDQ0MVxcdTA0M2RcXHUwNDNlXFx1MDQzMlxcdTA0MzAgXFx1MDQzNVxcdTA0MzNcXHUwNDNlIFxcdTA0M2VcXHUwNDQyXFx1MDQ0MFxcdTA0MzVcXHUwNDM0XFx1MDQzMFxcdTA0M2FcXHUwNDQyXFx1MDQzOFxcdTA0NDBcXHUwNDNlXFx1MDQzMlxcdTA0MzBcXHUwNDQyXFx1MDQ0Yy5cIl0sW1wiX19qc29uX21lc3NhZ2VcIiwwLDI1LFwiXFx1MDQzZlxcdTA0M2VcXHUwNDNiXFx1MDQ0Y1xcdTA0MzdcXHUwNDNlXFx1MDQzMlxcdTA0MzBcXHUwNDQyXFx1MDQzNVxcdTA0M2JcXHUwNDRjIFxcXCJhZG1pblxcXCIgXFx1MDQzMVxcdTA0NGJcXHUwNDNiIFxcdTA0NDNcXHUwNDQxXFx1MDQzZlxcdTA0MzVcXHUwNDQ4XFx1MDQzZFxcdTA0M2UgXFx1MDQzOFxcdTA0MzdcXHUwNDNjXFx1MDQzNVxcdTA0M2RcXHUwNDM1XFx1MDQzZC5cIl0sW1wiX19qc29uX21lc3NhZ2VcIiwwLDI1LFwiXFx1MDQzZlxcdTA0M2VcXHUwNDNiXFx1MDQ0Y1xcdTA0MzdcXHUwNDNlXFx1MDQzMlxcdTA0MzBcXHUwNDQyXFx1MDQzNVxcdTA0M2JcXHUwNDRjIFxcXCJhZG1pblxcXCIgXFx1MDQzMVxcdTA0NGJcXHUwNDNiIFxcdTA0NDNcXHUwNDQxXFx1MDQzZlxcdTA0MzVcXHUwNDQ4XFx1MDQzZFxcdTA0M2UgXFx1MDQzOFxcdTA0MzdcXHUwNDNjXFx1MDQzNVxcdTA0M2RcXHUwNDM1XFx1MDQzZC5cIl0sW1wiX19qc29uX21lc3NhZ2VcIiwwLDI1LFwiXFx1MDQzZlxcdTA0M2VcXHUwNDNiXFx1MDQ0Y1xcdTA0MzdcXHUwNDNlXFx1MDQzMlxcdTA0MzBcXHUwNDQyXFx1MDQzNVxcdTA0M2JcXHUwNDRjIFxcXCJhZG1pblxcXCIgXFx1MDQzMVxcdTA0NGJcXHUwNDNiIFxcdTA0NDNcXHUwNDQxXFx1MDQzZlxcdTA0MzVcXHUwNDQ4XFx1MDQzZFxcdTA0M2UgXFx1MDQzOFxcdTA0MzdcXHUwNDNjXFx1MDQzNVxcdTA0M2RcXHUwNDM1XFx1MDQzZC5cIl0sW1wiX19qc29uX21lc3NhZ2VcIiwwLDI1LFwiXFx1MDQxZlxcdTA0MzBcXHUwNDQwXFx1MDQzZVxcdTA0M2JcXHUwNDRjIFxcdTA0NDNcXHUwNDQxXFx1MDQzZlxcdTA0MzVcXHUwNDQ4XFx1MDQzZFxcdTA0M2UgXFx1MDQzOFxcdTA0MzdcXHUwNDNjXFx1MDQzNVxcdTA0M2RcXHUwNDM1XFx1MDQzZC5cIl0sW1wiX19qc29uX21lc3NhZ2VcIiwwLDMwLFwiXFx1MDQyN1xcdTA0NDJcXHUwNDNlXFx1MDQzMVxcdTA0NGIgXFx1MDQzZlxcdTA0NDBcXHUwNDNlXFx1MDQzOFxcdTA0MzdcXHUwNDMyXFx1MDQzNVxcdTA0NDFcXHUwNDQyXFx1MDQzOCBcXHUwNDM0XFx1MDQzNVxcdTA0MzlcXHUwNDQxXFx1MDQ0MlxcdTA0MzJcXHUwNDM4XFx1MDQ0ZiBcXHUwNDNkXFx1MDQzMFxcdTA0MzQgXFx1MDQzZVxcdTA0MzFcXHUwNDRhXFx1MDQzNVxcdTA0M2FcXHUwNDQyXFx1MDQzMFxcdTA0M2NcXHUwNDM4LCBcXHUwNDNkXFx1MDQzNVxcdTA0M2VcXHUwNDMxXFx1MDQ0NVxcdTA0M2VcXHUwNDM0XFx1MDQzOFxcdTA0M2NcXHUwNDNlIFxcdTA0MzhcXHUwNDQ1IFxcdTA0MzJcXHUwNDRiXFx1MDQzMVxcdTA0NDBcXHUwNDMwXFx1MDQ0MlxcdTA0NGMuIFxcdTA0MWVcXHUwNDMxXFx1MDQ0YVxcdTA0MzVcXHUwNDNhXFx1MDQ0MlxcdTA0NGIgXFx1MDQzZFxcdTA0MzUgXFx1MDQzMVxcdTA0NGJcXHUwNDNiXFx1MDQzOCBcXHUwNDM4XFx1MDQzN1xcdTA0M2NcXHUwNDM1XFx1MDQzZFxcdTA0MzVcXHUwNDNkXFx1MDQ0Yi5cIl0sW1wiX19qc29uX21lc3NhZ2VcIiwwLDI1LFwiXFx1MDQyM1xcdTA0NDFcXHUwNDNmXFx1MDQzNVxcdTA0NDhcXHUwNDNkXFx1MDQzZSBcXHUwNDQzXFx1MDQzNFxcdTA0MzBcXHUwNDNiXFx1MDQzNVxcdTA0M2RcXHUwNDRiIDEwMCBcXHUwNDE4XFx1MDQzZFxcdTA0MzNcXHUwNDQwXFx1MDQzOFxcdTA0MzRcXHUwNDM4XFx1MDQzNVxcdTA0M2RcXHUwNDQyXFx1MDQ0Yi5cIl0sW1wiX19qc29uX21lc3NhZ2VcIiwwLDI1LFwiXFx1MDQyM1xcdTA0NDFcXHUwNDNmXFx1MDQzNVxcdTA0NDhcXHUwNDNkXFx1MDQzZSBcXHUwNDQzXFx1MDQzNFxcdTA0MzBcXHUwNDNiXFx1MDQzNVxcdTA0M2RcXHUwNDRiIDM3IFxcdTA0MThcXHUwNDNkXFx1MDQzM1xcdTA0NDBcXHUwNDM4XFx1MDQzNFxcdTA0MzhcXHUwNDM1XFx1MDQzZFxcdTA0NDJcXHUwNDRiLlwiXSxbXCJfX2pzb25fbWVzc2FnZVwiLDAsMjUsXCJvcHRpb24gXFxcIlxcdTA0MTFcXHUwNDQwXFx1MDQzZVxcdTA0M2RcXHUwNDM4XFx1MDQ0MFxcdTA0M2VcXHUwNDMyXFx1MDQzMFxcdTA0M2RcXHUwNDM4XFx1MDQzNSAtIDBcXFwiIFxcdTA0MzFcXHUwNDRiXFx1MDQzYiBcXHUwNDQzXFx1MDQ0MVxcdTA0M2ZcXHUwNDM1XFx1MDQ0OFxcdTA0M2RcXHUwNDNlIFxcdTA0MzRcXHUwNDNlXFx1MDQzMVxcdTA0MzBcXHUwNDMyXFx1MDQzYlxcdTA0MzVcXHUwNDNkLlwiXSxbXCJfX2pzb25fbWVzc2FnZVwiLDAsMjUsXCJvcHRpb24gXFxcIlxcdTA0MWRcXHUwNDMwXFx1MDQzYlxcdTA0MzhcXHUwNDQ3XFx1MDQzOFxcdTA0MzUgXFx1MDQzZlxcdTA0NDBcXHUwNDM1XFx1MDQ0N1xcdTA0MzVcXHUwNDNhXFx1MDQzMCAtIDBcXFwiIFxcdTA0MzFcXHUwNDRiXFx1MDQzYiBcXHUwNDQzXFx1MDQ0MVxcdTA0M2ZcXHUwNDM1XFx1MDQ0OFxcdTA0M2RcXHUwNDNlIFxcdTA0MzRcXHUwNDNlXFx1MDQzMVxcdTA0MzBcXHUwNDMyXFx1MDQzYlxcdTA0MzVcXHUwNDNkLlwiXV0ifQ==	2015-03-15 22:05:18.588508+03
ek37x05f91oqeaaky7p4wdor09813q71	ZDk5ZDk0MzZiZWY5NTBhYWU4OGEzOThlYzRhYmEzM2Y1Zjk3ZGYxMTp7Il9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX2F1dGhfdXNlcl9pZCI6N30=	2015-03-15 22:33:12.089235+03
pq1up82s86ofif765enuio84j0ghy3el	ZDk5ZDk0MzZiZWY5NTBhYWU4OGEzOThlYzRhYmEzM2Y1Zjk3ZGYxMTp7Il9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX2F1dGhfdXNlcl9pZCI6N30=	2015-03-16 21:20:37.943587+03
4je70zqh5f3971j1q1j50lgbtnt5p6yk	ZjgxMmIyNmFlMGY3NjRkMWFkZGU4ZTQ2ZGE5OWFjYzA4ZWJlZjc4Njp7Il9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX2F1dGhfdXNlcl9pZCI6NX0=	2015-03-17 22:41:02.620495+03
wbgugjrxn44d8mv2usee8rhsnowjjwoq	ZjljNDFlOTlkZmU2MWM1MWZmYzU4NTNiNzc0OTViNDc2Yjc0Njg0ZDp7Il9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX2F1dGhfdXNlcl9pZCI6MX0=	2015-03-18 21:17:52.295158+03
sj9ap1bugzanvswlenb3dofq8rrvagv8	ZjgxMmIyNmFlMGY3NjRkMWFkZGU4ZTQ2ZGE5OWFjYzA4ZWJlZjc4Njp7Il9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX2F1dGhfdXNlcl9pZCI6NX0=	2015-03-19 20:10:13.873759+03
bpcg61caprtivimmedtl15o11vj1dth9	OWVlYWE5MmY0ODY0NjJkYmZlNTRkZjE2MjA3OWIxZGRiYjJjYjc4YTp7fQ==	2015-03-19 21:17:56.079316+03
50pg35bykgdixu2nm7fr6i1iwfvfyffp	ZjgxMmIyNmFlMGY3NjRkMWFkZGU4ZTQ2ZGE5OWFjYzA4ZWJlZjc4Njp7Il9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX2F1dGhfdXNlcl9pZCI6NX0=	2015-03-19 21:32:10.622173+03
wpdu31gctzrzqpyg3cho5x7c00qnblsi	ZjgxMmIyNmFlMGY3NjRkMWFkZGU4ZTQ2ZGE5OWFjYzA4ZWJlZjc4Njp7Il9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX2F1dGhfdXNlcl9pZCI6NX0=	2015-03-20 00:40:03.046237+03
y9bu20zfw2x00g3j83u15jnc1lgqhe7h	NTZkZmUwOWI3NjAyNDdlNTlkY2IwZDIwYWNlZTE4OGY1ZDhjYzA1Zjp7Il9hdXRoX3VzZXJfaGFzaCI6IjM1ZTViNGI4MzZjZTFjZjJiZWRhOTI3MDUyNzA4NGJmOGVjMTUxMjMiLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCIsIl9hdXRoX3VzZXJfaWQiOiIxIn0=	2015-07-11 13:08:48.325244+03
\.


--
-- Data for Name: django_site; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY django_site (id, domain, name) FROM stdin;
1	example.com	example.com
\.


--
-- Name: django_site_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('django_site_id_seq', 1, true);


--
-- Name: auth_group_name_key; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY auth_group
    ADD CONSTRAINT auth_group_name_key UNIQUE (name);


--
-- Name: auth_group_permissions_group_id_permission_id_key; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY auth_group_permissions
    ADD CONSTRAINT auth_group_permissions_group_id_permission_id_key UNIQUE (group_id, permission_id);


--
-- Name: auth_group_permissions_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY auth_group_permissions
    ADD CONSTRAINT auth_group_permissions_pkey PRIMARY KEY (id);


--
-- Name: auth_group_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY auth_group
    ADD CONSTRAINT auth_group_pkey PRIMARY KEY (id);


--
-- Name: auth_permission_content_type_id_codename_key; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY auth_permission
    ADD CONSTRAINT auth_permission_content_type_id_codename_key UNIQUE (content_type_id, codename);


--
-- Name: auth_permission_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY auth_permission
    ADD CONSTRAINT auth_permission_pkey PRIMARY KEY (id);


--
-- Name: auth_user_groups_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY auth_user_groups
    ADD CONSTRAINT auth_user_groups_pkey PRIMARY KEY (id);


--
-- Name: auth_user_groups_user_id_group_id_key; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY auth_user_groups
    ADD CONSTRAINT auth_user_groups_user_id_group_id_key UNIQUE (user_id, group_id);


--
-- Name: auth_user_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY auth_user
    ADD CONSTRAINT auth_user_pkey PRIMARY KEY (id);


--
-- Name: auth_user_user_permissions_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY auth_user_user_permissions
    ADD CONSTRAINT auth_user_user_permissions_pkey PRIMARY KEY (id);


--
-- Name: auth_user_user_permissions_user_id_permission_id_key; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY auth_user_user_permissions
    ADD CONSTRAINT auth_user_user_permissions_user_id_permission_id_key UNIQUE (user_id, permission_id);


--
-- Name: auth_user_username_key; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY auth_user
    ADD CONSTRAINT auth_user_username_key UNIQUE (username);


--
-- Name: cafe_bron_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY cafe_bron
    ADD CONSTRAINT cafe_bron_pkey PRIMARY KEY (id);


--
-- Name: cafe_deposit_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY cafe_deposit
    ADD CONSTRAINT cafe_deposit_pkey PRIMARY KEY (id);


--
-- Name: cafe_doprashod_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY cafe_doprashod
    ADD CONSTRAINT cafe_doprashod_pkey PRIMARY KEY (id);


--
-- Name: cafe_menu_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY cafe_menu
    ADD CONSTRAINT cafe_menu_pkey PRIMARY KEY (id);


--
-- Name: cafe_music_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY cafe_music
    ADD CONSTRAINT cafe_music_pkey PRIMARY KEY (id);


--
-- Name: cafe_option_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY cafe_option
    ADD CONSTRAINT cafe_option_pkey PRIMARY KEY (id);


--
-- Name: cafe_playlist_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY cafe_playlist
    ADD CONSTRAINT cafe_playlist_pkey PRIMARY KEY (id);


--
-- Name: cafe_prihod_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY cafe_prihod
    ADD CONSTRAINT cafe_prihod_pkey PRIMARY KEY (id);


--
-- Name: cafe_printer_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY cafe_printer
    ADD CONSTRAINT cafe_printer_pkey PRIMARY KEY (id);


--
-- Name: cafe_rashod_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY cafe_rashod
    ADD CONSTRAINT cafe_rashod_pkey PRIMARY KEY (id);


--
-- Name: cafe_rashodcat_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY cafe_rashodcat
    ADD CONSTRAINT cafe_rashodcat_pkey PRIMARY KEY (id);


--
-- Name: cafe_schet_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY cafe_schet
    ADD CONSTRAINT cafe_schet_pkey PRIMARY KEY (id);


--
-- Name: cafe_schethistory_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY cafe_schethistory
    ADD CONSTRAINT cafe_schethistory_pkey PRIMARY KEY (id);


--
-- Name: cafe_skidka_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY cafe_skidka
    ADD CONSTRAINT cafe_skidka_pkey PRIMARY KEY (id);


--
-- Name: cafe_sklad_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY cafe_sklad
    ADD CONSTRAINT cafe_sklad_pkey PRIMARY KEY (id);


--
-- Name: cafe_smena_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY cafe_smena
    ADD CONSTRAINT cafe_smena_pkey PRIMARY KEY (id);


--
-- Name: cafe_sostav_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY cafe_sostav
    ADD CONSTRAINT cafe_sostav_pkey PRIMARY KEY (id);


--
-- Name: cafe_stol_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY cafe_stol
    ADD CONSTRAINT cafe_stol_pkey PRIMARY KEY (id);


--
-- Name: cafe_tovar_name_key; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY cafe_tovar
    ADD CONSTRAINT cafe_tovar_name_key UNIQUE (name);


--
-- Name: cafe_tovar_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY cafe_tovar
    ADD CONSTRAINT cafe_tovar_pkey PRIMARY KEY (id);


--
-- Name: cafe_usluga_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY cafe_usluga
    ADD CONSTRAINT cafe_usluga_pkey PRIMARY KEY (id);


--
-- Name: cafe_vozvrat_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY cafe_vozvrat
    ADD CONSTRAINT cafe_vozvrat_pkey PRIMARY KEY (id);


--
-- Name: cafe_zakaz_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY cafe_zakaz
    ADD CONSTRAINT cafe_zakaz_pkey PRIMARY KEY (id);


--
-- Name: cafe_zakazhistory_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY cafe_zakazhistory
    ADD CONSTRAINT cafe_zakazhistory_pkey PRIMARY KEY (id);


--
-- Name: django_admin_log_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY django_admin_log
    ADD CONSTRAINT django_admin_log_pkey PRIMARY KEY (id);


--
-- Name: django_content_type_app_label_model_key; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY django_content_type
    ADD CONSTRAINT django_content_type_app_label_model_key UNIQUE (app_label, model);


--
-- Name: django_content_type_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY django_content_type
    ADD CONSTRAINT django_content_type_pkey PRIMARY KEY (id);


--
-- Name: django_migrations_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY django_migrations
    ADD CONSTRAINT django_migrations_pkey PRIMARY KEY (id);


--
-- Name: django_session_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY django_session
    ADD CONSTRAINT django_session_pkey PRIMARY KEY (session_key);


--
-- Name: django_site_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY django_site
    ADD CONSTRAINT django_site_pkey PRIMARY KEY (id);


--
-- Name: auth_group_name_like; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX auth_group_name_like ON auth_group USING btree (name varchar_pattern_ops);


--
-- Name: auth_group_permissions_group_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX auth_group_permissions_group_id ON auth_group_permissions USING btree (group_id);


--
-- Name: auth_group_permissions_permission_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX auth_group_permissions_permission_id ON auth_group_permissions USING btree (permission_id);


--
-- Name: auth_permission_content_type_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX auth_permission_content_type_id ON auth_permission USING btree (content_type_id);


--
-- Name: auth_user_groups_group_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX auth_user_groups_group_id ON auth_user_groups USING btree (group_id);


--
-- Name: auth_user_groups_user_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX auth_user_groups_user_id ON auth_user_groups USING btree (user_id);


--
-- Name: auth_user_user_permissions_permission_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX auth_user_user_permissions_permission_id ON auth_user_user_permissions USING btree (permission_id);


--
-- Name: auth_user_user_permissions_user_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX auth_user_user_permissions_user_id ON auth_user_user_permissions USING btree (user_id);


--
-- Name: auth_user_username_like; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX auth_user_username_like ON auth_user USING btree (username varchar_pattern_ops);


--
-- Name: cafe_bron_stol_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX cafe_bron_stol_id ON cafe_bron USING btree (stol_id);


--
-- Name: cafe_deposit_97f91e86; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX cafe_deposit_97f91e86 ON cafe_deposit USING btree (schet_id);


--
-- Name: cafe_deposit_e8701ad4; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX cafe_deposit_e8701ad4 ON cafe_deposit USING btree (user_id);


--
-- Name: cafe_doprashod_cat_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX cafe_doprashod_cat_id ON cafe_doprashod USING btree (cat_id);


--
-- Name: cafe_music_playlist_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX cafe_music_playlist_id ON cafe_music USING btree (playlist_id);


--
-- Name: cafe_prihod_tovar_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX cafe_prihod_tovar_id ON cafe_prihod USING btree (tovar_id);


--
-- Name: cafe_prihod_user_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX cafe_prihod_user_id ON cafe_prihod USING btree (user_id);


--
-- Name: cafe_rashod_tovar_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX cafe_rashod_tovar_id ON cafe_rashod USING btree (tovar_id);


--
-- Name: cafe_rashod_user_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX cafe_rashod_user_id ON cafe_rashod USING btree (user_id);


--
-- Name: cafe_rashod_usluga_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX cafe_rashod_usluga_id ON cafe_rashod USING btree (usluga_id);


--
-- Name: cafe_schet_stol_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX cafe_schet_stol_id ON cafe_schet USING btree (stol_id);


--
-- Name: cafe_schet_user_close_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX cafe_schet_user_close_id ON cafe_schet USING btree (user_close_id);


--
-- Name: cafe_schet_user_open_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX cafe_schet_user_open_id ON cafe_schet USING btree (user_open_id);


--
-- Name: cafe_schethistory_schet_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX cafe_schethistory_schet_id ON cafe_schethistory USING btree (schet_id);


--
-- Name: cafe_schethistory_user_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX cafe_schethistory_user_id ON cafe_schethistory USING btree (user_id);


--
-- Name: cafe_sklad_tovar_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX cafe_sklad_tovar_id ON cafe_sklad USING btree (tovar_id);


--
-- Name: cafe_smena_user_close_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX cafe_smena_user_close_id ON cafe_smena USING btree (user_close_id);


--
-- Name: cafe_sostav_tovar_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX cafe_sostav_tovar_id ON cafe_sostav USING btree (tovar_id);


--
-- Name: cafe_sostav_usluga_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX cafe_sostav_usluga_id ON cafe_sostav USING btree (usluga_id);


--
-- Name: cafe_tovar_menu_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX cafe_tovar_menu_id ON cafe_tovar USING btree (menu_id);


--
-- Name: cafe_tovar_name_like; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX cafe_tovar_name_like ON cafe_tovar USING btree (name varchar_pattern_ops);


--
-- Name: cafe_usluga_menu_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX cafe_usluga_menu_id ON cafe_usluga USING btree (menu_id);


--
-- Name: cafe_vozvrat_user_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX cafe_vozvrat_user_id ON cafe_vozvrat USING btree (user_id);


--
-- Name: cafe_vozvrat_usluga_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX cafe_vozvrat_usluga_id ON cafe_vozvrat USING btree (usluga_id);


--
-- Name: cafe_zakaz_menu_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX cafe_zakaz_menu_id ON cafe_zakaz USING btree (menu_id);


--
-- Name: cafe_zakaz_schet_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX cafe_zakaz_schet_id ON cafe_zakaz USING btree (schet_id);


--
-- Name: cafe_zakaz_usluga_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX cafe_zakaz_usluga_id ON cafe_zakaz USING btree (usluga_id);


--
-- Name: cafe_zakazhistory_menu_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX cafe_zakazhistory_menu_id ON cafe_zakazhistory USING btree (menu_id);


--
-- Name: cafe_zakazhistory_schet_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX cafe_zakazhistory_schet_id ON cafe_zakazhistory USING btree (schet_id);


--
-- Name: cafe_zakazhistory_user_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX cafe_zakazhistory_user_id ON cafe_zakazhistory USING btree (user_id);


--
-- Name: cafe_zakazhistory_usluga_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX cafe_zakazhistory_usluga_id ON cafe_zakazhistory USING btree (usluga_id);


--
-- Name: cafe_zakazhistory_zakaz_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX cafe_zakazhistory_zakaz_id ON cafe_zakazhistory USING btree (zakaz_id);


--
-- Name: django_admin_log_content_type_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX django_admin_log_content_type_id ON django_admin_log USING btree (content_type_id);


--
-- Name: django_admin_log_user_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX django_admin_log_user_id ON django_admin_log USING btree (user_id);


--
-- Name: django_session_expire_date; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX django_session_expire_date ON django_session USING btree (expire_date);


--
-- Name: django_session_session_key_like; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX django_session_session_key_like ON django_session USING btree (session_key varchar_pattern_ops);


--
-- Name: triggerRashod; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER "triggerRashod" AFTER INSERT ON cafe_zakazhistory FOR EACH ROW EXECUTE PROCEDURE "trRashod"();


--
-- Name: auth_group_permissions_permission_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY auth_group_permissions
    ADD CONSTRAINT auth_group_permissions_permission_id_fkey FOREIGN KEY (permission_id) REFERENCES auth_permission(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: auth_user_groups_group_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY auth_user_groups
    ADD CONSTRAINT auth_user_groups_group_id_fkey FOREIGN KEY (group_id) REFERENCES auth_group(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: auth_user_user_permissions_permission_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY auth_user_user_permissions
    ADD CONSTRAINT auth_user_user_permissions_permission_id_fkey FOREIGN KEY (permission_id) REFERENCES auth_permission(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: cafe_bron_stol_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY cafe_bron
    ADD CONSTRAINT cafe_bron_stol_id_fkey FOREIGN KEY (stol_id) REFERENCES cafe_stol(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: cafe_deposit_schet_id_995ad42_fk_cafe_schet_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY cafe_deposit
    ADD CONSTRAINT cafe_deposit_schet_id_995ad42_fk_cafe_schet_id FOREIGN KEY (schet_id) REFERENCES cafe_schet(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: cafe_deposit_user_id_19044275_fk_auth_user_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY cafe_deposit
    ADD CONSTRAINT cafe_deposit_user_id_19044275_fk_auth_user_id FOREIGN KEY (user_id) REFERENCES auth_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: cafe_doprashod_cat_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY cafe_doprashod
    ADD CONSTRAINT cafe_doprashod_cat_id_fkey FOREIGN KEY (cat_id) REFERENCES cafe_rashodcat(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: cafe_music_playlist_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY cafe_music
    ADD CONSTRAINT cafe_music_playlist_id_fkey FOREIGN KEY (playlist_id) REFERENCES cafe_playlist(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: cafe_prihod_tovar_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY cafe_prihod
    ADD CONSTRAINT cafe_prihod_tovar_id_fkey FOREIGN KEY (tovar_id) REFERENCES cafe_tovar(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: cafe_prihod_user_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY cafe_prihod
    ADD CONSTRAINT cafe_prihod_user_id_fkey FOREIGN KEY (user_id) REFERENCES auth_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: cafe_rashod_tovar_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY cafe_rashod
    ADD CONSTRAINT cafe_rashod_tovar_id_fkey FOREIGN KEY (tovar_id) REFERENCES cafe_tovar(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: cafe_rashod_user_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY cafe_rashod
    ADD CONSTRAINT cafe_rashod_user_id_fkey FOREIGN KEY (user_id) REFERENCES auth_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: cafe_rashod_usluga_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY cafe_rashod
    ADD CONSTRAINT cafe_rashod_usluga_id_fkey FOREIGN KEY (usluga_id) REFERENCES cafe_usluga(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: cafe_schet_stol_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY cafe_schet
    ADD CONSTRAINT cafe_schet_stol_id_fkey FOREIGN KEY (stol_id) REFERENCES cafe_stol(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: cafe_schet_user_close_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY cafe_schet
    ADD CONSTRAINT cafe_schet_user_close_id_fkey FOREIGN KEY (user_close_id) REFERENCES auth_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: cafe_schet_user_open_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY cafe_schet
    ADD CONSTRAINT cafe_schet_user_open_id_fkey FOREIGN KEY (user_open_id) REFERENCES auth_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: cafe_schethistory_schet_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY cafe_schethistory
    ADD CONSTRAINT cafe_schethistory_schet_id_fkey FOREIGN KEY (schet_id) REFERENCES cafe_schet(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: cafe_schethistory_user_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY cafe_schethistory
    ADD CONSTRAINT cafe_schethistory_user_id_fkey FOREIGN KEY (user_id) REFERENCES auth_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: cafe_sklad_tovar_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY cafe_sklad
    ADD CONSTRAINT cafe_sklad_tovar_id_fkey FOREIGN KEY (tovar_id) REFERENCES cafe_tovar(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: cafe_smena_user_close_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY cafe_smena
    ADD CONSTRAINT cafe_smena_user_close_id_fkey FOREIGN KEY (user_close_id) REFERENCES auth_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: cafe_sostav_tovar_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY cafe_sostav
    ADD CONSTRAINT cafe_sostav_tovar_id_fkey FOREIGN KEY (tovar_id) REFERENCES cafe_tovar(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: cafe_sostav_usluga_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY cafe_sostav
    ADD CONSTRAINT cafe_sostav_usluga_id_fkey FOREIGN KEY (usluga_id) REFERENCES cafe_usluga(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: cafe_tovar_menu_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY cafe_tovar
    ADD CONSTRAINT cafe_tovar_menu_id_fkey FOREIGN KEY (menu_id) REFERENCES cafe_menu(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: cafe_usluga_menu_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY cafe_usluga
    ADD CONSTRAINT cafe_usluga_menu_id_fkey FOREIGN KEY (menu_id) REFERENCES cafe_menu(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: cafe_vozvrat_user_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY cafe_vozvrat
    ADD CONSTRAINT cafe_vozvrat_user_id_fkey FOREIGN KEY (user_id) REFERENCES auth_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: cafe_vozvrat_usluga_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY cafe_vozvrat
    ADD CONSTRAINT cafe_vozvrat_usluga_id_fkey FOREIGN KEY (usluga_id) REFERENCES cafe_usluga(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: cafe_zakaz_menu_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY cafe_zakaz
    ADD CONSTRAINT cafe_zakaz_menu_id_fkey FOREIGN KEY (menu_id) REFERENCES cafe_menu(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: cafe_zakaz_schet_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY cafe_zakaz
    ADD CONSTRAINT cafe_zakaz_schet_id_fkey FOREIGN KEY (schet_id) REFERENCES cafe_schet(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: cafe_zakaz_usluga_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY cafe_zakaz
    ADD CONSTRAINT cafe_zakaz_usluga_id_fkey FOREIGN KEY (usluga_id) REFERENCES cafe_usluga(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: cafe_zakazhistory_menu_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY cafe_zakazhistory
    ADD CONSTRAINT cafe_zakazhistory_menu_id_fkey FOREIGN KEY (menu_id) REFERENCES cafe_menu(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: cafe_zakazhistory_schet_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY cafe_zakazhistory
    ADD CONSTRAINT cafe_zakazhistory_schet_id_fkey FOREIGN KEY (schet_id) REFERENCES cafe_schet(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: cafe_zakazhistory_user_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY cafe_zakazhistory
    ADD CONSTRAINT cafe_zakazhistory_user_id_fkey FOREIGN KEY (user_id) REFERENCES auth_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: cafe_zakazhistory_usluga_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY cafe_zakazhistory
    ADD CONSTRAINT cafe_zakazhistory_usluga_id_fkey FOREIGN KEY (usluga_id) REFERENCES cafe_usluga(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: cafe_zakazhistory_zakaz_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY cafe_zakazhistory
    ADD CONSTRAINT cafe_zakazhistory_zakaz_id_fkey FOREIGN KEY (zakaz_id) REFERENCES cafe_zakaz(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: content_type_id_refs_id_d043b34a; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY auth_permission
    ADD CONSTRAINT content_type_id_refs_id_d043b34a FOREIGN KEY (content_type_id) REFERENCES django_content_type(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: django_admin_log_content_type_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY django_admin_log
    ADD CONSTRAINT django_admin_log_content_type_id_fkey FOREIGN KEY (content_type_id) REFERENCES django_content_type(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: django_admin_log_user_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY django_admin_log
    ADD CONSTRAINT django_admin_log_user_id_fkey FOREIGN KEY (user_id) REFERENCES auth_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: group_id_refs_id_f4b32aac; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY auth_group_permissions
    ADD CONSTRAINT group_id_refs_id_f4b32aac FOREIGN KEY (group_id) REFERENCES auth_group(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: user_id_refs_id_40c41112; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY auth_user_groups
    ADD CONSTRAINT user_id_refs_id_40c41112 FOREIGN KEY (user_id) REFERENCES auth_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: user_id_refs_id_4dc23c39; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY auth_user_user_permissions
    ADD CONSTRAINT user_id_refs_id_4dc23c39 FOREIGN KEY (user_id) REFERENCES auth_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: public; Type: ACL; Schema: -; Owner: postgres
--

REVOKE ALL ON SCHEMA public FROM PUBLIC;
REVOKE ALL ON SCHEMA public FROM postgres;
GRANT ALL ON SCHEMA public TO postgres;
GRANT ALL ON SCHEMA public TO PUBLIC;


--
-- PostgreSQL database dump complete
--

