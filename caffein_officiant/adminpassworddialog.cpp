#include "adminpassworddialog.h"
#include "ui_adminpassworddialog.h"
#include "config.h"
#include <QMessageBox>
AdminPasswordDialog::AdminPasswordDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::AdminPasswordDialog)
{
    ui->setupUi(this);

    connect(ui->widgetNumPad,SIGNAL(clearClicked()),SLOT(clearClicked()));
    connect(ui->widgetNumPad,SIGNAL(backspaceClicked()),SLOT(backspaceClicked()));
    connect(ui->widgetNumPad,SIGNAL(buttonClicked(QString)),SLOT(letterClicked(QString)));

}

AdminPasswordDialog::~AdminPasswordDialog()
{
    delete ui;
}


void AdminPasswordDialog::clearClicked()
{
    ui->lineEdit->clear();
}


void AdminPasswordDialog::backspaceClicked()
{
    if(ui->lineEdit->text().length()==0) return;
    ui->lineEdit->setText(ui->lineEdit->text().left(ui->lineEdit->text().length()-1));
}

void AdminPasswordDialog::letterClicked(QString a)
{
    ui->lineEdit->setText(ui->lineEdit->text()+a);
}



void AdminPasswordDialog::on_pbEnter_clicked()
{
    // проверка пароля
    QString password = Config::get_option("admin_password");
    if(password!=ui->lineEdit->text()){
        QMessageBox::warning(this,trUtf8("Ошибка"),trUtf8("Неверный пароль"));
        ui->lineEdit->clear();
        return;
    }
    else{
        accept();
    }

}

