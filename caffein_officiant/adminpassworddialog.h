#ifndef ADMINPASSWORDDIALOG_H
#define ADMINPASSWORDDIALOG_H

#include <QDialog>

namespace Ui {
class AdminPasswordDialog;
}

class AdminPasswordDialog : public QDialog
{
    Q_OBJECT

public:
    explicit AdminPasswordDialog(QWidget *parent = 0);
    ~AdminPasswordDialog();

private slots:
    void clearClicked();
    void backspaceClicked();
    void letterClicked(QString a);
    void on_pbEnter_clicked();
private:
    Ui::AdminPasswordDialog *ui;
};

#endif // ADMINPASSWORDDIALOG_H
