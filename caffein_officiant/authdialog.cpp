#include "authdialog.h"
#include "ui_authdialog.h"
#include <QtSql>
#include <QMessageBox>

#include "config.h"
AuthDialog::AuthDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::AuthDialog)
{
    ui->setupUi(this);


    QSqlQuery query;
    if(!query.exec("select first_name,username,id from auth_user where is_active=true and id in (select user_id from auth_user_groups where group_id=3)")){
        qWarning() << Q_FUNC_INFO << query.lastError().text();
        QMessageBox::warning(this,trUtf8("Ошибка загрузки официантов"),query.lastError().text());
        return;
    }

    while(query.next()){
        ui->comboBox->addItem(query.value(0).toString(),query.value(1));
        ui->comboBox->setItemData(ui->comboBox->count()-1,query.value(2),Qt::UserRole+1);
    }

    connect(ui->widgetNumPad,SIGNAL(clearClicked()),SLOT(clearClicked()));
    connect(ui->widgetNumPad,SIGNAL(backspaceClicked()),SLOT(backspaceClicked()));
    connect(ui->widgetNumPad,SIGNAL(buttonClicked(QString)),SLOT(letterClicked(QString)));

}

AuthDialog::~AuthDialog()
{
    delete ui;
}

void AuthDialog::clearClicked()
{
    ui->lineEdit->clear();
}


void AuthDialog::backspaceClicked()
{
    if(ui->lineEdit->text().length()==0) return;
    ui->lineEdit->setText(ui->lineEdit->text().left(ui->lineEdit->text().length()-1));
}

void AuthDialog::letterClicked(QString a)
{
    ui->lineEdit->setText(ui->lineEdit->text()+a);
}

void AuthDialog::on_pbEnter_clicked()
{
    QString data = Config::apiQuery(QString("/api/auth/?uname=%1&passwd=%2").arg(Config::currentData(ui->comboBox).toString()).arg(ui->lineEdit->text()));
    if(data.isNull()){
        QMessageBox::warning(this,trUtf8("Ошибка соединения с сервером"),trUtf8("Ошибка соединения с сервером, обратитесь к администратору\n")+data);
        return;
    }

    if(!data.isEmpty()){
        QMessageBox::warning(this,trUtf8("Ошибка аутентификации"),data);
        ui->lineEdit->clear();
    }else{

        Config::username  = Config::currentData(ui->comboBox).toString();
        Config::password  = ui->lineEdit->text();
        Config::full_name = ui->comboBox->currentText();
        Config::user_id   = ui->comboBox->itemData(ui->comboBox->currentIndex(),Qt::UserRole+1).toInt();
        accept();
    }
}
