#include "brondialog.h"
#include "ui_brondialog.h"
#include <QtSql>
#include <QMessageBox>
BronDialog::BronDialog(int sid) :
    QDialog(0),
    ui(new Ui::BronDialog)
{
    ui->setupUi(this);
    _id = sid;

    ui->dateEdit->setDate(QDateTime::currentDateTime().date());

    loadData();

    ui->editText->setFocus();

    connect(ui->KBWidget,SIGNAL(clearClicked()),SLOT(clearClicked()));
    connect(ui->KBWidget,SIGNAL(backspaceClicked()),SLOT(backspaceClicked()));
    connect(ui->KBWidget,SIGNAL(buttonClicked(QString)),SLOT(letterClicked(QString)));
}

BronDialog::~BronDialog()
{
    delete ui;
}

void BronDialog::on_pbClose_clicked()
{
    close();
}

void BronDialog::on_lwBrons_clicked(const QModelIndex &index)
{
    int a = QMessageBox::question(0,trUtf8("Вопрос"),trUtf8("Удалить бронирование?"),QMessageBox::Yes|QMessageBox::No);
    if(a==QMessageBox::Yes){
        QListWidgetItem *item = ui->lwBrons->takeItem(index.row());
        int id = item->data(Qt::UserRole).toInt();
        QSqlQuery query;
        query.prepare("delete from cafe_bron where id=?");
        query.addBindValue(id);
        if(!query.exec()){
            qWarning() << Q_FUNC_INFO << query.lastError().text();
            QMessageBox::warning(this,trUtf8("Ошибка удаления бронирования"),query.lastError().text());
        }
        delete item;
   }
}

void BronDialog::clearClicked()
{
    ui->editText->clear();
}


void BronDialog::backspaceClicked()
{
    if(ui->editText->text().length()==0) return;
    ui->editText->setText(ui->editText->text().left(ui->editText->text().length()-1));
}

void BronDialog::letterClicked(QString a)
{
    ui->editText->setText(ui->editText->text()+a);
}

void BronDialog::on_pbAdd_clicked()
{
    QDateTime dt;
    dt.setDate(ui->dateEdit->date());
    dt.setTime(QDateTime::fromString(ui->comboBox->currentText(),"hh:mm").time());


    QSqlQuery query;
    query.prepare("insert into cafe_bron (prim,dt,stol_id) values (?,?,?)");

    query.addBindValue(ui->editText->text());
    query.addBindValue(dt);
    query.addBindValue(_id);
    if(!query.exec()){
        qWarning() << Q_FUNC_INFO << query.lastError().text();
        QMessageBox::warning(this,trUtf8("Ошибка удаления бронирования"),query.lastError().text());
    }
    loadData();
}

void BronDialog::loadData()
{
    ui->lwBrons->clear();
    QSqlQuery query;
    query.prepare("select id,prim,dt from cafe_bron where stol_id=? and dt>=? order by dt");
    query.addBindValue(_id);
    query.addBindValue(QDateTime::currentDateTime().addSecs(-60*60*3));
    if(!query.exec()){
        qWarning() << Q_FUNC_INFO << query.lastError().text();
        QMessageBox::warning(this,trUtf8("Ошибка получения бронирования"),query.lastError().text());
        return;
    }
    while(query.next()){
        ui->lwBrons->addItem(query.value(2).toDateTime().toString("dd.MM.yy hh:mm") + " - " + query.value(1).toString());
        QListWidgetItem *item=ui->lwBrons->item(ui->lwBrons->count()-1);
        item->setData(Qt::UserRole,query.value(0).toInt());
    }

}
