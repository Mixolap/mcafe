#ifndef BRONDIALOG_H
#define BRONDIALOG_H

#include <QDialog>
#include <QModelIndex>
namespace Ui {
class BronDialog;
}

class BronDialog : public QDialog
{
    Q_OBJECT
    int _id;
public:
    explicit BronDialog(int sid);
    ~BronDialog();

private slots:
    void on_pbClose_clicked();

    void on_lwBrons_clicked(const QModelIndex &index);
    void clearClicked();
    void backspaceClicked();
    void letterClicked(QString a);
    void on_pbAdd_clicked();
    void loadData();
private:
    Ui::BronDialog *ui;
};

#endif // BRONDIALOG_H
