#-------------------------------------------------
#
# Project created by QtCreator 2015-05-26T09:10:34
#
#-------------------------------------------------

QT       += core gui sql network

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = caffein_officiant
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    zalwidget.cpp \
    zakazwidget.cpp \
    stolbutton.cpp \
    tabzakazwidget.cpp \
    skidkadialog.cpp \
    numpadwidget.cpp \
    brondialog.cpp \
    keyboardwidget.cpp \
    depositdialog.cpp \
    primdialog.cpp \
    http.cpp \
    closeschetdialog.cpp \
    authdialog.cpp \
    config.cpp \
    adminpassworddialog.cpp \
    smenawidget.cpp \
    schetdialog.cpp \
    menuwidget.cpp \
    vozvratwidget.cpp \
    menueditwidget.cpp

HEADERS  += mainwindow.h \
    zalwidget.h \
    zakazwidget.h \
    stolbutton.h \
    tabzakazwidget.h \
    skidkadialog.h \
    numpadwidget.h \
    brondialog.h \
    keyboardwidget.h \
    depositdialog.h \
    primdialog.h \
    http.h \
    closeschetdialog.h \
    authdialog.h \
    config.h \
    adminpassworddialog.h \
    smenawidget.h \
    schetdialog.h \
    menuwidget.h \
    vozvratwidget.h \
    menueditwidget.h

FORMS    += mainwindow.ui \
    zalwidget.ui \
    zakazwidget.ui \
    tabzakazwidget.ui \
    skidkadialog.ui \
    numpadwidget.ui \
    brondialog.ui \
    keyboardwidget.ui \
    depositdialog.ui \
    primdialog.ui \
    closeschetdialog.ui \
    authdialog.ui \
    adminpassworddialog.ui \
    smenawidget.ui \
    schetdialog.ui \
    menuwidget.ui \
    vozvratwidget.ui \
    menueditwidget.ui

RESOURCES += \
    res.qrc
