#include "closeschetdialog.h"
#include "ui_closeschetdialog.h"

CloseSchetDialog::CloseSchetDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::CloseSchetDialog)
{
    ui->setupUi(this);
    ui->rbNal->setChecked(true);
}

CloseSchetDialog::~CloseSchetDialog()
{
    delete ui;
}

int CloseSchetDialog::type()
{
    if(ui->rbCard->isChecked()) return 1;
    return 0;
}
