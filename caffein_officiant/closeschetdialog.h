#ifndef CLOSESCHETDIALOG_H
#define CLOSESCHETDIALOG_H

#include <QDialog>

namespace Ui {
class CloseSchetDialog;
}

class CloseSchetDialog : public QDialog
{
    Q_OBJECT

public:
    explicit CloseSchetDialog(QWidget *parent = 0);
    ~CloseSchetDialog();
    int type();
private:
    Ui::CloseSchetDialog *ui;
};

#endif // CLOSESCHETDIALOG_H
