#include "config.h"
#include <QtSql>
#include <QMessageBox>
#include "http.h"
#ifdef Q_OS_WIN
//QString Config::server="192.168.2.114";
QString Config::server="127.0.0.1";
#else
QString Config::server="";
#endif
int     Config::port = 80;
int     Config::user_id = 1;
QString Config::username = QString();
QString Config::password = QString();
QString Config::full_name = QString();


Config::Config()
{
    QFile file(qApp->applicationDirPath()+"/config.txt");
    qDebug() << qApp->applicationDirPath()+"/config.txt";
    if(file.exists()){
        if(file.open(QIODevice::ReadOnly)){
            QString content = file.readAll();
            QStringList lines = content.split("\n");
            if(lines.count()>0){
                QStringList list_addr = lines.at(0).split(":");
                Config::server = list_addr.at(0).simplified();
                if(list_addr.count()>0) Config::port   = list_addr.at(1).simplified().toInt();
                qDebug() << Q_FUNC_INFO << "connecting"<<Config::server<<Config::port;
            }
        }else{
            qWarning() << "error open file"<<file.fileName();
        }

        file.close();
    }else{
        qWarning() << "file not exist"<<file.fileName();
    }

}

Config::~Config()
{

}

QString Config::get_option(const QString &name)
{
    QSqlQuery query;
    query.prepare("select value from cafe_option where code=?");
    query.addBindValue(name);
    if(!query.exec()){
        qWarning() << Q_FUNC_INFO << query.lastError().text() << name;
        return QString();
    }

    while(query.next()){
        return query.value(0).toString();
    }
    return QString();
}

void Config::set_option(const QString &name,const QString &value)
{
    //qDebug() << "setting" << name;
    QSqlQuery query;
    query.prepare("select count(*) from cafe_option where code=?");
    query.addBindValue(name);
    if(!query.exec()){
        qWarning() << Q_FUNC_INFO << query.lastError().text() << name;
        QMessageBox::warning(0,QObject::trUtf8("1. Ошибка установки опции")+name,query.lastError().text());
        return;
    }

    int cnt = 0;
    while(query.next()){
        cnt = query.value(0).toInt();
    }
    //qDebug() << "cnt="<<cnt;
    if(cnt==0){
        query.prepare("insert into cafe_option (code,name,value) values (?,'schet_num',?)");
        query.addBindValue(name);
        query.addBindValue(value);
    }else{
        query.prepare("update cafe_option set value=? where code=?");
        query.addBindValue(value);
        query.addBindValue(name);
    }
    if(!query.exec()){
        qWarning() << Q_FUNC_INFO << query.lastError().text() << name;
        QMessageBox::warning(0,QObject::trUtf8("2. Ошибка установки опции")+name,query.lastError().text());
        return;
    }

    return;
}

QVariant Config::currentData(const QComboBox *box)
{
    return box->itemData(box->currentIndex());
}

int Config::lastInsertId(const QString &tname)
{
    QSqlQuery query;
    if(!query.exec("select max(id) from "+tname)){
        qWarning() << Q_FUNC_INFO << query.lastError().text() << tname;
        QMessageBox::warning(0,QObject::trUtf8("Ошибка получения идентификатора ")+tname,query.lastError().text());
		return 0;
	}
	if(query.next()){
		return query.value(0).toInt();
	}
	qWarning() << Q_FUNC_INFO << query.lastError().text() << tname;
	QMessageBox::warning(0,QObject::trUtf8("2. Ошибка получения идентификатора ")+tname,query.lastError().text());
	
    return 0;
}

QString Config::apiQuery(const QString &url)
{
    Http h;
    h.setHost(Config::server,Config::port);
    return QString::fromUtf8(h.get(url));
}
