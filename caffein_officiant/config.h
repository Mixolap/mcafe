#ifndef CONFIG_H
#define CONFIG_H
#include <QtCore>
#include <QComboBox>

struct Schet{
    int id;
    int stol_id;
    double price;
    QString name;
    QDateTime dt_open;
    QDateTime dt_close;
    int user_open_id;
    int skidka;
    double price_skidka;
    int card;
    int precheck;
    QString prim;
    double skidka_fix;
    int num;
};

class Config
{
public:
    Config();
    ~Config();

    static QString server;
    static int port;
    static int user_id;
    static QString username;
    static QString password;
    static QString full_name;

    static QString get_option(const QString &name);
    static void set_option(const QString &name,const QString &value);

    static QVariant currentData(const QComboBox *box);
	static int lastInsertId(const QString &tname);
    static QString apiQuery(const QString &url);
};

#endif // CONFIG_H
