#include "depositdialog.h"
#include "ui_depositdialog.h"
#include <QtSql>
#include <QMessageBox>
DepositDialog::DepositDialog(int schet_id) :
    QDialog(0),
    ui(new Ui::DepositDialog)
{
    ui->setupUi(this);
    _sid = schet_id;

    ui->rbNal->setChecked(true);

    loadData();


    connect(ui->widgetNumPad,SIGNAL(clearClicked()),SLOT(clearClicked()));
    connect(ui->widgetNumPad,SIGNAL(backspaceClicked()),SLOT(backspaceClicked()));
    connect(ui->widgetNumPad,SIGNAL(buttonClicked(QString)),SLOT(letterClicked(QString)));

}

DepositDialog::~DepositDialog()
{
    delete ui;
}

void DepositDialog::on_pbAdd_clicked()
{
    bool ok;
    int value = ui->edit->text().toInt(&ok);
    if(!ok) return;
    QSqlQuery query;
    query.prepare("insert into cafe_deposit (summ,card,dt,schet_id,user_id) values (?,?,?,?,?)");
    query.addBindValue(value);
    query.addBindValue(ui->rbCard->isChecked()?1:0);
    query.addBindValue(QDateTime::currentDateTime());
    query.addBindValue(_sid);
    query.addBindValue(currentUser());
    if(!query.exec()){
        qWarning() << Q_FUNC_INFO << query.lastError().text();
        QMessageBox::warning(this,trUtf8("Ошибка добавления депозита"),query.lastError().text());
        return;
    }

    loadData();
}

void DepositDialog::loadData()
{
    ui->listWidget->clear();
    QSqlQuery query;
    query.prepare("select summ,card,dt from cafe_deposit where schet_id=?");
    query.addBindValue(_sid);
    if(!query.exec()){
        qWarning() << Q_FUNC_INFO << query.lastError().text();
        QMessageBox::warning(this,trUtf8("Ошибка получения депозитов"),query.lastError().text());
        return;
    }

    while(query.next()){
        ui->listWidget->addItem(query.value(2).toDateTime().toString("dd.MM.yy hh:mm")+" - "+(query.value(1).toInt()==0?trUtf8("наличные"):trUtf8("карта"))+" - "+query.value(0).toString()+trUtf8(" руб."));
    }
}

int DepositDialog::currentUser()
{
    return 1;
}

void DepositDialog::clearClicked()
{
        ui->edit->clear();
}

void DepositDialog::backspaceClicked()
{
    if(ui->edit->text().length()==0) return;
    ui->edit->setText(ui->edit->text().left(ui->edit->text().length()-1));

}

void DepositDialog::letterClicked(QString a)
{
    ui->edit->setText(ui->edit->text()+a);
}

void DepositDialog::on_pbClose_clicked()
{
    close();
}
