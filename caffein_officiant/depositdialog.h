#ifndef DEPOSITDIALOG_H
#define DEPOSITDIALOG_H

#include <QDialog>

namespace Ui {
class DepositDialog;
}

class DepositDialog : public QDialog
{
    Q_OBJECT
    int _sid;
public:
    explicit DepositDialog(int schet_id);
    ~DepositDialog();

private slots:
    void on_pbAdd_clicked();

private:
    Ui::DepositDialog *ui;

    void loadData();
    int currentUser();
private slots:
    void clearClicked();
    void backspaceClicked();
    void letterClicked(QString a);

    void on_pbClose_clicked();
};

#endif // DEPOSITDIALOG_H
