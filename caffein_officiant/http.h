#ifndef Http_H
#define Http_H
#include <QtNetwork>
/// класс релизации http протокола с поддержкой передачи переменных (GET,POST,cookie)
class Http: public QTcpSocket
{
		QString _host;	
		quint16 _port;
		QString _proxy_addr;
		quint16 _proxy_port;
		QString _lasterror;
		QByteArray _body;
		bool _follow_moving; //< следовать ли перенаправлению (false)
		int _lastQueryResultCode; //<результат выполнения последнего запроса
		bool _isPost; //< post выполнять или get
		QMap <QString,QString> _cookies;
		QMap <QString,QString> _text_variables;
		QMap <QString,QString> _header_variables;
		QMap <QString,QString> _files;
		
		QByteArray makeHeader(const QString &url,QString method);//< создает заголовок.		
		QByteArray encoding();//< возвращает кодировку для вебсервера.
		QString cookies(); //< возвращает строку с кукисами.
		void splitCoockie(const QStringList &header);
		QByteArray sendQuery(const QByteArray &header); //< посылает запрос с заголовком header
		QString _content_type;
		QByteArray joinVariables(); //< объединить переменные в запрос.
		QString boundary();
		QString fileName(const QString &fname); //< возвращает наименование файла
		QString fileType(const QString &fname); //< возвращает mime тип файла
		QByteArray followMoving(const QString &header); //< следовать перенаправлению.
		QString headerParam(const QString &header, const QString name); //< возвращает параметр из заголовка.
	public:
		Http();
	
		static QString MultipartFormData;
		static QString XWWWFormUrlencoded;
	
		void setHost(const QString &host,quint16 port=80);//< устанавливает адрес и порт сервера для соединения.
		void setUser(const QString &user,const QString &passwd); //< имя пользователя и пароль при необходимости аутентификации.
		void addCookie(const QString &name,const QString &value);//< добавляет переменную куки (если такая имеется, то он перезаписывается).
		void addVariable(const QString &name,const QString &value);//< добавлет переменную (get,post) (если такая имеется, то он перезаписывается).
		void addVariable(const QString &name,int value);//< добавлет переменную (get,post) (если такая имеется, то он перезаписывается). число преобразуется в текст.
		void addFile(const QString &name,const QString &fname); //< добавить файл. типа содержимого становится "multipart/form-data".
		void addHeaderVariable(const QString &name,const QString &value);//< устанавливает переменную заголовка (если такая имеется, то он перезаписывается).
		void removeHeaderVariable(const QString &name);
		void setProxy(const QString &host, quint16 port); //<установить проксисервер для соединения.
		QString cookie(const QString &name); //< возвращает значение куки по имени.
		QByteArray post(const QString &url);//< отправить post запрос.		
		QByteArray get(const QString &url);//< отправить get запрос.
		void setBody(const QByteArray &data); //< тело запроса
		/*! обработка заголовка ответа.
		 * \param answer содержание ответа.
		 */
		QByteArray processAnswer(const QByteArray &answer);
	
		void setMethod(const QString &mt);
		int lastQueryResultCode();//< возвращает результат выполнения последнего запроса
		QString lastError();
		void setContentType(const QString &ct); //< установить тип содержимого.
		void setFollowMoving(bool f); //< следовать ли перенаправлению если получен код 302
}; 


#endif
