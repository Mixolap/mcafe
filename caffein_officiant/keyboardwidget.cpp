#include "keyboardwidget.h"
#include "ui_keyboardwidget.h"
#include <QtDebug>
KeyboardWidget::KeyboardWidget(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::KeyboardWidget)
{
    ui->setupUi(this);


    connect(ui->pbClear,SIGNAL(clicked()),SIGNAL(clearClicked()));
    connect(ui->pbBackspace,SIGNAL(clicked()),SIGNAL(backspaceClicked()));

    for(int i=0;i<ui->gridLayout->count();i++){
        connect(ui->gridLayout->itemAt(i)->widget(),SIGNAL(clicked()),SLOT(buttonClick()));
    }

    for(int i=0;i<ui->gridLayout_2->count();i++){
        if(ui->gridLayout_2->itemAt(i)->widget()->objectName()!="pbClear" && ui->gridLayout_2->itemAt(i)->widget()->objectName()!="pbBackspace")
        connect(ui->gridLayout_2->itemAt(i)->widget(),SIGNAL(clicked()),SLOT(buttonClick()));
    }

}

KeyboardWidget::~KeyboardWidget()
{
    delete ui;
}

void KeyboardWidget::buttonClick()
{
    QString v = ((QPushButton*)sender())->text();
    if(v.isEmpty()) v = " ";
    emit buttonClicked(v);
}
