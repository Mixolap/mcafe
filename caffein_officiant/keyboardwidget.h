#ifndef KEYBOARDWIDGET_H
#define KEYBOARDWIDGET_H

#include <QWidget>

namespace Ui {
class KeyboardWidget;
}

class KeyboardWidget : public QWidget
{
    Q_OBJECT

public:
    explicit KeyboardWidget(QWidget *parent = 0);
    ~KeyboardWidget();

private:
    Ui::KeyboardWidget *ui;
signals:
    void buttonClicked(QString v);
    void clearClicked();
    void backspaceClicked();
private slots:
    void buttonClick();
};

#endif // KEYBOARDWIDGET_H
