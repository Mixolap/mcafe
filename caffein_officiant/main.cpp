#include "mainwindow.h"
#include <QApplication>
#include "config.h"
int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    QTranslator qtTranslator;
    qtTranslator.load("://localization/qt_ru.qm");
    a.installTranslator(&qtTranslator);

    Config conf;
    MainWindow w; 
	w.showFullScreen();
    w.showMaximized();

    return a.exec();
}
