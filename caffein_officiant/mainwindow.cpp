#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "zalwidget.h"
#include <QtGui>
#include <QtSql>
#include <QMessageBox>
#include "config.h"
MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    connectDatabase();

    showZal();
    ui->widget->loadData();
    ui->zakazWidget->loadMenu();

    connect(ui->widget,SIGNAL(showStol(int,QString)),SLOT(showStol(int,QString)));
    connect(ui->widget,SIGNAL(showSmena()),SLOT(showSmena()));
    connect(ui->widget,SIGNAL(showVozvrat()),SLOT(showVozvrat()));
    connect(ui->widget,SIGNAL(showMenuEdit()),SLOT(showMenuEdit()));
    connect(ui->zakazWidget,SIGNAL(toZal()),SLOT(showZal()));
    connect(ui->widgetSmena,SIGNAL(toZal()),SLOT(showZal()));
    connect(ui->widgetVozvrat,SIGNAL(toZal()),SLOT(showZal()));
    connect(ui->widgetMenuEdit,SIGNAL(toZal()),SLOT(showZal()));

    //resize(1024,768);
    //setStyleSheet("background-color: #ffffff;");
    layout()->setSpacing(2);
	QTimer *timer = new QTimer();
	timer->setSingleShot(true);
	connect(timer,SIGNAL(timeout()),SLOT(toFullScreen()));
    timer->start(1000);
	
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::connectDatabase()
{
    QSqlDatabase dbase = QSqlDatabase::addDatabase("QPSQL");
    dbase.setHostName(Config::server);
    dbase.setDatabaseName("caffein");
    if(!dbase.open("postgres","")){
        qWarning() << "database caffein on localhost not opened" << dbase.lastError().text();
        QMessageBox::warning(this,trUtf8("Ошибка"),trUtf8("Ошибка соединения с базой данных %1").arg(dbase.lastError().text()));
        return;
    }
}

void MainWindow::showStol(int id, const QString &name)
{
    ui->zakazWidget->loadStol(id,name);
    ui->stackedWidget->setCurrentIndex(1);
}

void MainWindow::showZal()
{
    ui->stackedWidget->setCurrentIndex(0);
    ui->widget->updateStols();
}

void MainWindow::showSmena()
{

    ui->stackedWidget->setCurrentWidget(ui->pageSmena);
    ui->widgetSmena->loadData();

}

void MainWindow::showVozvrat()
{
    ui->widgetVozvrat->loadData();
    ui->stackedWidget->setCurrentWidget(ui->pageVozvrat);
}

void MainWindow::showMenuEdit()
{
    ui->widgetMenuEdit->loadData();
    ui->stackedWidget->setCurrentWidget(ui->pageMenu);
}

void MainWindow::toFullScreen()
{
	qDebug() << "moving";
	showMaximized();
	setWindowState(Qt::WindowFullScreen);
	setVisible(true);
	move(0,0);
	setFixedSize(1024,755);
}

