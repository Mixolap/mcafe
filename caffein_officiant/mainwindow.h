#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QHBoxLayout>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private:
    Ui::MainWindow *ui;

    void connectDatabase();
private slots:
    void showStol(int id,const QString &name);
    void showZal();
    void showSmena();
    void showVozvrat();
    void showMenuEdit();
	void toFullScreen();

};

#endif // MAINWINDOW_H
