#include "menueditwidget.h"
#include "ui_menueditwidget.h"
#include <QtSql>
#include <QMessageBox>
#include "config.h"
MenuEditWidget::MenuEditWidget(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::MenuEditWidget)
{
    ui->setupUi(this);
    _currentEdit = 0;
    _uid = 0;
    connect(ui->KBWidget,SIGNAL(clearClicked()),SLOT(clearClicked()));
    connect(ui->KBWidget,SIGNAL(backspaceClicked()),SLOT(backspaceClicked()));
    connect(ui->KBWidget,SIGNAL(buttonClicked(QString)),SLOT(letterClicked(QString)));

    connect(ui->widgetMenu,SIGNAL(submenuClicked(int,int)),SLOT(onSubmenuClicked(int,int)));
    connect(ui->widgetMenu,SIGNAL(uslugaClicked(int,QString,double,int)),SLOT(onUslugaClicked(int,QString,double,int)));

    connect(qApp,SIGNAL(focusChanged(QWidget*,QWidget*)),SLOT(onFocusChanged(QWidget*,QWidget*)));

    connect(ui->pbUslugaRemove,SIGNAL(clicked()),SLOT(removeUsluga()));

    ui->labUID->setText(QString::number(_uid));


}

MenuEditWidget::~MenuEditWidget()
{
    delete ui;
}

void MenuEditWidget::loadData()
{
    ui->widgetMenu->loadMenu();


    QSqlQuery query;
    if(!query.exec("select id,name from cafe_menu where pid<>0")){
        qWarning() << Q_FUNC_INFO << query.lastError().text();
        QMessageBox::warning(this,trUtf8("Ошибка загрузки разделов"),query.lastError().text());
        return;
    }

    ui->cboxSection->clear();
    while(query.next()){
        ui->cboxSection->addItem(query.value(1).toString(),query.value(0).toInt());
    }


}

void MenuEditWidget::on_pbZal_clicked()
{
    emit toZal();
}



void MenuEditWidget::on_editName_cursorPositionChanged(int arg1, int arg2)
{
    Q_UNUSED(arg1)
    Q_UNUSED(arg2)
    _currentEdit = ui->editName;
}

void MenuEditWidget::on_editPrice_cursorPositionChanged(int arg1, int arg2)
{
    Q_UNUSED(arg1)
    Q_UNUSED(arg2)
    _currentEdit = ui->editPrice;
}

void MenuEditWidget::clearClicked()
{
    if(_currentEdit==0) return;
    _currentEdit->clear();
}


void MenuEditWidget::backspaceClicked()
{
    if(_currentEdit==0) return;
    if(_currentEdit->text().length()==0) return;
    _currentEdit->setText(_currentEdit->text().left(_currentEdit->text().length()-1));
}

void MenuEditWidget::letterClicked(QString a)
{
    if(_currentEdit==0) return;
    _currentEdit->setText(_currentEdit->text()+a);
}

void MenuEditWidget::onSubmenuClicked(int id, int index)
{
    _index = index;
    for(int i=0;i<ui->cboxSection->count();i++){
        if(ui->cboxSection->itemData(i).toInt()==id){
            ui->cboxSection->setCurrentIndex(i);
            break;
        }
    }
}

void MenuEditWidget::onUslugaClicked(int id, QString name, double price, int mid)
{
    Q_UNUSED(mid)
    _uid = id;
    ui->labUID->setText(QString::number(id));

    ui->editName->setText(name.toUpper());
    ui->editPrice->setText(QString::number(price));
}

void MenuEditWidget::onFocusChanged(QWidget *old, QWidget *now)
{
    Q_UNUSED(now)
    if(old==0) return;
    if(old->objectName()=="editName"){
        _currentEdit = ui->editName;
    }
    if(old->objectName()=="editPrice"){
        _currentEdit = ui->editPrice;
    }
}

void MenuEditWidget::on_pbAdd_clicked()
{
    _uid = 0;
    ui->labUID->setText(QString::number(_uid));
}

void MenuEditWidget::on_pbSave_clicked()
{
    QString name = ui->editName->text().simplified().toUpper();
    if(name.isEmpty()){
        QMessageBox::warning(this,trUtf8("Ошибка"),trUtf8("Введите наименование"));
        ui->editName->setFocus();
        return;
    }

    bool ok;
    double price = ui->editPrice->text().simplified().toDouble(&ok);
    if(!ok){
        QMessageBox::warning(this,trUtf8("Ошибка"),trUtf8("Введите цену"));
        ui->editPrice->setFocus();
        return;
    }
    int mid = ui->cboxSection->itemData(ui->cboxSection->currentIndex()).toInt();
    ui->labUID->setText(QString::number(_uid));
    QSqlQuery query;    
    if(_uid==0){
        query.prepare("insert into cafe_usluga (name,menu_id,price,dt,np,active) values (?,?,?,?,?,?)");
        query.addBindValue(name);
        query.addBindValue(mid);
        query.addBindValue(price);        
        query.addBindValue(QDateTime::currentDateTime());
        query.addBindValue(100);
        query.addBindValue(1);

    }else{
        query.prepare("update cafe_usluga set name=?,menu_id=?,price=?,dt=? where id=?");
        query.addBindValue(name);
        query.addBindValue(mid);
        query.addBindValue(price);
        query.addBindValue(QDateTime::currentDateTime());
        query.addBindValue(_uid);
    }

    if(!query.exec()){
        qWarning() << Q_FUNC_INFO << query.lastError().text();
        QMessageBox::warning(this,trUtf8("Ошибка записи услуги"),query.lastError().text());
    }
    if(_uid==0) checkUslugaContent();
    ui->widgetMenu->reloadMenu(_index);
}

void MenuEditWidget::removeUsluga()
{
    if(_uid==0) return;
    if(QMessageBox::question(this,trUtf8("Подтверждение"),trUtf8("Вы действительно хотите удалить услугу %1").arg(ui->editName->text()),QMessageBox::Yes|QMessageBox::No)!=QMessageBox::Yes) return;
    QSqlQuery query;
    query.prepare("update cafe_usluga set active=0 where id=?");
    query.addBindValue(_uid);
    if(!query.exec()){
        qWarning() << Q_FUNC_INFO << query.lastError().text();
        QMessageBox::warning(this,trUtf8("Ошибка деактивации услуги"),query.lastError().text());
    }
    ui->widgetMenu->reloadMenu(_index);
    _uid = 0;
    ui->labUID->setText(QString::number(_uid));
}

void MenuEditWidget::checkUslugaContent()
{
    QString data = Config::apiQuery("/api/check/usluga/content/");
    if(data.isNull()){
        QMessageBox::warning(this,trUtf8("Ошибка соединения с сервером"),trUtf8("Ошибка соединения с сервером, обратитесь к администратору\n")+data);
        return;
    }
    if(data!="ok"){
        QMessageBox::warning(this,trUtf8("Ошибка ответа"),data);
        return;
    }
}
