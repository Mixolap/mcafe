#ifndef MENUEDITWIDGET_H
#define MENUEDITWIDGET_H

#include <QWidget>
#include <QLineEdit>

namespace Ui {
class MenuEditWidget;
}

class MenuEditWidget : public QWidget
{
    Q_OBJECT
    QLineEdit *_currentEdit;
    int _uid;
    int _index;
public:
    explicit MenuEditWidget(QWidget *parent = 0);
    ~MenuEditWidget();

    void loadData();
private slots:
    void on_pbZal_clicked();

    void on_editName_cursorPositionChanged(int arg1, int arg2);

    void on_editPrice_cursorPositionChanged(int arg1, int arg2);

    void clearClicked();
    void backspaceClicked();
    void letterClicked(QString a);
    void onSubmenuClicked(int id,int index);
    void onUslugaClicked(int id,QString name,double price,int mid);
    void onFocusChanged(QWidget *old,QWidget *now);
    void on_pbAdd_clicked();

    void on_pbSave_clicked();

    void removeUsluga();


private:
    Ui::MenuEditWidget *ui;

    void checkUslugaContent();
signals:
    void toZal();
};

#endif // MENUEDITWIDGET_H
