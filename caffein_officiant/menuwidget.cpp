#include "menuwidget.h"
#include "ui_menuwidget.h"
#include <QtSql>

MenuWidget::MenuWidget(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::MenuWidget)
{
    ui->setupUi(this);
    ui->pbMenu1->setVisible(false);
    ui->pbMenu2->setVisible(false);
    ui->pbMenu3->setVisible(false);
    ui->pbMenu4->setVisible(false);

    ui->gbox1->setVisible(false);
    ui->gbox2->setVisible(false);
    ui->gbox3->setVisible(false);
    ui->gbox4->setVisible(false);


    while(ui->stackedSubmenuWidget->widget(0)){
        ui->stackedSubmenuWidget->removeWidget(ui->stackedSubmenuWidget->widget(0));
    }

    while(ui->stackedUslugaWidget->widget(0)){
        ui->stackedUslugaWidget->removeWidget(ui->stackedUslugaWidget->widget(0));
    }

    _subCount = 0;
    _sid = 0;
    ui->widgetLeftMenu->hide();
    connect(ui->pbMenu1,SIGNAL(clicked()),SLOT(onMenuClicked()));
    connect(ui->pbMenu2,SIGNAL(clicked()),SLOT(onMenuClicked()));
    connect(ui->pbMenu3,SIGNAL(clicked()),SLOT(onMenuClicked()));
    connect(ui->pbMenu4,SIGNAL(clicked()),SLOT(onMenuClicked()));
}

MenuWidget::~MenuWidget()
{
    delete ui;
}

void MenuWidget::loadMenu()
{
    _subCount = submenuCount();
    if(_subCount<20){
        ui->widgetTopMenu->hide();
    }


    QSqlQuery query;
    query.exec("select id,name from cafe_menu where pid=0 and active=1 order by np,id");
    int i=0;
    while(query.next()){

        switch(i){
            case 0:
                ui->pbMenu1->setText(query.value(1).toString());
                ui->pbMenu1->setProperty("id",query.value(0).toInt());
                ui->pbMenu1->setProperty("index",loadSubmenu(query.value(0).toInt()));
                ui->pbMenu1->setVisible(true);
                break;
            case 1:
                ui->pbMenu2->setText(query.value(1).toString());
                ui->pbMenu2->setProperty("id",query.value(0).toInt());
                ui->pbMenu2->setProperty("index",loadSubmenu(query.value(0).toInt()));
                ui->pbMenu2->setVisible(true);
                break;
            case 2:
                ui->pbMenu3->setText(query.value(1).toString());
                ui->pbMenu3->setProperty("id",query.value(0).toInt());
                ui->pbMenu3->setProperty("index",loadSubmenu(query.value(0).toInt()));
                ui->pbMenu3->setVisible(true);
                break;
            case 3:
                ui->pbMenu4->setText(query.value(1).toString());
                ui->pbMenu4->setProperty("id",query.value(0).toInt());
                ui->pbMenu4->setProperty("index",loadSubmenu(query.value(0).toInt()));
                ui->pbMenu4->setVisible(true);
                break;
        }
        i++;

    }


    if(ui->stackedUslugaWidget->count()>0){
        ui->stackedUslugaWidget->setCurrentIndex(0);

    }
//    qDebug() << ui->stackedUslugaWidget->count();

}

void MenuWidget::showFirst()
{
    ui->pbMenu1->click();
}

void MenuWidget::reloadMenu(int index)
{
    while(ui->stackedUslugaWidget->count()>0){
        ui->stackedUslugaWidget->removeWidget(ui->stackedUslugaWidget->widget(0));
    }
    loadMenu();
    ui->stackedUslugaWidget->setCurrentIndex(index);
}

int MenuWidget::loadSubmenu(int pid)
{
    QGroupBox *gbox=0;
    switch(pid){
        case 1:
            gbox = ui->gbox1;
            break;
        case 2:
            gbox = ui->gbox2;
            break;
        case 3:
            gbox = ui->gbox3;
            break;
        case 36:
            gbox = ui->gbox4;
            break;

    }
    delete gbox->layout();
    QWidget *w = new QWidget();
    w->setProperty("index",ui->stackedSubmenuWidget->count());

    QVBoxLayout *hlayout = new QVBoxLayout();
    QGridLayout *glayout = new QGridLayout();
    hlayout->setSpacing(2);
    glayout->setSpacing(2);

    QSqlQuery query;
    query.prepare("select id,name from cafe_menu where pid=:pid and active=1 order by np,id");
    query.addBindValue(pid);
    query.exec();
    int cnt=0;
    int row=0;
    int col=0;
    while(query.next()){
        QPushButton *pbSubmenu = new QPushButton(query.value(1).toString());
        pbSubmenu->setProperty("id",query.value(0));
        pbSubmenu->setProperty("index",loadMenuUslugs(query.value(0).toInt()));
        pbSubmenu->setMinimumHeight(45);
        pbSubmenu->setMaximumWidth(140);
        connect(pbSubmenu,SIGNAL(clicked()),SLOT(onSubmenuClicked()));
        glayout->addWidget(pbSubmenu,col,row);
        cnt += 1;
        row++;
        if(row==2){
            row = 0;
            col++;
        }

    }

    hlayout->addLayout(glayout);
    hlayout->addSpacerItem(new QSpacerItem(1,1, QSizePolicy::Expanding, QSizePolicy::Expanding));
    w->setLayout(hlayout);
    ui->stackedSubmenuWidget->addWidget(w);
    if(_subCount<20 && gbox!=0){
        gbox->setLayout(hlayout);
        ui->stackedSubmenuWidget->hide();
        ui->widgetLeftMenu->show();
    }
    if(cnt>0 && gbox!=0) gbox->show();
    //qDebug() << Q_FUNC_INFO << "cnt=" << cnt;
    return ui->stackedSubmenuWidget->count()-1;
}


int MenuWidget::loadMenuUslugs(int pid)
{
    QWidget *w = new QWidget();
    QVBoxLayout *hlayout = new QVBoxLayout();
    QGridLayout *glayout = new QGridLayout();

    QSqlQuery query;
    query.prepare("select u.id,u.name,u.price,m.pid from cafe_usluga u,cafe_menu m where u.menu_id=m.id and u.active=1 and m.id=:mid order by u.np,u.id");
    query.addBindValue(pid);
    //qDebug() << "pid"<<pid;
    if(!query.exec()){
        qWarning() << Q_FUNC_INFO << query.lastError().text();
        return 0;
    }
    int row=0;
    int col=0;

    while(query.next()){
        QPushButton *pbUsluga = new QPushButton(trUtf8("%1\n%2 р.").arg(query.value(1).toString()).arg(query.value(2).toDouble()));
        pbUsluga->setProperty("id",query.value(0).toInt());
        pbUsluga->setProperty("name",query.value(1).toString().toLower());
        pbUsluga->setProperty("price",query.value(2).toDouble());
        pbUsluga->setProperty("mid",query.value(3).toDouble());
        pbUsluga->setMinimumHeight(50);
        pbUsluga->setMaximumWidth(160);
        pbUsluga->setStyleSheet("font: 8pt 'Sans' bold;");

        connect(pbUsluga,SIGNAL(clicked()),SLOT(onUslugaClicked()));

        glayout->addWidget(pbUsluga,col,row);
        row++;
        if(row==2){
            row = 0;
            col++;
        }
        //qDebug() << "adding" << col <<row<<query.value(1).toString();
    }
    hlayout->addLayout(glayout);
    hlayout->addSpacerItem(new QSpacerItem(1,1, QSizePolicy::Expanding, QSizePolicy::Expanding));
    w->setLayout(hlayout);

    ui->stackedUslugaWidget->addWidget(w);
    return ui->stackedUslugaWidget->count()-1;
}

int MenuWidget::submenuCount()
{
    QSqlQuery query;
    query.prepare("select count(*) from cafe_menu where active=1 and pid<>0");

    //qDebug() << "pid"<<pid;
    if(!query.exec()){
        qWarning() << Q_FUNC_INFO << query.lastError().text();
        return 0;
    }

    while(query.next()){
        return query.value(0).toInt();
    }
    return 100;
}

void MenuWidget::onMenuClicked()
{
    QPushButton *pb = (QPushButton*)sender();
    ui->stackedSubmenuWidget->setCurrentIndex(pb->property("index").toInt());
    QWidget *w = ui->stackedSubmenuWidget->currentWidget();
    foreach(QObject *cw,w->children()){
        if(!cw->property("index").isNull()){
            ui->stackedUslugaWidget->setCurrentIndex(cw->property("index").toInt());
            break;
        }
    }
}

void MenuWidget::onSubmenuClicked()
{
    QPushButton *pb = (QPushButton*)sender();
    ui->stackedUslugaWidget->setCurrentIndex(pb->property("index").toInt());
    _sid = pb->property("id").toInt();
    emit submenuClicked(_sid,pb->property("index").toInt());
}

void MenuWidget::onUslugaClicked()
{
    int id = sender()->property("id").toInt();
    QString name = sender()->property("name").toString();
    double price = sender()->property("price").toDouble();
    int mid = sender()->property("mid").toDouble();
    emit uslugaClicked(id,name,price,mid);
}
