#ifndef MENUWIDGET_H
#define MENUWIDGET_H

#include <QWidget>

namespace Ui {
class MenuWidget;
}

class MenuWidget : public QWidget
{
    Q_OBJECT
    int _subCount;
    int _sid;
public:
    explicit MenuWidget(QWidget *parent = 0);
    ~MenuWidget();
    void loadMenu();
    void showFirst();
    void reloadMenu(int index);
private:
    Ui::MenuWidget *ui;

    int loadSubmenu(int pid);
    int loadMenuUslugs(int pid);
    int submenuCount();
private slots:
    void onMenuClicked();
    void onSubmenuClicked();
    void onUslugaClicked();
signals:
    void uslugaClicked(int id,const QString &name,double price, int menu_id);
    void submenuClicked(int id,int index);
};

#endif // MENUWIDGET_H
