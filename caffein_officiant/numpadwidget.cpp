#include "numpadwidget.h"
#include "ui_numpadwidget.h"

NumPadWidget::NumPadWidget(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::NumPadWidget)
{
    ui->setupUi(this);

    connect(ui->pbClear,SIGNAL(clicked()),SIGNAL(clearClicked()));
    connect(ui->pbBackspace,SIGNAL(clicked()),SIGNAL(backspaceClicked()));

    connect(ui->pb0,SIGNAL(clicked()),SLOT(buttonClick()));
    connect(ui->pb1,SIGNAL(clicked()),SLOT(buttonClick()));
    connect(ui->pb2,SIGNAL(clicked()),SLOT(buttonClick()));
    connect(ui->pb3,SIGNAL(clicked()),SLOT(buttonClick()));
    connect(ui->pb4,SIGNAL(clicked()),SLOT(buttonClick()));
    connect(ui->pb5,SIGNAL(clicked()),SLOT(buttonClick()));
    connect(ui->pb6,SIGNAL(clicked()),SLOT(buttonClick()));
    connect(ui->pb7,SIGNAL(clicked()),SLOT(buttonClick()));
    connect(ui->pb8,SIGNAL(clicked()),SLOT(buttonClick()));
    connect(ui->pb9,SIGNAL(clicked()),SLOT(buttonClick()));
}

NumPadWidget::~NumPadWidget()
{
    delete ui;
}

void NumPadWidget::buttonClick()
{
    QString v = ((QPushButton*)sender())->text();
    emit buttonClicked(v);
}
