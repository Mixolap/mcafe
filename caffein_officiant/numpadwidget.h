#ifndef NUMPADWIDGET_H
#define NUMPADWIDGET_H

#include <QWidget>

namespace Ui {
class NumPadWidget;
}

class NumPadWidget : public QWidget
{
    Q_OBJECT

public:
    explicit NumPadWidget(QWidget *parent = 0);
    ~NumPadWidget();

private:
    Ui::NumPadWidget *ui;
signals:
    void buttonClicked(QString v);
    void clearClicked();
    void backspaceClicked();
private slots:
    void buttonClick();
};

#endif // NUMPADWIDGET_H
