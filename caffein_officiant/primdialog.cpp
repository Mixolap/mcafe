#include "primdialog.h"
#include "ui_primdialog.h"
#include <QtSql>
#include <QMessageBox>
PrimDialog::PrimDialog(int sid) :
    QDialog(0),
    ui(new Ui::PrimDialog)
{
    ui->setupUi(this);
    _sid = sid;

    QSqlQuery query;
    query.prepare("select prim from cafe_schet where id=?");
    query.addBindValue(_sid);
    if(!query.exec()){
        qWarning() << Q_FUNC_INFO <<  _sid;
        QMessageBox::warning(this,trUtf8("Ошибка извлечения примечания"),query.lastError().text());
        return;
    }
    while(query.next()){
        ui->editText->setText(query.value(0).toString());
    }

    connect(ui->KBWidget,SIGNAL(clearClicked()),SLOT(clearClicked()));
    connect(ui->KBWidget,SIGNAL(backspaceClicked()),SLOT(backspaceClicked()));
    connect(ui->KBWidget,SIGNAL(buttonClicked(QString)),SLOT(letterClicked(QString)));

    connect(ui->pushButton,SIGNAL(clicked()),SLOT(saveAndClose()));

}

PrimDialog::~PrimDialog()
{
    delete ui;
}

void PrimDialog::clearClicked()
{
    ui->editText->clear();
}


void PrimDialog::backspaceClicked()
{
    if(ui->editText->text().length()==0) return;
    ui->editText->setText(ui->editText->text().left(ui->editText->text().length()-1));
}

void PrimDialog::letterClicked(QString a)
{
    ui->editText->setText(ui->editText->text()+a);
}

void PrimDialog::saveAndClose()
{
    qDebug() << Q_FUNC_INFO << "saving prim";
    QSqlQuery query;
    query.prepare("update cafe_schet set prim=? where id=?");
    query.addBindValue(ui->editText->text());
    query.addBindValue(_sid);
    if(!query.exec()){
        qWarning() << Q_FUNC_INFO <<  _sid;
        QMessageBox::warning(this,trUtf8("Ошибка сохранения примечания"),query.lastError().text());
        return;
    }
    close();
}
