#ifndef PRIMDIALOG_H
#define PRIMDIALOG_H

#include <QDialog>

namespace Ui {
class PrimDialog;
}

class PrimDialog : public QDialog
{
    Q_OBJECT
    int _sid;
public:
    explicit PrimDialog(int sid);
    ~PrimDialog();

private:
    Ui::PrimDialog *ui;
private slots:
    void clearClicked();
    void backspaceClicked();
    void letterClicked(QString a);

    void saveAndClose();
};

#endif // PRIMDIALOG_H
