#include "schetdialog.h"
#include "ui_schetdialog.h"
#include "config.h"
#include <QtSql>
#include <QMessageBox>

SchetDialog::SchetDialog(int sid) :
    QDialog(0),
    ui(new Ui::SchetDialog)
{
    ui->setupUi(this);
    _sid = sid;
    qDebug() << Q_FUNC_INFO << "sid" << sid;
    QSqlQuery query;
    query.prepare("select id,name,price,dt_open,user_open_id,skidka,price_skidka,card,precheck,prim,skidka_fix,num,stol_id,dt_close from cafe_schet where id=?");
    query.addBindValue(sid);
    if(!query.exec()){
        qWarning() << Q_FUNC_INFO << query.lastError().text();
        QMessageBox::warning(this,trUtf8("Ошибка загрузки счёта"),query.lastError().text());
        return;
    }
    if(query.next()){

        Schet *z = new Schet();
        z->id       = query.value(0).toInt();
        z->stol_id  = query.value(12).toInt();
        z->name     = query.value(1).toString();
        z->price    = query.value(2).toDouble();
        z->dt_open  = query.value(3).toDateTime();
        z->dt_close = query.value(13).toDateTime();
        z->user_open_id = query.value(4).toInt();
        z->skidka   = query.value(5).toInt();

        z->price_skidka = query.value(6).toDouble();
        z->card     = query.value(7).toInt();
        z->precheck = query.value(8).toInt();
        z->prim     = query.value(9).toString();
        z->skidka_fix = query.value(10).toDouble();
        z->num      = query.value(11).toInt();
        setWindowTitle(trUtf8("Счёт №%1 (%2)").arg(z->num).arg(z->id));
        ui->labOpen->setText(z->dt_open.toString("dd.MM.yy hh:mm"));
        ui->labClose->setText(z->dt_close.toString("dd.MM.yy hh:mm"));
        ui->labSkidka->setText(trUtf8("%1% (%2)").arg(z->skidka).arg(z->skidka_fix));
        ui->labSumm->setText(trUtf8("%1 р.").arg(z->price));
        ui->labTotal->setText(trUtf8("%1 р.").arg(z->price_skidka));
        delete z;
    }
    loadZakaz(sid);

}

SchetDialog::~SchetDialog()
{
    delete ui;
}

void SchetDialog::loadZakaz(int sid)
{
    QSqlQuery query;
    query.prepare("select u.name,z.cnt,z.price from cafe_zakaz z,cafe_usluga u where z.usluga_id=u.id and schet_id=? and z.cnt>0 order by z.menu_id");
    query.addBindValue(sid);
    if(!query.exec()){
        qWarning() << Q_FUNC_INFO << query.lastError().text();
        QMessageBox::warning(this,trUtf8("Ошибка загрузки заказа"),query.lastError().text());
        return;
    }
    ui->twZakaz->clear();
    while(query.next()){
        QString name = query.value(0).toString();
        QString cnt = query.value(1).toString();
        QString price = query.value(2).toString();

        ui->twZakaz->invisibleRootItem()->addChild(new QTreeWidgetItem(QStringList() << name << cnt << price));
    }
    ui->twZakaz->setColumnWidth(0,300);
}
