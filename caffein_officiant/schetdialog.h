#ifndef SCHETDIALOG_H
#define SCHETDIALOG_H

#include <QDialog>

namespace Ui {
class SchetDialog;
}

class SchetDialog : public QDialog
{
    Q_OBJECT
    int _sid;
public:
    explicit SchetDialog(int sid);
    ~SchetDialog();

private:
    Ui::SchetDialog *ui;

    void loadZakaz(int sid);
};

#endif // SCHETDIALOG_H
