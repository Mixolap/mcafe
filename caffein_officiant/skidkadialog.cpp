#include "skidkadialog.h"
#include "ui_skidkadialog.h"
#include <QtSql>
#include <QMessageBox>
#include <QRadioButton>
SkidkaDialog::SkidkaDialog(double proc, double fixed) :
    QDialog(0),
    ui(new Ui::SkidkaDialog)
{
    ui->setupUi(this);

    QSqlQuery query;
    if(!query.exec("select value from cafe_skidka order by value")){
        qWarning() << Q_FUNC_INFO << query.lastError().text();
        QMessageBox::warning(this,trUtf8("Ошибка получения скидок"),query.lastError().text());
        return;
    }

    while(query.next()){
        QRadioButton *rb = new QRadioButton(query.value(0).toString()+"%",this);
        rb->setProperty("value",query.value(0).toDouble());
        rb->setChecked(proc==query.value(0).toDouble());
        ui->SkidkaProcLayout->addWidget(rb);
    }
    if(fixed==0){
        ui->editSkidkaFix->setText(QString());
    }else{
        ui->editSkidkaFix->setText(QString::number(fixed));
    }


    connect(ui->widgetNumPad,SIGNAL(clearClicked()),SLOT(clearClicked()));
    connect(ui->widgetNumPad,SIGNAL(backspaceClicked()),SLOT(backspaceClicked()));
    connect(ui->widgetNumPad,SIGNAL(buttonClicked(QString)),SLOT(letterClicked(QString)));
}

SkidkaDialog::~SkidkaDialog()
{
    delete ui;
}

void SkidkaDialog::clearClicked()
{
    ui->editSkidkaFix->clear();
}


void SkidkaDialog::backspaceClicked()
{
    if(ui->editSkidkaFix->text().length()==0) return;
    ui->editSkidkaFix->setText(ui->editSkidkaFix->text().left(ui->editSkidkaFix->text().length()-1));
}

void SkidkaDialog::letterClicked(QString a)
{
    ui->editSkidkaFix->setText(ui->editSkidkaFix->text()+a);
}

double SkidkaDialog::skidka()
{
    for(int i=0;i<ui->SkidkaProcLayout->count();i++){
        QRadioButton *rb = (QRadioButton*)ui->SkidkaProcLayout->itemAt(i)->widget();
        if(rb->isChecked()) return rb->property("value").toDouble();
    }
    return 0;
}

double SkidkaDialog::skidka_fix()
{
   bool ok=false;
   double r = ui->editSkidkaFix->text().toInt(&ok);
   if(!ok) return 0;
   return r;
}



