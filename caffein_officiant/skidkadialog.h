#ifndef SKIDKADIALOG_H
#define SKIDKADIALOG_H

#include <QDialog>

namespace Ui {
class SkidkaDialog;
}

class SkidkaDialog : public QDialog
{
    Q_OBJECT

public:
    explicit SkidkaDialog(double proc,double fixed);
    ~SkidkaDialog();

private:
    Ui::SkidkaDialog *ui;
private slots:
    void clearClicked();
    void backspaceClicked();
    void letterClicked(QString a);
public:
    double skidka();
    double skidka_fix();
};

#endif // SKIDKADIALOG_H
