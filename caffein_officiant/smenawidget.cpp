#include "smenawidget.h"
#include "ui_smenawidget.h"
#include <QtSql>
#include <QMessageBox>
#include "schetdialog.h"
SmenaWidget::SmenaWidget(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::SmenaWidget)
{
    ui->setupUi(this);
    ui->tabWidget->setCurrentIndex(0);
    ui->tabWidget->setStyleSheet("tabWidget::tab:disabled { width: 0; height: 0; margin: 0; padding: 0; border: none; }");
}

SmenaWidget::~SmenaWidget()
{
    delete ui;
}

void SmenaWidget::loadData()
{
    qApp->setOverrideCursor(QCursor(Qt::WaitCursor));
    ui->listWidget->clear();

    QSqlQuery query;

    if(!query.exec("delete from cafe_smena where dt_open is null")){
        QMessageBox::warning(this,trUtf8("Ошибка"),trUtf8("Ошибка удаления смен без даты открытия"));
    }

    if(!query.exec("select dt_open,dt_close,prim from cafe_smena order by dt_open desc")){
        QMessageBox::warning(this,trUtf8("Ошибка"),trUtf8("Ошибка загрузки смен"));
    }
    while(query.next()){
        QString text = trUtf8("с %1").arg(query.value(0).toDateTime().toString("dd.MM.yy hh:mm"));
        if(!query.value(1).isNull()){
            text += trUtf8(" по ")+query.value(1).toDateTime().toString("dd.MM.yy hh:mm");
        }
        QListWidgetItem *item = new QListWidgetItem(text);
        item->setData(Qt::UserRole+1,query.value(0));
        item->setData(Qt::UserRole+2,query.value(1));
        ui->listWidget->addItem(item);
    }
    if(ui->listWidget->count()==0){
        if(!query.exec("select min(dt_open) from cafe_schet")){
            QMessageBox::warning(this,trUtf8("Ошибка"),trUtf8("Ошибка получения минимальной даты"));
            return;
        }
        QDateTime dt = QDateTime::currentDateTime();
        if(query.next()){
            dt = query.value(0).toDateTime();
        }
        query.prepare("insert into cafe_smena (dt_open) values (?)");
        query.addBindValue(dt);
        if(!query.exec()){
            QMessageBox::warning(this,trUtf8("Ошибка"),trUtf8("Ошибка создания первой смены"));
            return;
        }
        loadData();
    }
    else{
        QListWidgetItem *item = ui->listWidget->item(0);
        QDateTime dt1 = item->data(Qt::UserRole+1).toDateTime();
        QDateTime dt2;
        dt2 = item->data(Qt::UserRole+2).isNull()?QDateTime::currentDateTime():item->data(Qt::UserRole+2).toDateTime();
        loadSmenaData(dt1,dt2);
    }

    ui->twPr1->setHeaderHidden(false);
    ui->twPr2->setHeaderHidden(false);
    ui->twPr3->setHeaderHidden(false);
    ui->twPr4->setHeaderHidden(false);

    ui->twPr1->setColumnWidth(0,300);
    ui->twPr1->setColumnWidth(1,80);
    ui->twPr1->setColumnWidth(2,80);
    ui->twPr1->setColumnWidth(3,80);

    ui->twPr2->setColumnWidth(0,300);
    ui->twPr2->setColumnWidth(1,80);
    ui->twPr2->setColumnWidth(2,80);
    ui->twPr2->setColumnWidth(3,80);

    ui->twPr3->setColumnWidth(0,300);
    ui->twPr3->setColumnWidth(1,80);
    ui->twPr3->setColumnWidth(2,80);
    ui->twPr3->setColumnWidth(3,80);

    ui->twPr4->setColumnWidth(0,300);
    ui->twPr4->setColumnWidth(1,80);
    ui->twPr4->setColumnWidth(2,80);
    ui->twPr4->setColumnWidth(3,80);




    qApp->restoreOverrideCursor();
}

void SmenaWidget::on_pbZal_clicked()
{
    emit toZal();
}

void SmenaWidget::loadSmenaData(QDateTime dt1, QDateTime dt2)
{
    double v1  = calcVozvrat(1,dt1,dt2);
    double v2  = calcVozvrat(2,dt1,dt2);
    double v3  = calcVozvrat(3,dt1,dt2);
    double v36 = calcVozvrat(36,dt1,dt2);
    ui->labSmenaSumm->setText(trUtf8("%1 р.").arg(calcSmenaSumm(dt1,dt2) - v1 - v2 - v3 - v36));
    ui->labNal->setText(trUtf8("%1 р.").arg(calcSmenaSummNal(dt1,dt2,0) - v1 - v2 - v3 - v36));
    ui->labCard->setText(trUtf8("%1 р.").arg(calcSmenaSummNal(dt1,dt2,1)));
    ui->tabMenu->setTabText(0,trUtf8("Кухня %1 р.").arg(calcMenuSumm(1,dt1,dt2)));
    ui->tabMenu->setTabText(1,trUtf8("Бар %1 р.").arg(calcMenuSumm(2,dt1,dt2)));
    ui->tabMenu->setTabText(2,trUtf8("Кальян %1 р.").arg(calcMenuSumm(3,dt1,dt2)));
    ui->tabMenu->setTabText(3,trUtf8("Имущество %1 р.").arg(calcMenuSumm(36,dt1,dt2)));

    if(!haveMenu(3))  ui->tabMenu->setTabEnabled(2,false);
    if(!haveMenu(36)) ui->tabMenu->setTabEnabled(3,false);
    loadMenuSklad(1,ui->twPr1,dt1,dt2);
    loadMenuSklad(2,ui->twPr2,dt1,dt2);
    //loadMenuSklad(3,ui->twPr3,dt1,dt2);
    //loadMenuSklad(36,ui->twPr4,dt1,dt2);

    loadSkidka(dt1,dt2);
    loadCancels(dt1,dt2);
    loadSchets(dt1,dt2);
    loadVozvrat(dt1,dt2);
}




double SmenaWidget::calcMenuSumm(int type,QDateTime dt1, QDateTime dt2)
{
    QSqlQuery query;
    query.prepare("select sum(price_skidka) from cafe_zakaz where dt>=? and dt<=? and menu_id=?");
    query.addBindValue(dt1);
    query.addBindValue(dt2);
    query.addBindValue(type);
    if(!query.exec()){
        QMessageBox::warning(this,trUtf8("Ошибка"),trUtf8("Ошибка calcSmenaSumm"));
        return 0;
    }

    if(query.next()) return query.value(0).toDouble() - calcVozvrat(type,dt1,dt2);
    return 0;

}

void SmenaWidget::loadMenuSklad(int type, QTreeWidget *tw, QDateTime dt1, QDateTime dt2)
{
    tw->hideColumn(1);
    tw->hideColumn(3);
    tw->clear();
    QSqlQuery query;
    query.prepare("select id,name,portion from cafe_tovar where menu_id=?");
    query.addBindValue(type);
    if(!query.exec()){
        QMessageBox::warning(this,trUtf8("Ошибка"),trUtf8("Ошибка loadMenuSklad"));
        return;
    }
    while(query.next()){
        double portion = query.value(2).toDouble();
        if(portion==0) continue;
        double p = 0;//calcTovarPrihod(query.value(0).toInt(),dt1,dt2)/portion;
        double r = calcTovarRashod(query.value(0).toInt(),dt1,dt2)/portion;
        double o = 0;//calcTovarCurrent(query.value(0).toInt())/portion;
        //if(p!=0 || r!=0 || o!=0){
        if(r>0){
            QTreeWidgetItem *item1 = new QTreeWidgetItem(QStringList() << query.value(1).toString() << QString::number(p) << QString::number(r) << QString::number(o));
            tw->invisibleRootItem()->addChild(item1);
        }
    }


}

double SmenaWidget::calcTovarPrihod(int tid, QDateTime dt1, QDateTime dt2)
{
    QSqlQuery query;
    query.prepare("select sum(volume) from cafe_prihod where tovar_id=? and dt>=? and dt<=?");
    query.addBindValue(tid);
    query.addBindValue(dt1);
    query.addBindValue(dt2);
    if(!query.exec()){
        QMessageBox::warning(this,trUtf8("Ошибка"),trUtf8("Ошибка calcTovarPrihod"));
        return 0;
    }
    if(query.next()){
        return query.value(0).toDouble();
    }
    return 0;
}

double SmenaWidget::calcTovarRashod(int tid, QDateTime dt1, QDateTime dt2)
{
    QSqlQuery query;
    query.prepare("select sum(volume) from cafe_rashod where tovar_id=? and dt>=? and dt<=?");
    query.addBindValue(tid);
    query.addBindValue(dt1);
    query.addBindValue(dt2);
    if(!query.exec()){
        QMessageBox::warning(this,trUtf8("Ошибка"),trUtf8("Ошибка calcTovarRashod"));
        return 0;
    }
    if(query.next()){
        return query.value(0).toDouble();
    }
    return 0;
}

double SmenaWidget::calcTovarCurrent(int tid)
{

    QSqlQuery query;
    query.prepare("select volume from cafe_sklad where tovar_id=?");
    query.addBindValue(tid);
    if(!query.exec()){
        QMessageBox::warning(this,trUtf8("Ошибка"),trUtf8("Ошибка calcTovarRashod"));
        return 0;
    }
    if(query.next()){
        return query.value(0).toDouble();
    }
    return 0;
}

void SmenaWidget::loadSkidka(QDateTime dt1, QDateTime dt2)
{
    QSqlQuery query;
    query.prepare("select id,skidka,skidka_fix,num,dt_open,price,price_skidka from cafe_schet where (skidka!=0 or skidka_fix!=0) and dt_open>? and dt_close<?");
    query.addBindValue(dt1);
    query.addBindValue(dt2);
    if(!query.exec()){
        QMessageBox::warning(this,trUtf8("Ошибка"),trUtf8("Ошибка получения скидок"));
        return;
    }
    ui->twSkidka->clear();
    while(query.next()){
        int id = query.value(0).toInt();
        double skidka_proc = query.value(1).toDouble();
        double skidka_fix  = query.value(2).toDouble();
        int num = query.value(3).toInt();
        QDateTime dt_open = query.value(4).toDateTime();
        double price = query.value(5).toDouble();
        double price_skidka = query.value(6).toDouble();


        QString name = trUtf8("Счёт №%1").arg(num);
        QString date = dt_open.toString("dd.MM.yy hh:mm");
        QStringList skidka;
        if(skidka_proc!=0) skidka << QString::number(skidka_proc)+"%";
        if(skidka_fix!=0)  skidka << QString::number(skidka_fix)+" руб.";
        QString sprice = trUtf8("%1 руб.").arg(price);
        QString sprice_final = trUtf8("%1 руб.").arg(price_skidka);
        QString skidka_summ = trUtf8("%1 руб.").arg(price-price_skidka);
        QTreeWidgetItem *item = new QTreeWidgetItem(QStringList() << name << date << skidka.join(",") << sprice << sprice_final << skidka_summ);
        item->setData(0,Qt::UserRole+1,id);
        ui->twSkidka->invisibleRootItem()->addChild(item);

    }

}

void SmenaWidget::loadCancels(QDateTime dt1, QDateTime dt2)
{
    QSqlQuery query;
    query.prepare("select s.num,u.name,cnt,zh.dt,s.id from cafe_zakazhistory zh,cafe_schet s,cafe_usluga u where zh.schet_id=s.id and zh.usluga_id=u.id and cnt<0 and zh.dt>=? and zh.dt<=?  order by id desc");
    query.addBindValue(dt1);
    query.addBindValue(dt2);
    if(!query.exec()){
        QMessageBox::warning(this,trUtf8("Ошибка"),trUtf8("Ошибка loadCancels"));
        return;
    }
    ui->twCancels->clear();
    while(query.next()){
        QString num = query.value(0).toString();
        QString uname = query.value(1).toString();
        int cnt = query.value(2).toInt();
        QString dt = query.value(3).toDateTime().toString("dd.MM.yy hh:mm");
        int sid = query.value(4).toInt();
        QTreeWidgetItem *item = new QTreeWidgetItem(QStringList() << num << dt << uname << QString::number(0-cnt) );
        item->setData(0,Qt::UserRole+1,sid);
        ui->twCancels->invisibleRootItem()->addChild(item);
    }
}

void SmenaWidget::loadSchets(QDateTime dt1, QDateTime dt2)
{
    QSqlQuery query;
    query.prepare("select s.id,num,dt_open,dt_close,price,skidka,skidka_fix,price_skidka,card,prim,t.name from cafe_schet s, cafe_stol t where s.stol_id=t.id and dt_open>=? and dt_open<=? order by id desc");
    query.addBindValue(dt1);
    query.addBindValue(dt2);
    if(!query.exec()){
        QMessageBox::warning(this,trUtf8("Ошибка"),trUtf8("Ошибка loadSchets"));
        return;
    }
    ui->twSchets->clear();
    while(query.next()){
        int sid = query.value(0).toInt();
        QString num       = query.value(1).toString();
        QString dt_open   = query.value(2).toDateTime().toString("dd.MM.yy hh:mm");
        QString dt_close  = query.value(3).toDateTime().isNull()?trUtf8("открыт"):query.value(3).toDateTime().toString("dd.MM.yy hh:mm");
        QString price     = query.value(4).toString();
        QString skidka    = query.value(5).toInt()==0?"":query.value(5).toString()+"%";
        QString skidka_fix= query.value(6).toInt()==0?"":query.value(6).toString()+trUtf8(" руб.");
        QString price_skidka= query.value(7).toString();
        QString card      = query.value(8).toInt()==0?trUtf8("наличные"):trUtf8("карта");
        QString prim      = query.value(9).toString();
        QString tname     = query.value(10).toString();
        QStringList slist;
        if(skidka!="")  slist << skidka;
        if(skidka_fix!="") slist << skidka_fix;
        skidka = slist.join(",");
        QTreeWidgetItem *item = new QTreeWidgetItem(QStringList() << num << dt_open << dt_close << price << skidka << price_skidka << card << tname  << prim);
        item->setData(0,Qt::UserRole+1,sid);
        ui->twSchets->invisibleRootItem()->addChild(item);
    }
    ui->twSchets->setColumnWidth(0,40);
    ui->twSchets->setColumnWidth(1,100);
    ui->twSchets->setColumnWidth(2,100);
}

void SmenaWidget::loadVozvrat(QDateTime dt1, QDateTime dt2)
{

        QSqlQuery query;
        query.prepare("select u.name,sum(v.cnt),sum(v.summ) from cafe_vozvrat v,cafe_usluga u where v.usluga_id=u.id and v.dt>=? and v.dt<=? group by u.name");
        query.addBindValue(dt1);
        query.addBindValue(dt2);
        if(!query.exec()){
            qWarning() << Q_FUNC_INFO << dt1 << dt2;
            QMessageBox::warning(this,trUtf8("Ошибка"),trUtf8("Ошибка loadVozvrat"));
            return;
        }
        ui->twVozvrat->clear();
        while(query.next()){
            QString name  = query.value(0).toString();
            QString cnt   = query.value(1).toString();
            QString price = query.value(2).toString();
            QTreeWidgetItem *item = new QTreeWidgetItem(QStringList() << name << cnt << price);
            ui->twVozvrat->invisibleRootItem()->addChild(item);
        }
        ui->twVozvrat->setColumnWidth(0,300);
}

double SmenaWidget::calcVozvrat(int mid,QDateTime dt1,QDateTime dt2)
{
    QSqlQuery query;
    query.prepare("select sum(summ) from cafe_vozvrat v, cafe_usluga u, cafe_menu m where v.usluga_id=u.id and u.menu_id=m.id and pid=? and v.dt>=? and v.dt<=?");
    query.addBindValue(mid);
    query.addBindValue(dt1);
    query.addBindValue(dt2);
    if(!query.exec()){
        QMessageBox::warning(this,trUtf8("Ошибка"),trUtf8("Ошибка calcVozvrat"));
        return 0;
    }

    if(query.next()) return query.value(0).toDouble();
    return 0;
}

bool SmenaWidget::haveMenu(int mid)
{
    QSqlQuery query;
    query.prepare("select count(*) from cafe_menu where id=?");
    query.addBindValue(mid);
    if(!query.exec()){
        QMessageBox::warning(this,trUtf8("Ошибка"),trUtf8("Ошибка haveMenu"));
        return true;
    }
    query.next();
    return query.value(0).toInt()>0;
}

double SmenaWidget::calcSmenaSumm(QDateTime dt1, QDateTime dt2)
{
    QSqlQuery query;
    query.prepare("select sum(price_skidka) from cafe_schet where dt_open>=? and dt_open<=?");
    query.addBindValue(dt1);
    query.addBindValue(dt2);
    if(!query.exec()){
        QMessageBox::warning(this,trUtf8("Ошибка"),trUtf8("Ошибка calcSmenaSumm"));
        return 0;
    }

    if(query.next()) return query.value(0).toDouble();
    return 0;
}

double SmenaWidget::calcSmenaSummNal(QDateTime dt1, QDateTime dt2,int card=0)
{
    QSqlQuery query;
    query.prepare("select sum(price_skidka) from cafe_schet where card=? and dt_open>=? and dt_open<=?");
    query.addBindValue(card);
    query.addBindValue(dt1);
    query.addBindValue(dt2);
    if(!query.exec()){
        QMessageBox::warning(this,trUtf8("Ошибка"),trUtf8("Ошибка calcSmenaSumm"));
        return 0;
    }

    if(query.next()) return query.value(0).toDouble();
    return 0;
}



void SmenaWidget::on_pbNewSmena_clicked()
{
    QDateTime dt = QDateTime::currentDateTime();

    QSqlQuery query;
    query.prepare("update cafe_smena set dt_close=? where dt_close is null");
    query.addBindValue(dt);
    if(!query.exec()){
        QMessageBox::warning(this,trUtf8("Ошибка"),trUtf8("Ошибка закрытия смены"));
        return;
    }

    query.prepare("insert into cafe_smena (dt_open) values (?)");
    query.addBindValue(dt);
    if(!query.exec()){
        QMessageBox::warning(this,trUtf8("Ошибка"),trUtf8("Ошибка создания смены"));
        return;
    }
    loadData();
}



void SmenaWidget::on_listWidget_clicked(const QModelIndex &index)
{
    if(!index.isValid()) return;
    QListWidgetItem *item = ui->listWidget->item(index.row());
    if(item==0) return;
    QDateTime dt1 = item->data(Qt::UserRole+1).toDateTime();
    QDateTime dt2;
    dt2 = item->data(Qt::UserRole+2).isNull()?QDateTime::currentDateTime():item->data(Qt::UserRole+2).toDateTime();
    qApp->setOverrideCursor(QCursor(Qt::WaitCursor));
    loadSmenaData(dt1,dt2);
    qApp->restoreOverrideCursor();
}


void SmenaWidget::on_twSchets_clicked(const QModelIndex &index)
{
    if(!index.isValid()) return;
    QTreeWidgetItem *item = ui->twSchets->invisibleRootItem()->child(index.row());
    if(item==0) return;
    int sid = item->data(0,Qt::UserRole+1).toInt();
    SchetDialog sd(sid);
    sd.exec();
}
