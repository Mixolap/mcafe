#ifndef SMENAWIDGET_H
#define SMENAWIDGET_H

#include <QWidget>
#include <QTreeWidget>
namespace Ui {
class SmenaWidget;
}

class SmenaWidget : public QWidget
{
    Q_OBJECT

public:
    explicit SmenaWidget(QWidget *parent = 0);
    ~SmenaWidget();

    void loadData();
private slots:
    void on_pbZal_clicked();

    void on_pbNewSmena_clicked();


    void on_listWidget_clicked(const QModelIndex &index);

    void on_twSchets_clicked(const QModelIndex &index);

private:
    Ui::SmenaWidget *ui;

    void loadSmenaData(QDateTime dt1,QDateTime dt2);
    double calcSmenaSumm(QDateTime dt1,QDateTime dt2);
    double calcSmenaSummNal(QDateTime dt1, QDateTime dt2, int card);
    double calcMenuSumm(int type,QDateTime dt1, QDateTime dt2);
    void loadMenuSklad(int type,QTreeWidget *tw,QDateTime dt1, QDateTime dt2);    
    double calcTovarPrihod(int tid,QDateTime dt1, QDateTime dt2);
    double calcTovarRashod(int tid,QDateTime dt1, QDateTime dt2);
    double calcTovarCurrent(int tid);
    void loadSkidka(QDateTime dt1, QDateTime dt2);
    void loadCancels(QDateTime dt1, QDateTime dt2);
    void loadSchets(QDateTime dt1, QDateTime dt2);
    void loadVozvrat(QDateTime dt1, QDateTime dt2);
    double calcVozvrat(int mid, QDateTime dt1, QDateTime dt2);
    bool haveMenu(int mid);
signals:
    void toZal();

};

#endif // SMENAWIDGET_H
