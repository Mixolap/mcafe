#include "stolbutton.h"
#include <QPainter>
#include <QtSql>
StolButton::StolButton(int id, QString name, int status)
{
    _id = id;
    _status = status;
    _precheck = false;

    setText(name);


    updateData();
}

StolButton::~StolButton()
{

}

void StolButton::updateData()
{
    _listBron.clear();
    QSqlQuery query;
    query.prepare("select dt from cafe_bron where stol_id=? and dt>=? and dt<=? order by dt");
    query.addBindValue(_id);
    query.addBindValue(QDateTime::currentDateTime().addSecs(-60*60));
    query.addBindValue(QDateTime::currentDateTime().addSecs(60*60*20));
    if(!query.exec()){
        qWarning() << Q_FUNC_INFO << "ERROR BRON" << _id;
        return;
    }
    while(query.next()){
        _listBron << query.value(0).toDateTime().toString("HH:mm");
    }

    query.prepare("select count(*) from cafe_schet where stol_id=? and precheck=1 and status=0");
    query.addBindValue(_id);
    if(!query.exec()){
        qWarning() << Q_FUNC_INFO << "ERROR PRECHECK" << _id << query.lastError().text();
        return;
    }
    while(query.next()){
        _precheck = query.value(0).toInt()>0;
    }

    query.prepare("select count(*) from cafe_schet where stol_id=? and status=0");
    query.addBindValue(_id);
    if(!query.exec()){
        qWarning() << Q_FUNC_INFO << "ERROR PRECHECK" << _id << query.lastError().text();
        return;
    }
    while(query.next()){
        _status = query.value(0).toInt()>0?1:0;
    }
    if(_status == 1){
        setStyleSheet("background-color: rgb(218, 83, 44);font: 75 20pt 'Sans' bold;");
    }else{
        setStyleSheet("background-color: #00a300;font: 75 20pt 'Sans' bold;");
    }
}

void StolButton::paintEvent(QPaintEvent *e)
{
    QPushButton::paintEvent(e);

    QPainter painter(this);
    if(_listBron.count()>0){
        painter.drawPixmap(this->width()-35,5,30,30,QPixmap(":/i/imgs/bron.png"));
        int h = 30+15;
        painter.setFont(QFont("serif",10));
        foreach(QString text,_listBron){
            painter.drawText(this->width()-38,h,text);
            h+=15;
        }
    }

    if(_precheck)
        painter.drawPixmap(0,this->height()-35,30,30,QPixmap(":/i/imgs/precheck.png"));


}

