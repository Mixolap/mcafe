#ifndef STOLBUTTON_H
#define STOLBUTTON_H

#include <QPushButton>

class StolButton : public QPushButton
{
    Q_OBJECT
    int _id;
    int _status;
    QStringList _listBron;
    bool _precheck;
public:
    StolButton(int id,QString name,int status);
    ~StolButton();

    int id(){return _id;}
    QString name(){return text();}
public slots:
    void updateData();
protected:
    void paintEvent(QPaintEvent *e);
};

#endif // STOLBUTTON_H
