#include "tabzakazwidget.h"
#include "ui_tabzakazwidget.h"
#include <QtSql>
#include <QMessageBox>
#include "skidkadialog.h"
#include "depositdialog.h"
#include "primdialog.h"
#include "http.h"
#include "closeschetdialog.h"
#include "config.h"
#include "adminpassworddialog.h"

TabZakazWidget::TabZakazWidget(Schet *z) :
    QWidget(0),
    ui(new Ui::TabZakazWidget)
{
    ui->setupUi(this);

    ui->tvDozakaz->setModel(&_modelNewZakaz);
    ui->tvZakaz->setModel(&_modelZakaz);
    _schet = z;

	if(Config::get_option("CONFIRM_ZAKAZ")=="0"){
		ui->tvDozakaz->hide();
        ui->pushButton->hide();
        ui->label_11->hide();
        ui->labDozakazSum->hide();
	}
    loadZakaz();
}


TabZakazWidget::~TabZakazWidget()
{
    delete _schet;
    delete ui;
}

void TabZakazWidget::addZakaz(int id,int mid, const QString &name,double price)
{
    bool added = false;
    for(int i=0;i<_modelNewZakaz.rowCount();i++){
        if(_modelNewZakaz.item(i)->data().toInt()==id){
            int curr_count = _modelNewZakaz.item(i,1)->text().toInt();
            _modelNewZakaz.item(i,1)->setText(QString::number(curr_count+1));
            added = true;
            break;
        }
    }

    if(!added){
        QStandardItem *item1 = new QStandardItem(name);
        item1->setEditable(false);
        item1->setData(id);        
        item1->setData(mid,Qt::UserRole+2);
        QStandardItem *item2 = new QStandardItem("1");
        item2->setEditable(false);
        item2->setData(price);
        _modelNewZakaz.appendRow(QList<QStandardItem*>() << item1 << item2);
        ui->tvDozakaz->scrollToBottom();
    }

    recalcPriceDozakaz();
    ui->tvDozakaz->setColumnWidth(0,250);
	if(Config::get_option("CONFIRM_ZAKAZ")=="0"){
		on_pushButton_clicked();
	}
}


void TabZakazWidget::on_tvDozakaz_clicked(const QModelIndex &index)
{
    if(!index.isValid()) return;

    QStandardItem *item2 = _modelNewZakaz.item(index.row(),1);
    if(item2==0) return;

    int cnt = item2->text().toInt()-1;
    if(cnt<=0){
        _modelNewZakaz.removeRow(index.row());
    }else{
        item2->setText(QString::number(cnt));
    }

    recalcPriceDozakaz();
}

void TabZakazWidget::loadZakaz()
{
    if(_schet==0) return;
    Schet *s = _schet;
    if(s==0) return;

    QSqlQuery query;
    query.prepare("select z.id,m.id as mid,m.name,u.id as uid,u.name,cnt,u.price,z.price from cafe_zakaz z,cafe_menu m,cafe_usluga u where z.menu_id=m.id and z.usluga_id=u.id and cnt>0 and schet_id=? order by m.id");
    query.addBindValue(s->id);
    if(!query.exec()){
        qWarning() << Q_FUNC_INFO << query.lastError().text();
        return;
    }
    freeModel(&_modelZakaz);
    double total = 0;
    while(query.next()){
        int cnt = query.value(5).toInt();
        double price = query.value(6).toDouble();
        QStandardItem *item1 = new QStandardItem(query.value(4).toString());
        //qDebug() << query.value(2).toInt() << query.value(8).toInt();
        item1->setData(query.value(3).toInt(),Qt::UserRole+1); // код услуги
        item1->setData(query.value(1).toInt(),Qt::UserRole+2); // код раздела меню
        item1->setData(query.value(0).toInt(),Qt::UserRole+3); // код заказа
        item1->setData(query.value(6).toDouble(),Qt::UserRole+4); // цена
        QStandardItem *item2 = new QStandardItem(QString::number(cnt));
        QStandardItem *item3 = new QStandardItem(trUtf8("%1 р.").arg(cnt*query.value(6).toDouble()));
        _modelZakaz.appendRow(QList<QStandardItem*>() << item1 << item2<< item3);
        total += price*cnt;
        /*Zakaz *z = new Zakaz;
        z->id = query.value(0).toInt();
        z->mid = query.value(1).toInt();
        z->mname = query.value(2).toString();
        z->uid = query.value(3).toInt();
        z->uname = query.value(4).toString();
        z->cnt = query.value(5).toInt();
        z->price = query.value(6).toDouble();
        z->total_price = query.value(7).toDouble();
        _zakazs.append(z);*/
    }
    ui->tvZakaz->setColumnWidth(0,200);
    // всего
    ui->labTotal->setText(trUtf8("%1 р.").arg(total));
    // скидка
    ui->labSkidka->setText(trUtf8("нет"));
    //qDebug() << _schet->id << _schet->skidka;

    double skidka_summ=0;
    if(_schet->skidka>0){
        skidka_summ = total*(_schet->skidka)/100.;
        ui->labSkidka->setText(trUtf8("%1 р. (%2%)").arg(skidka_summ).arg(_schet->skidka));
    }



    if(_schet->skidka_fix>0){        
        skidka_summ += _schet->skidka_fix;
        ui->labSkidka->setText(trUtf8("%1 р.").arg(skidka_summ));
        if(_schet->skidka>0) ui->labSkidka->setText(trUtf8("%1 р. (%2%)").arg(skidka_summ).arg(_schet->skidka));
    }



    // депозит
    double deposit = 0;
    query.prepare("select sum(summ) from cafe_deposit where schet_id=?");
    query.addBindValue(_schet->id);
    if(!query.exec()){
        qWarning() << Q_FUNC_INFO << _schet->id;
        QMessageBox::warning(this,trUtf8("Ошибка подсчёта депозита"),query.lastError().text());
    }
    if(query.next()){
        deposit = query.value(0).toInt();
    }
    // к оплате
    double oplata = total-skidka_summ-deposit;

    if(deposit>0){
        double ostatok = -oplata;
        if(ostatok>0){
            ui->labDeposit->setText(trUtf8("%1 р. (%2 р.)").arg(deposit).arg(ostatok));
        }else{
            ui->labDeposit->setText(trUtf8("%1 р.").arg(deposit));
        }
    }else{
        ui->labDeposit->setText(trUtf8("нет"));
    }

    if(oplata<0) oplata = 0;
    ui->labOplata->setText(trUtf8("%1 р.").arg(oplata));


}

void TabZakazWidget::recalcPriceDozakaz()
{
    double total_price = 0;
    for(int i=0;i<_modelNewZakaz.rowCount();i++){
        QStandardItem *item = _modelNewZakaz.item(i,1);
        total_price += item->data().toDouble()*item->text().toInt();
    }
    ui->labDozakazSum->setText(trUtf8("%1 р.").arg(total_price));
}

void TabZakazWidget::logSchet(int user_id, int schet_id, int op, const QString &text)
{
    QSqlQuery query;
    query.prepare("insert into cafe_schethistory (user_id,schet_id,operation,text,dt) values (?,?,?,?,?)");
    query.addBindValue(user_id);
    query.addBindValue(schet_id);
    query.addBindValue(op);
    query.addBindValue(text);
    query.addBindValue(QDateTime::currentDateTime());
    if(!query.exec()){
        qWarning() << Q_FUNC_INFO << query.lastError().text() << user_id << schet_id << op << text;
        QMessageBox::warning(this,trUtf8("Ошибка аудита счёта"),query.lastError().text());
    }
}

int TabZakazWidget::newSchetNum()
{
    QString v = Config::get_option("schet_num");
    if(v.isNull()){
        Config::set_option("schet_num","1");
        return 1;
    }
    Config::set_option("schet_num",QString::number(v.toInt()+1));
    return v.toInt()+1;
}

int TabZakazWidget::zakazId(int sid, int uid, int mid, int cnt, double price)
{
    //qDebug() << sid << uid << mid << cnt <<price;
    int id = 0;
    QSqlQuery query;
    query.prepare("select id,cnt,price from cafe_zakaz where schet_id=? and menu_id=? and usluga_id=?");
    query.addBindValue(sid);
    query.addBindValue(mid);
    query.addBindValue(uid);
    if(!query.exec()){
        qWarning() << Q_FUNC_INFO << query.lastError().text() << sid << mid << uid;
        QMessageBox::warning(this,trUtf8("Ошибка получения параметров заказа"),query.lastError().text());
        return 0;
    }
    if(query.next()){
        // Заказ существует
        id = query.value(0).toInt();
        int xcnt = query.value(1).toInt();
        int xprice = query.value(2).toDouble();
        query.prepare("update cafe_zakaz set cnt=?,price=? where id=?");
        query.addBindValue(xcnt+cnt);
        query.addBindValue(xprice+cnt*price);
        query.addBindValue(id);
        if(!query.exec()){
            qWarning() << Q_FUNC_INFO << query.lastError().text() << id << xcnt << xprice;
            QMessageBox::warning(this,trUtf8("Ошибка обновления заказа"),query.lastError().text());
        }
        //qDebug() << "existing zakaz_id" << id;
    }else{
        query.prepare("insert into cafe_zakaz (schet_id,status,menu_id,usluga_id,cnt,price,dt,price_skidka) values (?,?,?,?,?,?,?,?)");
        //qDebug() << "inserting" << mid << uid << cnt <<price;
        query.addBindValue(sid);
        query.addBindValue(0);
        query.addBindValue(mid);
        query.addBindValue(uid);
        query.addBindValue(cnt);
        query.addBindValue(cnt*price);
        query.addBindValue(QDateTime::currentDateTime());
        query.addBindValue(cnt*price);
        if(!query.exec()){
            qWarning() << Q_FUNC_INFO << query.lastError().text() << sid<<mid<<uid;
            QMessageBox::warning(this,trUtf8("Ошибка добавления заказа"),query.lastError().text());
            return 0;
        }
        id = Config::lastInsertId("cafe_zakaz");
		//id = query.lastInsertId().toInt();
        qDebug() << "new zakaz_id" << id;
    }

    //qDebug() << "zakaz_id" << id;
    return id;

}




void TabZakazWidget::freeModel(QStandardItemModel *model)
{
    while(model->rowCount()){
        foreach(QStandardItem *item,model->takeRow(0)){
            delete item;
        }
    }
}

void TabZakazWidget::newSchet()
{
    int num = newSchetNum();
    QSqlQuery query;
    query.prepare("insert into cafe_schet (stol_id,status,price,name,dt_open,user_open_id,card,precheck,prim,num,skidka,price_skidka) values (?,?,?,?,?,?,?,?,?,?,?,?) ");
    query.addBindValue(_schet->stol_id);
    query.addBindValue(0);
    query.addBindValue(0);
    query.addBindValue(QString::number(num));
    query.addBindValue(QDateTime::currentDateTime());
    query.addBindValue(currentUser());
    query.addBindValue(0);
    query.addBindValue(0);
    query.addBindValue(QString());
    query.addBindValue(num);
    query.addBindValue(0);
    query.addBindValue(0);

    if(!query.exec()){
        qWarning() << Q_FUNC_INFO << num;
        QMessageBox::warning(this,trUtf8("Ошибка добавления счёта"),query.lastError().text());
        return;
    }
    _schet->id = Config::lastInsertId("cafe_schet");//query.lastInsertId().toInt();
	qDebug() << "new_schet" << _schet->id;
    logSchet(1,_schet->id,2,trUtf8("счет №%1 открыт").arg(_schet->num));
    // стол в состояние "занят"
    query.prepare("update cafe_stol set status=1 where id=?");
    query.addBindValue(_schet->stol_id);
    if(!query.exec()){
        qWarning() << Q_FUNC_INFO << num;
        qApp->restoreOverrideCursor();
        QMessageBox::warning(this,trUtf8("Ошибка обновления статуса стола"),query.lastError().text());
        return;
    }
}




void TabZakazWidget::on_pushButton_clicked()
{
    // подтверждение заказа
    if(_modelNewZakaz.rowCount()==0) return;


    qApp->setOverrideCursor(QCursor(Qt::WaitCursor));

    QSqlQuery query;
    if(_schet->id==0){
        newSchet();
    }


    for(int i=0;i<_modelNewZakaz.rowCount();i++){
        QStandardItem *item1 = _modelNewZakaz.item(i,0);
        QStandardItem *item2 = _modelNewZakaz.item(i,1);
        int uid = item1->data().toInt();
        int mid = item1->data(Qt::UserRole+2).toInt();
        int cnt = item2->text().toInt();
        int price = item2->data().toDouble();

        //qDebug() << "adding" << uid << mid << cnt << price;
		int zid = zakazId(_schet->id,uid,mid,cnt,price);
        query.prepare("insert into cafe_zakazhistory (schet_id,zakaz_id,menu_id,usluga_id,cnt,price,dt,user_id,status) values (?,?,?,?,?,?,?,?,?)");
        query.addBindValue(_schet->id);
        query.addBindValue(zid);
        query.addBindValue(mid);
        query.addBindValue(uid);
        query.addBindValue(cnt);
        query.addBindValue(cnt * price);
        query.addBindValue(QDateTime::currentDateTime());
        query.addBindValue(currentUser());
        query.addBindValue(1);
        if(!query.exec()){
            qWarning() << Q_FUNC_INFO <<  mid << uid << cnt << price << _schet->id;
            qApp->restoreOverrideCursor();
            QMessageBox::warning(this,trUtf8("Ошибка добавления истории заказа"),query.lastError().text());            
            return;
        }
        // запись в расход выполняется по триггеру !!!!
    }

    freeModel(&_modelNewZakaz);
    reloadSchet();
    loadZakaz();
    recalcPriceDozakaz();
    qApp->restoreOverrideCursor();
}

int TabZakazWidget::currentUser()
{
    return Config::user_id;
}

QString TabZakazWidget::username()
{
    return Config::username;
}

QString TabZakazWidget::password()
{
    return Config::password;
}

void TabZakazWidget::reloadSchet()
{
    if(_schet->id==0) return;

    QSqlQuery query;
    query.prepare("select id,name,price,dt_open,user_open_id,skidka,price_skidka,card,precheck,prim,skidka_fix,num from cafe_schet where id=? and status=0");
    query.addBindValue(_schet->id);
    if(!query.exec()){
        qWarning() << Q_FUNC_INFO << query.lastError().text();
        return;
    }

    while(query.next()){
        Schet *z = _schet;
        z->id           = query.value(0).toInt();
        z->name         = query.value(1).toString();
        z->price        = query.value(2).toDouble();
        z->dt_open      = query.value(3).toDateTime();
        z->user_open_id = query.value(4).toInt();
        z->skidka       = query.value(5).toInt();
        z->price_skidka = query.value(6).toDouble();
        z->card         = query.value(7).toInt();
        z->precheck     = query.value(8).toInt();
        z->prim         = query.value(9).toString();
        z->skidka_fix   = query.value(10).toDouble();
        z->num          = query.value(11).toInt();
    }

}


void TabZakazWidget::on_pushButton_2_clicked()
{
    // скидка
    SkidkaDialog sd(_schet->skidka,_schet->skidka_fix);
    if(sd.exec()){
        _schet->skidka = sd.skidka();
        _schet->skidka_fix = sd.skidka_fix();
        //qDebug() << _schet->skidka << _schet->skidka_fix;
        QSqlQuery query;
        query.prepare("update cafe_schet set skidka=?,skidka_fix=? where id=?");
        query.addBindValue(_schet->skidka);
        query.addBindValue(_schet->skidka_fix);
        query.addBindValue(_schet->id);
        if(!query.exec()){
            qWarning() << Q_FUNC_INFO;
            QMessageBox::warning(this,trUtf8("Ошибка установки скидки"),query.lastError().text());
            return;
        }


        if(_schet->skidka>0){
            logSchet(currentUser(),_schet->id,3,trUtf8("указана скидка %1%").arg(_schet->skidka));
        }
        if(_schet->skidka_fix>0){
            logSchet(currentUser(),_schet->id,3,trUtf8("указана скидка %1 р.").arg(_schet->skidka_fix));
        }
        reloadSchet();
        loadZakaz();
    }

}

void TabZakazWidget::on_pbDeposite_clicked()
{
    if(_schet->id==0) newSchet();
    DepositDialog dd(_schet->id);
    dd.exec();
    reloadSchet();
    loadZakaz();
}

void TabZakazWidget::on_pbPrim_clicked()
{
    // примечание
    if(_schet->id==0) newSchet();
    PrimDialog pd(_schet->id);
    pd.exec();
}

void TabZakazWidget::on_pbAddSchet_clicked()
{
    // добавить счёт
    emit makeNewSchet();
}

void TabZakazWidget::on_pbPrecheck_clicked()
{
    // напечатать пречек
    Http h;
    h.setHost(Config::server,Config::port);
    QByteArray data=h.get(QString("/precheck/%1/").arg(_schet->id));
    if(!data.isEmpty()){
        QMessageBox::warning(this,trUtf8("Ошибка печати пречека"),data);
    }else{
        QMessageBox::information(this,trUtf8("Печать пречека"),trUtf8("Пречек отправлен на печать"));
    }
}

void TabZakazWidget::on_pbClose_clicked()
{
    // закрыть счёт

    if(_schet->id==0){
        newSchet();
    }

    CloseSchetDialog sd;
    if(sd.exec()){
        qApp->setOverrideCursor(QCursor(Qt::WaitCursor));
        Http h;
        h.setHost(Config::server,Config::port);
        QByteArray data;
        if(sd.type()==0){
            data =h.get(QString("/check/close/%1/?uname=%2&passwd=%3").arg(_schet->id).arg(username()).arg(password()));
        }else{
            data =h.get(QString("/check/close/%1/%2/?uname=%3&passwd=%4").arg(_schet->id).arg(sd.type()).arg(username()).arg(password()));
        }
        if(!data.isEmpty() || data.isNull()){
            QMessageBox::warning(this,trUtf8("Ошибка печати чека"),data);
        }else{
            //QMessageBox::information(this,trUtf8("Печать чека"),trUtf8("Пречек отправлен на печать"));
            _modelNewZakaz.clear();
            _modelZakaz.clear();
            _schet->id=0;
            loadZakaz();
            recalcPriceDozakaz();
        }
        qApp->restoreOverrideCursor();
    }
}

void TabZakazWidget::on_tvZakaz_clicked(const QModelIndex &index)
{
    if(!index.isValid()) return;

    AdminPasswordDialog ad;
    if(Config::get_option("admin_password").isEmpty() || ad.exec()){
        QStandardItem *item = _modelZakaz.item(index.row(),0);

        QString uname = item->text();
        int uid      = item->data(Qt::UserRole+1).toInt();
        int mid      = item->data(Qt::UserRole+2).toInt();
        int zid      = item->data(Qt::UserRole+3).toInt();
        double price = item->data(Qt::UserRole+4).toDouble();
        int cnt = -1;

        //ZakazHistory(schet=zakaz.schet,status=1,usluga=zakaz.usluga,cnt=0-cnt,price=zakaz.usluga.price*cnt,user=user).save()
        QSqlQuery query;
        query.prepare("insert into cafe_zakazhistory (schet_id,zakaz_id,menu_id,usluga_id,cnt,price,dt,user_id,status) values (?,?,?,?,?,?,?,?,?)");
        query.addBindValue(_schet->id);
        query.addBindValue(zid);
        query.addBindValue(mid);
        query.addBindValue(uid);
        query.addBindValue(cnt);
        query.addBindValue(cnt * price);
        query.addBindValue(QDateTime::currentDateTime());
        query.addBindValue(currentUser());
        query.addBindValue(1);
        if(!query.exec()){
            qWarning() << Q_FUNC_INFO << _schet->id <<  mid << uid << cnt << price << uname;
            //qApp->restoreOverrideCursor();
            QMessageBox::warning(this,trUtf8("Ошибка удаления позиции"),query.lastError().text());
            return;
        }

        zakazId(_schet->id,uid,mid,cnt,price);
        logSchet(currentUser(),_schet->id,6,trUtf8("отмена услуги %1 в количестве %2").arg(uname).arg(-cnt));

        reloadSchet();
        loadZakaz();
        recalcPriceDozakaz();
    }
}
