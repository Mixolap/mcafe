#ifndef TABZAKAZWIDGET_H
#define TABZAKAZWIDGET_H

#include <QWidget>
#include <QStandardItemModel>
#include <QDateTime>
#include "config.h"
namespace Ui {
class TabZakazWidget;
}



struct Zakaz{
    int id;
    int mid;
    QString mname;
    int uid;
    QString uname;
    int cnt;
    double price;
    double total_price;
};


class TabZakazWidget : public QWidget
{
    Q_OBJECT
    QStandardItemModel _modelNewZakaz;
    QStandardItemModel _modelZakaz;
    Schet *_schet;
    QList<Zakaz*> _zakazs;
public:
    explicit TabZakazWidget(Schet *z);
    ~TabZakazWidget();

    void addZakaz(int id, int mid, const QString &name, double price);
private slots:


    void on_tvDozakaz_clicked(const QModelIndex &index);

    void on_pushButton_clicked();

    void on_pushButton_2_clicked();

    void on_pbDeposite_clicked();

    void on_pbPrim_clicked();

    void on_pbAddSchet_clicked();

    void on_pbPrecheck_clicked();

    void on_pbClose_clicked();

    void on_tvZakaz_clicked(const QModelIndex &index);

private:
    Ui::TabZakazWidget *ui;
    int currentUser();
    QString username();
    QString password();
    void reloadSchet();
    void loadZakaz();
    void recalcPriceDozakaz();
    void logSchet(int user_id,int schet_id,int op,const QString &text);
    int newSchetNum();
    int zakazId(int sid,int uid,int mid,int cnt,double price);
    void freeModel(QStandardItemModel *model);
    void newSchet();
signals:
    void makeNewSchet();
};

#endif // TABZAKAZWIDGET_H
