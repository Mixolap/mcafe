#include "vozvratwidget.h"
#include "ui_vozvratwidget.h"
#include "config.h"
#include <QtSql>
#include <QMessageBox>
VozvratWidget::VozvratWidget(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::VozvratWidget)
{
    ui->setupUi(this);
    connect(ui->menuWidget,SIGNAL(uslugaClicked(int,QString,double,int)),SLOT(onUslugaClicked(int,QString,double,int)));

    ui->tvVozvrat->setModel(&_modelVozvrat);
}

VozvratWidget::~VozvratWidget()
{
    delete ui;
}

void VozvratWidget::on_pbToZal_clicked()
{
    emit toZal();
}

void VozvratWidget::onUslugaClicked(int id, QString name, double price, int mid)
{
    bool added = false;
    for(int i=0;i<_modelVozvrat.rowCount();i++){
        if(_modelVozvrat.item(i)->data().toInt()==id){
            int curr_count = _modelVozvrat.item(i,1)->text().toInt();
            _modelVozvrat.item(i,1)->setText(QString::number(curr_count+1));
            added = true;
            break;
        }
    }

    if(!added){
        QStandardItem *item1 = new QStandardItem(name);
        item1->setEditable(false);
        item1->setData(id);
        item1->setData(mid,Qt::UserRole+2);
        QStandardItem *item2 = new QStandardItem("1");
        item2->setEditable(false);
        item2->setData(price);
        _modelVozvrat.appendRow(QList<QStandardItem*>() << item1 << item2);
        ui->tvVozvrat->scrollToBottom();
    }
    ui->tvVozvrat->setColumnWidth(0,250);
}

void VozvratWidget::loadData()
{
    ui->menuWidget->loadMenu();
}

void VozvratWidget::on_pbClear_clicked()
{
    _modelVozvrat.clear();
}

void VozvratWidget::on_tvVozvrat_clicked(const QModelIndex &index)
{
    if(!index.isValid()) return;
    QStandardItem *item2 = _modelVozvrat.item(index.row(),1);
    if(item2==0) return;

    int cnt = item2->text().toInt()-1;
    if(cnt<=0){
        _modelVozvrat.removeRow(index.row());
    }else{
        item2->setText(QString::number(cnt));
    }
}

void VozvratWidget::on_pbConfirm_clicked()
{
    for(int i=0;i<_modelVozvrat.rowCount();i++){
        QStandardItem *item1 = _modelVozvrat.item(i,0);
        QStandardItem *item2 = _modelVozvrat.item(i,1);
        int uid = item1->data().toInt();
        int cnt = item2->text().toInt();
        int price = item2->data().toDouble();

        QSqlQuery query;
        query.prepare("insert into cafe_vozvrat (user_id,usluga_id,cnt,summ,dt) values (?,?,?,?,?)");
        query.addBindValue(Config::user_id);
        query.addBindValue(uid);
        query.addBindValue(cnt);
        query.addBindValue(cnt*price);
        query.addBindValue(QDateTime::currentDateTime());
        if(!query.exec()){
            qWarning() << Q_FUNC_INFO << uid << cnt << price << Config::user_id << query.lastError().text();
            QMessageBox::warning(this,trUtf8("Ошибка"),trUtf8("Ошибка возврата %1").arg(query.lastError().text()));
            return;
        }
    }

    _modelVozvrat.clear();
}
