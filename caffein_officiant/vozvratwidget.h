#ifndef VOZVRATWIDGET_H
#define VOZVRATWIDGET_H

#include <QWidget>
#include <QStandardItemModel>
namespace Ui {
class VozvratWidget;
}

class VozvratWidget : public QWidget
{
    Q_OBJECT

public:
    explicit VozvratWidget(QWidget *parent = 0);
    ~VozvratWidget();

private slots:
    void on_pbToZal_clicked();

private:
    Ui::VozvratWidget *ui;

    QStandardItemModel _modelVozvrat;
private slots:
    void onUslugaClicked(int id, QString name, double price, int mid);
    void on_pbClear_clicked();

    void on_tvVozvrat_clicked(const QModelIndex &index);

    void on_pbConfirm_clicked();

public:
    void loadData();
signals:
    void toZal();
};

#endif // VOZVRATWIDGET_H
