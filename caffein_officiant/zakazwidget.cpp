#include "zakazwidget.h"
#include "ui_zakazwidget.h"
#include "tabzakazwidget.h"
#include "brondialog.h"
#include <QtSql>
#include <QMessageBox>
#include "config.h"
ZakazWidget::ZakazWidget(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::ZakazWidget)
{
    ui->setupUi(this);
    connect(ui->menuWidget,SIGNAL(uslugaClicked(int,QString,double,int)),SLOT(onUslugaClicked(int,QString,double,int)));
}

ZakazWidget::~ZakazWidget()
{
    delete ui;
}

void ZakazWidget::loadMenu()
{
    ui->menuWidget->loadMenu();
}

void ZakazWidget::loadStol(int id, const QString &name)
{
    ui->labStolName->setText(name);
    ui->menuWidget->showFirst();
    _id = id;

    while(ui->tabZakaz->count()) ui->tabZakaz->removeTab(0);

    QSqlQuery query;
    query.prepare("select id,name,price,dt_open,user_open_id,skidka,price_skidka,card,precheck,prim,skidka_fix,num from cafe_schet where stol_id=? and status=0");
    query.addBindValue(id);
    if(!query.exec()){
        qWarning() << Q_FUNC_INFO << query.lastError().text();
        QMessageBox::warning(this,trUtf8("Ошибка загрузки стола"),query.lastError().text());
        return;
    }
    while(query.next()){

        Schet *z = new Schet();
        z->id = query.value(0).toInt();
        z->stol_id = _id;
        z->name = query.value(1).toString();
        z->price = query.value(2).toDouble();
        z->dt_open = query.value(3).toDateTime();
        z->user_open_id = query.value(4).toInt();
        z->skidka = query.value(5).toInt();

        z->price_skidka = query.value(6).toDouble();
        z->card = query.value(7).toInt();
        z->precheck = query.value(8).toInt();
        z->prim = query.value(9).toString();
        z->skidka_fix = query.value(10).toDouble();
        z->num = query.value(11).toInt();
        TabZakazWidget *w = new TabZakazWidget(z);
        connect(w,SIGNAL(makeNewSchet()),SLOT(newSchet()));
        ui->tabZakaz->addTab(w,trUtf8("Счёт №%1").arg(z->num));
    }

    if(ui->tabZakaz->count()==0){
        newSchet();
    }


}

void ZakazWidget::on_pbToZal_clicked()
{
    emit toZal();
}



void ZakazWidget::onUslugaClicked(int id,QString name,double price,int mid)
{
    ((TabZakazWidget*)ui->tabZakaz->currentWidget())->addZakaz(id,mid,name,price);

}


int ZakazWidget::currentUser()
{
    return Config::user_id;
}


void ZakazWidget::on_pbBron_clicked()
{
    // бронирование
    BronDialog dlg(_id);
    dlg.exec();
}

void ZakazWidget::newSchet()
{
    Schet *z = new Schet();
    z->id = 0;
    z->stol_id = _id;
    z->name = trUtf8("Счёт №%1").arg(ui->tabZakaz->count()+1);
    z->price = 0;
    z->dt_open = QDateTime::currentDateTime();
    z->user_open_id = currentUser();
    z->skidka = 0;
    z->price_skidka = 0;
    z->card = 0;
    z->precheck = 0;
    z->prim = QString();
    z->skidka_fix = 0;
    z->num = ui->tabZakaz->count()+1;
    TabZakazWidget *w = new TabZakazWidget(z);
    connect(w,SIGNAL(makeNewSchet()),SLOT(newSchet()));
    ui->tabZakaz->addTab(w,trUtf8("Счёт №%1").arg(z->num));
    ui->tabZakaz->setCurrentIndex(ui->tabZakaz->count()-1);
}
