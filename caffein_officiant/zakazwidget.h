#ifndef ZAKAZWIDGET_H
#define ZAKAZWIDGET_H

#include <QWidget>


namespace Ui {
class ZakazWidget;
}

class ZakazWidget : public QWidget
{
    Q_OBJECT
    int _id; ///< код стола

public:
    explicit ZakazWidget(QWidget *parent = 0);
    ~ZakazWidget();
    void loadMenu();
    void loadStol(int id,const QString &name);

private slots:
    void on_pbToZal_clicked();
    void onUslugaClicked(int id, QString name, double price, int mid);
    void on_pbBron_clicked();
    void newSchet();
private:
    Ui::ZakazWidget *ui;
    

    int currentUser();
signals:
    void toZal();
    
};

#endif // ZAKAZWIDGET_H
