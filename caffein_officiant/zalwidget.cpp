#include "zalwidget.h"
#include "ui_zalwidget.h"
#include <QtDebug>
#include <QtCore>
#include <QtGui>
#include <QtSql>
#include <QMessageBox>
#include "stolbutton.h"
#include "authdialog.h"
#include "config.h"
ZalWidget::ZalWidget(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::ZalWidget)
{
    ui->setupUi(this);
    ui->pbPrihod->setVisible(false);
    //ui->pbVozvrat->setVisible(false);
    QFile file(qApp->applicationDirPath()+"/zal.txt");
    if(file.exists()){
        if(file.open(QIODevice::ReadOnly)){
            _positionContent = file.readAll();
            _posList = loadPositions();
        }else{
            qWarning() << "error open file"<<file.fileName();
        }

        file.close();
    }else{
        qWarning() << "file not exist"<<file.fileName();
    }

    ui->pbEnter->setStyleSheet("background-color: #00c400;font: 75 20pt 'Sans' bold;");

}

ZalWidget::~ZalWidget()
{
    delete ui;
}

void ZalWidget::loadData()
{

    QSqlQuery query;
    query.exec("select id,name,status from cafe_stol where visible=1");
    while(query.next()){
        //qDebug() << query.value(0).toInt();
        int id = query.value(0).toInt();
        foreach(StolPosition *sp,_posList){
            if(sp->id==id){
                sp->name = query.value(1).toString();
                sp->status = query.value(2).toInt();
            }
        }
    }


    foreach(StolPosition *sp,_posList){
        StolButton *item = new StolButton(sp->id,sp->name,sp->status);
        connect(this,SIGNAL(updateData()),item,SLOT(updateData()));
        connect(item,SIGNAL(clicked()),SLOT(showStolClick()));
        item->setMinimumHeight(145);
        item->setMinimumWidth(100);
        ui->gridLayout->addWidget(item,sp->row,sp->column,sp->rowSpan,sp->columnSpan);
    }

}

QList<StolPosition*> ZalWidget::loadPositions()
{

    QList<StolPosition*> posList;

    foreach(QString line,_positionContent.split("\n")){
        if(line.simplified().isEmpty()) continue;
        QStringList slist = line.simplified().split(",");
        if(slist.count()<5){
            qWarning() << "error line"<<line;
            continue;
        }

        StolPosition *sp = new StolPosition;
        sp->id         = slist.at(0).toInt();
        sp->row        = slist.at(1).toInt();
        sp->column     = slist.at(2).toInt();
        sp->rowSpan    = slist.at(3).toInt()==0?1:slist.at(3).toInt();
        sp->columnSpan = slist.at(4).toInt()==0?1:slist.at(4).toInt();
        posList.append(sp);

    }
    return posList;
}

void ZalWidget::updateStols()
{
    emit updateData();
    update();
}

void ZalWidget::showStolClick()
{
    if(Config::password.isEmpty()){
        on_pbEnter_clicked();
        if(Config::password.isEmpty()) return;
    }
    StolButton *b = (StolButton*)sender();
    if(b==0) return;

    emit showStol(b->id(),b->name());
}


void ZalWidget::on_pbEnter_clicked()
{
    // вход

    if(Config::password.isEmpty()){
        AuthDialog ad;
        if(ad.exec()){
            ui->pbEnter->setText(trUtf8("%1 (выход)").arg(Config::full_name));
            ui->pbEnter->setStyleSheet("background-color: rgb(218, 83, 22);font: 75 20pt 'Sans' bold;");
        }
    }else{
        Config::password = "";
        ui->pbEnter->setText(trUtf8("Вход"));
        ui->pbEnter->setStyleSheet("background-color: #00c400;font: 75 20pt 'Sans' bold;");
    }


}

void ZalWidget::on_pbSmena_clicked()
{
    emit showSmena();
}

void ZalWidget::on_pbVozvrat_clicked()
{
    emit showVozvrat();
}

void ZalWidget::on_pbNewSmena_clicked()
{
    if(QMessageBox::question(this,trUtf8("Новая смена"),trUtf8("Начать новую смену?"),QMessageBox::Yes|QMessageBox::No)==QMessageBox::Yes){
        QDateTime dt = QDateTime::currentDateTime();

        QSqlQuery query;
        query.prepare("update cafe_smena set dt_close=? where dt_close is null");
        query.addBindValue(dt);
        if(!query.exec()){
            QMessageBox::warning(this,trUtf8("Ошибка"),trUtf8("Ошибка закрытия смены"));
            return;
        }

        query.prepare("insert into cafe_smena (dt_open) values (?)");
        query.addBindValue(dt);
        if(!query.exec()){
            QMessageBox::warning(this,trUtf8("Ошибка"),trUtf8("Ошибка создания смены"));
            return;
        }
        QMessageBox::warning(this,trUtf8("Новая смена"),trUtf8("Новая смена начата"));
    }
}

void ZalWidget::on_pbReload_clicked()
{
    qDebug() << qApp->applicationFilePath();
    QProcess::startDetached(qApp->applicationFilePath());
    QApplication::quit();
}

void ZalWidget::on_pbMenu_clicked()
{
    emit showMenuEdit();
}
