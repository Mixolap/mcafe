#ifndef ZALWIDGET_H
#define ZALWIDGET_H

#include <QWidget>

namespace Ui {
class ZalWidget;
}


struct StolPosition{
    int id;
    int row;
    int column;
    int rowSpan;
    int columnSpan;
    QString name;
    int status;
};

class ZalWidget : public QWidget
{
    Q_OBJECT
    QString _positionContent;
    QList<StolPosition*> _posList;
public:
    explicit ZalWidget(QWidget *parent = 0);
    ~ZalWidget();
    void loadData();

    void updateStols();
private:
    Ui::ZalWidget *ui;

    QList<StolPosition*> loadPositions();

private slots:
    void showStolClick();    
    void on_pbEnter_clicked();

    void on_pbSmena_clicked();

    void on_pbVozvrat_clicked();

    void on_pbNewSmena_clicked();

    void on_pbReload_clicked();

    void on_pbMenu_clicked();

signals:
    void showStol(int id,const QString &name);
    void updateData();
    void showSmena();
    void showVozvrat();
    void showMenuEdit();
};

#endif // ZALWIDGET_H
