#!/bin/bash
# скопировать конфигурацию mcafe для nginx
cp mcafe.conf /etc/nginx/conf.d
# создать ссылку на django
ln -s /usr/lib/python2.7/site-packages/Django-1.5.8-py2.7.egg/django /usr/lib/python2.7/site-packages/django 
# перезапустить nginx
systemctl enable nginx.service 
systemctl stop nginx.service 
systemctl start nginx.service 
# добавить запись в chrontab для запуска mcafe
echo '@reboot root uwsgi --chdir /opt/mcafe/mcafe -y /opt/mcafe/mcafe/uwsgi.yaml' >> /etc/crontab
# запустить uwsgi для mcafe
uwsgi --chdir /opt/mcafe/mcafe -y /opt/mcafe/mcafe/uwsgi.yaml 
# создать директорию автозагрузки
mkdir /home/caffein/.config/autostart/ 
# добавить в автозагрузку /opt/mcafe/mprinter/mprinter
cp /opt/mcafe/mprinter.desktop /home/caffein/.config/autostart/ 
# добавить в автозагрузку браузер firefox
cp /opt/mcafe/firefox.desktop /home/caffein/.config/autostart/ 
# установить драйвер  принтера
rpm -Uvh /opt/mcafe/starcupsdrv-3.4.2-1.i386.rpm
# установить права на исполняемый файл принтера
chmod 777 /opt/mcafe/mprinter/mprinter
# перезагрузиться
#reboot
