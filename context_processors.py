from cafe.models import *
#from mcafe.cafe.views import home


def vars(request):
    #print "context_vars"
    return {
        "stols": Stol.objects.all(),
        "prihod_menu":Menu.objects.filter(pid=0),
        "is_buh":isBuh(request),
        "is_admin":isAdmin(request),
        "is_officiant":isOfficiant(request),
        "HAVE_BRON":get_option("HAVE_BRON",1),
        "NO_DISCOUNT_CARDS_OPTION": int(get_option("NO_DISCOUNT_CARDS", 0)),
    }