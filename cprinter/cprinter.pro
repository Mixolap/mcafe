#-------------------------------------------------
#
# Project created by QtCreator 2014-07-19T18:14:00
#
#-------------------------------------------------

QT       += core gui network printsupport

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = cprinter
TEMPLATE = app


SOURCES += main.cpp\
        printersdialog.cpp \
    printerwidget.cpp

HEADERS  += printersdialog.h \
    printerwidget.h

FORMS    += printersdialog.ui \
    printerwidget.ui
