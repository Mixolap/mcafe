#include "printersdialog.h"
#include <QApplication>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    a.setApplicationName("cprinter");
    a.setOrganizationName("caffein");
    PrintersDialog w;
    w.show();

    return a.exec();
}
