#include "printersdialog.h"
#include "ui_printersdialog.h"
#include <QtCore>
//#include <QtPrintSupport>
#include <QPrinter>
#include <QPrintDialog>
#include <QPrinterInfo>
#include <QtGui>

PrintersDialog::PrintersDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::PrintersDialog)
{
    ui->setupUi(this);
    QString def_server = "https://127.0.0.1";
    QString def_username = "caffein";
    QString def_password = "";
    QFile file(QApplication::applicationDirPath()+"/params.txt");

    if(file.open(QIODevice::ReadOnly)){
        QStringList slist = QString::fromUtf8(file.readAll()).split("\n");
        if(slist.count()>1){
            def_server = slist.at(0).simplified();
            def_username = slist.at(1).simplified();
            def_password = slist.at(2).simplified();
        }
        file.close();
    }

    QSettings sets;

    ui->editServer->setText(sets.value("server",def_server).toString());
    ui->editUsername->setText(sets.value("username",def_username).toString());
    ui->editPassword->setText(sets.value("password",def_password).toString());

    loadPrinters();

    QVBoxLayout *layout = new QVBoxLayout;
    foreach(Printer *p,_printers){
        PrinterWidget *pw = new PrinterWidget(p);
        connect(pw,SIGNAL(changed()),SLOT(on_pbSave_clicked()));
        layout->addWidget(pw);
    }

    ui->widgetPrinters->setLayout(layout);
    ui->treeWidget->setColumnWidth(0,50);
    ui->treeWidget->setColumnWidth(1,150);
    ui->treeWidget->setColumnWidth(2,200);
    ui->treeWidget->setColumnWidth(3,200);

    connect(&_manager, SIGNAL(finished(QNetworkReply*)), SLOT(downloadFinished(QNetworkReply*)));
    connect(&_manager,SIGNAL(authenticationRequired(QNetworkReply*,QAuthenticator*)),SLOT(authRequired(QNetworkReply*,QAuthenticator*)));
    _timer.start(1000);
    connect(&_timer,SIGNAL(timeout()),SLOT(checkPrint()));
    on_pbSave_clicked();

}

PrintersDialog::~PrintersDialog()
{
    delete ui;
}

int PrintersDialog::printerId(const QString &name)
{
    qDebug() << Q_FUNC_INFO << name;
    foreach(Printer *p,_printers){
        qDebug() << Q_FUNC_INFO << "name="<<p->name;
        if(p->name == name) return p->id;
    }
    return 0;
}

int PrintersDialog::newPrinterId()
{
    for(int i=1;i<100;i++){
        bool have = false;
        foreach(Printer *p,_printers){
            if(p->id==i) have = true;
        }
        if(!have) return i;
    }
    return 0;
}

void PrintersDialog::loadPrinters()
{

    QSettings sets;
    int cnt = sets.beginReadArray("printers");
    qDebug() << Q_FUNC_INFO << cnt;
    for(int i=0;i<cnt;i++){
        sets.setArrayIndex(i);
        qDebug() << Q_FUNC_INFO <<"p1 from settigns" <<sets.value("name").toString()<< sets.value("p1").toBool();
        Printer *p = new Printer();
        p->id   = sets.value("id").toInt();
        p->name = sets.value("name").toString();
        p->p1   = sets.value("p1").toBool();
        p->p2   = sets.value("p2").toBool();
        p->p3   = sets.value("p3").toBool();
        p->p4   = sets.value("p4").toBool();
        _printers << p;
    }
    sets.endArray();


    QStringList plist;
    foreach(QPrinterInfo pi,QPrinterInfo::availablePrinters()){
        plist << pi.printerName();
        if(printerId(pi.printerName())==0){
            Printer *p = new Printer();
            p->id = newPrinterId();
            p->name = pi.printerName();
            p->available = true;
            _printers << p;
        }
    }
}

void PrintersDialog::on_pbSave_clicked()
{
    QSettings sets;
    //sets.setValue("print_files",ui->editPath->text());
    sets.setValue("server",ui->editServer->text());
    sets.setValue("username",ui->editUsername->text());
    sets.setValue("password",ui->editPassword->text());

    sets.beginWriteArray("printers");
    int i=0;
    foreach(Printer *p,_printers){
        sets.setArrayIndex(i++);
        sets.setValue("id",  p->id);
        sets.setValue("name",p->name);
        sets.setValue("p1",  p->p1);
        sets.setValue("p2",  p->p2);
        sets.setValue("p3",  p->p3);
        sets.setValue("p4",  p->p4);
    }
    sets.endArray();
}
void PrintersDialog::printFile(int id, const QString &fname)
{
    if(!QFile::exists(fname)){
        addText("no file",fname);
        return;
    }

    QFile file(fname);
    if (!file.open(QIODevice::ReadOnly | QIODevice::Text)) {
        addText("file is busy",fname);
        return;
    }
    QString data = QString::fromUtf8(file.readAll());
    addText(QFileInfo(fname).fileName(),"");
    printData(id,data);
}


int PrintersDialog::selectPrinter(int type)
{
    if(type==1){
        foreach(Printer *p,_printers){
            if(p->p1) return p->id;
        }
    }

    if(type==2){
        foreach(Printer *p,_printers){
            if(p->p2) return p->id;
        }
    }

    if(type==3){
        foreach(Printer *p,_printers){
            if(p->p3) return p->id;
        }
    }

    if(type==4){
        foreach(Printer *p,_printers){
            if(p->p4) return p->id;
        }
    }
    return 0;
}

void PrintersDialog::setCurrentPrinter(QPrinter *printer, const QString &name)
{

}


void PrintersDialog::checkPrint()
{
    QSettings sets;


    QNetworkRequest request(QUrl(sets.value("server").toString()+"/print/"));
    QNetworkReply *reply = _manager.get(request);
    reply->ignoreSslErrors();
/*
#ifndef QT_NO_SSL
    connect(reply, SIGNAL(sslErrors(QList<QSslError>)), SLOT(sslErrors(QList<QSslError>)));
#endif*/

}

void PrintersDialog::downloadFinished(QNetworkReply *reply)
{

    if(reply->error()){
        addText(reply->url().toString(),reply->errorString());
    }else{
        QString data = QString::fromUtf8(reply->readAll()).trimmed();
        if(!data.isEmpty()){
            QStringList list = data.split("\n");
            QString istr = list.takeAt(0);
            data = list.join("\n");
            bool ok;
            int type = istr.replace("type","").toInt(&ok);
            if(!ok){
                addText(reply->url().toString(),trUtf8("Ошибочный формат файла"));
            }else{
                QString tname;
                switch (type) {
                case 1:
                    tname = trUtf8("Пречек");
                    break;
                case 2:
                    tname = trUtf8("Чек");
                    break;
                case 3:
                    tname = trUtf8("Кухня");
                    break;
                case 4:
                    tname = trUtf8("Бар");
                    break;
                default:
                    tname = trUtf8("Неизвестно");
                    break;
                }


                addText(reply->url().toString(),tname);

                printData(selectPrinter(type),data);
            }
        }
    }
    reply->deleteLater();
}



void PrintersDialog::sslErrors(QList<QSslError> elist)
{
    foreach(QSslError error,elist){
        addText("",error.errorString());
    }
}



void PrintersDialog::addText(QString t1, QString t2)
{
    if(ui->treeWidget->invisibleRootItem()->childCount()>=1000){
       delete ui->treeWidget->invisibleRootItem()->takeChild(0);
    }
    QTreeWidgetItem *item = new QTreeWidgetItem();
    item->setText(0,QString("%1").arg(ui->treeWidget->topLevelItemCount()+1));
    item->setText(1,QDateTime::currentDateTime().toString("dd.MM.yy hh:mm:ss"));
    item->setText(2,t1);
    item->setText(3,t2);
    ui->treeWidget->addTopLevelItem(item);
}

void PrintersDialog::authRequired(QNetworkReply *, QAuthenticator *auth)
{
    QSettings sets;
    auth->setUser(sets.value("username").toString());
    auth->setPassword(sets.value("password").toString());
}

void PrintersDialog::printData(int id, const QString &text)
{
    if(id==0){
        addText(trUtf8("Не выбран принтер"),"");
        return;
    }
    Printer *pr = 0;
    foreach(Printer *p,_printers){
        if(p->id==id){
            pr = p;
            break;
        }
    }
    if(pr==0){
    qDebug() << "no printer"<<id;
    return;
    }

    QPrinter printer;
    //setCurrentPrinter(&printer,pr->name);
#ifdef Q_OS_WIN
    printer.setPrinterName(pr->name);
#else
    QProcess proc;
    qDebug() << "lpoptions -d '"+pr->name+"'";
    proc.start("lpoptions",QStringList() << "-d"<<pr->name);
    proc.waitForFinished();
#endif
    //printer.setOrientation(QPrinter::Portrait);
    //printer.setResolution(300);
    //printer.setFullPage(true);
    //printer.setCopyCount(1);
    printer.setPageMargins(0,0,0,0,QPrinter::Point);

    //printer.setPageSize(QPagedPaintDevice::B7);

    qDebug() << "valid" << printer.isValid();
    QPrintDialog dialog(&printer);
    qDebug() << text;
    QTextDocument *document = new QTextDocument();
    //QTextEncoder *encoder = data;//QTextCodec::codecForName("Windows-1251")->makeEncoder();
    document->setHtml(text);
    document->setPageSize(QSizeF(document->idealWidth(),-1));
    document->print(&printer);
    delete document;
    qDebug()<<"must printed";
}

