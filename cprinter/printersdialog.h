#ifndef PRINTERSDIALOG_H
#define PRINTERSDIALOG_H

#include <QDialog>
#include <QTimer>
#include <QNetworkAccessManager>
#include <QPrinter>
#include <QNetworkReply>
#include <QSslError>
#include <QAuthenticator>
#include "printerwidget.h"
namespace Ui {
class PrintersDialog;
}

class PrintersDialog : public QDialog
{
    Q_OBJECT
    QList<Printer*> _printers;
    QTimer _timer;
    QNetworkAccessManager _manager;

public:
    explicit PrintersDialog(QWidget *parent = 0);
    ~PrintersDialog();

private:
    Ui::PrintersDialog *ui;
    int printerId(const QString &name);
    int newPrinterId();
    void loadPrinters();
    void printFile(int id,const QString &fname);
    void printData(int id,const QString &text);
    int selectPrinter(int type);
    void setCurrentPrinter(QPrinter *printer,const QString &name);
private slots:
    void on_pbSave_clicked();
    void checkPrint();
    void downloadFinished(QNetworkReply*reply);
    void sslErrors(QList<QSslError> elist);
    void addText(QString t1,QString t2);
    void authRequired(QNetworkReply*, QAuthenticator*auth);

};

#endif // PRINTERSDIALOG_H
