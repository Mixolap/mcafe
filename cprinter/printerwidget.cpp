#include "printerwidget.h"
#include "ui_printerwidget.h"
#include <QDebug>
PrinterWidget::PrinterWidget(Printer *printer, QWidget *parent) :
    QWidget(parent),
    ui(new Ui::PrinterWidget)
{
    ui->setupUi(this);
    _printer = printer;
    if(printer==0) return;

    ui->printerName->setTitle(_printer->name);
    qDebug() << "p1 for printer"<< _printer->p1;
    ui->cbox1->setChecked(_printer->p1);
    ui->cbox2->setChecked(_printer->p2);
    ui->cbox3->setChecked(_printer->p3);
    ui->cbox4->setChecked(_printer->p4);
}

PrinterWidget::~PrinterWidget()
{
    delete ui;
}

void PrinterWidget::on_cbox1_clicked()
{
    _printer->p1 = ((QCheckBox*)sender())->checkState() == Qt::Checked;
    emit changed();
}

void PrinterWidget::on_cbox2_clicked()
{
    _printer->p2 = ((QCheckBox*)sender())->checkState() == Qt::Checked;
    emit changed();
}

void PrinterWidget::on_cbox3_clicked()
{
    _printer->p3 = ((QCheckBox*)sender())->checkState() == Qt::Checked;
    emit changed();
}

void PrinterWidget::on_cbox4_clicked()
{
    _printer->p4 = ((QCheckBox*)sender())->checkState() == Qt::Checked;
    emit changed();
}
