#ifndef PRINTERWIDGET_H
#define PRINTERWIDGET_H

#include <QWidget>
struct Printer{
    int id;
    QString name;
    bool available;
    bool p1;// пречек
    bool p2;// чек
    bool p3;// кухня
    bool p4;// бар
};

namespace Ui {
class PrinterWidget;
}

class PrinterWidget : public QWidget
{
    Q_OBJECT
    Printer *_printer;
public:
    explicit PrinterWidget(Printer *printer,QWidget *parent = 0);
    ~PrinterWidget();

private slots:
    void on_cbox1_clicked();

    void on_cbox2_clicked();

    void on_cbox3_clicked();

    void on_cbox4_clicked();
signals:
    void changed();
private:
    Ui::PrinterWidget *ui;
};

#endif // PRINTERWIDGET_H
