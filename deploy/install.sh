cd /opt
apt-get install -y vim openssh-server postgresql nginx python-psycopg2 g++ qtbase5-dev apparmor-utils libqt4-dev xinput-calibrator libpq-dev libqt4-sql-psql python3 python3-pip python3-dev uwsgi-plugin-python3
#apt-get install opera
#pip install coverage
pip3 install setuptools
pip3 install uwsgi
pip3 install Django
pip3 install psycopg2-binary
#rm -f /opt/caffein.tar.gz
#wget https://100imho.ru/static/caffein.tar.gz
#tar -xzf caffein.tar.gz
chmod 777 /opt/caffein -R
mkdir /mnt/backup
chmod 777 /mnt/backup

# python /opt/mcafe/send_signal.py

rm -rf /etc/nginx/sites-enabled/default
cp -rf /opt/caffein/mcafe/conf/mcafe.conf /etc/nginx/conf.d/caffein.conf
cp -rf /opt/caffein/mcafe/conf/smb.conf /etc/samba/smb.conf
cp -rf /opt/caffein/mcafe/conf/pg_hba.conf /etc/postgresql/10/main/pg_hba.conf
cp -rf /opt/caffein/mcafe/conf/crontab /etc/crontab

service nginx restart
service postgresql restart
systemctl enable nginx
systemctl enable postgresql

dropdb -U postgres caffein
createdb -U postgres caffein
psql -U postgres caffein < /opt/caffein/mcafe/caffein.sql
#psql -U postgres caffein < /opt/caffein/mcafe/alter.sql
uwsgi -d -y /opt/caffein/mcafe/mcafe/uwsgi.yaml

cd /opt/caffein/mcafe/caffein_officiant
qmake-qt4
make

cd /opt/caffein/mcafe/cprinter
qmake-qt4
make
cd /opt/caffein/mcafe/printed
chmod 777 /opt/caffein/mcafe/printed

cp /opt/caffein/mcafe/apps/printers/sewoocupsinstall/install/rastertosewoo /usr/lib/cups/filter

#inputattach --daemon --always -fjt /dev/ttyS0 
