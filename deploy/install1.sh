cd /opt
rm /etc/samba/smb.conf
apt-get install -y openssh-server samba postgresql g++ qtbase5-dev python3-pip apparmor-utils xinput-calibrator libpq-dev qt5-default libqt5sql5-psql python3-pip git sublime-text
#apt-get install opera
#pip install coverage
pip3 install setuptools
pip3 install Django psycopg2-binary
#rm -f /opt/caffein.tar.gz
#wget https://100imho.ru/static/caffein.tar.gz
#tar -xzf caffein.tar.gz
chmod 777 /opt/caffein -R
mkdir /mnt/backup
chmod 777 /mnt/backup

# python /opt/mcafe/send_signal.py

rm -rf /etc/nginx/sites-enabled/default
cp -rf /opt/caffein/mcafe/conf/mcafe.conf /etc/nginx/conf.d/caffein.conf
cp -rf /opt/caffein/mcafe/conf/smb.conf /etc/samba/smb.conf
cp -rf /opt/caffein/mcafe/conf/pg_hba.conf /etc/postgresql/9.5/main/pg_hba.conf
cp -rf /opt/caffein/mcafe/conf/crontab /etc/crontab
mkdir /root/.config/caffein
cp /opt/caffein/mcafe/conf/cprinter.conf /root/.config/caffein


#service nginx restart
#systemctl enable nginx

service postgresql restart
systemctl enable postgresql

dropdb -U postgres caffein
createdb -U postgres caffein
psql -U postgres caffein < /opt/caffein/mcafe/caffein.sql
#psql -U postgres caffein < /opt/caffein/mcafe/alter.sql
#uwsgi -d -y /opt/caffein/mcafe/mcafe/uwsgi.yaml

cd /opt/caffein/mcafe/cprinter
qmake
make distclean
qmake
make

mkdir /opt/caffein/mcafe/printed

chmod 777 /opt/caffein -R

#cp /opt/caffein/sewoocupsinstall/install/rastertosewoo /usr/lib/cups/filter

#inputattach --daemon --always -fjt /dev/ttyS0 
