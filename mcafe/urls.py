#-*- encoding: utf-8 -*-
from django.conf.urls import include, url

# Uncomment the next two lines to enable the admin:
from django.contrib import admin
admin.autodiscover()
from cafe import views,api

urlpatterns = [
    # Examples:
    url(r'^$', views.home, name='home'),
    url(r'^zal/$', views.zal, name='zal'),
    url(r'^menu/$', views.menu, name='menu'),
    url(r'^discounts/$', views.discounts, name='discounts'),
    url(r'^clearbase/$', views.clearbase, name='clearbase'),
    url(r'^menu/(?P<id>\d+)/$', views.editMenu, name='menu'),    
    url(r'^menu/u/(?P<uid>\d+)/$', views.usluga, name='usluga'),
    url(r'^menu/remove/u/(?P<uid>\d+)/$', views.removeUsluga, name='remove_usluga'),
    url(r'^menu/remove/(?P<id>\d+)/$', views.removeMenu, name='remove_menu'),
    url(r'^menu/pf/add/$', views.editPf, name='add_pf'),
    url(r'^menu/pf/(?P<id>\d+)/$', views.editPf, name='edit_pf'),
    
    url(r'^schet/(?P<status>\d+)/$', views.schet, name='schet'),
    url(r'^zakaz/edit/(?P<id>\d+)/$', views.editZakaz, name='edit_zakaz'),    
    url(r'^bron/$', views.bronAll, name='bron'),
    url(r'^bron/(?P<id>\d+)/$', views.bron, name='bron'),
    url(r'^zakaz/(?P<id>\d+)/$', views.zakaz, name='zakaz'),
    url(r'^zakaz/(?P<id>\d+)/(?P<user_id>\d+)/$', views.zakaz, name='zakaz'),
    url(r'^precheck/(?P<id>\d+)/$', views.precheck, name='precheck'),
    url(r'^check/add/cash/$', views.schetAddCash, name='schet_add_cash'),
    url(r'^check/close/(?P<id>\d+)/$', views.checkClose, name='close_check'),
    url(r'^check/close/(?P<id>\d+)/(?P<card>\d+)/$', views.checkClose, name='close_check'),
    url(r'^check/skidka/(?P<id>\d+)/(?P<sid>\d+)/$', views.skidka, name='skidka'),
    url(r'^check/skidka/fix/$', views.skidkaFix, name='skidka_fix'),
    url(r'^check/prim/$', views.checkPrim, name='schet_prim'),
    url(r'^check/deposit/$', views.checkDeposit, name='schet_deposit'),
    url(r'^check/move/(?P<stol_id>\d+)/(?P<schet_id>\d+)/$', views.checkMove, name='check_move'),
    url(r'^usluga/move/(?P<id>\d+)/(?P<id_to>\d+)/$', views.uslugaMove, name='usluga_move'),
    url(r'^usluga/cancel/(?P<id>\d+)/$', views.uslugaCancel, name='usluga_cancel'),
    url(r'^accounts/login/$', views.login_user, name='login'),
    url(r'^accounts/logout/$', views.logout_user, name='logout'),
    url(r'^administrator/$', views.administrator, name='administrator'),
    url(r'^buhgalter/$', views.buhgalter, name='buhgalter'),
    url(r'^prihod/$', views.prihod, name='prihod'),
    url(r'^prihod/(?P<id>\d+)/$', views.prihod, name='prihod'),
    url(r'^rashod/$', views.rashod, name='rashod'),
    url(r'^rashod/(?P<id>\d+)/$', views.rashod, name='rashod'),
    url(r'^sklad/$', views.sklad, name='sklad'),
    url(r'^vozvrat/$', views.vozvrat, name='vozvrat'),
    
    url(r'^smena/close/$', views.closeSmena, name='smena_close'),    
    url(r'^smena/(?P<id>\d+)/$', views.closeSmena, name='smena'),    
    
    url(r'^reports/$', views.reports, name='reports'),    
    url(r'^report/prihod/$', views.report_prihod, name='report_prihod'),
    url(r'^report/dohod/$', views.report_dohod, name='report_dohod'),
    url(r'^report/skidka/$', views.report_skidka, name='report_skidka'),
    url(r'^report/doprashod/$', views.report_doprashod, name='report_doprashod'),
    url(r'^report/statistics/$', views.report_statistics, name='report_statistics'),    
    
    url(r'^music/$', views.music, name='music'),
    url(r'^music/up/(?P<id>\d+)/$', views.musicUp, name='music_up'),
    url(r'^music/down/(?P<id>\d+)/$', views.musicDown, name='music_down'),
    
    url(r'^print/$', views.sendPrint, name='print'), ## отправить задание на печать.
    url(r'^printers/$', views.printers, name='printers'),
    url(r'^printers/(?P<id>\d+)/$', views.printers, name='printers'),
    
    url(r'^guests/$', views.guests, name='guests'),
    url(r'^guests/(?P<id>\d+)/$', views.guestEdit, name='guest_edit'),
    
    url(r'^api/auth/$', views.apiAuth, name='apiAuth'),
    url(r'^api/check/usluga/content/$', views.apiCheckUslugsContent, name='apiCheckUslugsContent'),
    url(r'^api/check/sklad/$', views.apiCheckSklad, name='apiCheckSklad'),
    url(r'^api/demo/$', views.apiDemo, name='apiDemo'),
    url(r'^api/schets/(?P<stol_id>\d+)/$', api.schets, name='api_schets'),
    url(r'^api/schets/delete/(?P<sid>\d+)/$', api.schetDelete, name='api_schet_delete'),
    
    url(r'^admin/', admin.site.urls),
]


