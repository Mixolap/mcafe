var PLAY_TIME = 0;
var CURRENT_PLIST = 1;
var CURRENT_MUSIC = 0;

$(function(){
	$("#jquery_jplayer_1").jPlayer({
		ready: function () {
			$(this).jPlayer("setMedia", {
				//mp3:"http://150.150.102.31/links/First%20State%20-%20Brave.mp3",
			});
		},
		swfPath: "/static/swf/jplayer.swf",
		solution: "flash",
		supplied: "mp3, m4a",
		wmode: "window",
	        volume: 0.5,
		//timeupdate:function(e){ console.debug(e.jPlayer.status.currentTime)}
	}).bind($.jPlayer.event.volumechange, function(event){
      var vol = $("#jquery_jplayer_1").data("jPlayer").options.volume
      $.cookie('jplayer_vol_level', vol)
    }).bind($.jPlayer.event.pause, function(event){
		IS_PAUSED = event.jPlayer.status.paused;
		if(event.jPlayer.status.currentTime==0) PLAY_TIME=0;
		$('.audio_pause_button').removeClass("audio_pause_button").addClass("audio_play_button")
		//console.debug("pause "+event.jPlayer.status.currentTime)
    }).bind($.jPlayer.event.play, function(event){
		if(!IS_PAUSED) PLAY_TIME = 0;
		//console.debug(IS_PAUSED)
		IS_PAUSED = false;
		if($('.playing').hasClass("audio_button"))
			$('.playing').removeClass("audio_play_button").addClass("audio_pause_button")
    }).bind($.jPlayer.event.ended, function(){
		PLAY_TIME = 0;
        	//if ($("a.jp-repeat-off").eq(0).css("display") == "none") 
		$("a.jp-next").click();
		$("a.jp-play").click();
    });
	
	
	function play(elem){
		if(elem.hasClass("playing")){
			if(elem.hasClass("audio_play_button"))
				$("#jquery_jplayer_1").jPlayer("play");		
			else
				$("#jquery_jplayer_1").jPlayer("pause");		
			return;
		}
		
		$("#jquery_jplayer_1").jPlayer("setMedia", { mp3:elem.attr("rel") });
		$("#jquery_jplayer_1").jPlayer("play");		
		$(".playing").removeClass("playing")
		$(".play_text").removeClass("play_text")
		$(".audio_pause_button").removeClass("audio_pause_button").addClass('audio_play_button')
		elem.addClass("playing")
		elem.parent().find('a').addClass('play_text')
		if(elem.hasClass('audio_button')){
			elem.removeClass('audio_play_button')
			elem.addClass('audio_pause_button')
		}
		CURRENT_PLIST = elem.attr("role")
		CURRENT_MUSIC = elem.attr("fid")
		$("#music_name").text(elem.text())
	}
	
	$(".audio").live('click',function(){
		$("#audio_player_panel").parent().attr("style","")
		play($(this))
		return false;
	})
	

	
	$("a.jp-play").click(function(){
		if($(".playing").size()==0) play($($(".audio")[0]))
	})


	$("a.jp-next").click(function(){	
		var a=$(".audio[role="+CURRENT_PLIST+"]")
		if($(".playing").size()==0 && a.size()>0){
			play($(a[0]))			
			return false
		}
		for(var i=0;i<a.size();i++){
				if($(a[i]).hasClass("playing")){
					var elem
					if(i==a.size()-1) elem = $(a[0])
					else elem = $(a[i+1])
					play(elem)
		
					break
				}
		    }
		return false;
	})	
	
})
