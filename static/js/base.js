
$(function(){
	jQuery(window).scroll(function () {      
		if (jQuery(this).scrollTop() != 0) {
			jQuery("#toTop").fadeIn();
		}else {
			jQuery("#toTop").fadeOut();
		}
	})
	jQuery("#toTop").click(function () {
		if (jQuery.browser.opera) $(jQuery("body,html").scrollTop(0))
		else
		jQuery("body,html").animate({
			scrollTop: 0
		},
		800);
	})	
})